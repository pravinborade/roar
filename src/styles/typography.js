import {Platform} from 'react-native';

export const PT_SANS_BOLD =
  Platform.OS === 'android' ? 'PTSans-Bold' : 'PT Sans Bold';
export const PT_SANS_BOLD_ITALIC =
  Platform.OS === 'android' ? 'PtSans-BoldItalic' : 'PT Sans Bold Italic';
export const PT_SANS_REGULAR =
  Platform.OS === 'android' ? 'PTSans-Regular' : 'PT Sans';
export const PT_SANS_NARROW_BOLD =
  Platform.OS === 'android' ? 'PTSansNarrow-Bold' : 'PT Sans Narrow Bold';
export const PT_SANS_NARROW_REGULAR =
  Platform.OS === 'android' ? 'PTSansNarrow-Regular' : 'PT Sans Narrow';
