import {colors} from './colors';
import {Dimensions, PixelRatio, StyleSheet} from 'react-native';
import {PT_SANS_BOLD, PT_SANS_NARROW_BOLD, PT_SANS_REGULAR} from './typography';
import {fontToDp, heightToDp, widthToDp} from '../utils/Responsive';

const React = require('react-native');

const WINDOW_WIDTH = Dimensions.get('window').width;
const guidelineBaseWidth = 375;
export const scaleSize = (size) => (WINDOW_WIDTH / guidelineBaseWidth) * size;

export const scaleFont = (size) => (size * PixelRatio.getFontScale()) / 1.8;

export const commonStyle = StyleSheet.create({
  justifyAlignCenter: {justifyContent: 'center', alignItems: 'center'},
  rowAlignCenter: {flexDirection: 'row', alignItems: 'center'},
  flex1: {flex: 1},

  regular13: {
    fontSize: fontToDp(11),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },
  regular16: {
    fontSize: fontToDp(10),
    color: colors.darkGreen,
    fontFamily: PT_SANS_REGULAR,
  },
  regular18: {
    fontSize: fontToDp(15),
    color: colors.primaryGreen,
    fontFamily: PT_SANS_REGULAR,
  },
  regular20: {
    fontSize: fontToDp(10),
    color: colors.primaryGreen,
    fontFamily: PT_SANS_REGULAR,
  },
  regular22: {
    fontSize: fontToDp(13.5),
    color: '#505C74',
    fontFamily: PT_SANS_REGULAR,
  },
  regular24: {
    fontSize: fontToDp(13),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },
  bold24: {
    fontSize: fontToDp(14),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bold28: {
    fontSize: fontToDp(16),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bold25: {
    fontSize: fontToDp(15),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  regular26: {
    fontSize: fontToDp(16),
    color: colors.primaryGrey,
    fontFamily: PT_SANS_REGULAR,
  },
  regular28: {
    fontSize: fontToDp(15),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },
  regular30: {
    fontSize: fontToDp(16),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },
  regular32: {
    fontSize: fontToDp(17),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },
  regular50: {
    fontSize: fontToDp(26),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },
  narrow22: {
    fontSize: fontToDp(14),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  narrow28: {
    fontSize: fontToDp(13),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  narrow30: {
    fontSize: fontToDp(16),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  narrow32: {
    fontSize: fontToDp(17),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  narrow36: {
    fontSize: fontToDp(18),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  narrow40: {
    fontSize: fontToDp(20),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bold40: {
    fontSize: fontToDp(19.5),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bold30: {
    fontSize: fontToDp(15),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_BOLD,
  },
  bold36: {
    fontSize: fontToDp(20),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_BOLD,
  },
  bold32: {
    fontSize: fontToDp(22),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_BOLD,
  },
  bold20: {
    fontSize: fontToDp(16),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_BOLD,
  },
  bold72: {
    fontSize: fontToDp(40),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_BOLD,
  },
  bold16: {
    fontSize: fontToDp(10),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_BOLD,
  },
  regular28Little: {
    fontSize: fontToDp(15.5),
    color: colors.primaryBlue,
    fontFamily: PT_SANS_REGULAR,
  },

  //screens
  locationImageStyle: {
    resizeMode: 'contain',
    width: widthToDp('3.2%'),
    height: widthToDp('3.9%'),
    marginRight: 4,
  },
  detailsContainer: {
    backgroundColor: 'white',
    paddingHorizontal: widthToDp(4.5),
    paddingVertical: heightToDp(2.0),
  },
  detailsContainerforservices: {
    backgroundColor: 'white',
    paddingHorizontal: widthToDp(4.5),
    paddingVertical: heightToDp(2.0),
    flexDirection: 'row',
  },

  darkIconContainer: {
    backgroundColor: '#505C74',
    alignSelf: 'center',
    padding: widthToDp(1.5),
    borderRadius: widthToDp(6),
  },
  darkImageStyle: {
    width: widthToDp(5),
    height: widthToDp(5),
    resizeMode: 'contain',
    tintColor: '#fff',
    alignSelf: 'center',
  },
  dateIcon: {
    width: widthToDp(3),
    height: widthToDp(3),
    marginRight: widthToDp(1),
    resizeMode: 'contain',
    padding: widthToDp(1),
  },

  inputContainer: {
    flexDirection: 'row',
    width: widthToDp('90%'),
    height: heightToDp('6.5%'),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1.5,
  },
  inputBox: {
    textAlign: 'center',
    fontFamily: PT_SANS_NARROW_BOLD,
    color: colors.primaryBlue,
    marginHorizontal: '5%',
    height: 49,
    fontSize: fontToDp(18),
    width: widthToDp('73%'),
  },
});
