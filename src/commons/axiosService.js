import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {store} from '../redux/store';
import {ifValid, ScreenNames, showToastMsg} from '../utils';
import {navigate} from '../root/navigation/RootNavigation';
import React from 'react';
import {apiURL} from '../config';
import {Alert} from 'react-native';

const header = {
  accept: 'application/json',
  'content-type': 'application/json',
};

const loginHeader = {
  accept: 'application/json',
  'content-type': 'application/x-www-form-urlencoded',
};

class httpService {
  constructor() {
    this.setInterceptor();
  }

  setInterceptor = () => {
    let service = axios.create({
      baseURL: apiURL,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });
    service.interceptors.request.use(
      async (config) => {
        let token = await AsyncStorage.getItem('userToken');
        config.params = {access_token: token};
        return config;
      },
      (error) => {
        return Promise.reject(error);
      },
    );

    service.interceptors.response.use(
      function (response) {
        return response;
      },
      async (error) => {
        console.log('request in rejected');
        let originalRequest = error.config;
        console.log(JSON.stringify(error.message) + ' ' + originalRequest.url);
        if (ifValid(error.message) && error.message === 'Network Error') {
          console.log(
            JSON.stringify(error.message) + ' ' + originalRequest.url,
          );
          await this.logoutUser();
        } else if (
          error.response &&
          error.response.status &&
          error.response.status === 400
        ) {
          if (error.response.data && error.response.data.statusMessage) {
            showToastMsg(error?.response?.data?.statusMessage, '');
          }
        } else if (error.response.status === 401) {
          const response = await AsyncStorage.getItem('userToken');
          if (error.response.status === 401 && response) {
            return null;
          }
          console.log(
            JSON.stringify(error.message) + ' ' + originalRequest.url,
          );
          await this.logoutUser();

          if (error.response.data && error.response.data.statusMessage) {
            showToastMsg(
              'Something went wrong',
              error.response.data.statusMessage,
            );
          }
        } else if (error.response.status) {
          console.warn(error.response.status);
        }
        return error;
      },
    );
    this.service = service;
  };

  async logoutUser() {
    try {
      let {dispatch} = store;
      dispatch({type: 'RETRIEVE_TOKEN', payload: null});
      await AsyncStorage.removeItem('userToken');
      await AsyncStorage.removeItem('userDetails');
      if (ifValid(navigate)) {
        navigate(ScreenNames.LOGIN);
      }
    } catch (e) {
      console.log(e);
    }
  }

  /**
   *
   * @param path : url path without token
   * @param dispatch : for dispatching action from this function
   * @returns response or error
   */
  async get(path) {
    let _path = this.getPath(path);
    try {
      let res = await this.service.get(_path);
      return res;
    } catch (err) {
      return err;
    }
  }

  async getMem(path) {
    let _path = this.getPath(path);
    let res = await this.service.get(_path);
    return res;
  }

  /**d
   *
   * @param path : url path without token
   * @returns response or error
   */
  getPath(path) {
    let params = `${path.includes('?') ? '&' : ''}`;
    return `${path}${params}`;
  }

  /**
   *
   * @param path : url path without token
   * @param payload : object you want to send
   * @param dispatch : for dispatching action from this function
   * @returns  response or error
   */
  post(path, payload) {
    let _path = this.getPath(path);
    return this.service
      .request({
        method: 'POST',
        url: _path,
        data: payload,
      })
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
  }

  /**
   *
   * @param path path
   * @param payload payload
   * @param dispatch dispatch
   * @returns response
   */
  put(path, payload) {
    let _path = this.getPath(path);
    return this.service
      .request({
        method: 'PUT',
        url: _path,
        data: payload,
      })
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
  }

  /**
   *
   * @param path url
   * @param dispatch dispatch
   * @returns response
   */
  delete(path, dispatch) {
    let _path = this.getPath(path);
    return this.service
      .request({
        method: 'DELETE',
        url: _path,
      })
      .then((response) => this.successResponse(response))
      .catch((error) => this.errorResponse(error));
  }

  successResponse(response) {
    return response;
  }

  errorResponse(error) {
    return error;
  }
}

export default new httpService();
