import {StackActions} from '@react-navigation/routers';
import {setFromPayment} from '../redux/actions/login';
import {fromSettingsScreen} from '../redux/actions/screenState';
import {ScreenNames} from '../utils/ScreenNames';

const resetStack = (navigation) =>
  navigation.reset({index: 0, routes: [{name: ScreenNames.HOME}]});

export const backHandler = (dispatch, navigation) => {
  dispatch(fromSettingsScreen(false));
  dispatch(setFromPayment('default'));
  // resetStack(navigation);
  navigation.dispatch(
    StackActions.replace(ScreenNames.HOME, {
      screen: ScreenNames.SETTINGS,
    }),
  );
};
