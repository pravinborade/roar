import React from 'react';
import {Button, InputAccessoryView, Keyboard, View} from 'react-native';
import {inputAccessoryViewID} from '../config/index';
const InputDone = () => {
  return (
    <InputAccessoryView nativeID={inputAccessoryViewID}>
      <View
        style={{
          width: '100%',
          alignItems: 'flex-end',
          backgroundColor: '#FFFFFF',
        }}>
        <Button
          onPress={() => {
            Keyboard.dismiss();
          }}
          title="Done"
        />
      </View>
    </InputAccessoryView>
  );
};

export default InputDone;
