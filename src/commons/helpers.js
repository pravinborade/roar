import moment from 'moment';
import React, {Component} from 'react';
//import { PermissionsAndroid, Platform } from 'react-native';
//import { Card, Text, Thumbnail, CardItem, Item, Toast, List, ListItem, Body } from 'native-base';
//import Images from '../themes/images';
//import variable from '../../native-base-theme/variables/platform';
//import { NavigationActions } from 'react-navigation';
//var _ = require('lodash');
//import Geolocation from '@react-native-community/geolocation';
//import { setLocation } from '../actions/locationAction';
//import { setDashData, setFilterData } from '../actions/dashDataAction';

export const helper = {
  formatDate(date) {
    return `${moment(date).format('ddd')} ${moment(date).format(
      'DD MMMM YYYY',
    )}`;
  },
  formatTime(time) {
    return time.slice(0, 5);
  },
  formatCurrancy() {
    return '₹';
  },
  calculateGST(val) {
    return (val / 100) * 9;
  },
  convertPstoRs(val) {
    return val / 100;
  },
  convertRstoPs(val) {
    return val * 100;
  },
  calculateFinalPrice(valOne, valTwo) {
    return (parseInt(valOne) - parseInt(valTwo)).toFixed(2);
  },
  // check_UnavailableSlots(data) {
  //   var unavailableSlots = [];
  //   data.map((item) => {
  //     if (item.isBooked === 'YES') {
  //       unavailableSlots.push(item)
  //     }
  //   })
  //   return unavailableSlots.length > 0;
  // },
  // checkSlotTimout(date, startTime) {
  //   var nowTime = moment(`${moment(new Date()).format('DD-MM-YYYY HH:mm:ss')}`);
  //   var slotTime = moment(`${moment(date).format('DD-MM-YYYY')} ${startTime}`);
  //   if (slotTime.isAfter(nowTime)) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // },
  // calculateDistance(dlatitude, dlongitude, mlatitude, mlongitude) {
  //   console.log('$$$calculateDistance', dlatitude, dlongitude, mlatitude, mlongitude);
  //   var latitude = dlatitude ? dlatitude : mlatitude;
  //   var lang = dlongitude ? dlongitude : mlongitude;
  //   var radlat1 = Math.PI * mlatitude / 180;
  //   var radlat2 = Math.PI * latitude / 180;
  //   var theta = mlongitude - lang;
  //   var radtheta = Math.PI * theta / 180;
  //   var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  //   if (dist > 1) {
  //     dist = 1;
  //   }
  //   dist = Math.acos(dist)
  //   dist = dist * 180 / Math.PI
  //   dist = dist * 60 * 1.1515
  //   //this.setState({distance:(dist = dist * 1.609344).toFixed(2)});
  //   return (dist = dist * 1.609344).toFixed(2);
  // },
  // sortArrayAsc(array, key) {
  //   if (array.length) {
  //     return array.sort(function (a, b) {
  //       return b.distance > a.distance ? -1
  //         : b.distance < a.distance ? 1
  //           : 0
  //     })
  //   } else {
  //     return []
  //   }

  // },
  // listOfMonths: [
  //   { "label": "January", "value": "1" },
  //   { "label": "February", "value": "2" },
  //   { "label": "March", "value": "3" },
  //   { "label": "April", "value": "4" },
  //   { "label": "May", "value": "5" },
  //   { "label": "June", "value": "6" },
  //   { "label": "July", "value": "7" },
  //   { "label": "August", "value": "8" },
  //   { "label": "September", "value": "9" },
  //   { "label": "October", "value": "10" },
  //   { "label": "November", "value": "11" },
  //   { "label": "December", "value": "12" }],
  // listOfYears: [{ label: "2020", value: "2020" }, { label: "2021", value: "2021" },
  // { label: "2022", value: "2022" }, { label: "2023", value: "2023" }, { label: "2024", value: "2024" },
  // { label: "2025", value: "2025" }, { label: "2026", value: "2026" }, { label: "2027", value: "2027" },
  // { label: "2028", value: "2028" }, { label: "2029", value: "2029" }, { label: "2030", value: "2030" },
  // { label: "2031", value: "2031" }, { label: "2032", value: "2032" }, { label: "2033", value: "2033" },
  // { label: "2034", value: "2034" }, { label: "2035", value: "2035" }, { label: "2036", value: "2036" },
  // { label: "2037", value: "2037" }, { label: "2038", value: "2038" }, { label: "2039", value: "2039" },
  // { label: "2040", value: "2040" }, { label: "2041", value: "2041" }, { label: "2042", value: "2042" },
  // { label: "2043", value: "2043" }, { label: "2044", value: "2044" }, { label: "2045", value: "2045" },
  // { label: "2046", value: "2046" }, { label: "2047", value: "2047" }, { label: "2048", value: "2048" },
  // { label: "2049", value: "2049" }, { label: "2050", value: "2050" }],
  // successToast(message) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     duration: 3000
  //   })
  // },
  // successToastWithBack(message, scope) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     duration: 3000,
  //     onClose: function () {
  //       scope.props.navigation.goBack();
  //     }
  //   })
  // },
  // successToastWithRoute(message, scope, route) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     duration: 3000,
  //     onClose: function () {
  //       scope.props.navigation.navigate(route);
  //     }
  //   })
  // },
  // errorToast(message) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     buttonText: "Ok",
  //     duration: 3000
  //   })
  // },
  // errorToastReBooking(message) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     buttonText: "Ok",
  //     duration: 30000
  //   })
  // },
  // errorToastWithBack(message, scope) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     duration: 3000,
  //     onClose: function () {
  //       scope.props.navigation.goBack();
  //     }
  //   })
  // },
  // errorToastWithRoute(message, scope, route) {
  //   Toast.show({
  //     text: message,
  //     textStyle: { color: '#fdc630' },
  //     position: "top",
  //     duration: 3000,
  //     onClose: function () {
  //       scope.props.navigation.navigate(route);
  //     }
  //   })
  // },
  // errorToastToLogin(scope) {
  //   Toast.show({
  //     text: "Something went wrong, please try after sometime",
  //     textStyle: { color: '#fff' },
  //     position: "top",
  //     type: "warning",
  //     duration: 2000,
  //     onClose: function () {
  //       scope.props.navigation.navigate('Logout');
  //     }
  //   })
  // },

  // filterDashData(allData, isSearchText, scope) {
  //   let data = allData;
  //   if (isSearchText) {
  //     data = allData.filter((item) => {
  //       var res = item.resource;
  //       var site = item.site;
  //       var org = site.organization;
  //       let str = `${res.category} ${res.discription} ${res.name} ${site.area} ${site.city} ${site.country} ${site.discription} ${site.locationArea} ${site.name} ${org.name}`;
  //       str = str.toLowerCase();
  //       isSearchText = isSearchText.toLowerCase();
  //       return str.includes(isSearchText);
  //     })
  //   }
  //   data = helper.sortArrayAsc(data);
  //   let sports = _.groupBy(data, 'resource.category');
  //   sports = {
  //     'ALL': data,
  //     ...sports
  //   }
  //   // if(params){
  //   //   data = _.filter(data,[params,text]);
  //   // }
  //   //return data;
  //   scope.props.dispatch(setFilterData(sports))
  // },

  // renderNoDataFound(msg) {
  //   return <Item noLine style={{ flexDirection: 'column', height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', paddingTop: 10 }}>
  //     <Text>{msg}</Text>
  //   </Item>
  // },
  // async requestLocationPermission(scope, callback) {
  //   try {
  //     const granted = await PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //       {
  //         'title': 'Example App',
  //         'message': 'Player.Do App access to your location '
  //       }
  //     )
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       Geolocation.getCurrentPosition(
  //         (position) => {
  //           console.log("$$position", position)
  //           scope.props.dispatch(setLocation(position));
  //           callback(true);
  //         },
  //         (error) => {
  //           console.log(error.code, error.message);
  //           callback(false);
  //         },
  //         { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
  //       );
  //       Geolocation.watchPosition((position) => {
  //         const lastPosition = position;
  //         scope.props.dispatch(setLocation(lastPosition));
  //       });
  //     } else {
  //       console.log("location permission denied")
  //       callback(true);
  //     }
  //   } catch (err) {
  //     console.warn(err)
  //     callback(false)
  //   }
  // },
  // async requestLocationPermissionforIos(scope, callback) {
  //   console.log("$$position requestLocationPermissionforIos");
  //   Geolocation.getCurrentPosition(
  //     (position) => {
  //       console.log("$$position", position)
  //       scope.props.dispatch(setLocation(position));
  //       callback(true)
  //     },
  //     (error) => {
  //       console.log(error.code, error.message);
  //       callback(false)
  //     },
  //     { enableHighAccuracy: false, timeout: 50000 }
  //     // { enableHighAccuracy: true, timeout: 30000, maximumAge: 10000 }
  //   );
  //   Geolocation.watchPosition((position) => {
  //     const lastPosition = position;
  //     scope.props.dispatch(setLocation(lastPosition));
  //   });

  // }
};
