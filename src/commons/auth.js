import AsyncStorage from '@react-native-community/async-storage';

export const setToken = async (token) => {
  await AsyncStorage.setItem('accessToken', token.accessToken);
};

export const getToken = async () => {
  return await AsyncStorage.getItem('accessToken');
};
