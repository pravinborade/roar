import {apiURL} from '../config/index';

// import { show, hide } from '../actions/loaderAction';
// import helper from './helpers';

const header = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};
const headerLogin = {
  accept: 'application/json',
  'content-type': 'multipart/form-data',
  authorization: 'Basic YWJjOjEyMzQ=',
};

const httpService = {
  postlogin(url, data) {
    //props? props.dispatch(show()) : null;
    return fetch(apiURL + url, {
      method: 'POST',
      headers: headerLogin,
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('@@data', data);
        return data;
      })
      .catch((error) => {
        console.log('Error');
      });
  },

  post(url, data) {
    //props? props.dispatch(show()) : null;
    return fetch(apiURL + url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => {
        //props? props.dispatch(hide()) : null;
        return data;
      })
      .catch((error) => {
        console.log('##error', error);
        //	helper.errorToast('Something went wrong');
      });
    //	props? props.dispatch(hide()) : null;
  },

  get(url, paymentchk) {
    //dispatch(show()) : null;
    console.log('get url ####', url);
    return fetch(apiURL + url, {
      method: 'GET',
      headers: header,
    })
      .then((response) => response.json())
      .then((resJson) => {
        if (resJson) {
          console.log('resJson ##', resJson);
          //props? props.dispatch(hide()) : null;
          return resJson;
        }
      })
      .catch((error) => {
        //console.log("resJson ##", error)
        //props? props.dispatch(hide()) : null;
        //if(!paymentchk)
        //helper.errorToast('Something went wrong')
        //return error;
      });
  },
};

export default httpService;
