const Images = {
  loginLogo: require('./Login/logo.png'),
  logo: require('./logo.png'),
  events: require('./events.png'),
  home: require('./home.png'),
  membership: require('./membership.png'),
  bookings: require('./previousQueue.png'),
  settings: require('./settings.png'),
  onboarding_1: require('./OnBoarding/onboarding_1.png'),
  onboarding_2: require('./OnBoarding/onboarding_2.png'),
  onboarding_3: require('./OnBoarding/onboarding_3.png'),
  googleButton: require('./Login/googleButton.png'),

  basketball: require('./Bookings/basketball.png'),
  backArrow: require('./backArrow.png'),
  eye: require('./Login/eye.png'),
  eyeOff: require('./Login/eye-off.png'),

  crossImage: require('./crossImage.png'),

  //Settings
  Settings_events: require('./Settings/events.png'),
  Settings_about: require('./Settings/about.png'),
  Settings_bookings: require('./Settings/bookings.png'),
  Settings_membership: require('./Settings/membership.png'),
  Settings_profile: require('./Settings/profile.png'),
  Settings_star: require('./Settings/star.png'),
  Settings_terms: require('./Settings/terms.png'),
  User_avatar: require('./Settings/userAvatar.png'),

  //homeScreen
  homeBell: require('./home/bell.png'),
  homeLocation: require('./home/location.png'),
  homeSearch: require('./home/search.png'),
  add1: require('./home/add1.png'),
  filter: require('./home/filter.png'),
  bat: require('./home/bat.png'),
  soccer: require('./home/soccker.png'),
  badminton: require('./home/badmintan.png'),
  feature: require('./feature.jpg'),

  yoga: require('./home/yoga.png'),
  zumba: require('./home/Zumba.png'),
  rugby: require('./home/rugby.png'),

  arrowRight: require('./arrowRight.png'),
  arrowBottom: require('./arrowBottom.png'),
  shirt: require('./home/shirt.png'),
  helmet: require('./home/helmet.png'),
  cross: require('./cross.png'),
  crossRed: require('./crossRed.png'),
  footBall: require('./home/footBall.png'),
  badmintonDark: require('./home/badmintanDark.png'),
  footballDark: require('./home/footballDark.png'),
  cricketDark: require('./home/cricketDark.png'),
  box: require('./home/box.png'),
  dropdown: require('./home/dropdown.png'),
  baseBallDark: require('./home/baseBall.png'),
  basketBallDark: require('./home/basketBall.png'),
  tenisDark: require('./home/tensi.png'),
  batDark: require('./home/batDark.png'),
  ///home sports
  boxing: require('./home/sports/boxing.png'),
  baseball: require('./home/sports/baseball.png'),
  fencing: require('./home/sports/fencing.png'),
  diving: require('./home/sports/diving.png'),
  hockky: require('./home/sports/hockky.png'),
  tenis: require('./home/sports/tenis.png'),
  archary: require('./home/sports/archary.png'),
  vollyball: require('./home/sports/vollyball.png'),

  gymnastics: require('./home/sports/gymnastics.png'),
  squash: require('./home/sports/squash.png'),
  swimming: require('./home/sports/swimming.png'),
  //
  khokho: require('./home/sports/khokho.png'),
  volleyball: require('./home/sports/volleyball.png'),
  dogeball: require('./home/sports/dogeball.png'),
  defaultSport: require('./home/sports/default.png'),

  ///home bookings
  clock: require('./home/clock.png'),
  calendar: require('./home/calendar.png'),
  amenities: require('./home/amenities.png'),
  checkWhite: require('./home/checkWhite.png'),
  checkBlue: require('./home/checkBlue.png'),

  //Bookings
  sampleBooking: require('./Bookings/bookingImage.png'),

  //Events
  sampleEvents: require('./myEvents.png'),

  //My Services
  myServices: require('./myServices.png'),
  myserviceicon: require('./home/shirt.png'),

  //My Membership
  orangeCircle: require('./Membership/orangeCircle.png'),
  blueCircle: require('./Membership/blueCircle.png'),
  greenCircle: require('./Membership/greenCircle.png'),
  blueImg: require('./Membership/blueImg.png'),
  greenImg: require('./Membership/greenImg.png'),
  orangeImg: require('./Membership/orangeImg.png'),
  calenderButton: require('./Membership/calenderButton.png'),

  location: require('./location.png'),
  cuurentlocation: require('./budicon-location.png'),
  product: require('./Bookings/product.png'),
  locationBlue: require('./locationBlue.png'),
  dollar: require('./Bookings/doller.png'),
  barcode: require('./barcode.png'),
  star: require('./star.png'),
  starNot: require('./starNot.png'),
};

export default Images;
