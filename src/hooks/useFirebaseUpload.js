import React, {useEffect, useState} from 'react';
import storage from '@react-native-firebase/storage';
import {errorLog} from '../utils/fireLog';

export const useFirebaseUpload = () => {
  const [uploading, setUploading] = useState(false);
  const [transferred, setTransferred] = useState(0);
  const [fileData, setfileData] = useState(null);
  const [downloadUrl, setdownloadUrl] = useState(null);
  const [UserId, setUserId] = useState(null);
  const [deleted, setDeleted] = useState(false);

  useEffect(() => {
    const uploadFile = async () => {
      const filename = UserId.toString();
      const uploadUri =
        Platform.OS === 'ios' ? fileData.replace('file://', '') : fileData;
      var metadata = {
        customMetadata: {
          UserId: UserId.toString(),
        },
      };

      setUploading(true);
      setTransferred(0);
      const task = storage().ref(filename).putFile(uploadUri, metadata);
      task.on(
        'state_changed',
        (snapshot) => {
          setTransferred(
            Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000,
          );
        },
        (error) => {
          setUploading(false);
          setTransferred(0);
        },
      );

      try {
        await task;
        let fileRef = storage().ref(filename);
        await fileRef
          .getDownloadURL()
          .then((url) => {
            setdownloadUrl(url);
          })
          .catch((e) => errorLog(e));
      } catch (e) {
        setUploading(false);
        setTransferred(0);
        errorLog(e);
      }
      setUploading(false);
    };

    fileData && uploadFile();
  }, [fileData]);

  useEffect(() => {
    const deletedFile = async () => {
      const filename = UserId.toString();
      let task = await storage()
        .ref(filename)
        .delete()
        .catch((x) => {
          setdownloadUrl('-');
        })
        .done((x) => {
          setdownloadUrl('-');
        });
    };
    deleted && deletedFile();
  }, [deleted]);

  const setFileSource = (uri, id, deletePhoto = false) => {
    if (deletePhoto) {
      setUserId(id);
      setDeleted(deletePhoto);
    } else {
      setUserId(id);
      setfileData(uri);
    }
  };

  return [uploading, transferred, setFileSource, downloadUrl, deleted];
};
