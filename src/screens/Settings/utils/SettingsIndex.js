import Images from '../../../assets/Images/Images';
import {Content} from '../../../utils/content/Content';
import {ScreenNames} from '../../../utils/ScreenNames';

export const DATA = [
  {
    id: '1',
    title: Content.MyProfile,
    image: Images.Settings_profile,
    navigationPage: ScreenNames.MY_PROFILE,
  },
  {
    id: '2',
    title: Content.MyBookings,
    image: Images.Settings_bookings,
    navigationPage: ScreenNames.BOOKINGS,
  },
  {
    id: '3',
    title: Content.MyMembership,
    image: Images.Settings_membership,
    navigationPage: ScreenNames.MYMEMBERSHIP,
  },
  {
    id: '4',
    title: Content.MyServices,
    image: Images.Settings_star,
    navigationPage: ScreenNames.MY_SERVICES,
  },
  {
    id: '5',
    title: Content.MyEvents,
    image: Images.Settings_events,
    navigationPage: ScreenNames.EVENTS,
  },
  {
    id: '6',
    title: Content.Reports,
    image: Images.Settings_events,
    navigationPage: ScreenNames.REPORTS,
  },
  {
    id: '7',
    title: Content.About,
    image: Images.Settings_about,
    navigationPage: ScreenNames.ABOUT_ROAR,
  },
  {
    id: '8',
    title: Content.TermsOfUse,
    image: Images.Settings_terms,
    navigationPage: ScreenNames.TERMS_OF_USE,
  },
];
