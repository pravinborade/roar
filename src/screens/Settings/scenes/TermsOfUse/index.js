import React from 'react';
import {Image, Text, View, ScrollView} from 'react-native';
import Container from '../../../../components/Container';
import {ScreenNames} from '../../../../utils/ScreenNames';
import LinearGradient from 'react-native-linear-gradient';
import Images from '../../../../assets/Images/Images';
import {colors} from '../../../../styles/colors';
import {Content} from '../../../../utils/content/Content';
import {HeaderBack} from '../../../../components';
import {styles} from '../AboutRoar/styles';
import {backHandler} from '../../../../commons/settingsBackHandler';
import {useDispatch} from 'react-redux';
import {useBackHandler} from '@react-native-community/hooks';
const TermsOfUSe = ({navigation}) => {
  const dispatch = useDispatch();
  const backActionHandler = () => {
    backHandler(dispatch, navigation);
    return true;
  };
  useBackHandler(backActionHandler);

  return (
    <Container>
      <HeaderBack
        title={ScreenNames.TERMS_OF_USE}
        onPress={() => backHandler(dispatch, navigation)}
      />
      <LinearGradient
        colors={[colors.lightGreen, colors.primaryGreen]}
        style={{flex: 1}}>
        <ScrollView>
          <View style={styles.TopContainer}>
            <Image style={styles.logo} source={Images.logo} />

            <View style={{padding: 20, marginTop: 20}}>
              <Text style={styles.subHeading}>{Content.Terms}</Text>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    </Container>
  );
};

export default TermsOfUSe;
