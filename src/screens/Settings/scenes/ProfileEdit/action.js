import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import axiosService from '../../../../commons/axiosService';
import {ValidationMsg} from '../../../../utils';

export const editCurrentProfile = (data, token) => {
  return (dispatch) => {
    axiosService
      .put(`api/users`, data, dispatch)
      .then((res) => {
        let data = res.response ? res.response : res;
        if (data.status == 200) {
          let userresponse = {...res?.data?.data[0], token: token};
          dispatch({type: 'SET_PROFILE', payload: userresponse});
          AsyncStorage.setItem('userDetails', JSON.stringify(userresponse));
          Toast.show({
            type: 'success',
            text1: 'Success',
            text2: data.data.statusMessage,
          });
        }
      })
      .catch(function (response) {
        Toast.show({
          type: 'error',
          text1: 'Error',
          text2: ValidationMsg.problemWithYourProfile,
        });
        console.log(response);
      });
  };
};
