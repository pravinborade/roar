import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Images from '../../../../assets/Images/Images';
import {
  ButtonCommon,
  Container,
  HeaderBack,
  LoaderAndMsg,
} from '../../../../components';
import {colors} from '../../../../styles';
import {PT_SANS_BOLD, PT_SANS_REGULAR} from '../../../../styles/typography';
import {fontToDp, height, width} from '../../../../utils/Responsive';
import {Content, ScreenNames, ValidationMsg} from '../../../../utils';
import {launchImageLibrary} from 'react-native-image-picker';
import analytics from '@react-native-firebase/analytics';
import {editCurrentProfile} from './action';
import {useFirebaseUpload} from '../../../../hooks/useFirebaseUpload';
import common from 'react-native-linear-gradient/common';
import {ifNotValid} from '../../../../utils/helper';

var validate = require('validate.js');

const ProfileEdit = ({navigation}) => {
  const [loading, setloading] = useState(false);
  const [ImageSource, setImageSource] = useState(Images.User_avatar);
  const [choosePhoto, setchoosePhoto] = useState(false);

  const [editUserDetails, seteditUserDetails] = useState({
    firstName: '',
    lastName: '',
    email: '',
    phoneNo: '',
    address: '',
  });
  const [fnamerror, setfnameError] = useState(false);
  const [lnamerror, setlnameError] = useState(false);
  const [emailerror, setemailError] = useState(false);
  const [phoneNoerror, setphoneNoError] = useState(false);
  const [addresserror, setaddressError] = useState(false);

  const [uploading, transferred, setFileSource, downloadUrl] =
    useFirebaseUpload(null);
  const dispatch = useDispatch();
  const userToken = useSelector((state) => state.login.userToken);
  const userDetails = useSelector((state) => state.login.data);

  useEffect(() => {
    seteditUserDetails({
      firstName: userDetails?.firstName ? userDetails?.firstName : '',
      lastName: userDetails?.lastName ? userDetails?.lastName : '',
      email: userDetails?.username ? userDetails?.username : '',
      phoneNo: userDetails?.phoneNo ? userDetails?.phoneNo : '',
      address: userDetails?.address ? userDetails?.address : '',
    });
  }, [userDetails]);

  const handleChange = (value, name) => {
    if (name === 'firstName' || name === 'lastName') {
      value = value.replace(/[^A-Za-z]/gi, '');
      seteditUserDetails((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    } else {
      seteditUserDetails((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    }
  };

  useEffect(() => {
    if (downloadUrl) {
      setloading(true);
      const value = {
        id: userDetails.id,
        firstName: editUserDetails.firstName,
        image: downloadUrl === '-' || downloadUrl === '' ? '' : downloadUrl,
        lastName: editUserDetails.lastName,
        email: editUserDetails.email,
        address: editUserDetails.address,
        phoneNo: editUserDetails.phoneNo,
      };
      dispatch(editCurrentProfile(value, userToken, navigation));
      setloading(false);
      navigation?.goBack();
    }
  }, [downloadUrl]);

  const emeilid = () => {
    var re = /\S+@\S+\.\S+/;
    return re.test(editUserDetails.email);
  };

  const validateMobile = () => {
    var re = /^(\+\d{1,3}[- ]?)?\d{10}$/;
    return re.test(editUserDetails.phoneNo);
  };

  const validation = () => {
    if (validate.isEmpty(editUserDetails.firstName)) {
      setfnameError(true);
    } else if (validate.isEmpty(editUserDetails.lastName)) {
      setlnameError(true);
    } else if (
      !emeilid(editUserDetails.email) ||
      validate.isEmpty(editUserDetails.email)
    ) {
      setemailError(true);
    } else if (
      !validateMobile(editUserDetails.phoneNo) ||
      validate.isEmpty(editUserDetails.phoneNo)
    ) {
      setphoneNoError(true);
    } else if (validate.isEmpty(editUserDetails.address)) {
      setaddressError(true);
    } else {
      return true;
    }
    return false;
  };

  const onEdit = async () => {
    if (validation()) {
      await analytics().logEvent(Content.editProfile, {
        edit_Profile: true,
      });
      if (typeof ImageSource === 'object') {
        setloading(true);
        let fileUrl =
          Platform.OS === 'android'
            ? ImageSource.uri
            : ImageSource.uri.replace('file://', '');
        setFileSource(fileUrl, userDetails.id);
      } else {
        setloading(true);
        const value = {
          id: userDetails.id,
          firstName: editUserDetails.firstName,
          lastName: editUserDetails.lastName,
          email: editUserDetails.email,
          address: editUserDetails.address,
          phoneNo: editUserDetails.phoneNo,
        };

        setfnameError(false);
        setlnameError(false);
        setemailError(false);
        setphoneNoError(false);
        setaddressError(false);
        dispatch(editCurrentProfile(value, userToken, navigation));
        setloading(false);
      }
    }
  };

  const _onChoosePhoto = () => {
    ifNotValid(userDetails?.image) || userDetails?.image === ''
      ? Alert.alert(Content.profilePicture, Content.updateYourProfilePic, [
          {text: Content.cancelBtn, onPress: () => {}},
          {text: Content.gallery, onPress: () => _choosePhoto()},
        ])
      : Alert.alert(Content.profilePicture, Content.updateYourProfilePic, [
          {text: Content.cancelBtn, onPress: () => {}},
          {text: Content.gallery, onPress: () => _choosePhoto()},
          {
            text: Content.removePhoto,
            onPress: () => {
              setloading(true);
              setFileSource('', userDetails?.id, true);
            },
            style: 'cancel ',
          },
        ]);
  };

  const _choosePhoto = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };
    launchImageLibrary(options, async (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = {uri: response.uri};
        setImageSource(source);
        setchoosePhoto(true);
        await analytics().logEvent(Content.choosePhoto, {
          choose_Photo: true,
        });
      }
    });
  };

  return (
    <Container>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' && 'padding'}
        style={{flex: 1}}>
        <View style={styles.containerInner}>
          <HeaderBack
            title={ScreenNames.PROFILE_EDIT}
            navigation={navigation}
          />

          <View style={styles.mainContainer}>
            <TouchableOpacity onPress={_onChoosePhoto}>
              <Image
                resizeMode="contain"
                style={styles.imageView}
                source={
                  userDetails?.image && choosePhoto === false
                    ? {uri: userDetails?.image}
                    : userDetails?.image === false &&
                      userDetails?.image === '-' &&
                      choosePhoto === false
                    ? ImageSource
                    : ImageSource
                }
              />
            </TouchableOpacity>

            <View style={{marginTop: 5}}>
              <Text style={styles.headerText}>{Content.chngProfile}</Text>
            </View>
          </View>
          <ScrollView>
            <View style={styles.subView}>
              <Text style={styles.viewText}>{Content.FirstName}</Text>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                onChangeText={(value) => {
                  handleChange(value, 'firstName');
                }}
                value={editUserDetails?.firstName}
                defaultValue={userDetails?.firstName}
                style={styles.textInput}
              />
              {fnamerror && (
                <Text style={styles.errorText}>
                  {ValidationMsg.enterFirstName}
                </Text>
              )}
            </View>

            <View style={styles.subView}>
              <Text style={styles.viewText}>{Content.LastName}</Text>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                onChangeText={(value) => handleChange(value, 'lastName')}
                value={editUserDetails?.lastName}
                defaultValue={userDetails?.lastName}
                style={styles.textInput}
              />
              {lnamerror && (
                <Text style={styles.errorText}>
                  {ValidationMsg.enterLastName}
                </Text>
              )}
            </View>

            <View style={styles.subView}>
              <Text style={styles.viewText}>{Content.eMailId}</Text>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                onChangeText={(value) => {
                  handleChange(value, 'email');
                }}
                editable={false}
                defaultValue={userDetails?.username}
                style={styles.textInput}
              />
              {emailerror && (
                <Text style={styles.errorText}>
                  {ValidationMsg.emailCannotBeEmpty}
                </Text>
              )}
            </View>

            <View style={styles.subView}>
              <Text style={styles.viewText}>{Content.phNo}</Text>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="phone-pad"
                onChangeText={(value) => {
                  handleChange(value, 'phoneNo');
                }}
                maxLength={10}
                defaultValue={userDetails?.phoneNo}
                style={styles.textInput}
              />
              {phoneNoerror && (
                <Text style={styles.errorText}>
                  {ValidationMsg.phoneNoCannotEmpty}
                </Text>
              )}
            </View>

            <View style={styles.subView}>
              <Text style={styles.viewText}>{Content.address}</Text>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                onChangeText={(value) => {
                  handleChange(value, 'address');
                }}
                defaultValue={userDetails?.address}
                style={styles.textInput}
              />
              {addresserror && (
                <Text style={styles.errorText}>
                  {ValidationMsg.addressCannotEmpty}
                </Text>
              )}
            </View>

            <ButtonCommon
              loading={loading}
              onPress={onEdit}
              text={'Save'}
              textStyle={{fontSize: fontToDp(14)}}
            />
          </ScrollView>
          {loading && (
            <View style={styles.loaderContainer}>
              <LoaderAndMsg text={'Please wait'} />
            </View>
          )}
        </View>
      </KeyboardAvoidingView>
    </Container>
  );
};

export default ProfileEdit;

const styles = StyleSheet.create({
  containerInner: {
    backgroundColor: colors.background,
    flex: 1,
  },
  subView: {
    padding: 12,
    borderBottomWidth: 3,
    borderBottomColor: colors.background,
  },
  mainContainer: {
    borderTopColor: colors.background,
    borderTopWidth: 4,
    backgroundColor: colors.white,
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewText: {
    color: '#505C74',
    fontFamily: PT_SANS_BOLD,
    fontSize: fontToDp(12),
    marginBottom: 5,
  },
  textInput: {
    padding: 10,
    backgroundColor: colors.white,
    height: 40,
  },
  headerText: {
    padding: 10,
    backgroundColor: colors.white,
    height: 40,
    color: '#202B46',
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(14),
  },
  imageView: {
    resizeMode: 'cover',
    height: fontToDp(80),
    width: fontToDp(80),
    borderRadius: fontToDp(80 / 2),
    overflow: 'hidden',
  },
  loaderContainer: {
    position: 'absolute',
    width,
    height,
    ...common.justifyAlignCenter,
  },
  errorText: {
    color: '#FF0033',
    marginHorizontal: '0%',
  },
});
