import React, {useEffect, useState} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import axiosService from '../../../../commons/axiosService';
import Images from '../../../../assets/Images/Images';
import {Container, HeaderBack} from '../../../../components';
import {fontsize} from '../../../../utils/fontSize';
import ButtonCommon from '../../../../components/ButtonCommon';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../styles/typography';
import {ScreenNames} from '../../../../utils/ScreenNames';
import {Content} from '../../../../utils/content/Content';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';
import {ifNotValid, ifValid} from '../../../../utils/helper';
import helper from '../../../../commons/helperFunctions';
import RazorpayCheckout from 'react-native-razorpay';
import {successLog} from '../../../../utils/fireLog';
import {getOptions} from '../../../../screens/Core/Home/scenes/summary/comp/action';
import validate from 'validate.js';
import {colors} from '../../../../styles';
import {fontToDp} from '../../../../utils/Responsive';

const myServices = ({route, navigation, props}) => {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.login.data);
  let loading = useSelector((x) => x.loader.loading);
  const {item} = route.params;
  let checkUrl = validate(
    {website: item.services.image},
    {website: {url: true}},
  );
  let image =
    ifValid(item.services.image) &&
    item.services.image !== '' &&
    checkUrl?.website === undefined
      ? {uri: item.services.image}
      : Images.myServices;

  const [membership, setMemberShip] = useState(false);
  useEffect(() => {
    renewbuttonlogic();
  }, []);

  const renewbuttonlogic = () => {
    var date1 = moment(item.enquiryDate).add(
      item.services.period,
      item.services.periodUnit,
    );
    var date2 = moment();
    if (date2.isAfter(date1)) {
      setMemberShip(true);
    } else {
      setMemberShip(false);
    }
  };

  const _onPaymentButtonPress = () => {
    console.log('MEMBERSHIP data-------->', membership);
    successLog('validating');
    let priceTotal = helper.convertRstoPs(item.services.rate);
    successLog(item.services.rate);
    if (priceTotal === 0) {
      console.warn('invalid price' + priceTotal);
      return;
    }
    if (!ifValid(userData) || !ifValid(userData.firstName)) {
      dispatch(setFromPayment('venue_book'));
      // props.navigation.navigate(ScreenNames.INITIAL)
      return;
    }
    console.log('PRIZE TOTAL    1------------------>', priceTotal);
    prePaymentApiCall(priceTotal);
  };

  //TODO: step: 2 prepayment api
  const prePaymentApiCall = (priceTotal) => {
    console.log('PRIZE TOTAL    2------------------>', priceTotal);
    successLog('prePaymentApiCall');
    dispatch({type: 'SET_LOADING', payload: true});

    let payload = {
      amount: priceTotal,
      userId: userData.id,
      type: 'service',
    };
    axiosService
      .post(`api/payments?order=true`, payload)
      .then((response) => {
        console.log('PRE PAYMENT SERVICES---------->', response);
        successLog('prePaymentApiCall', response.status);
        if (response.status === 200) {
          response = response.data;
          if (ifNotValid(response)) {
            successLog('invalid response');
            return;
          }
          if (ifValid(response.data[0])) {
            makePayment(response.data[0], priceTotal);
          } else {
            alert('Something went wrong while pre payment check');
          }
        } else {
          alert('Something went wrong..');
        }
      })
      .catch((o) => {
        console.log('makeInitialTransaction error ' + JSON.stringify(o));
      });
  };

  //TODO: step: 3 make payment
  const makePayment = (prePaymentRes, priceTotal) => {
    console.log('PRIZE TOTAL    3------------------>', priceTotal);
    successLog('makePayment');
    let options = getOptions(
      userData,
      priceTotal,
      prePaymentRes.orderId,
      `service`,
    );
    RazorpayCheckout.open(options)
      .then((res) => {
        successLog('onPaymentSuccess');
        let paymentResponse = res;
        //TODO: step: 4  book slot, membership, inventory and service.
        bookServiceApi(paymentResponse, prePaymentRes, priceTotal);
      })
      .catch((error) => {
        dispatch({type: 'SET_LOADING', payload: false});
        console.log('ERRORR------------>', error.error.description);
        alert(error.error.description);
      });
  };

  const bookServiceApi = (paymentResponse, prePaymentRes, priceTotal) => {
    let requestBodyServices = {
      paymentDetails: {
        ...prePaymentRes,
        ...paymentResponse,
        paymentSource: 'MOBILE',
        siteId: item.services.site.id,
        orgId: item.services.site.organization.id,
        paymentId: paymentResponse.razorpay_payment_id,
      },
      serviceRequest: [
        {
          id: item.id,
          quantity: item.quantity !== '' ? item.quantity : 1,
          services: {
            id: item.services.id,
          },
          user: {
            id: userData.id,
          },
          site: {
            id: item.services.site.id,
          },
          amount: priceTotal,
          paymentMode: 'UPI',
          paymentStatus: 'PAID',
        },
      ],
    };

    console.log('bookServiceApi---------12345-------->', requestBodyServices);
    if (ifValid(requestBodyServices)) {
      console.log('requestBodyServices-------->', requestBodyServices);
      axiosService
        .post(
          `api/services/save/serviceRequestList?userId=${
            userData.id
          }&comment=${'no comment'}`,
          requestBodyServices,
        )
        .then((response) => {
          console.log('BOOK SERVICE API', response);
          successLog('4) bookServiceApi', response.status);
          if (response.status === 200) {
            console.log('bookService api successful');
            navigation.navigate(ScreenNames.SERVICE_ACKNOWLEDGEMENT);
          } else {
            alert('something went wrong while book service appointment');
          }
          dispatch({type: 'SET_LOADING', payload: false});
        });
    } else {
      dispatch({type: 'SET_LOADING', payload: false});
      console.log('bookService api no payload moving to ACKNOWLEDGEMENT ');
      props.navigation.navigate(ScreenNames.HOME_ACKNOWLEDGEMENT);
    }
  };

  return (
    <Container>
      <HeaderBack title={ScreenNames.Service_Details} navigation={navigation} />
      <ScrollView>
        <View style={{backgroundColor: 'white'}}>
          <View style={styles.padding15}>
            <Text style={styles.serviceProvided}>{item.services.name}</Text>
          </View>
          <View>
            <Image style={styles.imageStyle} source={image} />
          </View>

          <View style={styles.padding15}>
            <Text style={styles.serviceProvided}>{Content.serProvided}</Text>
            <Text style={styles.description}>
              {item.services.description
                ? item.services.description
                : 'No description'}
            </Text>
          </View>

          <View
            style={[
              styles.rowCenter,
              {flexDirection: 'column', alignItems: 'flex-start'},
            ]}
          >
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.amount}>Rs. {item.services.rate}</Text>
              <Text style={styles.duration}>
                {'(' +
                  item.services.period +
                  ' ' +
                  item.services.periodUnit +
                  ')'}
              </Text>
            </View>

            <Text style={styles.amount}>
              Paid Amount. {item.services.rate * item.quantity}
            </Text>
            <Text style={styles.amount}>Qty. {item.quantity}</Text>
          </View>

          <View style={styles.padding15}>
            <Text style={styles.location}>{item.services.site.name}</Text>

            <View style={{flexDirection: 'row'}}>
              <Image
                style={{
                  tintColor: '#9092A5',
                  resizeMode: 'contain',
                  height: fontToDp(14),
                  width: fontToDp(12),
                  marginRight: 2,
                }}
                source={Images.homeLocation}
              />
              <Text style={styles.area}>
                {item.services.site.area +
                  ', ' +
                  item.services.site.city +
                  ', ' +
                  item.services.site.country}
              </Text>
            </View>
          </View>

          <View style={styles.dateContainer}>
            <View>
              <Text style={styles.dateStyle}>{Content.startDate}</Text>

              <View style={styles.startDate_View}>
                <Image
                  style={styles.startDate_Text_2_Image}
                  source={Images.calenderButton}
                />
                <Text style={styles.startDate_Text_2}>
                  {moment(item.enquiryDate).format('D MMM, YYYY')}
                </Text>
              </View>

              {/* <Text>{moment(item.enquiryDate).format('D MMM, YYYY')}</Text> */}
            </View>

            <Text style={styles.to}>{Content.to}</Text>

            <View>
              <Text style={styles.dateStyle}>{Content.endDate}</Text>

              <View style={styles.startDate_View}>
                <Image
                  style={styles.startDate_Text_2_Image}
                  source={Images.calenderButton}
                />
                <Text style={styles.startDate_Text_2}>
                  {moment(item.enquiryDate)
                    .add(item.services.period, item.services.periodUnit)
                    .format('D MMM, YYYY')}
                </Text>
              </View>

              {/* <Text>{moment(item.enquiryDate).add(item.services.period, item.services.periodUnit).format('D MMM, YYYY')}</Text> */}
            </View>
          </View>
          {/* bookServiceApi */}
          {membership == true && (
            <ButtonCommon
              text={'RENEW'}
              loading={loading}
              onPress={() => _onPaymentButtonPress()}
            />
          )}
        </View>
      </ScrollView>
    </Container>
  );
};

export default myServices;

const styles = StyleSheet.create({
  imageStyle: {height: 200, width: '100%'},
  serviceProvided: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    fontSize: fontsize.size_18,
  },
  description: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontsize.size_12,
  },
  rowCenter: {
    borderBottomWidth: 4,
    borderBottomColor: colors.background,
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
  },
  amount: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    fontSize: fontsize.size_18,
  },
  duration: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    marginLeft: 5,
    fontSize: fontsize.size_12,
  },
  location: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    fontSize: fontsize.size_18,
  },
  area: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontsize.size_12,
  },
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  dateStyle: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    fontSize: fontsize.size_18,
  },
  to: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontsize.size_13,
    alignSelf: 'flex-end',
    marginBottom: fontToDp(8),
  },
  padding15: {
    borderBottomWidth: 4,
    borderBottomColor: colors.background,
    padding: 15,
  },

  startDate_Text_1: {
    color: '#202B46',
    fontSize: fontToDp(12),
    fontFamily: PT_SANS_REGULAR,
    marginBottom: 5,
  },
  startDate_View: {
    flexDirection: 'row',
    backgroundColor: '#E5ECF9',
    width: 90,
    height: 'auto',
    padding: 4,
    marginVertical: 5,
  },
  startDate_Text_2_Image: {
    resizeMode: 'contain',
    height: 10,
    width: 10,
    alignSelf: 'center',
    margin: 2,
  },
  startDate_Text_2: {
    fontFamily: PT_SANS_REGULAR,
    color: '#202B46',
    fontSize: fontToDp(10),
  },
});
