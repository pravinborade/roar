import {StyleSheet} from 'react-native';
import {colors} from '../../../../styles';
import {PT_SANS_REGULAR} from '../../../../styles/typography';
import {fontToDp, heightToDp, widthToDp} from '../../../../utils/Responsive';

export const styles = StyleSheet.create({
  logo: {
    height: heightToDp('25%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },
  subHeading: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp('14'),
    color: '#202B46',
    textAlign: 'left',
  },
  TopContainer: {
    height: '100%',
    justifyContent: 'space-between',
  },
});
