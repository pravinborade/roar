import React from 'react';
import {Image, Text, View, ScrollView, Alert, BackHandler} from 'react-native';
import Container from '../../../../components/Container';
import {ScreenNames} from '../../../../utils/ScreenNames';
import LinearGradient from 'react-native-linear-gradient';
import Images from '../../../../assets/Images/Images';
import {colors} from '../../../../styles/colors';
import {Content} from '../../../../utils/content/Content';
import {HeaderBack} from '../../../../components';
import {styles} from './styles';
import {backHandler} from '../../../../commons/settingsBackHandler';
import {useDispatch} from 'react-redux';

import {useBackHandler} from '@react-native-community/hooks';

const AboutRoar = ({navigation}) => {
  const dispatch = useDispatch();

  const backActionHandler = () => {
    backHandler(dispatch, navigation);
    return true;
  };
  useBackHandler(backActionHandler);

  return (
    <Container>
      <HeaderBack
        title={ScreenNames.ABOUT_ROAR}
        onPress={() => backHandler(dispatch, navigation)}
      />
      <LinearGradient
        colors={[colors.lightGreen, colors.primaryGreen]}
        style={{flex: 1}}>
        <ScrollView>
          <View style={styles.TopContainer}>
            <Image style={styles.logo} source={Images.logo} />

            <View style={{marginHorizontal: 10, marginTop: 20}}>
              <Text style={styles.subHeading}>{Content.AboutRoar}</Text>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    </Container>
  );
};

export default AboutRoar;
