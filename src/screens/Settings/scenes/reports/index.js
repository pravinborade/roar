import React, {useState} from 'react';
import {ActivityIndicator, View} from 'react-native';
import {WebView} from 'react-native-webview';
import {Container, HeaderBack} from '../../../../components';
import {height, If, ScreenNames, width} from '../../../../utils';
import {REPORT_URL} from '../../../../config';

const Reports = ({navigation}) => {
  const [loading, setLoading] = useState(false);

  return (
    <Container>
      <HeaderBack title={ScreenNames.REPORTS} navigation={navigation} />
      <WebView
        source={{uri: REPORT_URL}}
        style={{height, width}}
        onLoadStart={() => setLoading(true)}
        onLoadEnd={() => setLoading(false)}
      />
      <If show={loading}>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <ActivityIndicator color="#009688" size="large" />
        </View>
      </If>
    </Container>
  );
};

export default Reports;
