import React, {Component, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import Images from '../../../../../assets/Images/Images';
import {colors, commonStyle} from '../../../../../styles';
import moment from 'moment';
// import {ifValid} from '../../../../../../../utils/helper';
import {ifValid} from '../../../../../utils/helper';
import {fontToDp, heightToDp, widthToDp} from '../../../../../utils/Responsive';
import {Content} from '../../../../../utils/content/Content';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import {PT_SANS_NARROW_BOLD} from '../../../../../styles/typography';

const myServiceCard = ({data, navigation, props}) => {
  let image =
    ifValid(data?.services?.image) && data?.services?.image !== ''
      ? {uri: data.services.image}
      : Images.myServices;

  useEffect(() => {}, []);

  return (
    <TouchableOpacity
      style={styles.containerMain}
      onPress={() =>
        navigation.navigate(ScreenNames.Service_Details, {
          item: data,
          props: props,
        })
      }>
      <View style={styles.container}>
        <ImageBackground
          style={styles.backgroundStyle}
          imageStyle={styles.border10}
          source={image}>
          <View style={styles.row}>
            <View style={styles.width60}>
              <Text style={styles.nameStyle}>{data?.services?.name}</Text>
            </View>

            <View style={styles.button}>
              <Text style={styles.rateStyle}>₹ {data?.services?.rate}</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    </TouchableOpacity>
  );
};

export default myServiceCard;
const styles = StyleSheet.create({
  containerMain: {alignSelf: 'center', padding: 10},
  width60: {width: '60%'},
  border10: {borderRadius: 10},
  container: {height: heightToDp('15%'), width: widthToDp('45%')},
  row: {flexDirection: 'row', position: 'absolute', bottom: 10, flex: 1},
  backgroundStyle: {height: '100%', width: '100%'},
  nameStyle: {
    color: 'white',
    marginLeft: 10,
    fontSize: fontToDp(14),
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  rateStyle: {
    fontSize: 12,
    fontWeight: 'bold',
    color: colors.primaryBlue,
  },
  button: {
    backgroundColor: colors.primaryGreen,
    // alignSelf:'flex-end',
    bottom: 5,
    marginRight: widthToDp(10),
    width: widthToDp(13),
    height: heightToDp(3),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
});
