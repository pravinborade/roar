import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Container from '../../../../components/Container';
import {colors} from '../../../../styles';
import MyServiceCard from './comp/myServiceCard';
import {getmyServices} from '../../../../redux/actions/myServices';
import {ifNotValid} from '../../../../utils/helper';
import {ScreenNames} from '../../../../utils/ScreenNames';
import {HeaderBack} from '../../../../components';
import {backHandler} from '../../../../commons/settingsBackHandler';
import {useBackHandler} from '@react-native-community/hooks';

const MyServices = ({navigation, title}) => {
  const dispatch = useDispatch();
  const [isRefreshing, setIsRefreshing] = useState(false);
  const _myServices = useSelector((state) => state.myServices.myServices);
  const hasMore = useSelector((state) => state.myServices.myServicesHasMore);
  const pageNo = useSelector((state) => state.myServices.pageNo);
  const loadingData = useSelector(
    (state) => state.myServices.loadingServicesData,
  );
  const userData = useSelector((state) => state.login.data);
  const [myServices, setmyServices] = useState([]);

  useEffect(() => {
    if (!ifNotValid(_myServices)) {
      setmyServices(_myServices);
    }
  }, [_myServices]);

  useEffect(() => {
    dispatch(getmyServices(userData.id, 0));
  }, []);

  const backActionHandler = () => {
    backHandler(dispatch, navigation);
    return true;
  };
  useBackHandler(backActionHandler);

  const BookingsPageIncrement = () => {
    let profileId = userData?.id;
    dispatch(getmyServices(profileId, pageNo + 1));
  };

  const handleLoadMore = () => {
    hasMore && BookingsPageIncrement();
  };

  const _onRefresh = () => {
    let profileId = userData?.id;
    dispatch(getmyServices(profileId, 0));
    setIsRefreshing(false);
  };

  //Rendering services cards with booking name
  const _renderServicesCards = ({item, index}) => {
    //console.log("***mapflatlist", item, index)
    return <MyServiceCard data={item} navigation={navigation} />;
  };

  const renderFooter = () => {
    return (
      <View style={styles.progress}>
        {loadingData && <ActivityIndicator color="#35B3FF" size="large" />}
      </View>
    );
  };

  const _renderServices = () => {
    return (
      <FlatList
        keyExtractor={(item) => item.id.toString()}
        data={myServices}
        numColumns={2}
        renderItem={_renderServicesCards}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        ListFooterComponent={renderFooter}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => _onRefresh()}
          />
        }
      />
    );
  };

  return (
    <Container>
      <View
        style={{
          borderBottomWidth: 3,
          borderBottomColor: colors.background,
        }}>
        <HeaderBack
          title={ScreenNames.MY_SERVICES}
          onPress={() => backHandler(dispatch, navigation)}
        />
      </View>
      {myServices && myServices.length != undefined && myServices.length > 0 ? (
        _renderServices()
      ) : (
        <View
          style={{
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>No Services</Text>
        </View>
      )}
    </Container>
  );
};

export default MyServices;
const styles = StyleSheet.create({
  progress: {
    flex: 1,
    height: 40,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

// import React, { Component } from 'react'
// import { View, Text, FlatList, Image, TouchableOpacity, RefreshControl } from 'react-native'
// import { connect, useSelector } from 'react-redux'
// import Images from '../../../../assets/Images/Images'
// import Container from '../../../../components/Container'
// import { colors } from '../../../../styles/colors'
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import httpService from '../../../../commons/httpServices';
// import ProfileSkelton from '../../../../components/SkeletonView'
// import MyServiceCard from './comp/myServiceCard'
// import { HeaderBack } from '../../../../components'
// import { ScreenNames } from '../../../../utils/ScreenNames'
// import { widthToDp } from '../../../../utils/Responsive'
// const services = [
//     {
//         img: Images.myServices,
//         name: 'Gym Trainer'
//     },
//     {
//         img: Images.myServices,
//         name: 'Hockey Coach'
//     },
//     {
//         img: Images.myServices,
//         name: 'Swimming Coach'
//     },
//     {
//         img: Images.myServices,
//         name: 'Cricket Coach'
//     },
//     {
//         img: Images.myServices,
//         name: 'Football Coach'
//     },
// ]
// class MyServices extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             orginalServices: [],
//             services: [],
//             pageNo: 0,
//             pageSize: 10,
//             loadingMoreData: false,
//             userDetails: null,
//             noData: false,
//             refreshing: false,
//             loader: false,
//         }
//     }
//     async componentDidMount() {

//         await AsyncStorage.getItem('userDetails').then((details) => {
//             if (details) {
//                 details = JSON.parse(details)
//                 this.setState({ userDetails: details }, () => this._fetchBookings(details))

//             }
//         })
//         console.log('###PropsData', this.props.enum);
//     }

//     _fetchBookings() {
//         this.setState({ loader: true });
//         let url = `api/services?userId=${this.state.userDetails.id}&pageNo=${this.state.pageNo}&pageSize=${this.state.pageSize}&access_token=${this.props.token}`;
//         console.log("@@", url)
//         httpService.get(url, this.props).then((response) => {
//             console.log('@@_fetchservices', response);
//             if (response && response.status === 200 && response.data[0].length > 0) {

//                 this.setState({ orginalServices: [...this.state.orginalServices, ...response.data[0].content] }, () => this._pagingation());
//                 this.setState({ loader: false });
//             } else {
//                 this.setState({ loader: false });
//                 this.setState({ noData: true })
//             }
//         });
//     }

//     //used customize pagination to reduce the load from the app
//     _pagingation() {
//         let remainRecords = this.state.orginalServices.length // calculating remaining count of the records
//         if (remainRecords > 0) { //if there are remaing records of this.state.orginalServices then using intenal pagination
//             let spliceNoOfRecord = remainRecords >= 7 ? 7 : remainRecords; //showing only 5 records at a time
//             let services = this.state.orginalServices.splice(0, spliceNoOfRecord); // coping records from orginalServices
//             this.setState({
//                 services: [...this.state.services, ...services],
//                 loadingMoreData: false
//             })
//         } else { //fetching the next page records from the API
//             this.setState({ pageNo: this.state.pageNo + 1 }, () => this._fetchBookings())
//         }
//     }

//     //When reach to screen bottom on scroll
//     _onEndReached() {
//         console.log('_onEndReached::', !this.state.loadingMoreData, this.state.noData);
//         if (!this.state.loadingMoreData && !this.state.noData) {
//             this.setState({ loadingMoreData: true }, () => this._pagingation())
//         }
//     };

//     //Rendering booking using FlatList
//     _renderBookings() {
//         return <FlatList
//             data={this.state.services}
//             numColumns={2}
//             renderItem={(item, index) => this._renderBookingCards(item, index)}
//             keyExtractor={(item, index) => index.toString()}
//             onEndReachedThreshold={0.5}
//             onMomentumScrollBegin={() => { }}
//             onMomentumScrollEnd={() => this._onEndReached()}
//             progressViewOffset={10}
//         />
//     }

//     //Rendering booking cards with booking name
//     _renderBookingCards({ item, index }) {
//         return <MyServiceCard
//             data={item}
//             nav={this.props} />
//     }

//     render() {
//         return (
//             <Container>
//                 <View style={{
//                     borderBottomWidth: 3,
//                     borderBottomColor: colors.background
//                 }}>
//                     <HeaderBack title={ScreenNames.MY_SERVICES} navigation={this.props.navigation} />
//                 </View>
//                 {
//                     this.state.loader === true ? <ProfileSkelton /> :
//                         this.state.services.length <= 0 ?
//                             <View style={{ height: '100%', justifyContent: 'center', alignItems: 'center' }}>
//                                 <Text>
//                                     No Services
//                                  </Text>
//                             </View>
//                             :
//                             this.state.services.length > 0 ? this._renderBookings() : this.state.noData ? null : null
//                 }
//                 {/* {this._renderBookings()} */}
//             </Container>

//         );
//     }
// }
// const mapStateToProps = (state) => {
//     return {
//         token: state.login.userToken
//     }
// };

// export default connect(mapStateToProps)(MyServices);
