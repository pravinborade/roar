import React from 'react';
import AfterLogin from './index.afterLogin';
import {useSelector} from 'react-redux';

const Profile = ({navigation}) => {
  const userToken = useSelector((state) => state.login.userToken);
  return userToken ? <AfterLogin navigation={navigation} /> : null;
};
export default Profile;
