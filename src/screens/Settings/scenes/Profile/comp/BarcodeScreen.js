import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Content, widthToDp} from '../../../../../utils';
import {
  Container,
  HeaderBack,
  LoaderAndMsg,
  NotAvailable,
} from '../../../../../components';
import {colors} from '../../../../../styles';

const BarcodeScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {barcodeUrl, loading} = useSelector((state) => state.barcode);

  return (
    <Container>
      <HeaderBack title={'BARCODE'} navigation={navigation} />
      <View style={styles.viewContainer}>
        {loading ? (
          <LoaderAndMsg text={Content.loadingBarcodeWait} />
        ) : barcodeUrl && barcodeUrl !== '' ? (
          <Image source={{uri: barcodeUrl}} style={styles.barcodeImage} />
        ) : (
          <NotAvailable msg={Content.noBarcodeFound} />
        )}
      </View>
    </Container>
  );
};
export default BarcodeScreen;

const styles = StyleSheet.create({
  container: {},
  viewContainer: {
    backgroundColor: colors.background,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  barcodeImage: {
    marginVertical: widthToDp(1),
    alignSelf: 'center',
    width: widthToDp(80),
    height: widthToDp(65),
  },
});
