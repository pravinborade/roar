import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useState} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Images from '../../../../assets/Images/Images';
import {ButtonCommon, Container, HeaderBack} from '../../../../components';
import {colors} from '../../../../styles';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../styles/typography';
import {Content, fontToDp, heightToDp, ScreenNames} from '../../../../utils';
import {CommonActions} from '@react-navigation/routers';
import {clearMyBookings} from '../../../../redux/actions/myBookings';
import {clearLoginData, deleteFcmToken} from '../../../../redux/actions/login';
import {backHandler} from '../../../../commons/settingsBackHandler';
import {useBackHandler} from '@react-native-community/hooks';
import {getBarcodeImage} from './action';
import {ifValid} from '../../../../utils/helper';
import {errorLog} from '../../../../utils/fireLog';

const Profile = ({navigation}) => {
  const [imgSrc, setimgSrc] = useState(null);
  const dispatch = useDispatch();
  const userDetails = useSelector((state) => state.login.data);

  const backActionHandler = () => {
    backHandler(dispatch, navigation);
    return true;
  };
  useBackHandler(backActionHandler);

  const signOut = async () => {
    try {
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      let token = userDetails.token;

      await AsyncStorage.removeItem('userToken');
      await AsyncStorage.removeItem('userDetails');
      await AsyncStorage.removeItem('fcmToken');

      dispatch({type: 'USER_LOGOUT'});
      dispatch(clearMyBookings());
      dispatch(clearLoginData());
      dispatch(deleteFcmToken(fcmToken, token));

      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: ScreenNames.HOME}],
        }),
      );
    } catch (e) {
      errorLog(e);
    }
  };

  return (
    <Container>
      <View style={styles.mainContainer}>
        <HeaderBack
          onPressBarcode={() => {
            if (ifValid(userDetails?.id)) {
              dispatch(getBarcodeImage(userDetails.id));
              navigation.navigate(ScreenNames.BARCODE_SCREEN);
            }
          }}
          title={ScreenNames.MY_PROFILE}
          onPress={() => backHandler(dispatch, navigation)}
          onPressEdit={() => navigation.navigate(ScreenNames.PROFILE_EDIT)}
        />

        <View style={styles.subContainer}>
          <View>
            <Image
              resizeMode="contain"
              style={styles.userAvatar}
              source={
                userDetails?.image
                  ? {uri: userDetails?.image}
                  : Images.User_avatar
              }
            />
          </View>
          <View style={{marginTop: 5}}>
            <Text style={styles.headingName}>
              {(userDetails?.firstName ? userDetails?.firstName : '') +
                ' ' +
                (userDetails?.lastName ? userDetails?.lastName : '')}
            </Text>
          </View>
        </View>
        <ScrollView style={{backgroundColor: 'white'}}>
          <View style={styles.viewContainer}>
            <Text style={styles.textOne}>{Content.phNo}</Text>
            <Text style={styles.textTwo}>{'+91 ' + userDetails?.phoneNo}</Text>
          </View>
          <View style={styles.viewContainer}>
            <Text style={styles.textOne}>{Content.email}</Text>
            <Text style={styles.textTwo}>{userDetails?.email}</Text>
          </View>
          <View style={styles.viewContainer}>
            <Text style={styles.textOne}>{Content.address}</Text>
            <Text style={styles.textTwo}>{userDetails.address}</Text>
          </View>

          <ButtonCommon
            onPress={() => navigation.navigate(ScreenNames.RESET)}
            text={Content.changePassword}
            textStyle={{fontSize: fontToDp(14)}}
          />
          <ButtonCommon
            onPress={signOut}
            text={Content.logout}
            textStyle={{fontSize: fontToDp(14)}}
            style={styles.logoutButton}
          />
        </ScrollView>
      </View>
    </Container>
  );
};

export default Profile;

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: colors.background,
    flex: 1,
  },
  headingName: {
    color: '#FFFFFF',
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(18),
  },
  subContainer: {
    backgroundColor: '#202B46',
    height: heightToDp('20%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  userAvatar: {
    resizeMode: 'cover',
    borderRadius: fontToDp(70 / 2),
    height: fontToDp(70),
    width: fontToDp(70),
  },
  viewContainer: {
    backgroundColor: '#FFFFFF',
    padding: 12,
    borderBottomWidth: 3,
    borderBottomColor: colors.background,
  },
  textOne: {
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(16),
    marginBottom: 5,
  },
  textTwo: {
    color: '#505C74',
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(12),
  },
  logoutButton: {
    backgroundColor: colors.background,
    borderColor: '#202B46',
    borderWidth: 1,
    marginTop: 0,
  },
});
