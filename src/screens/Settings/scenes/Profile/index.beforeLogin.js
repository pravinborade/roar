import React from 'react';
import {HeaderBack} from '../../../../components';
import {ScreenNames} from '../../../../utils';
import Initial from '../../../Auth/Initial';

const Profile = ({navigation}) => {
  return (
    <>
      <HeaderBack title={ScreenNames.MY_PROFILE} navigation={navigation} />
      <Initial />
    </>
  );
};

export default Profile;
