import axiosService from '../../../../commons/axiosService';
import {errorLog, successLog} from '../../../../utils/fireLog';
import {ifNotValid, ifValid} from '../../../../utils/helper';

export const getBarcodeImage = (userID) => {
  try {
    if (ifNotValid(userID)) {
      errorLog('userId is invalid');
      return;
    }
    return (dispatch) => {
      dispatch({type: 'BARCODE_LOADING', payload: true});
      axiosService
        .get(`api/memberships/barcode/user/${userID}`)
        .then(async (response) => {
          successLog(
            `api/memberships/barcode/user/${userID}`,
            response?.status,
          );
          if (response?.status === 200) {
            let resObj =
              ifValid(response?.data?.data) && ifValid(response?.data?.data[0])
                ? response.data?.data[0]
                : {};
            if (ifValid(resObj?.barcodeUrl)) {
              dispatch({type: 'BARCODE', payload: resObj.barcodeUrl});
            }
          } else {
            dispatch({type: 'BARCODE', payload: ''});
          }
        })
        .catch((o) => {
          dispatch({type: 'BARCODE_LOADING', payload: false});
        });
    };
  } catch (e) {
    errorLog(e);
  }
};

export const getAndSetToken = () => {
  return async (dispatch) => {};
};
