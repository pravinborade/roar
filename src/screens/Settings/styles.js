import {StyleSheet} from 'react-native';
import {colors} from '../../styles';
import {PT_SANS_REGULAR} from '../../styles/typography';
import {fontToDp, heightToDp, widthToDp} from '../../utils/Responsive';

export const styles = StyleSheet.create({
  sub_item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderBottomWidth: 3,
    borderBottomColor: colors.background,
  },
  itemLogo: {
    width: widthToDp('5%'),
    height: heightToDp('5%'),
    resizeMode: 'contain',
  },
  arrowRight: {
    height: 10,
    width: 10,
  },
  headerContainer: {
    borderBottomWidth: 3,
    borderBottomColor: colors.background,
  },
  appVersion: {
    alignSelf: 'center',
    margin: 10,
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontToDp(12),
  },
  title: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(14),
    color: colors.primaryGrey,
    marginLeft: 10,
  },
});
