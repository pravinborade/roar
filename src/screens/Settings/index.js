import React, {useEffect, useState} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import Container from '../../components/Container';
import {ScreenNames} from '../../utils/ScreenNames';
import Images from '../../assets/Images/Images';
import {HeaderBack} from '../../components';
import {DATA} from './utils/SettingsIndex';
import {Content} from '../../utils/content/Content';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {styles} from './styles';
import {useDispatch, useSelector} from 'react-redux';
import {setFromPayment} from '../../redux/actions/login';
import {fromSettingsScreen} from '../../redux/actions/screenState';
import {colors} from '../../styles/colors';

const Settings = ({navigation}) => {
  const [userDetails, setuserDetails] = useState({});
  const [filteredData, setfilteredData] = useState([]);
  const userToken = useSelector((state) => state.login.userToken);

  const dispatch = useDispatch();

  useEffect(() => {
    AsyncStorage.getItem('userDetails').then((details) => {
      if (details) {
        let tempfilteredData = [];
        details = JSON.parse(details);
        setuserDetails(details);
        if (details.role.name === 'CUSTOMER') {
          tempfilteredData = DATA.filter(function (e) {
            return e.title != Content.Reports;
          });
        } else {
          tempfilteredData = DATA;
        }
        setfilteredData(tempfilteredData);
      } else {
        let tempfilteredData = [];
        tempfilteredData = DATA.filter(function (e) {
          return e.title != Content.Reports;
        });
        setfilteredData(tempfilteredData);
      }
    });
  }, []);

  const handleLoginButton = (navigationPage, navigationScreen) => {
    // console.log("--navigation page", navigationPage);
    // console.log("--navigation screen", navigationScreen)
    dispatch(fromSettingsScreen(true));
    dispatch(setFromPayment(navigationPage));
    navigation.navigate(navigationScreen);
  };

  const Item = ({title, image, navigationPage}) => {
    let navigationScreen = navigationPage;
    if (
      title === Content.MyProfile ||
      title === Content.MyServices ||
      title === Content.MyBookings ||
      title === Content.MyEvents ||
      title === Content.MyMembership
    ) {
      if (userToken) {
        navigationScreen = navigationPage;
      } else {
        //navigationScreen = ScreenNames.INITIAL
        navigationScreen = ScreenNames.LOGIN;
      }
    }
    //dispatch(setFromPayment(navigationPage))
    return (
      <TouchableOpacity
        onPress={() => handleLoginButton(navigationPage, navigationScreen)}>
        <View style={styles.item}>
          <View style={styles.sub_item}>
            <Image style={styles.itemLogo} source={image} />
            <Text style={styles.title}>{title}</Text>
          </View>
          <View>
            <Image
              source={Images.arrowRight}
              style={styles.arrowRight}
              resizeMode={'contain'}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const renderItem = ({item}) => (
    <Item
      image={item.image}
      title={item.title}
      navigationPage={item.navigationPage}
    />
  );
  return (
    <Container containerStyle={{backgroundColor: colors.background}}>
      <View style={styles.headerContainer}>
        <HeaderBack mainHeader title={ScreenNames.SETTINGS} />
      </View>
      <FlatList
        data={filteredData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
      <Text style={styles.appVersion}>{Content.AppVersion}</Text>
    </Container>
  );
};

export default Settings;
