import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {colors} from '../../../../styles/colors';
import {PT_SANS_NARROW_BOLD} from '../../../../styles/typography';
import {Content} from '../../../../utils/content/Content';
import {fontToDp, heightToDp, widthToDp} from '../../../../utils/Responsive';
import {ScreenNames} from '../../../../utils/ScreenNames';

const InitialButton = ({navigation, type}) => {
  let content = type === ScreenNames.LOGIN ? Content.Login : Content.SignUp;
  let bgColor = type === ScreenNames.LOGIN ? null : colors.primaryBlue;
  let fontColor =
    type === ScreenNames.LOGIN ? colors.primaryBlue : colors.white;

  return (
    <TouchableOpacity
      style={{...styles.containerStyle, backgroundColor: bgColor}}
      onPress={() => navigation.navigate(type)}>
      <Text style={{...styles.textStyle, color: fontColor}}>{content}</Text>
    </TouchableOpacity>
  );
};

export default InitialButton;

const styles = StyleSheet.create({
  containerStyle: {
    borderColor: colors.primaryBlue,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    height: heightToDp('6%'),
    width: widthToDp('45%'),
  },
  textStyle: {
    fontSize: fontToDp(14),
    fontFamily: PT_SANS_NARROW_BOLD,
  },
});
