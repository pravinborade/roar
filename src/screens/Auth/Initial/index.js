import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Container from '../../../components/Container';
import {ScreenNames} from '../../../utils/ScreenNames';
import LinearGradient from 'react-native-linear-gradient';
import Images from '../../../assets/Images/Images';
import {fontToDp, heightToDp} from '../../../utils/Responsive';
import InitialButton from './comp/InitialButton';
import {colors} from '../../../styles/colors';
import {Content} from '../../../utils/content/Content';
import {PT_SANS_BOLD, PT_SANS_REGULAR} from '../../../styles/typography';
import {ifValid} from '../../../utils/helper';

const Initial = ({navigation}) => {
  return (
    <Container>
      <LinearGradient
        colors={[colors.lightGreen, colors.primaryGreen]}
        style={{flex: 1}}>
        <View style={styles.TopContainer}>
          <Image style={styles.logo} source={Images.logo} />

          <View style={styles.middleContainer}>
            <Text style={styles.heading}> {Content.Initial_Heading} </Text>
            <Text style={styles.subHeading}>
              {' '}
              {Content.Initial_SubHeading}{' '}
            </Text>
          </View>

          <View style={styles.bottomContainer}>
            <View style={styles.buttonContainer}>
              <InitialButton navigation={navigation} type={ScreenNames.LOGIN} />
              <InitialButton
                navigation={navigation}
                type={ScreenNames.SIGNUP}
              />
            </View>
            <Text style={styles.terms}>{Content.TermsCond}</Text>
          </View>
        </View>
      </LinearGradient>
    </Container>
  );
};

export default Initial;

const styles = StyleSheet.create({
  logo: {
    height: heightToDp('25%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },

  heading: {
    fontFamily: PT_SANS_BOLD,
    fontSize: fontToDp(21),
    padding: fontToDp(5),
    lineHeight: fontToDp(25),
    color: '#202B46',
    textAlign: 'center',
  },

  subHeading: {
    padding: fontToDp(5),
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(14),
    color: '#505C74',
    textAlign: 'center',
  },
  terms: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(10),
    color: '#505C74',
    textAlign: 'center',
    margin: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  TopContainer: {
    height: '100%',
    justifyContent: 'space-between',
  },
  middleContainer: {
    height: '18%',
    justifyContent: 'space-between',
  },
  bottomContainer: {
    height: heightToDp('20%'),
    justifyContent: 'space-evenly',
    marginBottom: '5%',
  },
});
