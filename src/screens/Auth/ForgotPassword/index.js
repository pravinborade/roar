import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Platform,
} from 'react-native';
import Container from '../../../components/Container';
import {ScreenNames} from '../../../utils/ScreenNames';
import Images from '../../../assets/Images/Images';
import {fontToDp, heightToDp, widthToDp} from '../../../utils/Responsive';
import {
  PT_SANS_BOLD,
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../styles/typography';
import {colors} from '../../../styles/colors';
import {Content} from '../../../utils/content/Content';
import httpService from '../../../commons/httpServices';
import {showToastMsg} from '../../../utils/helper';
var validate = require('validate.js');
import {ButtonCommon, HeaderBack} from '../../../components';
import {commonStyle} from '../../../styles';

const ForgotPassword = ({navigation}) => {
  const [email, setemail] = useState('');
  const [loading, setloading] = useState(false);
  const [emailError, setemailError] = useState(false);

  const validation = () => {
    let constraints = {
      email: {
        presence: true,
        format: {
          pattern: /\S+@\S+\.\S+/,
          message: 'Please enter valid Email',
        },
      },
    };

    let emailCheck = validate.isEmpty(email);
    emailCheck = validate({email: email.trim() + ''}, constraints);
    emailCheck = emailCheck && emailCheck.email ? true : false;

    setemailError(emailCheck);

    if (emailCheck) {
      return false;
    } else {
      return true;
    }
  };

  const onSend = () => {
    if (validation()) {
      setloading(true);
      httpService
        .get(`login/sendForgetPasswordLink?userEmail=${email.toLowerCase()}`)
        .then((response) => {
          if (response.status === 200) {
            showToastMsg('Success', response.statusMessage, 'success');
            setloading(false);
            navigation.pop();
            setemailError(false);
            //helper.successToastWithRoute(response.statusMessage,scope,'Login');
          } else {
            setloading(false);
            showToastMsg('Error', response.statusMessage);
            //helper.errorToast(response.statusMessage);
          }
        });
      console.log('Hello ', email);
    }
  };

  return (
    <Container>
      <HeaderBack
        style={{backgroundColor: colors.background}}
        title={ScreenNames.FORGOT_PASSWORD}
        navigation={navigation}
      />
      <View style={styles.TopContainer}>
        <Image style={styles.logo} source={Images.loginLogo} />

        <View style={styles.inputView}>
          <TextInput
            autoCorrect={false}
            autoComplete={false}
            returnKeyType="done"
            keyboardType="default"
            onChangeText={(val) => setemail(val.trim())}
            placeholderTextColor={'#BFC7D8'}
            value={email}
            placeholder="Email"
            multiline={
              Platform.OS == 'ios' ? false : email.length ? false : true
            }
            style={{
              ...styles.input,
              backgroundColor: email.length > 0 ? 'white' : null,
              borderBottomColor: email.length ? colors.primaryGreen : '#BFC7D8',
            }}
          />
          {emailError && (
            <Text
              style={{
                color: '#FF0033',
                textAlign: 'center',
                marginHorizontal: '5%',
              }}
            >
              *Please enter valid email
            </Text>
          )}
        </View>

        <ButtonCommon
          style={{
            ...styles.f,
            backgroundColor: email.length ? colors.primaryGreen : '#BFC7D8',
          }}
          onPress={onSend}
          textStyle={{
            fontFamily: PT_SANS_NARROW_BOLD,
            fontSize: fontToDp(14),
            color: email.length ? 'black' : colors.white,
          }}
          loading={loading}
          text={Content.sendLink}
        />

        {/* <TouchableOpacity onPress={onSend} style={styles.loginButton}>
                    <Text style={{
                        color: '#FFFFFF',
                        fontFamily: PT_SANS_NARROW_BOLD,
                    }}>{Content.sendLink}</Text>
                </TouchableOpacity> */}
        {/* <View style={styles.bottomContainer}>
                    <Text style={styles.newUser}>{Content.New_User}</Text>
                    <TouchableOpacity onPress={() => navigation.navigate(ScreenNames.SIGNUP)}>
                        <Text style={styles.registerHere}>{Content.register_her}</Text>
                    </TouchableOpacity>
                </View> */}
      </View>
    </Container>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  googleButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    height: heightToDp('6%'),
    marginHorizontal: '30%',
    opacity: 1,
    borderRadius: 4,
  },

  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#BFC7D8',
    height: heightToDp('6%'),
    marginHorizontal: '5%',
    opacity: 1,
    borderRadius: 4,
    marginBottom: '60%',
  },

  inputView: {
    marginTop: widthToDp(15),
    marginBottom: widthToDp(10),
  },
  input: {
    textAlign: 'center',
    fontFamily: PT_SANS_NARROW_BOLD,
    color: colors.primaryBlue,
    marginHorizontal: '5%',
    height: 49,
    fontSize: fontToDp(18),
    borderBottomColor: '#BFC7D8',
    borderBottomWidth: 1.5,
  },

  logo: {
    height: heightToDp('10%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },

  heading: {
    fontFamily: PT_SANS_BOLD,
    fontSize: widthToDp('7%'),
    lineHeight: 25,
    color: '#202B46',
    textAlign: 'center',
  },

  forgot_pw: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: widthToDp('3%'),
    color: colors.darkGreen,
    textAlign: 'center',
  },

  connect: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: widthToDp('4%'),
    color: '#505C74',
    textAlign: 'center',
  },
  newUser: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: widthToDp('4%'),
    color: colors.primaryBlue,
    textAlign: 'center',
    marginRight: 4,
  },
  registerHere: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: widthToDp('4%'),
    color: colors.darkGreen,
    textAlign: 'center',
  },
  TopContainer: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%',
  },
  middleContainer: {
    height: '12%',
    justifyContent: 'space-between',
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '15%',
  },
});
