import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView,
} from 'react-native';
import Container from '../../../components/Container';
import {ScreenNames} from '../../../utils/ScreenNames';
import Images from '../../../assets/Images/Images';
import {fontToDp, heightToDp} from '../../../utils/Responsive';
import {colors} from '../../../styles/colors';
import {Content} from '../../../utils/content/Content';
import {PT_SANS_NARROW_BOLD} from '../../../styles/typography';
import Toast from 'react-native-toast-message';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import httpService from '../../../commons/httpServices';
import {ButtonCommon, HeaderBack} from '../../../components';
import {showToastMsg} from '../../../utils/helper';

var validate = require('validate.js');
let user = {};

class Reset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      user: null,
      currentPasswordError: false,
      newPasswordError: false,
      confirmPasswordError: false,
      newPasswordLengthError: false,
      oldPasswordError: false,
    };
  }

  async componentWillMount() {
    await AsyncStorage.getItem('userDetails').then((details) => {
      if (details) {
        user = JSON.parse(details);
      } else {
        this.props.navigation.navigate('Login');
      }
    });
  }

  validation() {
    //validating the data
    var constraints = {
      newPassword: {
        equality: {
          attribute: 'confirmPassword',
          message: 'is not equal',
          comparator: function (v1, v2) {
            return JSON.stringify(v1) === JSON.stringify(v2);
          },
        },
      },
      password: {
        presence: true,
        length: {
          minimum: 6,
          message: 'must be at least 6 characters',
        },
      },
    };

    const oldPasswordError = validate.isEmpty(this.state.oldPassword);
    const newPasswordError = validate.isEmpty(this.state.newPassword);
    const confirmPasswordError = validate.isEmpty(this.state.confirmPassword);
    // var confirmPasswordError = validate({ newPassword: this.state.newPassword, confirmPassword: this.state.confirmPassword }, constraints);
    // confirmPasswordError = confirmPasswordError && confirmPasswordError.newPassword ? true : false;
    var newPasswordLengthError = validate(
      {password: this.state.newPassword + ''},
      constraints,
    );
    newPasswordLengthError =
      newPasswordLengthError && newPasswordLengthError.password ? true : false;

    this.setState({
      oldPasswordError,
      newPasswordError,
      confirmPasswordError,
      newPasswordLengthError,
    });
    // if (newPasswordLengthError) {
    //     Toast.show({
    //         type: 'error',
    //         text1: 'Error',
    //         text2: 'New password length must be 6 digit.'
    //     });
    //     //helper.errorToast("New password length must be 6 digit.");
    // }
    if (this.state.newPassword !== this.state.confirmPassword) {
      return false;
    }

    if (
      oldPasswordError ||
      newPasswordError ||
      confirmPasswordError ||
      newPasswordLengthError
    ) {
      return false;
    } else {
      return true;
    }
  }

  newPasswordErrorfun() {
    if (this.state.newPasswordError == true) {
      return (
        <Text
          style={{
            color: '#FF0033',
            textAlign: 'center',
            marginHorizontal: '5%',
            fontSize: fontToDp(11),
          }}
        >
          *Please Enter Password
        </Text>
      );
    } else if (this.state.newPasswordLengthError == true) {
      return (
        <Text
          style={{
            color: '#FF0033',
            textAlign: 'center',
            marginHorizontal: '5%',
            fontSize: fontToDp(11),
          }}
        >
          *Password at must be 6 character
        </Text>
      );
    }
  }

  confirmPasswordErrorfun() {
    if (this.state.confirmPasswordError == true) {
      return (
        <Text
          style={{
            color: '#FF0033',
            textAlign: 'center',
            marginHorizontal: '5%',
            fontSize: fontToDp(11),
          }}
        >
          *Please Enter Password
        </Text>
      );
    } else if (
      this.state.confirmPassword != this.state.newPassword &&
      this.state.confirmPassword.length > 0
    ) {
      return (
        <Text
          style={{
            color: '#FF0033',
            textAlign: 'center',
            marginHorizontal: '5%',
            fontSize: fontToDp(11),
          }}
        >
          *Password doesn't match with new password
        </Text>
      );
    }
  }

  resetPassword = () => {
    var scope = this;
    if (this.validation()) {
      var url = `api/users/changePassword?access_token=${user.token}`;
      var requestData = {
        id: user.id,
        oldPassword: this.state.oldPassword,
        newPassword: this.state.newPassword,
      };
      console.log('##requestData', requestData);

      httpService
        .post(url, requestData, this.props)
        .then((response) => {
          console.log('###response', response);
          if (response.status === 200) {
            //this.props.dispatch({ type: 'RETRIEVE_TOKEN', payload: null });
            AsyncStorage.removeItem('userToken');
            AsyncStorage.removeItem('userDetails');
            console.log('PASSWORD----------->', response.statusMessage);
            if (
              response.statusMessage ==
              'Your password has been changed successfully'
            ) {
              this.setState({oldPassword: ''});
              this.setState({newPassword: ''});
              this.setState({confirmPassword: ''});
            }
            Toast.show({
              type: 'success',
              text1: 'Password changed Successfully',
              text2: response.statusMessage,
            });
            this.props.navigation.navigate(ScreenNames.MY_PROFILE);

            // helper.successToastWithRoute(response.statusMessage, this, 'Login');
          } else {
            showToastMsg('Error', response.statusMessage);
          }
        })
        .catch((error) => {
          console.log('error', error);
          // Toast.show({
          //     type: 'error',
          //     text1: 'Error',
          //     text2: error
          // });
        });
    }
  };

  render() {
    return (
      <Container>
        <HeaderBack
          title={'Reset Password'}
          style={{backgroundColor: colors.background}}
          navigation={this?.props?.navigation}
        />
        <KeyboardAvoidingView style={{flex: 1}} behavior="padding">
          <View style={styles.TopContainer}>
            <Image style={styles.logo} source={Images.loginLogo} />
            <View style={styles.inputView}>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                placeholder="Old Password"
                onChangeText={(val) => this.setState({oldPassword: val})}
                placeholderTextColor={'#BFC7D8'}
                style={styles.input}
              />
              {this.state.oldPasswordError && (
                <Text
                  style={{
                    color: '#FF0033',
                    textAlign: 'center',
                    marginHorizontal: '5%',
                    fontSize: fontToDp(11),
                  }}
                >
                  *Please Enter Password
                </Text>
              )}
            </View>
            <View style={styles.inputView}>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                placeholder="New Password"
                onChangeText={(val) => this.setState({newPassword: val})}
                placeholderTextColor={'#BFC7D8'}
                style={styles.input}
              />
              {this.newPasswordErrorfun()}
            </View>
            <View style={styles.inputView}>
              <TextInput
                autoCorrect={false}
                autoComplete={false}
                returnKeyType="done"
                keyboardType="default"
                placeholder="Confirm Password"
                onChangeText={(val) => this.setState({confirmPassword: val})}
                placeholderTextColor={'#BFC7D8'}
                style={styles.input}
              />
              {this.confirmPasswordErrorfun()}
            </View>

            {/* <TouchableOpacity onPress={this.resetPassword} style={styles.loginButton}>
                        <Text style={{
                            color: '#FFFFFF',
                            fontFamily: PT_SANS_NARROW_BOLD,
                        }}>{Content.resettt}</Text>
                    </TouchableOpacity> */}

            <ButtonCommon
              disabled={
                this.state.oldPassword.length &&
                this.state.newPassword.length &&
                this.state.confirmPassword.length &&
                this.state.newPassword === this.state.confirmPassword
                  ? false
                  : true
              }
              style={{
                ...styles.loginButton,
                backgroundColor:
                  this.state.oldPassword.length &&
                  this.state.newPassword.length &&
                  this.state.confirmPassword.length &&
                  this.state.newPassword === this.state.confirmPassword
                    ? colors.primaryGreen
                    : '#BFC7D8',
              }}
              onPress={this.resetPassword}
              textStyle={{
                ...styles.loginText,
                color:
                  this.state.oldPassword.length &&
                  this.state.newPassword.length &&
                  this.state.confirmPassword.length &&
                  this.state.newPassword === this.state.confirmPassword
                    ? 'black'
                    : colors.white,
              }}
              //loading={loading}
              text={Content.resettt}
            />

            {/* <View style={styles.bottomContainer}>
                        <Text style={styles.newUser}>{Content.New_User}</Text>
                        <TouchableOpacity onPress={() => navigation.navigate(ScreenNames.SIGNUP)}>
                            <Text style={styles.registerHere}>{Content.register_her}</Text>
                        </TouchableOpacity>
                    </View> */}
          </View>
        </KeyboardAvoidingView>
      </Container>
    );
  }
}

export default connect()(Reset);

const styles = StyleSheet.create({
  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#BFC7D8',
    height: heightToDp('6%'),
    marginHorizontal: '5%',
    opacity: 1,
    borderRadius: 4,
  },
  loginText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
  },
  input: {
    textAlign: 'center',
    fontFamily: PT_SANS_NARROW_BOLD,
    color: colors.primaryBlue,
    marginHorizontal: '5%',
    fontSize: fontToDp(18),
    borderBottomColor: '#BFC7D8',
    borderBottomWidth: 1.5,
  },

  logo: {
    height: heightToDp('10%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },

  newUser: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
    color: colors.primaryBlue,
    textAlign: 'center',
    marginRight: 4,
  },
  registerHere: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
    color: colors.darkGreen,
    textAlign: 'center',
  },
  TopContainer: {
    backgroundColor: colors.background,
    flex: 1,
    justifyContent: 'space-evenly',
  },
  // bottomContainer: {
  //     flexDirection: 'row',
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     marginBottom: '5%'
  // }
});
