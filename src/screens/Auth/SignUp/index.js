import React, {Component} from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Images from '../../../assets/Images/Images';
import {Content, fontToDp, ValidationMsg} from '../../../utils';
import {colors} from '../../../styles';
import httpService from '../../../commons/httpServices';
import {ButtonCommon, Container, HeaderBack} from '../../../components';
import {PT_SANS_NARROW_BOLD} from '../../../styles/typography';
import {connect} from 'react-redux';
import {styles} from './signup.styles';
import Toast from 'react-native-toast-message';

var validate = require('validate.js');

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      phoneNo: '',
      firstnameError: false,
      lastnameError: false,
      emailError: false,
      phoneNoError: false,
      phoneEmptyError: false,
      loading: false,
    };
  }

  validation() {
    var constraints = {
      phone: {
        length: {is: 10},
      },
      name: {
        format: {
          pattern: '[a-zA-Z]+',
          flags: 'i',
          message: ValidationMsg.canContainAZAN09,
        },
        length: {minimum: 3},
      },
    };
    const emailError =
      validate.single(this.state.email, {presence: true, email: true}) !==
      undefined;

    var firstnameError = validate.isEmpty(this.state.firstname);
    firstnameError = validate({name: this.state.firstname + ''}, constraints);
    firstnameError = firstnameError && firstnameError.name ? true : false;

    var lastnameError = validate.isEmpty(this.state.lastname);
    lastnameError = validate({name: this.state.lastname + ''}, constraints);
    lastnameError = lastnameError && lastnameError.name ? true : false;

    const phoneEmptyError = validate.isEmpty(this.state.phoneNo);
    var phoneNoError = validate({phone: this.state.phoneNo}, constraints);
    phoneNoError = phoneNoError && phoneNoError.phone ? true : false;
    this.setState({
      emailError,
      firstnameError,
      lastnameError,
      phoneNoError,
      phoneEmptyError,
    });
    if (
      emailError ||
      firstnameError ||
      lastnameError ||
      phoneNoError ||
      phoneEmptyError
    ) {
      return false;
    } else {
      return true;
    }
  }

  register = () => {
    if (this.validation()) {
      this.setState({
        loading: true,
      });
      var finalData = {
        firstName: this.state.firstname.trim(),
        lastName: this.state.lastname.trim(),
        phoneNo: this.state.phoneNo.trim(),
        email: this.state.email.toLowerCase().trim(),
        enable: true,
        username: this.state.email.toLowerCase().trim(),
        type: 'player',
        userRole: 'CUSTOMER',
      };
      httpService
        .post('login/signup', finalData, this.props)
        .then((response) => {
          if (response.status === 200) {
            this.setState({
              firstname: '',
              lastname: '',
              email: '',
              phoneNo: '',
              firstnameError: false,
              lastnameError: false,
              emailError: false,
              phoneNoError: false,
              phoneEmptyError: false,
              loading: false,
            });
            Toast.show({
              type: 'success',
              text1: response.statusMessage,
              text2: ValidationMsg.passwordSharedToMail,
            });
            this.props.navigation.pop();
          } else {
            this.setState({
              loading: false,
            });
            Toast.show({
              type: 'error',
              text1: response.statusMessage,
            });
          }
        });
    }
  };

  render() {
    return (
      <Container>
        <HeaderBack
          style={{backgroundColor: colors.background}}
          title={Content.signUp}
          navigation={this.props.navigation}
        />
        <KeyboardAvoidingView behavior={'padding'} style={{flex: 1}}>
          <ScrollView style={{backgroundColor: colors.background}}>
            <View style={styles.TopContainer}>
              <View style={styles.inputContainer}>
                <Image style={styles.logo} source={Images.loginLogo} />

                <View style={styles.inputContainerInner}>
                  <View style={styles.inputView}>
                    <TextInput
                      autoCorrect={false}
                      autoComplete={false}
                      returnKeyType="done"
                      keyboardType="default"
                      onChangeText={(val) => {
                        let value = val;
                        value = value.replace(/[^A-Za-z]/gi, '');
                        this.setState({
                          firstname: value.trim(),
                        });
                      }}
                      multiline={
                        Platform.OS == 'ios'
                          ? false
                          : this.state.firstname.length
                          ? false
                          : true
                      }
                      placeholder="Firstname"
                      placeholderTextColor={'#BFC7D8'}
                      value={this.state.firstname}
                      style={{
                        ...styles.input,
                        backgroundColor:
                          this.state.firstname.length > 0 ? 'white' : null,
                        borderBottomColor: this.state.firstname.length
                          ? colors.primaryGreen
                          : '#BFC7D8',
                      }}
                    />

                    {this.state.firstnameError && (
                      <Text style={styles.errorText}>
                        {ValidationMsg.signUpFirstName}
                      </Text>
                    )}
                  </View>

                  <View style={styles.inputView}>
                    <TextInput
                      autoCorrect={false}
                      autoComplete={false}
                      returnKeyType="done"
                      keyboardType="default"
                      onChangeText={(val) => {
                        let value = val;
                        value = value.replace(/[^A-Za-z]/gi, '');
                        this.setState({
                          lastname: value.trim(),
                        });
                      }}
                      multiline={
                        Platform.OS == 'ios'
                          ? false
                          : this.state.lastname.length
                          ? false
                          : true
                      }
                      placeholder="Lastname"
                      placeholderTextColor={'#BFC7D8'}
                      value={this.state.lastname}
                      style={{
                        ...styles.input,
                        backgroundColor:
                          this.state.lastname.length > 0 ? 'white' : null,
                        borderBottomColor: this.state.lastname.length
                          ? colors.primaryGreen
                          : '#BFC7D8',
                      }}
                    />

                    {this.state.lastnameError && (
                      <Text style={styles.errorText}>
                        {ValidationMsg.signUpLastName}
                      </Text>
                    )}
                  </View>
                  <View style={styles.inputView}>
                    <TextInput
                      autoCorrect={false}
                      autoComplete={false}
                      returnKeyType="done"
                      keyboardType="default"
                      onChangeText={(val) => {
                        this.setState({email: val.trim()});
                      }}
                      multiline={
                        Platform.OS == 'ios'
                          ? false
                          : this.state.email.length
                          ? false
                          : true
                      }
                      placeholder="Email"
                      placeholderTextColor={'#BFC7D8'}
                      value={this.state.email}
                      style={{
                        ...styles.input,
                        backgroundColor:
                          this.state.email.length > 0 ? 'white' : null,
                        borderBottomColor: this.state.email.length
                          ? colors.primaryGreen
                          : '#BFC7D8',
                      }}
                    />
                    {this.state.emailError && (
                      <Text style={styles.errorText}>
                        {ValidationMsg.signUpEmail}
                      </Text>
                    )}
                  </View>
                  <View style={styles.inputView}>
                    <TextInput
                      autoCorrect={false}
                      autoComplete={false}
                      returnKeyType="done"
                      keyboardType="phone-pad"
                      maxLength={10}
                      onChangeText={(val) => {
                        this.setState({phoneNo: val.trim()});
                      }}
                      multiline={
                        Platform.OS == 'ios'
                          ? false
                          : this.state.phoneNo.length
                          ? false
                          : true
                      }
                      placeholder="Phone"
                      placeholderTextColor={'#BFC7D8'}
                      value={this.state.phoneNo}
                      style={{
                        ...styles.input,
                        backgroundColor:
                          this.state.phoneNo.length > 0 ? 'white' : null,
                        borderBottomColor: this.state.phoneNo.length
                          ? colors.primaryGreen
                          : '#BFC7D8',
                      }}
                    />
                    {(this.state.phoneNoError ||
                      this.state.phoneEmptyError) && (
                      <Text style={styles.errorText}>
                        {ValidationMsg.signUpPhone}
                      </Text>
                    )}
                  </View>
                </View>

                <ButtonCommon
                  style={{
                    ...styles.loginButton,
                    backgroundColor:
                      this.state.firstname.length &&
                      this.state.lastname.length &&
                      this.state.email.length &&
                      this.state.phoneNo.length
                        ? colors.primaryGreen
                        : '#BFC7D8',
                  }}
                  onPress={this.register}
                  textStyle={{
                    fontFamily: PT_SANS_NARROW_BOLD,
                    fontSize: fontToDp(14),
                    color:
                      this.state.firstname.length &&
                      this.state.lastname.length &&
                      this.state.email.length &&
                      this.state.phoneNo.length
                        ? 'black'
                        : colors.white,
                  }}
                  loading={this.state.loading}
                  text={Content.SignUp}
                />
              </View>

              <View style={styles.bottomContainer}>
                <Text style={styles.newUser}>{Content.already_account}</Text>
                <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                  <Text style={styles.registerHere}>{Content.login_here}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </Container>
    );
  }
}

export default connect()(Signup);
