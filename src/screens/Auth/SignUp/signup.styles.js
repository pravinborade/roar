import {StyleSheet} from 'react-native';
import {colors} from '../../../styles';
import {
  PT_SANS_BOLD,
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../styles/typography';
import {fontToDp, heightToDp, widthToDp} from '../../../utils';

export const styles = StyleSheet.create({
  googleButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    height: heightToDp('6%'),
    marginHorizontal: '30%',
    opacity: 1,
    borderRadius: 4,
  },
  loginText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
  },
  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#BFC7D8',
    height: heightToDp('6%'),
    marginHorizontal: '5%',
    opacity: 1,
    borderRadius: 4,
  },
  inputView: {
    height: 48,
    borderBottomWidth: 3,
    borderBottomColor: colors.background,
  },
  input: {
    textAlign: 'center',
    fontFamily: PT_SANS_NARROW_BOLD,
    color: colors.primaryBlue,
    marginHorizontal: '5%',
    fontSize: fontToDp(18),
    borderBottomColor: '#BFC7D8',
    borderBottomWidth: 1.5,
  },
  logo: {
    height: heightToDp('10%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },
  heading: {
    fontFamily: PT_SANS_BOLD,
    fontSize: widthToDp('7%'),
    lineHeight: 25,
    color: '#202B46',
    textAlign: 'center',
  },
  forgot_pw: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: widthToDp('3%'),
    color: colors.darkGreen,
    textAlign: 'center',
  },
  connect: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: widthToDp('4%'),
    color: '#505C74',
    textAlign: 'center',
  },
  newUser: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: widthToDp('4%'),
    color: colors.primaryBlue,
    textAlign: 'center',
    marginRight: 4,
  },
  registerHere: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: widthToDp('4%'),
    color: colors.darkGreen,
    textAlign: 'center',
  },
  TopContainer: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
  },
  middleContainer: {
    height: '12%',
    justifyContent: 'space-between',
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '15%',
    top: '5%',
  },
  inputContainer: {
    justifyContent: 'space-between',
    height: heightToDp('70%'),
  },
  inputContainerInner: {
    justifyContent: 'space-evenly',
    height: heightToDp('35%'),
  },
  errorText: {
    color: '#FF0033',
    textAlign: 'center',
    marginHorizontal: '5%',
    fontSize: fontToDp(11),
  },
});
