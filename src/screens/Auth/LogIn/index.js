import React, {useEffect, useState} from 'react';
import {
  Alert,
  BackHandler,
  Image,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Container from '../../../components/Container';
import {ScreenNames} from '../../../utils/ScreenNames';
import Images from '../../../assets/Images/Images';
import {Content, fontToDp, heightToDp, widthToDp} from '../../../utils';
import {colors, commonStyle} from '../../../styles';
import httpService from '../../../commons/httpServices';
import {useDispatch, useSelector} from 'react-redux';
import {
  setFcmToken,
  setFromPayment,
  setLoginData,
} from '../../../redux/actions/login';
import {PT_SANS_NARROW_BOLD, PT_SANS_REGULAR} from '../../../styles/typography';
import Toast from 'react-native-toast-message';
import auth from '@react-native-firebase/auth';
import {ButtonCommon, HeaderBack} from '../../../components';
import {GoogleSignin} from '@react-native-community/google-signin';
import analytics from '@react-native-firebase/analytics';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ifValid} from '../../../utils/helper';
import {getMembership} from '../../Core/Home/scenes/venues/venue_detail/action';
import {successLog} from '../../../utils/fireLog';
import {fromSettingsScreen} from '../../../redux/actions/screenState';
import {useBackHandler} from '@react-native-community/hooks';
import messaging from '@react-native-firebase/messaging';

var validate = require('validate.js');

const Login = ({navigation}) => {
  const fromPayment = useSelector((state) => state.login.fromPayment);
  const [loading, setloading] = useState(false);
  const [username, setusername] = useState('');
  const [usernameError, setusernameError] = useState(false);
  const [password, setpassword] = useState('');
  const [passwordError, setpasswordError] = useState(false);
  const [pwdShow, setpwdShow] = useState(false);
  let selectedVenue = useSelector((x) => x.homeVenue.selectedVenue);

  let loginMainActive = useSelector(
    (state) => state.screenState.loginMainActive,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    checkPermission();
  }, []);

  const checkPermission = async () => {
    try {
      console.log('@@@@Enter Check Permission');
      const enabled = await messaging().hasPermission();
      console.log('@@##NOTIFICATION PERMISSION', enabled);
      if (!enabled) {
        requestPermission();
      } else if (enabled === -1) {
        requestPermission();
      } else if (enabled === 0) {
        requestPermission();
      } else {
        getFcmToken();
      }
    } catch (error) {
      console.log('@@@@ERROR GETING PERMISSION', error);
    }
  };

  const getFcmToken = async () => {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('@@@@fcmtoken----------------->', fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      console.log('@@@@fcmtoken inside await----------------->', fcmToken);
      if (fcmToken) {
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  };

  const requestPermission = async () => {
    try {
      await messaging().requestPermission();
      getFcmToken();
    } catch (error) {
      console.log('@@@@REQUEST PERMISSION', error);
    }
  };

  const validation = () => {
    let constraints = {
      username: {
        presence: true,
        format: {
          pattern: /\S+@\S+\.\S+/,
          message: 'Please enter valid Email',
        },
      },
      password: {
        presence: true,
        length: {
          minimum: 4,
        },
      },
    };

    let usernameCheck = validate.isEmpty(username);
    usernameCheck = validate({username: username.trim() + ''}, constraints);
    usernameCheck = usernameCheck && usernameCheck.username ? true : false;

    var passwordCheck = validate.isEmpty(password);
    passwordCheck = validate({password: password + ''}, constraints);
    passwordCheck = passwordCheck && passwordCheck.password ? true : false;

    setusernameError(usernameCheck);
    setpasswordError(passwordCheck);

    if (usernameCheck || passwordCheck) {
      return false;
    } else {
      return true;
    }
  };

  const _login = async () => {
    loginAPICall('regular');
  };

  const renderSwitchNavigation = (param, userId) => {
    switch (param) {
      case 'venue_book':
        return 1;
      case 'membership_book':
        _getMembership(userId);
        return 1;
      case 'event_book':
        return 1;
      case 'membership_services': {
        _getMembership(userId);
        return 1;
      }
      case 'membership_services_all': {
        _getMembership(userId);
        return 1;
      }
      default:
        return 0;
    }
  };

  const _getMembership = (userId) => {
    if (ifValid(selectedVenue?.site?.id)) {
      console.log(selectedVenue.site.id, '__' + userId);
      dispatch(getMembership(selectedVenue.site.id, userId));
    }
  };

  const loginAPICall = async (type) => {
    if (validation()) {
      setloading(true);
      var form = new FormData();

      form.append('username', username.toLowerCase());
      form.append('password', password);
      form.append('grant_type', 'password');
      await analytics().logEvent('Login', {
        email_id: username.toLowerCase(),
      });
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      httpService
        .postlogin('oauth/token', form)
        .then((response) => {
          console.log('@@ response', response);
          if (response.access_token) {
            httpService
              .get('oauth/check_token?token=' + response.access_token)
              .then((userresponse) => {
                console.log('******login===DATA=============>', userresponse);
                if (userresponse.user_name) {
                  userresponse = {
                    ...JSON.parse(userresponse.user_name),
                    token: response.access_token,
                  };
                  if (fcmToken != null) {
                    let body = {
                      user: {
                        id: userresponse.id,
                      },
                      fcmToken: fcmToken,
                    };
                    // Alert.alert("YES FCM TOKEN",fcmToken)
                    dispatch(setFcmToken(body));
                  }
                  // else {
                  //     Alert.alert("NO FCM TOKEN")
                  // }
                  auth()
                    .signInAnonymously()
                    .then((response) => {})
                    .catch((error) => {
                      if (error.code === 'auth/operation-not-allowed') {
                      }
                      console.error(error);
                    });

                  dispatch(setLoginData(userresponse));
                  setusernameError(false);
                  setpasswordError(false);
                  setloading(false);
                  successLog(userresponse);
                  if (renderSwitchNavigation(fromPayment, userresponse?.id)) {
                    navigation.pop();
                  } else {
                    navigation.replace(fromPayment);
                  }
                } else {
                  setloading(false);
                  Toast.show({
                    type: 'error',
                    text1: 'Error',
                    text2: response.error_description,
                  });
                }
              })
              .catch((error) => {
                setloading(false);
              });
          } else {
            setloading(false);
            Toast.show({
              type: 'error',
              text1: 'Error',
              text2: response.error_description,
            });
            console.log('@@', response);
          }
        })
        .catch((response) => {
          setloading(false);
          Toast.show({
            type: 'error',
            text1: 'Error',
            text2: response.error_description,
          });
        });
    }
  };

  const _forgotPass = () => {
    navigation.navigate(ScreenNames.FORGOT_PASSWORD);
    setusername('');
    setusernameError(false);
    setpassword('');
    setpasswordError(false);
  };

  const onGoogleButtonPress = async () => {
    const {idToken} = await GoogleSignin.signIn(); // Get the users ID token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken); // Create a Google credential with the token
    const response = await auth().signInWithCredential(googleCredential); // Sign-in the user with the credential

    //Register user through google signIn
    var finalData = {
      firstName: response.additionalUserInfo.profile.given_name,
      lastName: response.additionalUserInfo.profile.family_name,
      phoneNo: 9999111141,
      email: 'kalkb@mailinator.com'.toLowerCase(),
      enable: true,
      username: response.additionalUserInfo.profile.email.toLowerCase(),
      type: 'player',
      userRole: 'CUSTOMER',
    };
    httpService.post('login/signup', finalData).then((res) => {
      if (res.status === 200) {
        console.log('%$res', res.data[0]);
        let userresponse = {...res.data[0], token: response.user.uid};
        console.log('%$', userresponse);
        dispatch(setLoginData(userresponse));
        setloading(false);

        // helper.successToastWithBack(response.statusMessage + ' Password has been shared on your email id.', this);
      } else {
        console.log('%$re', res);
        // helper.errorToast(response.statusMessage);
      }
    });

    // if (response.user.displayName) {

    //     let userresponse = { ...response.user._user, token: response.user.uid };

    //     //console.log("$$userresponse", userresponse.token)
    //     dispatch(setLoginData(userresponse));
    // }
    //console.log("** result ", response.user.uid);
    //return result
  };

  const backActionHandlerAlert = () => {
    Alert.alert('Alert!', 'Are you sure you want to exit app?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => BackHandler.exitApp()},
    ]);
    return true;
  };

  const backHandler = () => {
    dispatch(fromSettingsScreen(false));
    dispatch(setFromPayment('default'));
    navigation.goBack();
  };

  const backActionHandler = () => {
    loginMainActive ? backActionHandlerAlert() : backHandler();
    return true;
  };
  useBackHandler(backActionHandler);

  return (
    <Container>
      <HeaderBack
        mainHeader={loginMainActive ? true : false}
        style={{backgroundColor: colors.background}}
        title={ScreenNames.LOGIN}
        onPress={backHandler}
      />

      <View style={styles.TopContainer}>
        <Image style={styles.logo} source={Images.loginLogo} />

        <View
          style={{
            ...commonStyle.inputContainer,
            backgroundColor: username.length > 0 ? 'white' : null,
            borderBottomColor: username.length
              ? colors.primaryGreen
              : '#BFC7D8',
          }}
        >
          <TextInput
            autoCorrect={false}
            autoComplete={false}
            returnKeyType="done"
            onChangeText={(val) => setusername(val.trim())}
            keyboardType="email-address"
            keyboardAppearance="light"
            value={username.trim()}
            placeholder="Email"
            multiline={
              Platform.OS == 'ios' ? false : username.length ? false : true
            }
            placeholderTextColor={'#BFC7D8'}
            style={{
              ...commonStyle.inputBox,
              textAlignVertical: 'top',
              borderBottomColor: username.length
                ? colors.primaryGreen
                : '#BFC7D8',
            }}
          />
        </View>
        {usernameError && (
          <Text
            style={{
              color: '#FF0033',
              textAlign: 'center',
              marginHorizontal: '5%',
            }}
          >
            *email can not be empty and should be valid
          </Text>
        )}

        <View
          style={{
            ...commonStyle.inputContainer,
            backgroundColor: password.length > 0 ? 'white' : null,
            borderBottomColor: password.length
              ? colors.primaryGreen
              : '#BFC7D8',
          }}
        >
          <TextInput
            autoCorrect={false}
            autoComplete={false}
            returnKeyType="done"
            onChangeText={(val) => setpassword(val.trim())}
            keyboardType="default"
            keyboardAppearance="light"
            value={password}
            secureTextEntry={!pwdShow}
            placeholder="Password"
            placeholderTextColor={'#BFC7D8'}
            multiline={
              Platform.OS == 'ios' ? false : password.length ? false : true
            }
            style={{
              ...commonStyle.inputBox,
              borderBottomColor: password.length
                ? colors.primaryGreen
                : '#BFC7D8',
            }}
          />
          {pwdShow ? (
            <TouchableOpacity
              style={styles.eyeContainer}
              onPress={() => setpwdShow(!pwdShow)}
            >
              <Image source={Images.eye} style={styles.eyeStyle} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.eyeContainer}
              onPress={() => setpwdShow(!pwdShow)}
            >
              <Image source={Images.eyeOff} style={styles.eyeStyle} />
            </TouchableOpacity>
          )}
        </View>
        {passwordError && (
          <Text
            style={{
              color: '#FF0033',
              textAlign: 'center',
              marginHorizontal: '5%',
            }}
          >
            *Please enter Password
          </Text>
        )}
        <View style={styles.middleContainer}>
          {/* <TouchableOpacity onPress={_login} style={{
                        ...styles.loginButton,
                        backgroundColor: (username.length && password.length) ? colors.primaryGreen : '#BFC7D8'
                    }}>
                        {
                            loading === true && <ActivityIndicator color={'white'} style={{ alignSelf: 'center' }} size={'small'} />

                        }
                        {
                            loading === false && <Text style={{
                                ...styles.loginText,
                                color: (username.length && password.length) ? 'black' : colors.white,
                            }}>{Content.Login}</Text>
                        }
                    </TouchableOpacity> */}

          <ButtonCommon
            style={{
              ...styles.loginButton,
              backgroundColor:
                username.length && password.length
                  ? colors.primaryGreen
                  : '#BFC7D8',
            }}
            onPress={_login}
            textStyle={{
              ...styles.loginText,
              color:
                username.length && password.length ? 'black' : colors.white,
            }}
            loading={loading}
            text={Content.Login}
          />

          <TouchableOpacity onPress={_forgotPass}>
            <Text style={styles.forgot_pw}> {Content.forgot_pw} </Text>
          </TouchableOpacity>
        </View>

        {/* ===========================Google signin ================================== */}

        <View style={styles.middleContainer}>
          {/* <Text style={styles.connect}> {Content.connect} </Text>
                    <TouchableOpacity style={styles.googleButton}>
                        <Image source={Images.googleButton} style={{ resizeMode: 'contain', height: 20, width: 60 }} />

                    </TouchableOpacity> */}
        </View>

        {/* ===========================Google signin ================================== */}

        <View style={styles.bottomContainer}>
          <Text style={styles.newUser}>{Content.New_User}</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate(ScreenNames.SIGNUP)}
          >
            <Text style={styles.registerHere}>{Content.register_her}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Container>
  );
};

export default Login;

const styles = StyleSheet.create({
  googleButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    height: heightToDp('6%'),
    marginHorizontal: '30%',
    opacity: 1,
    borderRadius: 4,
  },
  googleText: {
    color: colors.primaryBlue,
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
  },

  eyeContainer: {
    position: 'absolute',
    justifyContent: 'center',
    height: 49,
    padding: 10,
    right: 0,
    alignItems: 'center',
  },

  eyeStyle: {
    tintColor: '#BFC7D8',
    width: widthToDp(5.6),
    height: widthToDp(4.5),
    resizeMode: 'cover',
    alignSelf: 'center',
  },

  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: heightToDp('6%'),
    marginHorizontal: '5%',
    opacity: 1,
    borderRadius: 4,
  },
  loginText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
  },

  logo: {
    height: heightToDp('10%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },

  forgot_pw: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(12),
    color: colors.darkGreen,
    textAlign: 'center',
  },

  connect: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(14),
    color: '#505C74',
    textAlign: 'center',
  },
  newUser: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(15),
    color: colors.primaryBlue,
    textAlign: 'center',
    marginRight: 4,
  },
  registerHere: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(15),
    color: colors.darkGreen,
    textAlign: 'center',
  },
  TopContainer: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
  },
  middleContainer: {
    height: '14%',
    justifyContent: 'space-between',
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '15%',
  },
});
