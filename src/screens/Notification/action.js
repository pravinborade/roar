export function receiveNotification(value) {
  return {
    type: 'RECEIVED_NOTIFICTIONS',
    payload: value,
  };
}

export function receiveNotificationContent(data) {
  return {
    type: 'RECEIVED_NOTIFICTIONS_CONTENT',
    payload: data,
  };
}
