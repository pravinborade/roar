import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {Content} from '../../../utils';
import axiosService from '../../../commons/axiosService';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MyNotificationEventCard from './myNotificationVenueCard';

class EventsNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orginalNotification: [],
      notification: [],
      pageNo: 0,
      pageSize: 20,
      loadingMoreData: false,
      userDetails: null,
      noData: false,
      refreshing: false,
    };
  }

  async componentDidMount() {
    await AsyncStorage.getItem('userDetails').then((details) => {
      if (details) {
        details = JSON.parse(details);
        this.setState({userDetails: details}, () => this.getBookingDetails());
      }
    });
  }

  getBookingDetails() {
    let url = `api/users/notifications?userId=${this.state.userDetails.id}&pageNo=${this.state.pageNo}&pageSize=${this.state.pageSize}&type=ALL`;
    axiosService.get(url).then((response) => {
      if (
        response &&
        response.status === 200 &&
        response.data.data[0].content.length > 0
      ) {
        this.setState(
          {
            orginalNotification: [
              ...this.state.orginalNotification,
              ...response.data.data[0].content,
            ],
          },
          () => this._pagingation(),
        );
      } else {
        this.setState({noData: true});
      }
    });
  }

  //used customize pagination to reduce the load from the app
  _pagingation() {
    let remainRecords = this.state.orginalNotification.length; // calculating remaining count of the records
    if (remainRecords > 0) {
      //if there are remaing records of this.state.orginalBookings then using intenal pagination
      let spliceNoOfRecord = remainRecords >= 7 ? 7 : remainRecords; //showing only 5 records at a time
      let notification = this.state.orginalNotification.splice(
        0,
        spliceNoOfRecord,
      ); // coping records from orginalBookings
      this.setState({
        notification: [...this.state.notification, ...notification],
        loadingMoreData: false,
      });
    } else {
      //fetching the next page records from the API
      this.setState({pageNo: this.state.pageNo + 1}, () =>
        this.getBookingDetails(),
      );
    }
  }

  //When reach to screen bottom on scroll
  _onEndReached() {
    if (!this.state.loadingMoreData && !this.state.noData) {
      this.setState({loadingMoreData: true}, () => this._pagingation());
    }
  }

  //Rendering booking using FlatList
  _renderBookings() {
    return (
      <FlatList
        style={styles.flex1}
        data={this.state.notification}
        renderItem={(item, index) => this._renderBookingCards(item, index)}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => {}}
        onMomentumScrollEnd={() => this._onEndReached()}
        progressViewOffset={10}
      />
    );
  }

  //Rendering booking cards with booking name
  _renderBookingCards({item, index}) {
    return <MyNotificationEventCard data={item} nav={this.props} />;
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.notification?.length > 0 ? (
          this._renderBookings()
        ) : !this.state.noData ? (
          <Text style={styles.noNotification}>{Content.noNotification}</Text>
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.login.userToken,
  };
};

export default connect(mapStateToProps)(EventsNotification);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  flex1: {flex: 1},
  noNotification: {textAlign: 'center', top: '2%'},
});
