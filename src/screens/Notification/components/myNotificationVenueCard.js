import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../styles';
import moment from 'moment';
import {PT_SANS_REGULAR} from '../../../styles/typography';
import Images from '../../../assets/Images/Images';

class MyNotificationEventCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      venue: [],
    };
  }

  render() {
    var data = this.props.data && this.props.data;
    var system_date = moment(this.props.data.systemDateTime);
    var booking_end_date = moment(
      this.props?.data?.endDate + ' ' + this.props?.data?.endTime,
    );
    var booking_start_date = moment(
      this.props?.data?.startDate + ' ' + this.props?.data?.startTime,
    );
    var isPastDate =
      system_date >= booking_end_date
        ? 'past'
        : system_date <= booking_start_date
        ? 'coming'
        : 'ongoing';
    var message =
      isPastDate == 'past'
        ? 'Completed'
        : isPastDate == 'coming'
        ? 'Upcoming'
        : 'Ongoing';

    return (
      <View style={styles.container}>
        <View>
          <Image style={styles.imageView} source={Images.sampleBooking} />
        </View>
        <View style={styles.innerContainer}>
          <Text numberOfLines={2} style={styles.textBody}>
            {data.body}
          </Text>
          <Text style={styles.date}>
            {moment
              .utc(data?.creationDate)
              .local()
              .startOf('seconds')
              .fromNow()}{' '}
            {moment(data?.creationDate).format('LT')}
          </Text>
        </View>
      </View>
    );
  }
}

export default MyNotificationEventCard;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 15,
    backgroundColor: colors.white,
    marginBottom: 5,
    marginHorizontal: '5%',
  },
  innerContainer: {
    justifyContent: 'center',
    margin: 10,
    flex: 1,
  },
  imageView: {height: 60, width: 70, borderRadius: 3},
  textBody: {
    fontFamily: PT_SANS_REGULAR,
    color: '#202B46',
    fontSize: 12,
    marginBottom: 5,
  },
  date: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: 10,
  },
});
