import React, {Component, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import Images from '../../../assets/Images/Images';
import {colors} from '../../../styles/colors';
import moment from 'moment';
import axiosService from '../../../commons/axiosService';
import {helper} from '../../../commons/helpers';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {PT_SANS_REGULAR} from '../../../styles/typography';

class myNotificationCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event: [],
    };
  }

  // checknotificationvenuelist(data){

  //     for(var i=0; i <= data.length; i++){
  //         if(data[i].type == "EVENT"){
  //             this.state.event.push(data[i].type)
  //         }
  //     }
  // }

  render() {
    var data = this.props.data && this.props.data;
    var system_date = moment(this.props.data.systemDateTime);
    var booking_end_date = moment(
      this.props.data.endDate + ' ' + this.props.data.endTime,
    );
    var booking_start_date = moment(
      this.props.data.startDate + ' ' + this.props.data.startTime,
    );
    var isPastDate =
      system_date >= booking_end_date
        ? 'past'
        : system_date <= booking_start_date
        ? 'coming'
        : 'ongoing';
    var message =
      isPastDate == 'past'
        ? 'Completed'
        : isPastDate == 'coming'
        ? 'Upcoming'
        : 'Ongoing';
    return (
      <>
        {/SITE/.test(data.type) && (
          <View
            style={{
              flexDirection: 'row',
              padding: 15,
              backgroundColor: colors.white,
              marginBottom: 5,
            }}>
            <View>
              <Image
                style={{height: 60, width: 70, borderRadius: 3}}
                source={Images.sampleBooking}
              />
            </View>
            <View style={{justifyContent: 'center', margin: 10}}>
              <Text
                style={{
                  fontFamily: PT_SANS_REGULAR,
                  color: '#202B46',
                  fontSize: 12,
                  marginBottom: 5,
                  width: '90%',
                }}>
                {data.body}
              </Text>
              <Text
                style={{
                  fontFamily: PT_SANS_REGULAR,
                  color: '#505C74',
                  fontSize: 10,
                }}>
                {moment(data.creationDate).startOf('hour').fromNow()}{' '}
                {moment(data.creationDate).format('LT')}
              </Text>
            </View>
          </View>
        )}
      </>
    );
  }
}

export default myNotificationCard;
