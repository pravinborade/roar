import React, {useState} from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View,
  Platform,
  ScrollView,
} from 'react-native';

import Toast from 'react-native-toast-message';
import {Container, HeaderBack} from '../../components';
import {colors} from '../../styles';
import {ScreenNames} from '../../utils/ScreenNames';
import EventsNotification from './components/eventsNotification';
import {Content} from '../../utils/content/Content';
const Bookings = ({navigation}) => {
  const [index, setIndex] = useState('1');

  const onChangeIndex = (text) => {
    setIndex(text);
  };

  return (
    <Container>
      <HeaderBack title={ScreenNames.NOTIFICATION} navigation={navigation} />
      <View
        style={{
          backgroundColor: colors.background,
          borderBottomWidth: 5,
          borderBottomColor: colors.background,
          flex: 1,
        }}>
        <View style={styles.mainContainer}>
          <EventsNotification />
        </View>
      </View>
    </Container>
  );
};

export default Bookings;

const styles = StyleSheet.create({
  selectedIndex: {
    color: colors.darkGreen,
    fontSize: 11,
    fontWeight: 'bold',
    borderBottomWidth: 2,
    borderBottomColor: colors.darkGreen,
    lineHeight: 25,
  },
  unselectedIndex: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 11,
  },
  mainContainer: {
    paddingTop: 10,
    alignContent: 'center',
    alignItems: 'center',
  },
});
