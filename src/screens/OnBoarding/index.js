import React, {useEffect} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import Container from '../../components/Container';
import OnboardingSwiper from 'react-native-onboarding-swiper';
import {heightToDp, widthToDp} from '../../utils/Responsive';
import {ScreenNames} from '../../utils/ScreenNames';
import Images from '../../assets/Images/Images';
import {Content} from '../../utils/content/Content';
import {useDispatch} from 'react-redux';
import {setisFirstLaunch} from '../../redux/actions/onBoarding';
import {PT_SANS_NARROW_BOLD, PT_SANS_REGULAR} from '../../styles/typography';
import CodePush from 'react-native-code-push';
import {requestTrackingPermission} from 'react-native-tracking-transparency';

const Onboarding = ({navigation}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    CodePush.disallowRestart();
    requestTracking();
    return () => {
      CodePush.disallowRestart();
    };
  }, []);

  const requestTracking = async () => {
    const trackingStatus = await requestTrackingPermission();
    if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
    }
  };

  const Skip = ({...props}) => {
    return (
      <TouchableOpacity {...props}>
        <Text
          style={{
            fontFamily: PT_SANS_REGULAR,
            color: '#87C500',
            fontSize: widthToDp('4.5%'),
            margin: 20,
          }}>
          {Content.sKip}
        </Text>
      </TouchableOpacity>
    );
  };
  const Next = ({...props}) => {
    return (
      <TouchableOpacity {...props}>
        <Text
          style={{
            fontFamily: PT_SANS_REGULAR,
            color: '#87C500',
            fontSize: widthToDp('4.5%'),
            margin: 20,
          }}>
          {Content.nExt}
        </Text>
      </TouchableOpacity>
    );
  };

  const Done = ({...props}) => {
    return (
      <TouchableOpacity {...props}>
        <Text
          style={{
            fontFamily: PT_SANS_REGULAR,
            color: '#87C500',
            fontSize: widthToDp('4.5%'),
            margin: 20,
          }}>
          {Content.dOne}
        </Text>
      </TouchableOpacity>
    );
  };
  const Dots = ({selected}) => {
    let backgroundColor;
    backgroundColor = selected ? '#AFFF00' : '#BFC7D8';
    return (
      <View
        style={{
          width: widthToDp('2%'),
          height: widthToDp('2%'),
          borderRadius: widthToDp('2%'),
          marginHorizontal: widthToDp('2.5%'),
          bottom: heightToDp('12%'),
          backgroundColor,
        }}
      />
    );
  };

  return (
    <Container>
      <OnboardingSwiper
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        DotComponent={Dots}
        onSkip={() => navigation.navigate(ScreenNames.MY_LOCATION)}
        onDone={() => navigation.navigate(ScreenNames.MY_LOCATION)}
        bottomBarColor={'#FFFFFF'}
        pages={[
          {
            backgroundColor: '#FFFFFF',
            image: (
              <Image
                style={{
                  height: heightToDp('45%'),
                  width: widthToDp('65%'),
                  position: 'relative',
                  top: heightToDp('8%'),
                  resizeMode: 'contain',
                }}
                source={Images.onboarding_1}
              />
            ),
            title: 'Get Ready To Roar',
            subtitle: '',
            titleStyles: {
              fontFamily: PT_SANS_NARROW_BOLD,
              position: 'relative',
              bottom: heightToDp('60%'),
              color: '#202B46',
              fontSize: widthToDp('7%'),
            },
          },
          {
            backgroundColor: '#FFFFFF',
            image: (
              <Image
                style={{
                  height: heightToDp('45%'),
                  width: widthToDp('65%'),
                  position: 'relative',
                  top: heightToDp('8%'),
                  resizeMode: 'contain',
                }}
                source={Images.onboarding_2}
              />
            ),
            title: 'Put On A Skill Show',

            subtitle: '',
            titleStyles: {
              fontFamily: PT_SANS_NARROW_BOLD,
              position: 'relative',
              bottom: heightToDp('60%'),
              color: '#202B46',
              fontSize: widthToDp('7%'),
            },
          },
          {
            backgroundColor: '#FFFFFF',
            image: (
              <Image
                style={{
                  height: heightToDp('45%'),
                  width: widthToDp('65%'),
                  position: 'relative',
                  top: heightToDp('8%'),
                  resizeMode: 'contain',
                }}
                source={Images.onboarding_3}
              />
            ),
            title: 'Discover Sports,Venues \n & Events',
            subtitle: '',
            titleStyles: {
              fontFamily: PT_SANS_NARROW_BOLD,
              position: 'relative',
              bottom: heightToDp('60%'),
              color: '#202B46',
              fontSize: widthToDp('7%'),
            },
          },
        ]}
      />
    </Container>
  );
};

export default Onboarding;
