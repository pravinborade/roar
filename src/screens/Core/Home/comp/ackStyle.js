import React from 'react';
import {StyleSheet} from 'react-native';
import {heightToDp, widthToDp} from '../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../styles';

export const ackStyle = StyleSheet.create({
  logo: {
    height: heightToDp('15%'),
    width: '20%',
    resizeMode: 'contain',
  },
  linearGradient: {
    padding: widthToDp(4.5),
    flex: 1,
    ...commonStyle.justifyAlignCenter,
  },
  yayStyle: {
    textAlign: 'center',
    ...commonStyle.bold72,
  },
  slotBookedStyl: {
    textAlign: 'center',
    paddingVertical: widthToDp(3.5),
    ...commonStyle.regular50,
  },
  slotSubBookedStyl: {
    textAlign: 'center',
    paddingVertical: widthToDp(3.5),
    ...commonStyle.regular32,
  },
  button: {
    backgroundColor: colors.primaryBlue,
    paddingHorizontal: widthToDp(5),
    paddingVertical: widthToDp(3),
  },
  buttonText: {
    ...commonStyle.bold30,
    color: colors.white,
  },
});
