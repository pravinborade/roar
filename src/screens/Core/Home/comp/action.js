import axiosService from '../../../../commons/axiosService';
import {calculateDistance, ifNotValid, ifValid} from '../../../../utils/helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {setLocation, setFcmToken} from '../../../../redux/actions/login';
import {errorLog, successLog} from '../../../../utils';
import {setLoader} from '../../../../redux/actions/loader';
import moment from 'moment';
import messaging from '@react-native-firebase/messaging';

export const getEventList = (location) => {
  return (dispatch) => {
    dispatch(setLoader(true));
    dispatch({type: 'SET_EVENTS', payload: []});
    axiosService
      .get(`api/events`)
      .then(async (response) => {
        successLog('api events', response.status);
        try {
          if (response.status === 200) {
            response = response.data;
            if (ifNotValid(response)) {
              errorLog('invalid response ');
              return;
            }
            let res = ifValid(response.data[0]) ? response.data[0] : [];
            let latitude = '';
            let longitude = '';
            if (ifValid(location) && ifValid(location.lat)) {
              latitude = location.lat;
              longitude = location.lng;
            } else {
              let latLong = await getLatLong();
              if (ifValid(latLong) && ifValid(latLong.lat)) {
                latitude = latLong.lat;
                longitude = latLong.lng;
              }
            }

            if (res) {
              res = res.filter((x) => {
                return (
                  moment(x.endDate).isAfter(new Date()) ||
                  moment(x.endDate).isSame(new Date(), 'day')
                );
              });
            }

            if (res && res.length > 0 && latitude !== '') {
              res = res.map((x, index) => {
                let distance = -1;
                let latBackend = x.latitude;
                let lngBackend = x.longitude;
                if (
                  ifValid(latBackend) &&
                  ifValid(lngBackend) &&
                  latBackend !== 0
                ) {
                  distance = parseInt(
                    calculateDistance(
                      latBackend,
                      lngBackend,
                      latitude,
                      longitude,
                    ),
                  );
                }

                return {...x, distance};
              });
              res = res.sort(function (a, b) {
                return a.distance - b.distance;
              });
              let withMinus1 = res.filter((x) => x.distance === -1);
              let withGreater0 = res.filter((x) => x.distance >= 0);
              res = [...withGreater0, ...withMinus1];
              dispatch({type: 'SET_EVENTS_WITH_TEMP', payload: res});
            } else {
              dispatch({type: 'SET_EVENTS_WITH_TEMP', payload: res});
            }
            dispatch(setLoader(false));
          } else {
            console.log('not valid res ' + JSON.stringify(response));
          }
        } catch (e) {
          errorLog(e);
        }
      })
      .catch((o) => {
        console.log('getEventList error ' + JSON.stringify(o));
      });
  };
};

export const updateTokenToDB = async (userDetails, dispatch) => {
  try {
    if (ifNotValid(userDetails?.id)) {
      errorLog(userDetails);
      return;
    }
    let fToken = await messaging().getToken();
    let body = {user: {id: userDetails.id}, fcmToken: fToken};
    dispatch(setFcmToken(body));
  } catch (e) {
    errorLog(e);
  }
};

export const getAndSetToken = () => {
  return async (dispatch) => {
    let user = await AsyncStorage.getItem('userDetails');
    if (ifValid(user)) {
      user = JSON.parse(user);
      dispatch({type: 'LOGIN_SUCCESS', payload: user});
    }
  };
};

export const setSearchValue = (value = '') => {
  return {type: 'SET_SEARCH_VALUE', payload: value};
};

export const setEvents = (payload) => {
  return (dispatch) => {
    dispatch({type: 'SET_EVENTS', payload: payload});
  };
};

export const setLatLongOnly = (latLong) => {
  return (dispatch) => {
    dispatch({type: 'LAT_LONG', payload: latLong});
  };
};

export const setLatLong = (latLong, address) => {
  return (dispatch) => {
    dispatch({
      type: 'LAT_LONG_ADDRESS',
      payload: {location: latLong, address: address},
    });
  };
};

export const getAndSetLatLong = () => {
  return async (dispatch) => {
    let latLong = await AsyncStorage.getItem('latLong');
    let location = await AsyncStorage.getItem('location');
    if (ifValid(latLong) && ifValid(location)) {
      dispatch({
        type: 'LAT_LONG_ADDRESS',
        payload: {location: JSON.parse(latLong), address: location},
      });
    } else if (ifValid(location)) {
      dispatch(setLocation(data.description));
    }
  };
};

export const getLatLong = async () => {
  let latLong = await AsyncStorage.getItem('latLong');
  return JSON.parse(latLong);
};
