import {FlatList, StyleSheet, View} from 'react-native';
import React from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
} from '../../../../../../components';
import {Content} from '../../../../../../utils/content/Content';
import {useDispatch, useSelector} from 'react-redux';
import {
  serviceAddToCart,
  serviceOnClose,
  serviceOnSelect,
  serviceOnSelected,
  serviceOnTouch,
  serviceRemoveFromCart,
} from '../action';
import {ifValid} from '../../../../../../utils/helper';
import ServiceCard from '../comp/ServiceCard';
import {widthToDp} from '../../../../../../utils/Responsive';
import {colors} from '../../../../../../styles';

const HomeServicesAll = ({navigation}) => {
  const dispatch = useDispatch();
  const selectedVenue = useSelector((state) => state.homeVenue.selectedVenue);
  const {services, servicesTaken} = useSelector((state) => state.homeServices);
  let slot = useSelector((x) => x.homeBookings.slot);
  let offerRate = ifValid(slot) && ifValid(slot.offerRate) ? slot.offerRate : 0;

  const _onServiceSelected = (selected, item) =>
    serviceOnSelected(selected, item, services, servicesTaken, dispatch);

  const _addToCart = (item) =>
    serviceAddToCart(item, services, servicesTaken, dispatch);

  const _removeFromCart = (item) =>
    serviceRemoveFromCart(item, services, servicesTaken, dispatch);
  const _onPickerTouchService = (item) =>
    serviceOnTouch(item, services, servicesTaken, dispatch);
  const _onPickerSelectService = (value, item) =>
    serviceOnSelect(value, item, services, servicesTaken, dispatch);
  const _onPickerCloseService = () =>
    serviceOnClose(services, servicesTaken, dispatch);

  const _onContinuePress = () => {
    navigation.goBack();
  };

  return (
    <Container>
      <HeaderBack navigation={navigation} title={Content.additionalServices} />
      {services && services.length > 0 ? (
        <FlatList
          data={services}
          style={{backgroundColor: colors.background}}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}) => {
            return (
              <ServiceCard
                isVisible={item.isVisible}
                onPickerTouch={() => _onPickerTouchService(item)}
                onPickerSelect={(value) => _onPickerSelectService(value, item)}
                onPickerClose={() => _onPickerCloseService(item)}
                onServiceSelected={(selected, item) =>
                  _onServiceSelected(selected, item)
                }
                addToCartPress={(x) => _addToCart(x)}
                removeFromCartPress={(x) => _removeFromCart(x)}
                name={item.name}
                rate={item.rate}
                item={item}
                index={index}
              />
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <View style={{flex: 1}} />
      )}

      <ButtonCommon text={'BACk'} onPress={() => _onContinuePress()} />
    </Container>
  );
};
export default HomeServicesAll;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    marginHorizontal: widthToDp(4.5),
    marginVertical: widthToDp(1.5),
  },
  button: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  dropdownContainer: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  dropDownAndButtonContainer: {
    flexDirection: 'row',
    width: '85%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  imageStyle: {
    margin: widthToDp(2),
    width: widthToDp(15),
    height: widthToDp(15),
    backgroundColor: colors.background,
  },

  exploreButton: {
    alignSelf: 'flex-end',
    marginHorizontal: widthToDp(4.5),
  },
});
