import {FlatList, StyleSheet, View} from 'react-native';
import React from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
} from '../../../../../../components';
import {useDispatch, useSelector} from 'react-redux';
import {
  inventoryAddToCart,
  inventoryOnClose,
  inventoryOnSelect,
  inventoryOnSelected,
  inventoryOnTouch,
  inventoryRemoveFromCart,
} from '../action';
import {ifValid} from '../../../../../../utils/helper';
import ServiceCard from '../comp/ServiceCard';
import {colors} from '../../../../../../styles';

const HomeInventoriesAll = ({navigation}) => {
  const dispatch = useDispatch();
  const selectedVenue = useSelector((state) => state.homeVenue.selectedVenue);
  let slot = useSelector((x) => x.homeBookings.slot);
  let offerRate = ifValid(slot) && ifValid(slot.offerRate) ? slot.offerRate : 0;
  const {inventories, inventoriesTaken, membership, membershipTaken} =
    useSelector((state) => state.homeServices);

  const _onServiceSelectedInventory = (selected, item) =>
    inventoryOnSelected(
      selected,
      item,
      inventories,
      inventoriesTaken,
      dispatch,
    );
  const _addToCartInventory = (item) =>
    inventoryAddToCart(item, inventories, inventoriesTaken, dispatch);
  const _removeFromCartInventory = (item) =>
    inventoryRemoveFromCart(item, inventories, inventoriesTaken, dispatch);
  const _onPickerTouchInventory = (item) =>
    inventoryOnTouch(item, inventories, inventoriesTaken, dispatch);
  const _onPickerSelectInventory = (value, item) =>
    inventoryOnSelect(value, item, inventories, inventoriesTaken, dispatch);
  const _onPickerCloseInventory = () =>
    inventoryOnClose(inventories, inventoriesTaken, dispatch);

  const _onContinuePress = () => {
    navigation.goBack();
  };

  return (
    <Container>
      <HeaderBack navigation={navigation} title={'Additional Items'} />
      {inventories && inventories.length > 0 ? (
        <FlatList
          style={{backgroundColor: colors.background}}
          data={inventories}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}) => {
            return (
              <ServiceCard
                isVisible={item.isVisible}
                onPickerTouch={() => _onPickerTouchInventory(item)}
                onPickerSelect={(value) =>
                  _onPickerSelectInventory(value, item)
                }
                onPickerClose={() => _onPickerCloseInventory(item)}
                onServiceSelected={(selected, index, item) =>
                  _onServiceSelectedInventory(selected, index, item)
                }
                addToCartPress={(x) => _addToCartInventory(x)}
                removeFromCartPress={(x) => _removeFromCartInventory(x)}
                name={item.productName}
                rate={item.mrpAmount}
                item={item}
                index={item.name}
              />
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <View style={{flex: 1}} />
      )}

      <ButtonCommon text={'BACk'} onPress={() => _onContinuePress()} />
    </Container>
  );
};
export default HomeInventoriesAll;

const styles = StyleSheet.create({});
