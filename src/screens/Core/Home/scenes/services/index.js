import {BackHandler, ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
  LoaderAndMsg,
} from '../../../../../components';
import {
  Content,
  heightToDp,
  ScreenNames,
  ValidationMsg,
  width,
  widthToDp,
} from '../../../../../utils';
import {colors, commonStyle} from '../../../../../styles';
import {useDispatch, useSelector} from 'react-redux';
import {
  clearMembershipServiceInventory,
  inventoryAddToCart,
  inventoryOnClose,
  inventoryOnSelect,
  inventoryOnSelected,
  inventoryOnTouch,
  inventoryRemoveFromCart,
  membershipAddToCart,
  membershipRemoveFromCart,
  serviceAddToCart,
  serviceOnClose,
  serviceOnSelect,
  serviceOnSelected,
  serviceOnTouch,
  serviceRemoveFromCart,
  updateTotalPrice,
} from './action';
import {
  getLineView,
  ifGreaterArray,
  ifValid,
  showToastMsg,
} from '../../../../../utils/helper';
import ServiceCard from './comp/ServiceCard';
import ExploreMore from '../../../../../components/ExploreMore';
import MembershipCard from './comp/MemberCard';
import ListHeader from '../../../../../components/ListHeader';
import {setFromPayment} from '../../../../../redux/actions/login';
import {makeEntryToFirebaseForLockingSlot} from '../bookings/action';

const HomeServices = ({navigation}) => {
  const dispatch = useDispatch();
  const selectedVenue = useSelector((state) => state.homeVenue.selectedVenue);
  const {
    services,
    servicesTaken,
    loading,
    inventories,
    inventoriesTaken,
    membership,
    membershipTaken,
  } = useSelector((state) => state.homeServices);
  let {slotsSelected, slotSelectedByOther, court} = useSelector(
    (x) => x.homeBookings,
  );
  let firstmember = ifValid(membership[0]) ? membership[0] : undefined;
  let userData = useSelector((x) => x.login.data);
  const [tempSelectedValue, setTempSelectedValue] = useState('');

  const _onServiceSelected = (selected, item) =>
    serviceOnSelected(selected, item, services, servicesTaken, dispatch);
  const _addToCart = (item) =>
    serviceAddToCart(item, services, servicesTaken, dispatch);
  const _removeFromCart = (item) =>
    serviceRemoveFromCart(item, services, servicesTaken, dispatch);

  const _onPickerTouchService = (item) =>
    serviceOnTouch(item, services, servicesTaken, dispatch);
  const _onPickerSelectService = (value, item) =>
    serviceOnSelect(value, item, services, servicesTaken, dispatch);
  const _onPickerCloseService = () =>
    serviceOnClose(services, servicesTaken, dispatch);

  const _addToCartMembership = (item) => {
    if (!ifValid(userData.id)) {
      dispatch(setFromPayment('membership_services'));
      navigation?.navigate(ScreenNames.LOGIN);
      return;
    }

    if (item.isMember) {
      showToastMsg(
        Content.membershipService,
        ValidationMsg.membershipAlreadyPurchased,
      );
      return;
    }

    membershipAddToCart(item, membership, membershipTaken, dispatch);
  };
  const _removeFromCartMembership = (item) =>
    membershipRemoveFromCart(item, membership, membershipTaken, dispatch);

  //INVENTORY
  const _onServiceSelectedInventory = (selected, item) =>
    inventoryOnSelected(
      selected,
      item,
      inventories,
      inventoriesTaken,
      dispatch,
    );
  const _addToCartInventory = (item) =>
    inventoryAddToCart(item, inventories, inventoriesTaken, dispatch);
  const _removeFromCartInventory = (item) =>
    inventoryRemoveFromCart(item, inventories, inventoriesTaken, dispatch);
  /**
   * @param item
   * @private picker dropdown inventory..!
   */
  const _onPickerTouchInventory = (item) =>
    inventoryOnTouch(item, inventories, inventoriesTaken, dispatch);
  const _onPickerSelectInventory = (value, item) =>
    inventoryOnSelect(value, item, inventories, inventoriesTaken, dispatch);
  const _onPickerCloseInventory = () =>
    inventoryOnClose(inventories, inventoriesTaken, dispatch);

  const _onContinuePress = async () => {
    _makeEntryToFirebase();
    dispatch(
      updateTotalPrice(
        servicesTaken,
        membershipTaken,
        inventoriesTaken,
        slotsSelected,
      ),
    );
    navigation.navigate(ScreenNames.HOME_SUMMARY);
  };

  const _makeEntryToFirebase = () => {
    makeEntryToFirebaseForLockingSlot(
      court,
      slotsSelected,
      true,
      slotSelectedByOther,
    );
  };

  const _onSkipPress = async () => {
    _makeEntryToFirebase();
    dispatch(
      clearMembershipServiceInventory(
        membership,
        services,
        inventories,
        slotsSelected,
      ),
    );
    navigation.navigate(ScreenNames.HOME_SUMMARY);
  };

  return (
    <Container>
      <HeaderBack
        navigation={navigation}
        onPressSkip={() => _onSkipPress()}
        title={Content.services}
      />

      {ifValid(firstmember) && (
        <View style={commonStyle.detailsContainerforservices}>
          <Text style={commonStyle.narrow30}>{Content.learnFromExperts}</Text>
          <Text
            onPress={() => navigation.navigate(ScreenNames.MY_MEMBER)}
            style={styles.exploremore}>
            {Content.exploremore}
          </Text>
        </View>
      )}

      {ifValid(firstmember) ? (
        <MembershipCard
          addToCartMembership={(x) => _addToCartMembership(x)}
          removeFromCartMembership={(x) => _removeFromCartMembership(x)}
          name={firstmember?.name}
          duration={firstmember?.duration}
          item={firstmember}
        />
      ) : null}

      {loading ? (
        <LoaderAndMsg text={Content.lookingForServices} />
      ) : (
        <ScrollView
          style={{flex: 1, width: width, backgroundColor: colors.background}}>
          {services && services.length > 0 && (
            <View>
              {getLineView()}
              <ListHeader title={Content.AdditionalServices} />
              {services &&
                services.length > 0 &&
                services.slice(0, 2).map((item, index) => {
                  return (
                    <ServiceCard
                      isVisible={item.isVisible}
                      onPickerTouch={() => _onPickerTouchService(item)}
                      onPickerSelect={(value) =>
                        _onPickerSelectService(value, item)
                      }
                      onPickerClose={() => _onPickerCloseService(item)}
                      key={`key${item.id}${index}`}
                      onServiceSelected={(selected, item) =>
                        _onServiceSelected(selected, item)
                      }
                      addToCartPress={(x) => _addToCart(x)}
                      removeFromCartPress={(x) => _removeFromCart(x)}
                      name={item.name}
                      rate={item.rate}
                      item={item}
                      type={'ser'}
                    />
                  );
                })}
            </View>
          )}

          {services && services.length > 2 ? (
            <ExploreMore
              onPress={() => navigation.navigate(ScreenNames.HOME_SERVICES_ALL)}
            />
          ) : null}
          {getLineView()}

          {inventories && inventories.length > 0 && (
            <View>
              <ListHeader title={Content.AdditionalItems} />
              {inventories.slice(0, 2).map((item, index) => {
                return (
                  <ServiceCard
                    isVisible={item.isVisible}
                    onPickerTouch={() => _onPickerTouchInventory(item)}
                    onPickerSelect={(value) =>
                      _onPickerSelectInventory(value, item)
                    }
                    onPickerClose={() => _onPickerCloseInventory(item)}
                    onServiceSelected={(selected, index, item) =>
                      _onServiceSelectedInventory(selected, index, item)
                    }
                    addToCartPress={(x) => _addToCartInventory(x)}
                    key={`key${item.id}${index}`}
                    removeFromCartPress={(x) => _removeFromCartInventory(x)}
                    name={item.productName}
                    rate={item.mrpAmount}
                    item={item}
                    type={'inv'}
                  />
                );
              })}
            </View>
          )}

          {inventories && inventories.length > 2 ? (
            <ExploreMore
              onPress={() =>
                navigation.navigate(ScreenNames.HOME_INVENTORIES_ALL)
              }
            />
          ) : null}
          {getLineView()}
        </ScrollView>
      )}
      <ButtonCommon
        style={{
          backgroundColor: !(
            ifGreaterArray(servicesTaken) ||
            ifGreaterArray(membershipTaken) ||
            ifGreaterArray(inventoriesTaken)
          )
            ? '#b5b5b5'
            : colors.primaryGreen,
        }}
        disabled={
          !(
            ifGreaterArray(servicesTaken) ||
            ifGreaterArray(membershipTaken) ||
            ifGreaterArray(inventoriesTaken)
          )
        }
        text={'Continue'}
        onPress={() => _onContinuePress()}
      />
    </Container>
  );
};
export default HomeServices;

const styles = StyleSheet.create({
  container: {},

  button: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  exploremore: {
    position: 'absolute',
    right: widthToDp(2.5),
    ...commonStyle.regular24,
    color: colors.darkGreen,
    textAlignVertical: 'bottom',
    marginTop: heightToDp('2%'),
  },
});
