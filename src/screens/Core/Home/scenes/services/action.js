import axiosService from '../../../../../commons/axiosService';
import { ifNotValid, ifValid, showToastMsg } from '../../../../../utils/helper';
import { errorLog, successLog } from '../../../../../utils/fireLog';
import { setFinalPrice } from '../bookings/action';
import { updateMembershipTaken } from '../venues/venue_detail/action';

export const getServicesInventories = (siteId) => {
  return (dispatch) => {
    dispatch(setServicesInventories([], []));
    // dispatch(setLoadingServices(true));
    try {
      axiosService
        .get(`/api/services/site/${siteId}`)
        //.get(`/api/services/${siteId}`)
        .then((res) => {
          if (res.status === 200) {
            res = res.data;
            if (ifNotValid(res)) {
              errorLog('invalid response ');
              return;
            }
            if (ifValid(res.data[0])) {
              let _services = res.data[0].map((x) => {
                return {
                  ...x,
                  quantitySelected: '1',
                  addedToCart: false,
                  isVisible: false,
                };
              });
              // dispatch(setServices(_services));
              dispatch(getInventories(siteId, _services));
            }
          } else {
            // dispatch(setLoadingServices(false));
            console.log('not valid response ' + JSON.stringify(res));
          }
        })
        .catch((o) => {
          // dispatch(setLoadingServices(false));
          console.log('getServices error ' + o);
        });
    } catch (e) {
      errorLog(e);
    }
  };
};

export const getInventories = (siteId, services) => {
  return (dispatch) => {
    try {
      axiosService
        .get(`/api/inventories/findInventoryBySiteId/${siteId}`)
        .then((res) => {
          if (res.status === 200) {
            res = res?.data;
            if (ifNotValid(res)) {
              errorLog('invalid response ');
              return;
            }
            if (ifValid(res?.data[0])) {
              let _inventories = res.data[0].map((x) => {
                return {
                  ...x,
                  quantitySelected: '1',
                  isVisible: false,
                  addedToCart: false,
                };
              });
              _inventories = _inventories.filter((x) => x.quantity > 0);

              dispatch(setServicesInventories(services, _inventories));
            }
          } else {
            console.log('not valid response ' + JSON.stringify(res));
          }
          // dispatch(setLoadingServices(false));
        })
        .catch((o) => {
          console.log('getServices error ' + o);
          // dispatch(setLoadingServices(false));
        });
    } catch (e) {
      // dispatch(setLoadingServices(false));
      errorLog(e);
    }
  };
};

export const setServicesInventories = (services = [], inventories = []) => {
  return (dispatch) => {
    dispatch({
      type: 'SET_SERVICES_INVENTORIES',
      payload: {
        services,
        inventories,
      },
    });
  };
};

export const setServices = (data) => {
  return (dispatch) => {
    dispatch({ type: 'SET_SERVICES', payload: data });
  };
};

export const setLoadingServices = (value) => {
  return (dispatch) => {
    dispatch({ type: 'SET_LOADING_SERVICES', payload: value });
  };
};

export const setInventories = (data) => {
  return (dispatch) => {
    dispatch({ type: 'SET_INVENTORIES', payload: data });
  };
};

export const setIsPickerVisible = (data) => {
  return (dispatch) => {
    dispatch({ type: 'SET_IS_PICKER_VISIBLE', payload: data });
  };
};

export const updateServicesTaken = (servicesTaken, services) => {
  return (dispatch) => {
    dispatch({
      type: 'UPDATE_SERVICES_TAKEN',
      payload: {
        servicesTaken: servicesTaken,
        services: services,
      },
    });
  };
};

export const updateInventoriesTaken = (inventoriesTaken, inventories) => {
  return (dispatch) => {
    dispatch({
      type: 'UPDATE_INVENTORIES_TAKEN',
      payload: {
        inventoriesTaken: inventoriesTaken,
        inventories: inventories,
      },
    });
  };
};
//// service add update and remove section
export const serviceOnSelected = (
  selected,
  item,
  services,
  servicesTaken,
  dispatch,
) => {
  let _services = services.map((x) =>
    x.name === item.name ? { ...x, quantitySelected: selected } : x,
  );
  let _servicesTaken = servicesTaken.map((x) =>
    x.name === item.name ? { ...x, quantitySelected: selected } : x,
  );
  dispatch(updateServicesTaken(_servicesTaken, _services));
};

export const serviceAddToCart = (item, services, servicesTaken, dispatch) => {
  let _services = services.map((x) =>
    x.name === item.name ? { ...x, addedToCart: !x.addedToCart } : x,
  );
  dispatch(updateServicesTaken([...servicesTaken, item], _services));
  showToastMsg(`Added ${item.name} in cart`, '', 'success', 500);
};

export const serviceOnTouch = (item, services, servicesTaken, dispatch) => {
  let _services = services.map((x) =>
    x.id === item.id ? { ...x, isVisible: !item.isVisible } : x,
  );
  dispatch(updateServicesTaken(servicesTaken, _services));
};
export const serviceOnSelect = (
  value,
  item,
  services,
  servicesTaken,
  dispatch,
) => {
  let _services = services.map((x) =>
    x.id === item.id ? { ...x, quantitySelected: value, isVisible: false } : x,
  );
  let _servicesTaken = servicesTaken.map((x) =>
    x.id === item.id ? { ...x, quantitySelected: value } : x,
  );
  dispatch(updateServicesTaken(_servicesTaken, _services));
};

export const serviceOnClose = (services, servicesTaken, dispatch) => {
  let _services = services.map((x) => {
    return { ...x, isVisible: false };
  });
  dispatch(updateServicesTaken(servicesTaken, _services));
};

export const serviceRemoveFromCart = (
  item,
  services,
  servicesTaken,
  dispatch,
) => {
  let _servicesTaken = servicesTaken.filter((x) => x.id !== item.id);
  let _services = services.map((x) =>
    x.name === item.name ? { ...x, addedToCart: !x.addedToCart } : x,
  );
  dispatch(updateServicesTaken(_servicesTaken, _services));
  showToastMsg(`Removed ${item.name} from cart`, '', 'success', 500);
  return _servicesTaken;
};

export const clearMembershipServiceInventory = (
  membership,
  services,
  inventories,
  slotsSelected,
) => {
  return (dispatch) => {
    let _membership = membership.map((x) => {
      return { ...x, addedToCart: false, quantitySelected: '' };
    });
    let _services = services.map((x) => {
      return { ...x, addedToCart: false, quantitySelected: '' };
    });
    let _inventories = inventories.map((x) => {
      return { ...x, addedToCart: false, quantitySelected: '' };
    });

    dispatch({
      type: 'CLEAR_CART',
      payload: {
        membership: _membership,
        services: _services,
        inventories: _inventories,
      },
    });
    dispatch(updateTotalPrice([], [], [], slotsSelected));

  };
};

//// membership add update and remove section
export const membershipAddToCart = (
  item,
  membership,
  membershipTaken,
  dispatch,
) => {
  let _membership = membership.map((x) =>
    x.id === item.id ? { ...x, addedToCart: !x.addedToCart } : x,
  );
  dispatch(updateMembershipTaken([...membershipTaken, item], _membership));
  showToastMsg(`Added ${item.name} in cart`, '', 'success', 500);
};

export const membershipRemoveFromCart = (
  item,
  membership,
  membershipTaken,
  dispatch,
) => {
  let _membershipTaken = membershipTaken.filter((x) => x.id !== item.id);
  let _membership = membership.map((x) =>
    x.id === item.id ? { ...x, addedToCart: !x.addedToCart } : x,
  );
  dispatch(updateMembershipTaken(_membershipTaken, _membership));
  showToastMsg(`Removed ${item.name} from cart`, '', 'success', 500);
  return _membershipTaken;
};

//// inventory add update and remove section
export const inventoryOnSelected = (
  selected,
  item,
  inventories,
  inventoriesTaken,
  dispatch,
) => {
  let _inventories = inventories.map((x) =>
    x.id === item.id ? { ...x, quantitySelected: selected } : x,
  );
  let _inventoriesTaken = inventoriesTaken.map((x) =>
    x.id === item.id ? { ...x, quantitySelected: selected } : x,
  );
  dispatch(updateInventoriesTaken(_inventoriesTaken, _inventories));
};
export const inventoryOnTouch = (
  item,
  inventories,
  inventoriesTaken,
  dispatch,
) => {
  let _inventories = inventories.map((x) =>
    x.id === item.id ? { ...x, isVisible: !item.isVisible } : x,
  );
  dispatch(updateInventoriesTaken(inventoriesTaken, _inventories));
};

export const inventoryOnSelect = (
  value,
  item,
  inventories,
  inventoriesTaken,
  dispatch,
) => {
  let _inventories = inventories.map((x) =>
    x.id === item.id ? { ...x, quantitySelected: value, isVisible: false } : x,
  );
  let _inventoriesTaken = inventoriesTaken.map((x) =>
    x.id === item.id ? { ...x, quantitySelected: value } : x,
  );
  dispatch(updateInventoriesTaken(_inventoriesTaken, _inventories));
};

export const inventoryOnClose = (inventories, inventoriesTaken, dispatch) => {
  let _inventories = inventories.map((x) => {
    return { ...x, isVisible: false };
  });
  dispatch(updateInventoriesTaken(inventoriesTaken, _inventories));
};

export const inventoryAddToCart = (
  item,
  inventories,
  inventoriesTaken,
  dispatch,
) => {
  let _inventories = inventories.map((x) =>
    x.id === item.id ? { ...x, addedToCart: !x.addedToCart } : x,
  );
  dispatch(updateInventoriesTaken([...inventoriesTaken, item], _inventories));
  showToastMsg(`Added ${item.productName} in cart`, '', 'success', 500);
};

export const inventoryRemoveFromCart = (
  item,
  inventories,
  inventoriesTaken,
  dispatch,
) => {
  let _inventoriesTaken = inventoriesTaken.filter((x) => x.id !== item.id);
  let _inventories = inventories.map((x) =>
    x.id === item.id ? { ...x, addedToCart: !x.addedToCart } : x,
  );
  dispatch(updateInventoriesTaken(_inventoriesTaken, _inventories));
  showToastMsg(`Removed ${item.productName} from cart`, '', 'success', 500);
  return _inventoriesTaken;
};

export const updateTotalPrice = (
  servicesTaken,
  membershipTaken,
  inventoriesTaken,
  slotsSelected = [],
) => {
  return (dispatch) => {
    let sumServices = servicesTaken
      .map((x) => {
        let quantitySelected =
          x.quantitySelected !== '' ? parseInt(x.quantitySelected) : 1;
        return x.rate * quantitySelected;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
    successLog('sumServices', sumServices);

    let sumMembership = membershipTaken
      .map((x) => {
        return x.priceToPay;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
    successLog('sumMembership', sumMembership);

    let totalSumMembership = membershipTaken
      .map((x) => {
        return x.amount;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
    successLog('sumMembership', sumMembership);

    let sumInventories = inventoriesTaken
      .map((x) => {
        let quantitySelected =
          x.quantitySelected !== '' ? parseInt(x.quantitySelected) : 1;
        return x.mrpAmount * quantitySelected;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
    successLog('sumInventories', sumInventories);

    successLog(slotsSelected);
    let slotsSelectedTemp = slotsSelected.filter((x) => x.isCalulable === true && x.isSelected === true)

    let _slotsSelected =
    slotsSelectedTemp &&
    slotsSelectedTemp
        .map((x) => {
      
          return x.offerRate;
        })
        .reduce(function (a, b) {
       
          return a + b;
        }, 0);
    dispatch(
      setFinalPrice(
        sumServices + sumMembership + sumInventories + _slotsSelected,
      ),
    );
  
    dispatch({ type: 'SET_TOTAL_AMOUNT', payload: (sumServices + totalSumMembership + sumInventories + _slotsSelected) });
  };
};
