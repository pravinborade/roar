import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {Container, HeaderBack, Line} from '../../../../../../components';
import {Content, ScreenNames, widthToDp} from '../../../../../../utils';
import {colors, commonStyle} from '../../../../../../styles';
import Images from '../../../../../../assets/Images/Images';
import {useDispatch, useSelector} from 'react-redux';
import {membershipAddToCart, membershipRemoveFromCart} from '../action';
import {ifValid, showToastMsg} from '../../../../../../utils/helper';
import {setFromPayment} from '../../../../../../redux/actions/login';
import {styles as stylesMember} from '../../membership/membership_all/comp/style';

const ServiceData = ({navigation}) => {
  const dispatch = useDispatch();
  const {membership, membershipTaken} = useSelector(
    (state) => state.homeServices,
  );
  let userData = useSelector((x) => x.login.data);

  const _addToCart = (item) => {
    if (!ifValid(userData.id)) {
      dispatch(setFromPayment('membership_services_all'));
      navigation?.navigate(ScreenNames.LOGIN);
      return;
    }

    if (item?.isMember) {
      showToastMsg('Membership', 'This membership is already purchased');
      return;
    }
    membershipAddToCart(item, membership, membershipTaken, dispatch);
  };

  const _removeFromCart = (item) =>
    membershipRemoveFromCart(item, membership, membershipTaken, dispatch);

  const renderMembersNew = (item) => {
    let months = ifValid(item.duration) ? item.duration : '';
    let amount = ifValid(item.amount) ? item.amount : '';
    let title = ifValid(item.name) ? item.name : '';
    let subtitle = ifValid(item.membershipType) ? item.membershipType : '';
    let flatRate = ifValid(item.flatRate) ? item.flatRate : '';
    let discountPercentage = ifValid(item.discountPercentage)
      ? item.discountPercentage
      : '';
    let packageFlatRate = ifValid(item.packageFlatRate)
      ? item.packageFlatRate
      : '';
    let packageDiscount = ifValid(item.packageDiscount)
      ? item.packageDiscount
      : '';
    let priceToPay = ifValid(item.priceToPay) ? item.priceToPay : '';

    return (
      <View style={{...stylesMember.mainContainer, backgroundColor: '#E5ECF9'}}>
        <View style={stylesMember.mainSubContainer}>
          <View style={stylesMember.view_one}>
            <Image
              style={stylesMember.imageStyle_one}
              source={Images.blueImg}
            />
          </View>
          <View style={stylesMember.view_two}>
            <Text style={stylesMember.title}>{title}</Text>
            <Text style={stylesMember.subtitle}> {subtitle} </Text>
          </View>
          <Image style={stylesMember.imageStyle} source={Images.blueImg} />
        </View>

        <View style={stylesMember.rowStyle}>
          <Text style={[commonStyle.narrow28]}>{Content.rupies}</Text>
          <Text style={commonStyle.regular24}>{amount} </Text>
          <Text
            style={{
              ...commonStyle.regular16,
              color: colors.primaryBlue,
            }}
          >
            {' '}
            ({months})
          </Text>
        </View>

        {flatRate !== '' ? (
          <View style={stylesMember.rowStyle}>
            <Text style={commonStyle.narrow28}>Slots {Content.flatRate}</Text>
            <Text style={commonStyle.regular24}>{flatRate} Rs</Text>
          </View>
        ) : null}

        {discountPercentage !== '' ? (
          <View style={stylesMember.rowStyle}>
            <Text style={commonStyle.narrow28}>
              Slots {Content.discountService}
            </Text>
            <Text style={commonStyle.regular24}>{discountPercentage}%</Text>
          </View>
        ) : null}

        {packageFlatRate !== '' ? (
          <View style={stylesMember.rowStyle}>
            <Text style={commonStyle.narrow28}>Membership Flat Rate: </Text>
            <Text style={commonStyle.regular24}>{packageFlatRate} Rs</Text>
          </View>
        ) : null}

        {packageDiscount !== '' ? (
          <View style={stylesMember.rowStyle}>
            <Text style={commonStyle.narrow28}>Membership Discount: </Text>
            <Text style={commonStyle.regular24}>{packageDiscount}%</Text>
          </View>
        ) : null}

        <Line lineColor={'gray'} />

        <View style={stylesMember.bottomContainer}>
          {/* <View style={stylesMember.bottomSubContainer}>
            <Text style={commonStyle.narrow22}>
              {Content.rupies} {amount}{' '}
            </Text>
            <Text style={{...commonStyle.regular16, color: colors.primaryBlue}}>
              {' '}
              ({months})
            </Text>
          </View> */}
          <View style={stylesMember.bottomSubContainer}>
            <Text style={[commonStyle.narrow22, {color: colors.primaryBlue}]}>
              {'Price To Pay: '} {priceToPay}{' '}
            </Text>
          </View>

          <View style={styles.dropDownAndButtonServicesContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                item.addedToCart ? _removeFromCart(item) : _addToCart(item);
              }}
            >
              <Text style={commonStyle.narrow28}>
                {item.addedToCart ? 'Remove' : Content.add}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <Container>
      <HeaderBack navigation={navigation} title={Content.membership} />
      <FlatList
        data={membership}
        showsVerticalScrollIndicator={false}
        style={{backgroundColor: colors.background}}
        renderItem={({item}, position) => renderMembersNew(item, position)}
        keyExtractor={(item, index) => index.toString()}
      />
    </Container>
  );
};
export default ServiceData;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    marginHorizontal: widthToDp(4.5),
    marginVertical: widthToDp(1.5),
  },
  button: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  dropdownContainer: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  dropDownAndButtonContainer: {
    flexDirection: 'row',
    width: '85%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dropDownAndButtonServicesContainer: {
    alignSelf: 'flex-end',
    right: widthToDp('2%'),
  },

  imageStyle: {
    margin: widthToDp(2),
    width: widthToDp(15),
    height: widthToDp(15),
    backgroundColor: colors.background,
  },
  rowStyle: {
    flexDirection: 'row',
    marginVertical: widthToDp(0.5),
  },
});
