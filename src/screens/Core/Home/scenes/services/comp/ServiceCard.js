import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../styles';
import {widthToDp} from '../../../../../../utils/Responsive';
import Images from '../../../../../../assets/Images/Images';
import DropDownSmall from './DropdownSmall';
import {Content} from '../../../../../../utils/content/Content';
import {
  generateKey,
  ifValid,
  showToastMsg,
} from '../../../../../../utils/helper';
import validate from 'validate.js';

const ServiceCard = ({
  item,
  index,
  type = '-',
  name = '-',
  rate = 0,
  isVisible,
  onPickerSelect,
  onPickerClose,
  onPickerTouch,
  addToCartPress,
  removeFromCartPress,
  onServiceSelected,
}) => {
  let checkUrl = validate({website: item.image}, {website: {url: true}});
  let image =
    ifValid(item.image) && item.image !== '' && checkUrl?.website === undefined
      ? {uri: item.image}
      : Images.product;
  console.log(item.quantitySelected + ' in servciecard');
  return (
    <View key={generateKey(item.id, type)} style={styles.container}>
      <Image style={styles.imageStyle} source={image} />
      <View
        style={{
          width: widthToDp(80),
          marginVertical: widthToDp(2),
        }}>
        <Text style={commonStyle.regular24}>{name}</Text>
        <Text style={commonStyle.narrow30}>Rs {rate}</Text>
        <View style={styles.dropDownAndButtonContainer}>
          <View>
            {ifValid(item?.quantity) && item.quantity <= 0 ? (
              <Text style={commonStyle.regular24}>currently out of stock</Text>
            ) : (
              <DropDownSmall
                quantity={item?.quantity}
                key={index}
                onPickerTouch={onPickerTouch}
                onPickerSelect={onPickerSelect}
                onPickerClose={onPickerClose}
                isVisible={isVisible}
                value={item.quantitySelected.toString()}
                title={Content.qty}
              />
            )}
          </View>
          {ifValid(item?.quantity) && item.quantity <= 0 ? null : (
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                if (rate === 0) {
                  showToastMsg('invalid price');
                  return;
                }
                item.addedToCart
                  ? removeFromCartPress(item)
                  : addToCartPress(item);
              }}>
              <Text style={commonStyle.narrow28}>
                {item.addedToCart ? 'Remove' : Content.add}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};
export default ServiceCard;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    marginHorizontal: widthToDp(4.5),
    marginVertical: widthToDp(1.5),
  },
  button: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  dropdownContainer: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  dropDownAndButtonContainer: {
    flexDirection: 'row',
    width: '85%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  imageStyle: {
    margin: widthToDp(2),
    width: widthToDp(18),
    height: widthToDp(18),
  },
});
