import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../styles';
import {heightToDp, width, widthToDp} from '../../../../../../utils/Responsive';
import Images from '../../../../../../assets/Images/Images';
import {Content} from '../../../../../../utils/content/Content';
import {generateKey, ifValid} from '../../../../../../utils/helper';

const MembershipCard = ({
  item,
  name = '-',
  duration = '',
  addToCartMembership,
  removeFromCartMembership,
}) => {
  let flatRate = ifValid(item.flatRate) ? item.flatRate : '';
  let discountPercentage = ifValid(item.discountPercentage)
    ? item.discountPercentage
    : '';

  return (
    <View key={generateKey(item.id, 'mem')} style={{}}>
      <Image source={Images.sampleBooking} style={styles.imageStyle} />
      <View style={[commonStyle.detailsContainer, styles.container]}>
        <View
          style={{width: '85%', flexDirection: 'row', alignItems: 'center'}}>
          <Text style={commonStyle.regular24}>{name} </Text>
          <Text style={commonStyle.regular24}>( {duration} )</Text>
        </View>

        <View style={{flexDirection: 'row'}}>
          {flatRate !== '' ? (
            <View style={styles.rowStyle}>
              <Text style={commonStyle.narrow28}>Flat Rate: </Text>
              <Text style={commonStyle.regular24}>{flatRate} Rs</Text>
            </View>
          ) : null}

          {discountPercentage !== '' ? (
            <View style={styles.rowStyle}>
              <Text style={commonStyle.narrow28}>Discount: </Text>
              <Text style={commonStyle.regular24}>{discountPercentage}%</Text>
            </View>
          ) : null}

          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              item.addedToCart
                ? removeFromCartMembership(item)
                : addToCartMembership(item);
            }}>
            <Text style={commonStyle.narrow28}>
              {item.addedToCart ? 'Remove' : Content.add}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
export default MembershipCard;

const styles = StyleSheet.create({
  imageStyle: {
    width: width,
    height: heightToDp(25),
  },
  button: {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(2.9),
  },
  rowStyle: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: widthToDp(0.5),
  },
});
