import React from 'react';
import {
  FlatList,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {colors, commonStyle} from '../../../../../../styles';
import {fontToDp, height, widthToDp} from '../../../../../../utils/Responsive';
import {useDispatch} from 'react-redux';
import {
  getNumberOfQuantityItems,
  ifValid,
} from '../../../../../../utils/helper';
import Line from '../../../../../../components/Line';
import Images from '../../../../../../assets/Images/Images';

const DropDownSmall = ({
  title = '',
  quantity = 10,
  isVisible = false,
  value = '',
  onPickerTouch,
  onPickerSelect,
  onPickerClose,
  containerStyle,
  disabled = false,
}) => {
  const dispatch = useDispatch();

  let _quantity = ifValid(quantity) && quantity !== '' ? quantity : 0;
  //if greater than 10 show 10 else available quantity

  if (_quantity >= 10 && title !== 'Ticket Qty') {
    _quantity = 10;
  }
  const pickerView = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {}}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => onPickerClose()}
          style={styles.centeredView}
        >
          <View style={styles.modalView}>
            {
              <FlatList
                scrollEnabled
                showsVerticalScrollIndicator={true}
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={(props) => <Line />}
                data={getNumberOfQuantityItems(_quantity)}
                renderItem={({item}) => (
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => onPickerSelect(item.value)}
                    style={styles.flatListItem}
                  >
                    <Text
                      style={[
                        commonStyle.regular26,
                        {
                          flex: 8,
                          paddingVertical: widthToDp(2.6),
                          paddingHorizontal: widthToDp(3.5),
                          color: item.value ? '#038BEF' : 'black',
                        },
                      ]}
                    >
                      {item.label}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            }
          </View>
        </TouchableOpacity>
      </Modal>
    );
  };

  return (
    <View
      style={[{flexDirection: 'row', alignItems: 'center'}, containerStyle]}
    >
      <Text
        style={[
          commonStyle.regular20,
          {
            color: colors.primaryGrey,
            fontSize: fontToDp(11),
            paddingRight: widthToDp(1),
          },
        ]}
      >
        {title}
      </Text>
      <TouchableOpacity
        activeOpacity={1}
        onPress={onPickerTouch}
        style={[
          {
            flexDirection: 'row',
            paddingHorizontal: widthToDp(2),
          },
          styles.dropDownContainer,
        ]}
      >
        <Text style={[commonStyle.regular24, {flex: 1}]}>
          {value === '' ? '1' : value}
        </Text>
        <Image
          source={Images.arrowBottom}
          style={{width: widthToDp(2), height: widthToDp(2)}}
        />
      </TouchableOpacity>
      {pickerView()}
    </View>
  );
};
export default DropDownSmall;

const styles = StyleSheet.create({
  dropDownContainer: {
    alignItems: 'center',
    height: widthToDp(5),
    borderRadius: 2,
    justifyContent: 'center',
    margin: widthToDp(0.5),
    width: widthToDp(15),
    borderWidth: 0.5,
    borderColor: 'black',
  },
  dropDown: {
    marginRight: widthToDp(-3),
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: widthToDp(4.5),
    backgroundColor: 'white',
    borderRadius: 2,
    height: height / 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  flatListItem: {
    paddingVertical: widthToDp(2),
  },
});
