import {BackHandler, Image, StyleSheet, Text} from 'react-native';
import React, {useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Content} from '../../../../../../../utils/content/Content';
import Images from '../../../../../../../assets/Images/Images';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {ackStyle} from '../../../../comp/ackStyle';

const ServiceAcknowledgement = (props) => {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  function handleBackButtonClick(): boolean {
    resetStack();
    return true;
  }

  const resetStack = () =>
    props.navigation.reset({
      index: 0,
      key: null,
      routes: [{name: ScreenNames.HOME}],
    });

  return (
    <LinearGradient
      colors={['#ECFFC3', '#AFFF00']}
      style={ackStyle.linearGradient}>
      <Image style={ackStyle.logo} source={Images.myserviceicon} />
      <Text style={ackStyle.yayStyle}>{Content.yay}</Text>
      <Text style={ackStyle.slotBookedStyl}>{Content.yourServices}</Text>
      <TouchableOpacity
        style={ackStyle.button}
        onPress={() => {
          props.navigation.navigate(ScreenNames.MY_SERVICES);
          // resetStack();
        }}>
        <Text style={ackStyle.buttonText}>{Content.MyServices}</Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};
export default ServiceAcknowledgement;

const styles = StyleSheet.create({
  container: {},
});
