import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Images from '../../../../../../../assets/Images/Images';
import React from 'react';
import {
  fontToDp,
  heightToDp,
  widthToDp,
} from '../../../../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../../../../styles';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {calculateRating, ifValid} from '../../../../../../../utils/helper';
import {useDispatch, useSelector} from 'react-redux';
import {setSelectedVenue} from '../action';
import {getAminities} from '../../venue_detail/action';
import {clearSlotsAndSlotSportDateTime} from '../../../bookings/action';
import {Content} from '../../../../../../../utils/content/Content';
import validate from 'validate.js';
import Rating from 'react-native-easy-rating';
import {successLog} from '../../../../../../../utils';
import RatingComp from '../../venue_detail/comp/RatingComp';

export const RenderItem = ({navigation, item = {}, userDetails = {}}) => {
  const dispatch = useDispatch();

  let locationArea = ifValid(item?.area) ? item.area : '';
  let distance =
    ifValid(item?.distance) && item.distance !== -1
      ? ` | ${item?.distance} Km`
      : '';
  let id = ifValid(item.id) ? item.id : undefined;
  let checkUrl = validate({website: item?.image}, {website: {url: true}});
  let image =
    ifValid(item?.image) &&
    item?.image !== '' &&
    checkUrl?.website === undefined
      ? {uri: item?.image}
      : Images.sampleBooking;
  let title = ifValid(item?.name) ? item.name : '';
  let {selectedSport} = useSelector((x) => x.homeVenue);
  let amount = ifValid(item?.amount) ? item.amount : '';
  let rating = ifValid(item?.rating) ? item?.rating : 0;

  const _onPressBook = () => {
    dispatch(clearSlotsAndSlotSportDateTime());
    dispatch(setSelectedVenue(item));
    dispatch(
      getAminities(id, ifValid(userDetails) ? userDetails.id : undefined),
    );
    navigation.navigate(ScreenNames.VENUE_DETAILS);
  };

  return (
    <TouchableOpacity
      style={styles.renderContainer}
      onPress={() => _onPressBook()}>
      <Image source={image} style={styles.imageBox} />
      <View style={styles.cardContent}>
        <View style={styles.textContainer}>
          <Text numberOfLines={1} style={commonStyle.narrow32}>
            {title}
          </Text>
          <Text numberOfLines={1}>
            {locationArea}
            {distance}
          </Text>
          {amount !== '' ? (
            <Text numberOfLines={1} style={commonStyle.narrow22}>
              Rs {amount}
            </Text>
          ) : null}
          {rating !== 0 ? (
            <View
              style={{...commonStyle.rowAlignCenter, marginTop: widthToDp(1)}}>
              <Text style={styles.ratingText}>{rating}</Text>
              <RatingComp ratingCount={rating} imageSize={3} max={5} />
            </View>
          ) : null}
          <Text />
        </View>
        <View style={styles.buttonContainer}>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            {item.sportPlayedForIcons &&
              item.sportPlayedForIcons.slice(0, 3).map((x, index) => {
                return (
                  <Image
                    key={index.toString()}
                    source={x.image}
                    style={styles.imageStyle}
                  />
                );
              })}
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => _onPressBook()}>
            <Text style={styles.buttonTextStyle}>{Content.book}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const paddingSpace = {
  paddingHorizontal: widthToDp('1'),
  justifyContent: 'space-between',
  paddingVertical: widthToDp(1),
};
const height = 16;
const styles = StyleSheet.create({
  renderContainer: {
    paddingRight: 3,
    flexDirection: 'row',
    height: heightToDp(height),
    backgroundColor: colors.white,
    marginHorizontal: widthToDp(4.5),
    marginVertical: widthToDp('3'),
  },
  ratingText: {
    ...commonStyle.regular20,
    alignSelf: 'center',
    marginRight: widthToDp(1),
    color: colors.darkGreen,
    fontSize: fontToDp(11),
  },
  imageBox: {
    height: heightToDp(height),
    width: widthToDp('32%'),
    resizeMode: 'stretch',
    borderRadius: 10,
  },
  cardContent: {
    flexDirection: 'row',
    height: '100%',
  },
  textContainer: {
    width: widthToDp('38%'),
    marginLeft: '2%',
    height: '100%',
    justifyContent: 'space-between',
    ...paddingSpace,
  },
  buttonContainer: {
    marginVertical: widthToDp('3'),
    width: widthToDp('20%'),
    ...paddingSpace,
  },
  buttonStyle: {
    paddingVertical: widthToDp(1),
    backgroundColor: colors.primaryGreen,
    alignItems: 'center',
  },
  buttonTextStyle: {
    ...commonStyle.narrow22,
  },

  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp(4),
    height: widthToDp(4),
  },
});
