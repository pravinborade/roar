import {
  Alert,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
  Line,
} from '../../../../../../../components';
import Images from '../../../../../../../assets/Images/Images';
import {
  Content,
  errorLog,
  fontToDp,
  height,
  heightToDp,
  widthToDp,
} from '../../../../../../../utils';
import {colors, commonStyle} from '../../../../../../../styles';
import {useDispatch, useSelector} from 'react-redux';
import CheckBox from '@react-native-community/checkbox';
import {
  getLineView,
  ifNotValid,
  ifValid,
  showToastMsg,
} from '../../../../../../../utils/helper';
import {
  getVenuesFilteredApi,
  resetFilter,
  setDistanceSelected,
  setPopularitySelected,
  setSelectedFilterSportAndValue,
} from './action';
import {NotAvailable} from '../../../../../../../components/';
import {getVenuesList, setActiveSport} from '../../../sports/sport_all/action';
import {getSportImage} from '../../../summary/comp/action';

const VenueFilterScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const [arrowRight, setArrowRight] = useState(true);
  let {
    sportsFilterList,
    sportsSelected,
    activeSport,
    filterOriginal,
    popularitySelected,
    distanceSelected,
  } = useSelector((state) => state.homeVenue);
  let latLong = useSelector((state) => state.login.latLong);
  let sportsSelectedName =
    ifValid(sportsSelected) && ifValid(sportsSelected.name)
      ? sportsSelected.name
      : '';
  filterOriginal =
    ifValid(filterOriginal) && filterOriginal.length > 0 ? filterOriginal : [];

  const _onSelectedSpecialityChange = (item) => {
    setSelectedFilterSport(item);
    setArrowRight(!arrowRight);
  };

  const setSelectedFilterSport = (item) => {
    let result = sportsFilterList.map((x) => {
      return x.id === item.id ? {...x, value: !x.value} : {...x, value: false};
    });
    dispatch(
      setSelectedFilterSportAndValue(result, {...item, value: !item.value}),
    );
  };

  useEffect(() => {
    let activeSportObject =
      sportsFilterList && sportsFilterList.find((x) => x.name === activeSport);
    if (ifNotValid(activeSportObject)) {
      errorLog('not valid activeSportObject');
      return;
    }
    setSelectedFilterSport(activeSportObject);
  }, []);

  const _onShowResultPress = () => {
    if (sportsSelectedName !== '') {
      dispatch(
        getVenuesFilteredApi(sportsSelectedName, latLong, popularitySelected),
      );
    } else {
      dispatch(getVenuesList(latLong, popularitySelected, true));
    }
    navigation.goBack();
  };

  const handleBackClick = () => {
    if (sportsSelectedName !== '' || distanceSelected === true) {
      Alert.alert(
        'Filter',
        'One or more filter selected, Do you want to clear filter selected?',
        [
          {
            text: 'OK',
            onPress: () => {
              dispatch(resetFilter(sportsFilterList));
              dispatch(getVenuesList(latLong));
              navigation.goBack();
            },
          },
          {
            text: 'Cancel',
            onPress: () => {},
            style: 'cancel',
          },
        ],
      );
    } else {
      navigation.goBack();
    }
  };

  const _onBySportsClick = () => {
    if (activeSport !== 'ALL') {
      showToastMsg(`Filter screen`, `${activeSport} is currently active`);
      return;
    }
    setArrowRight(!arrowRight);
  };

  const _onResetClick = () => {
    dispatch(resetFilter(sportsFilterList));
    dispatch(setActiveSport('ALL'));
    dispatch({
      type: 'SET_FILTERED_BY_SPORT_VENUES',
      payload: {sportVenues: filterOriginal, sportVenuesCopy: filterOriginal},
    });
  };

  return (
    <Container>
      <HeaderBack
        navigation={navigation}
        image={Images.crossRed}
        title={'Filters'}
      />
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={commonStyle.bold28}>{Content.setPre}</Text>
          <TouchableOpacity onPress={() => _onResetClick()}>
            <Text style={[commonStyle.bold24, {color: colors.darkGreen}]}>
              {Content.resett}
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            style={[
              styles.filterRow,
              {
                paddingVertical:
                  ifNotValid(sportsSelectedName) || sportsSelectedName === ''
                    ? widthToDp(4.5)
                    : widthToDp(2),
              },
            ]}
            onPress={() => _onBySportsClick()}>
            <Text style={[commonStyle.bold25, {color: '#505C74'}]}>
              {Content.bySport}
            </Text>
            <Image
              source={arrowRight ? Images.arrowRight : Images.arrowBottom}
              style={{
                width: arrowRight ? widthToDp(2.5) : widthToDp(2.7),
                height: arrowRight ? widthToDp(2.5) : widthToDp(2.7),
                resizeMode: 'cover',
              }}
            />
          </TouchableOpacity>

          {ifValid(sportsSelectedName) &&
          sportsSelectedName !== '' &&
          sportsSelected.value ? (
            <TouchableOpacity
              activeOpacity={1}
              style={[
                styles.filterRow,
                {paddingBottom: widthToDp(2), paddingTop: 0, marginTop: 0},
              ]}
              onPress={() => _onBySportsClick()}>
              <View style={styles.sportsSelected}>
                <Image
                  source={getSportImage(sportsSelectedName)}
                  style={styles.imageStyleSelected}
                />
                <Text style={commonStyle.bold24}>{sportsSelectedName}</Text>
              </View>
            </TouchableOpacity>
          ) : null}
        </View>
        {getLineView()}

        {!arrowRight ? (
          <View style={{height: height / 3}}>
            <FlatList
              scrollEnabled
              showsVerticalScrollIndicator={true}
              style={{marginLeft: widthToDp(4.5)}}
              keyExtractor={(item, index) => index.toString()}
              data={sportsFilterList}
              ListEmptyComponent={() => <NotAvailable msg={Content.noGames} />}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => _onSelectedSpecialityChange(item)}
                  style={styles.titleCheckContainer}>
                  <Text
                    style={[
                      commonStyle.regular26,
                      {
                        flex: 8,
                        color: item.value ? '#038BEF' : 'black',
                      },
                    ]}>
                    {item.name}
                  </Text>
                  <Image
                    source={item.value ? Images.checkBlue : Images.checkWhite}
                    style={styles.checkBoxStyle}
                  />
                </TouchableOpacity>
              )}
            />
          </View>
        ) : null}
        <Line />
        <TouchableOpacity
          style={styles.filterRow}
          onPress={() => dispatch(setPopularitySelected(!popularitySelected))}>
          <Text style={[commonStyle.bold25, {color: '#505C74'}]}>
            {Content.popularity}
          </Text>
          <CheckBox
            disabled={false}
            value={popularitySelected}
            onValueChange={(newValue) =>
              dispatch(setPopularitySelected(newValue))
            }
          />
        </TouchableOpacity>
        <Line />
        <TouchableOpacity
          style={styles.filterRow}
          onPress={() => dispatch(setDistanceSelected(!distanceSelected))}>
          <Text style={[commonStyle.bold25, {color: '#505C74'}]}>
            {'By Distance'}
          </Text>
          <CheckBox
            disabled={false}
            value={distanceSelected}
            onValueChange={(newValue) =>
              dispatch(setDistanceSelected(newValue))
            }
          />
        </TouchableOpacity>
        <Line />
      </View>
      <ButtonCommon
        onPress={() => _onShowResultPress()}
        text={'Show results'}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: colors.background},
  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp(4),
    height: heightToDp(4),
  },
  header: {
    marginHorizontal: widthToDp(4.5),
    justifyContent: 'space-between',
    paddingVertical: heightToDp(1),
    marginVertical: heightToDp(0.5),
    ...commonStyle.rowAlignCenter,
  },
  filterRow: {
    paddingVertical: widthToDp(4.5),
    paddingHorizontal: widthToDp(4.5),
    backgroundColor: colors.white,
    ...commonStyle.rowAlignCenter,
    justifyContent: 'space-between',
  },
  rightArrowStyle: {
    resizeMode: 'contain',
    width: widthToDp(2),
    height: heightToDp(2),
  },
  iconStyle: {fontSize: fontToDp(20)},
  imageStyleSelected: {
    width: widthToDp(4),
    resizeMode: 'contain',
    height: widthToDp(4),
    marginRight: widthToDp(1),
  },
  sportsSelected: {
    alignItems: 'center',
    padding: widthToDp(2),
    backgroundColor: colors.background,
    flexDirection: 'row',
  },
  checkBoxStyle: {
    width: widthToDp(4.5),
    height: widthToDp(3.5),
    resizeMode: 'cover',
  },
  titleCheckContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingRight: 20,
  },
});
export default VenueFilterScreen;
