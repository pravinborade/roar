import axiosService from '../../../../../../../commons/axiosService';
import {errorLog, successLog} from '../../../../../../../utils';
import {
  calculateDistance,
  calculateRating,
  ifNotValid,
  ifValid,
} from '../../../../../../../utils/helper';
import {addSportImage} from '../../../sports/sport_all/action';
import {getLatLong} from '../../../../comp/action';
import {_dataPlain} from '../../../sports/data';
import {setLoading} from '../../../../../../../redux/actions/loader';
import {getSportImage} from '../../../summary/comp/action';

export function setSelectedFilterSportAndCOpy(item) {
  return (dispatch) => {
    dispatch({type: 'SET_SELECTED_SPORT_FILTER_AND_COPY', payload: item});
  };
}

export function setSelectedFilterSport(item) {
  return (dispatch) => {
    dispatch({type: 'SET_SELECTED_SPORT_FILTER', payload: item});
  };
}

export function setSelectedFilterSportAndValue(
  sportsFilterList,
  sportsSelected,
) {
  return (dispatch) => {
    dispatch({
      type: 'SET_SELECTED_SPORT_FILTER_AND_VALUE',
      payload: {
        sportsFilterList,
        sportsSelected,
      },
    });
  };
}

export function setDistanceSelected(value) {
  return {type: 'SET_DISTANCE_SELECTED', payload: value};
}

export function setPopularitySelected(value) {
  return {type: 'SET_POPULARITY_SELECTED', payload: value};
}

export function resetFilter(sportsFilterList) {
  try {
    return (dispatch) => {
      let result = [];
      if (sportsFilterList && sportsFilterList.length > 0) {
        result = sportsFilterList.map((x) => {
          return {...x, value: false};
        });
      }
      dispatch({type: 'RESET_FILTER', payload: result});
    };
  } catch (e) {
    errorLog(e);
  }
}

export const getSportListFilterApi = (state) => {
  return (dispatch) => {
    axiosService
      .get(`api/resources/category`)
      .then(async (response) => {
        successLog('api/resources/category', response.status);
        if (response.status === 200) {
          response = response.data;
          if (ifNotValid(response)) {
            errorLog('invalid response ');
            return;
          }
          let data = response.data;
          if (ifValid(data) && data.length > 0) {
            let result = data.map((x, i) => {
              let name = ifValid(x.category) ? x.category : '';
              return {
                value: false,
                id: i,
                name: name,
                ...x,
                image: getSportImage(name),
              };
            });
            dispatch(setSelectedFilterSportAndCOpy(result));
          }
        }
      })
      .catch((o) => {
        console.log('getSportListFilterApi error ' + JSON.stringify(o));
      });
  };
};

export const getVenuesFilteredApi = (
  category,
  location = {},
  popularitySelected = false,
) => {
  return (dispatch) => {
    dispatch({
      type: 'SET_FILTERED_BY_SPORT_VENUES',
      payload: {sportVenues: [], sportVenuesCopy: []},
    });
    dispatch(setLoading(true));
    // axiosService.get(`api/resources/site?category=${category}`).then(async (response) => {
    axiosService
      .get(`api/sites/searchsites/sport/${category}`)
      .then(async (response) => {
        successLog(`api/sites/searchsites/sport/${category}`, response.status);
        try {
          if (response.status === 200) {
            response = response.data;
            if (ifNotValid(response) || ifNotValid(response.data)) {
              response = {data: []};
              errorLog('invalid response ');
            }

            let siteData = response.data;

            let latitude = '';
            let longitude = '';
            if (ifValid(location.lat)) {
              latitude = location.lat;
              longitude = location.lng;
            } else {
              let latLong = await getLatLong();
              if (ifValid(latLong) && ifValid(latLong.lat)) {
                latitude = latLong.lat;
                longitude = latLong.lng;
              }
            }

            if (latitude !== '') {
              siteData.map((item) => {
                item.distance = -1;
                let latBackend = item?.latitude;
                let lngBackend = item?.longitude;
                if (ifValid(latBackend) && ifValid(lngBackend)) {
                  item.distance = parseInt(
                    calculateDistance(
                      latBackend,
                      lngBackend,
                      latitude,
                      longitude,
                    ),
                  );
                }
              });

              siteData = siteData.sort(function (a, b) {
                return a.distance - b.distance;
              });

              let withMinus1 = siteData.filter((x) => x.distance === -1);
              let withGreater0 = siteData.filter((x) => x.distance >= 0);
              siteData = [...withGreater0, ...withMinus1];
            }

            siteData = siteData.map((item) => {
              let ratings = ifValid(item?.ratings) ? item?.ratings : [];
              let rating = calculateRating(ratings);
              return {...item, rating};
            });

            if (popularitySelected)
              siteData = siteData.sort((x, y) => x.rating - y.rating).reverse();

            if (ifNotValid(siteData)) {
              errorLog('not valid site data');
              return;
            }
            filterVenueData(siteData, dispatch);
          } else {
            dispatch(setLoading(false));
          }
        } catch (e) {
          dispatch(setLoading(false));
          errorLog(e);
        }
      })
      .catch((o) => {
        dispatch(setLoading(false));
        console.log('getVenuesFilteredApi error ' + JSON.stringify(o));
      });
  };
};

const filterVenueData = (response, dispatch) => {
  try {
    let data = response;
    if (ifNotValid(data)) {
      errorLog('invalid data in filter');
      return;
    }

    if (ifValid(data) && data.length > 0) {
      try {
        data = data.map((x, index) => {
          return {
            ...x,
            sportPlayedForIcons: addSportImage(x?.sport, _dataPlain),
          };
        });
      } catch (e) {
        console.log('error sportImageGroup');
      }
    }
    dispatch({
      type: 'SET_FILTERED_BY_SPORT_VENUES',
      payload: {sportVenues: data, sportVenuesCopy: data},
    });
    dispatch(setLoading(false));
  } catch (e) {
    dispatch(setLoading(false));
    console.log(e);
  }
};

const filterByPopularity = () => {
  let data = {};
};

export function setFilterSportVenues(item) {
  return (dispatch) => {
    dispatch({type: 'SET_SPORT_VENUES', payload: item});
  };
}
