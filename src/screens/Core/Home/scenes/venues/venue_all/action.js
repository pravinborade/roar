import {ifValid} from '../../../../../../utils/helper';
import {_dataPlain} from '../../sports/data';
import {sportImageGroup} from '../../sports/sport_all/action';

export const checkData = (homeSportReducer) => {
  let data = [];
  let {activeSport, filterData} = homeSportReducer;
  if (ifValid(filterData) && ifValid(activeSport)) {
    data = filterData[activeSport];
    if (!ifValid(data)) {
      data = [];
    }
  }
  return data;
};

export function setSelectedVenue(item) {
  return (dispatch) => {
    dispatch({type: 'SET_SELECTED_VENUE', payload: item});
    // dispatch(setAvailableSportsImages(item));
  };
}

///TODO : set available sports in redux with image icons.
export function setAvailableSportsImages(item) {
  return (dispatch) => {
    let sportPlayedFor =
      ifValid(item) &&
      ifValid(item.resource) &&
      ifValid(item.resource.sportPlayedFor)
        ? item.resource.sportPlayedFor.split(',')
        : [];
    let sportPlayedDark = sportImageGroup(sportPlayedFor, _dataPlain);
    let sportPlayedPlain = sportImageGroup(sportPlayedFor, _dataPlain);
    dispatch({
      type: 'SET_ICONS_DARK_PLAIN',
      payload: {
        sportPlayedDark,
        sportPlayedPlain,
      },
    });
  };
}
