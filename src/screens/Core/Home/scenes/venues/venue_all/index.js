import {
  BackHandler,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useCallback, useEffect, useState} from 'react';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import Images from '../../../../../../assets/Images/Images';
import {colors, commonStyle} from '../../../../../../styles';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import {
  Container,
  HeaderBack,
  LoaderAndMsg,
} from '../../../../../../components';
import {Content} from '../../../../../../utils/content/Content';
import {RenderItem} from './comp/RenderItem';
import {ifValid} from '../../../../../../utils/helper';
import {useDispatch, useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {getVenuesList, setFilterData} from '../../sports/sport_all/action';
import {FilterButton, NotAvailable} from '../../../../../../components/';
import {
  getSportListFilterApi,
  getVenuesFilteredApi,
  setFilterSportVenues,
} from './filter_screen/action';
import InputSearchPass from '../../../../../../components/InputSearchPass';
import {errorLog} from '../../../../../../utils/fireLog';

const VenuesAll = ({navigation}) => {
  const dispatch = useDispatch();
  const [isRefreshing, setIsRefreshing] = useState(false);
  let {location, latLong} = useSelector((x) => x.login);
  let userDetails = useSelector((x) => x.login.data);
  let homeVenue = useSelector((x) => x.homeVenue);
  let venuesOriginal =
    ifValid(homeVenue.filterOriginal) && homeVenue.filterOriginal.length > 0
      ? homeVenue.filterOriginal
      : [];
  let {sportVenues, sportVenuesCopy, activeSport, popularitySelected} =
    useSelector((x) => x.homeVenue);

  let [searchValue, setSearchValue] = useState('');
  let loading = useSelector((x) => x.loader.loading);

  const onRefresh = () => {
    setIsRefreshing(true);
    if (activeSport === 'ALL') {
      dispatch(getVenuesList(latLong, popularitySelected));
    } else {
      if (ifValid(activeSport)) {
        dispatch(
          getVenuesFilteredApi(activeSport, latLong, popularitySelected),
        );
      } else {
        errorLog('invalid active sport..!');
      }
    }
    setIsRefreshing(false);
  };

  useEffect(() => {
    //dispatch(getSportListFilterApi());
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
  }, []);

  useEffect(() => {
    _onSearchVenues();
  }, [searchValue]);

  const _onSearchVenues = () => {
    try {
      if (searchValue !== '') {
        let filtered = sportVenuesCopy.filter((x) => {
          let title = x?.name?.toLowerCase();
          let area = x?.area?.toLowerCase();
          let sport = x?.sport
            ?.map((x) => x.name)
            ?.toString()
            ?.toLowerCase();
          title = title + ' ' + area + ' ' + sport ?? '';
          if (title.includes(searchValue.toLowerCase())) {
            return x;
          }
        });
        dispatch(setFilterSportVenues(filtered));
      } else {
        dispatch(setFilterSportVenues(sportVenuesCopy));
      }
    } catch (e) {
      console.log(e);
    }
  };

  useFocusEffect(
    useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
      return () => {
        BackHandler.removeEventListener(
          'hardwareBackPress',
          handleBackButtonClick,
        );
      };
    }, []),
  );

  function handleBackButtonClick() {
    dispatch({type: 'SET_SEARCH_VALUE', payload: ''});
    return true;
  }

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          handleBackButtonClick();
          // dispatch({type: 'SET_SEARCH_VALUE', payload: ''});
        }}
        title={ScreenNames.VENUES}
        navigation={navigation}
      />
      <InputSearchPass
        containerStyle={{marginVertical: heightToDp('1')}}
        placeholder={Content.venuesPlaceHolder}
        value={searchValue}
        onCrossPress={() => setSearchValue('')}
        onChangeText={(v) => setSearchValue(v)}
      />

      <View style={styles.locFilterContainer}>
        <View style={styles.locationContainer}>
          <Image
            source={Images.homeLocation}
            style={commonStyle.locationImageStyle}
          />
          <Text style={{...commonStyle.regular24, flex: 1}}>{location}</Text>
        </View>
        <FilterButton
          onPress={() => {
            setSearchValue('');
            dispatch(getSportListFilterApi());
            navigation.navigate(ScreenNames.VENUES_FILTER);
          }}
        />
      </View>
      {loading ? (
        <LoaderAndMsg />
      ) : (
        <View
          style={[styles.listContainer, {backgroundColor: colors.background}]}>
          <Text style={styles.listHeaderTitle}>
            {sportVenues.length} {Content.resultFound}
          </Text>
          {ifValid(sportVenues) && sportVenues.length > 0 ? (
            <FlatList
              data={sportVenues}
              onRefresh={() => onRefresh()}
              refreshing={isRefreshing}
              showsVerticalScrollIndicator={false}
              renderItem={({item}, position) => {
                return (
                  <RenderItem
                    userDetails={userDetails}
                    item={item}
                    position={position}
                    navigation={navigation}
                  />
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          ) : (
            <NotAvailable msg={Content.noResultFound} />
          )}
        </View>
      )}
    </Container>
  );
};

const styles = StyleSheet.create({
  locFilterContainer: {
    justifyContent: 'space-between',
    padding: widthToDp(4.5),
    backgroundColor: 'white',
    ...commonStyle.rowAlignCenter,
  },
  locationContainer: {...commonStyle.rowAlignCenter, flex: 1},
  listContainer: {flex: 1},
  listHeaderTitle: {
    marginVertical: heightToDp(1),
    marginHorizontal: widthToDp(4.5),
  },
});
export default VenuesAll;
