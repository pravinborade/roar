import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import HomeImageBox from '../comp/HomeImageBox';
import {ifValid} from '../../../../../utils/helper';
import {colors, commonStyle} from '../../../../../styles';
import {heightToDp, widthToDp} from '../../../../../utils/Responsive';
import {useDispatch, useSelector} from 'react-redux';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import {clearSlotsAndSlotSportDateTime} from '../bookings/action';
import {setActiveSport} from '../sports/sport_all/action';
import {Content} from '../../../../../utils/content/Content';

const margin = widthToDp('4.5%');
const Venues = ({props}) => {
  const dispatch = useDispatch();
  let userDetails = useSelector((x) => x.login.data);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const homeVenue = useSelector((state) => state.homeVenue);
  let venuesFiltered =
    ifValid(homeVenue.filterData) && homeVenue.filterData.length > 0
      ? homeVenue.filterData
      : [];
  let venuesOriginal =
    ifValid(homeVenue.filterOriginal) && homeVenue.filterOriginal.length > 0
      ? homeVenue.filterOriginal
      : [];
  let _all = venuesFiltered.slice(0, 3);

  const onRefresh = () => {
    setIsRefreshing(true);
    setIsRefreshing(false);
  };

  return (
    <View style={{margin}}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{Content.venu}</Text>
        {venuesFiltered && venuesFiltered.length > 3 ? (
          <Text
            onPress={() => {
              // dispatch(setFilterDataAndCopy(venuesOriginal));
              dispatch(clearSlotsAndSlotSportDateTime());
              dispatch(setActiveSport('ALL'));
              dispatch({
                type: 'SET_FILTERED_BY_SPORT_VENUES',
                payload: {
                  sportVenues: venuesOriginal,
                  sportVenuesCopy: venuesOriginal,
                },
              });
              props.navigation.navigate(ScreenNames.HOME_VENUES);
            }}
            style={styles.subTitle}>
            SEE ALL
          </Text>
        ) : null}
      </View>
      <View style={{}}>
        {ifValid(_all) && _all.length > 0 ? (
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={_all}
            onRefresh={() => onRefresh()}
            refreshing={isRefreshing}
            showsVerticalScrollIndicator={false}
            renderItem={({item, position}) => {
              return (
                <HomeImageBox
                  userDetails={userDetails}
                  item={item}
                  navigation={props.navigation}
                  index={position}
                />
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          <Text style={styles.container}>{Content.noVenuesFound}</Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: heightToDp(1),
  },
  title: {...commonStyle.bold40, alignSelf: 'center'},
  subTitle: {
    ...commonStyle.regular24,
    alignSelf: 'center',
    color: colors.darkGreen,
  },
});
export default Venues;
