import {StyleSheet, Text, ScrollView, TextInput, View} from 'react-native';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  ButtonCommon,
  Container,
  HeaderBack,
} from '../../../../../../../components';
import {
  Content,
  errorLog,
  fontToDp,
  heightToDp,
  ifNotValid,
  successLog,
  widthToDp,
} from '../../../../../../../utils';
import {colors, commonStyle} from '../../../../../../../styles';
import {PT_SANS_NARROW_BOLD} from '../../../../../../../styles/typography';
import Rating from 'react-native-easy-rating';
import Images from '../../../../../../../assets/Images/Images';
import {postRatingApi} from '../action';

const CHAR_LIMIT = 250;
const RatingsAdd = ({navigation}) => {
  const dispatch = useDispatch();
  const [description, setDescription] = useState('');
  const [ratingCount, setRatingCount] = useState(4);
  let userDetails = useSelector((x) => x.login.data);
  let selectedVenue = useSelector((x) => x.homeVenue.selectedVenue);

  const _onSubmit = () => {
    if (ifNotValid(userDetails?.id)) {
      errorLog('user not valid ');
      return;
    }
    if (ifNotValid(selectedVenue?.id)) {
      errorLog('selectedVenue not valid ');
      return;
    }
    let body = {
      site: {
        id: selectedVenue?.id,
      },
      user: {
        id: userDetails?.id,
      },
      description: description,
      rating: ratingCount,
      isActive: false,
    };
    dispatch(postRatingApi(body, navigation));
  };

  return (
    <Container style={commonStyle.flex1}>
      <HeaderBack title={Content.rateThis} navigation={navigation} />

      <ScrollView
        keyboardShouldPersistTaps="handled"
        style={styles.ratingContainer}>
        <Text style={styles.submitReview}>{Content.submitYourReview}</Text>

        <View style={{alignItems: 'center'}}>
          <Rating
            rating={ratingCount}
            iconSelected={Images.star}
            iconUnselected={Images.starNot}
            max={5}
            iconWidth={widthToDp(10)}
            iconHeight={widthToDp(10)}
            onRate={(x) => setRatingCount(x)}
          />
        </View>

        <View
          style={{
            ...styles.inputContainer,
            backgroundColor: description.length > 0 ? 'white' : null,
            borderBottomColor: description.length
              ? colors.primaryGreen
              : '#BFC7D8',
          }}>
          <TextInput
            maxHeight={heightToDp(25)}
            returnKeyType="done"
            onChangeText={(val) => setDescription(val)}
            keyboardType="default"
            keyboardAppearance="light"
            multiline={true}
            maxLength={CHAR_LIMIT}
            value={description}
            numberOfLines={3}
            placeholder={Content.reviewComment}
            placeholderTextColor={'#BFC7D8'}
            style={{
              ...styles.inputBox,
              borderBottomColor: description.length
                ? colors.primaryGreen
                : '#BFC7D8',
            }}
          />
        </View>
        <Text
          style={{
            ...styles.limitTextStyle,
            fontWeight: CHAR_LIMIT === description.length ? 'bold' : 'normal',
          }}>
          {description.length} of {CHAR_LIMIT}
        </Text>
      </ScrollView>

      <ButtonCommon text={Content.submit} onPress={() => _onSubmit()} />
    </Container>
  );
};
export default RatingsAdd;

const styles = StyleSheet.create({
  container: {},
  limitTextStyle: {
    ...commonStyle.regular13,
    textAlign: 'right',
    marginHorizontal: '5%',
    width: widthToDp('90%'),
  },
  submitReview: {
    ...commonStyle.narrow40,
    textAlign: 'center',
    marginBottom: widthToDp(4),
    marginTop: widthToDp(8),
  },
  ratingContainer: {
    flex: 1,
  },
  inputContainer: {
    flexDirection: 'row',
    width: widthToDp('90%'),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1.5,
    marginTop: widthToDp(8),
  },
  inputBox: {
    textAlignVertical: 'top',
    textAlign: 'center',
    fontFamily: PT_SANS_NARROW_BOLD,
    color: colors.primaryBlue,
    marginHorizontal: '5%',
    fontSize: fontToDp(18),
    width: widthToDp('73%'),
  },
});
