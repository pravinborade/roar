import {LogBox, ScrollView, StyleSheet} from 'react-native';
import React, {memo, useEffect} from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
} from '../../../../../../components';
import {
  errorLog,
  ifNotValid,
  ifValid,
  ScreenNames,
} from '../../../../../../utils';
import {colors, commonStyle} from '../../../../../../styles';
import {
  AboutPlayScape,
  AddressTime,
  Amenities,
  AvailableSports,
  BecomeMember,
} from './comp';
import Carousal from './comp/Carousal';
import BookingTitle from './comp/BookingTitle';
import {useDispatch, useSelector} from 'react-redux';
import {getCourtsAndSlotsApi} from '../../bookings/action';
import {calculateRating} from '../../../../../../utils/helper';

const VenueDetails = ({navigation}) => {
  let homeVenue = useSelector((x) => x.homeVenue.selectedVenue);
  let {selectedSport} = useSelector((x) => x.homeVenue);
  const dispatch = useDispatch();
  let userDetails = useSelector((x) => x.login.data);
  let rating = ifValid(homeVenue?.rating) ? homeVenue?.rating : '';

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  return (
    <Container style={{flex: 1}}>
      <HeaderBack title={ScreenNames.VENUE_DETAILS} navigation={navigation} />
      <BookingTitle
        item={homeVenue}
        navigation={navigation}
        rating={rating}
        userDetails={userDetails}
      />
      <ScrollView style={{flex: 1, backgroundColor: colors.background}}>
        <Carousal item={homeVenue} />
        <AddressTime item={homeVenue} />
        <AvailableSports item={homeVenue} />
        <BecomeMember item={homeVenue} navigation={navigation} />
        <AboutPlayScape item={homeVenue} />
        <Amenities item={homeVenue} />
      </ScrollView>

      <ButtonCommon
        disabled={ifNotValid(selectedSport?.name) || selectedSport?.name === ''}
        style={{
          backgroundColor:
            ifNotValid(selectedSport?.name) || selectedSport?.name === ''
              ? '#b5b5b5'
              : colors.primaryGreen,
        }}
        onPress={() => {
          try {
            dispatch(
              getCourtsAndSlotsApi(
                new Date(),
                homeVenue,
                userDetails,
                selectedSport?.name,
              ),
            );
            navigation.navigate(ScreenNames.HOME_BOOKINGS, {
              selectedSlots: undefined,
            });
          } catch (e) {
            errorLog(e);
          }
        }}
        text={'book'}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  title: {...commonStyle.narrow40},
  subTitle: {...commonStyle.narrow40},
  titleContainer: {flexDirection: 'row', justifyContent: 'space-between'},
});
export default memo(VenueDetails);
