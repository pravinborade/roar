import {
  Content,
  errorLog,
  ifNotValid,
  ifValid,
  successLog,
} from '../../../../../../utils';
import axiosService from '../../../../../../commons/axiosService';
import {Alert} from 'react-native';
import helperFunctions from '../../../../../../commons/helperFunctions';

export const getAminities = (id, userId) => {
  if (ifNotValid(id)) {
    console.log('invalid id' + id + 'userId ' + userId);
    return;
  }
  return (dispatch) => {
    axiosService
      .get(`api/amenities/${id}`)
      .then((amenities) => {
        if (amenities.status === 200) {
          amenities = amenities.data;
          if (ifNotValid(amenities)) {
            errorLog('invalid response ');
            return;
          }
          let payload = amenities.data[0];
          dispatch({
            type: 'SET_AMINITIES',
            payload: ifValid(payload) ? payload : [],
          });
          dispatch(getMembership(id, userId));
          // noAminitiesData: !amenities.data[0].length, amenities: amenities.data[0]
        } else {
          console.log('not valid response ' + JSON.stringify(amenities));
        }
      })
      .catch((o) => {
        console.log('getSportList error ' + o);
      });
  };
};

export const getMembership = (id, userId) => {
  console.log('CLIENT ID ----------------->', id, userId);
  let url = `api/plans/${id}?webCheck=false`;
  if (ifValid(userId)) {
    url = `api/plans/${id}?webCheck=false&userId=${userId}`;
  }
  return (dispatch) => {
    axiosService
      .get(url)
      .then((res) => {
        successLog(url, res.status);
        if (res.status === 200) {
          res = res.data;
          if (ifNotValid(res)) {
            errorLog('invalid response ');
            return;
          }
          //let payload = res.data[0];
          let memList = res.data[0];
          const payload = memList.map((membership, index) => {
            membership.priceToPay =
              helperFunctions.calculatePriceToPay(membership);
            return membership;
          });

          dispatch({
            type: 'SET_MEMBERSHIP',
            payload: ifValid(payload) ? payload : [],
          });
          dispatch({
            type: 'SET_MEMBERSHIP_LIST',
            payload: ifValid(payload) ? payload : [],
          });
        } else {
          console.log('not valid response ' + JSON.stringify(res));
        }
      })
      .catch((o) => {
        console.log('getMembership error ' + o);
      });
  };
};

export const setMembershipSelected = (payload) => {
  return {type: 'SET_MEMBERSHIP_SELECTED', payload: payload};
};

export const updateMembershipTaken = (membershipTaken, membership) => {
  return (dispatch) => {
    dispatch({
      type: 'UPDATE_MEMBERSHIP_TAKEN',
      payload: {
        membershipTaken: membershipTaken,
        membership: membership,
      },
    });
  };
};

export const setSelectedSport = (payload) => {
  return {type: 'SET_SELECTED_SPORT', payload: payload};
};

export const postRatingApi = (body, navigation) => {
  if (ifNotValid(body)) {
    console.log('body not valid ' + body);
    return;
  }

  return (dispatch) => {
    axiosService
      .post(`api/ratings`, body)
      .then((res) => {
        successLog('api/ratings', res.status);
        if (res.status === 200) {
          Alert.alert(Content.review, Content.thankForReview, [
            {text: Content.okBtn, onPress: () => navigation.goBack()},
          ]);
        }
      })
      .catch((o) => {
        console.log('postRatingApi error ' + o);
      });
  };
};
