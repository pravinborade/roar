import {Image, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {
  heightToDp,
  width,
  widthToDp,
} from '../../../../../../../utils/Responsive';
import Carousel from 'react-native-snap-carousel';
import {colors, commonStyle} from '../../../../../../../styles';
import {useSelector} from 'react-redux';
import {ifValid} from '../../../../../../../utils/helper';
import Images from '../../../../../../../assets/Images/Images';
import validate from 'validate.js';

const Carousal = ({item = {}}) => {
  const [activeSlide, setActiveSlide] = useState(0);
  let userDetails = useSelector((x) => x.login.data);

  let checkUrl = validate({website: item?.image}, {website: {url: true}});
  let image =
    ifValid(item?.image) &&
    item?.image !== '' &&
    checkUrl?.website === undefined
      ? {uri: item?.image}
      : Images.sampleBooking;

  const _renderItem = ({item, index}) => {
    return (
      <View style={styles.slide}>
        <Image source={image} style={styles.carousalStyle} />
      </View>
    );
  };

  function pagination() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          borderRadius: widthToDp(1),
          paddingHorizontal: widthToDp(2),
          position: 'absolute',
          bottom: 5,
          alignSelf: 'center',
        }}>
        <Text style={[commonStyle.regular20, {color: colors.primaryGrey}]}>
          {(activeSlide + 1).toString()}/1
        </Text>
      </View>
    );
  }

  return (
    <View style={{height: widthToDp(45)}}>
      <Carousel
        layout={'default'}
        data={[1]}
        onSnapToItem={(index) => setActiveSlide(index)}
        renderItem={_renderItem}
        sliderWidth={width}
        itemWidth={width}
      />
      {/*{pagination()}*/}
      {/*<Line />*/}
    </View>
  );
};
export default Carousal;

const styles = StyleSheet.create({
  container: {},
  carousalStyle: {
    resizeMode: 'cover',
    width: width,
    height: heightToDp('45%'),
  },
});
