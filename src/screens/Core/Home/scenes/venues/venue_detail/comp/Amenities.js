import {FlatList, Image, LogBox, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {colors, commonStyle} from '../../../../../../../styles';
import {Line, NotAvailable} from '../../../../../../../components';
import {venueStyle} from './style';
import {Content, ifValid, widthToDp} from '../../../../../../../utils';
import Images from '../../../../../../../assets/Images/Images';
import {useSelector} from 'react-redux';

const Amenities = () => {
  let amenities = useSelector((x) => x.homeVenue.amenities);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  return (
    <>
      <View style={venueStyle.detailsContainer}>
        <Text style={commonStyle.narrow32}>{Content.Amenities}</Text>
        <View style={{paddingVertical: widthToDp(3)}}>
          <FlatList
            numColumns={5}
            data={amenities}
            ListEmptyComponent={() => (
              <NotAvailable msg={Content.noAmenitiesFound} />
            )}
            renderItem={({item, index}) => {
              let amenitiesName = ifValid(item?.amenities?.name)
                ? item.amenities.name
                : '';
              let img = ifValid(item?.amenities?.imageUrl)
                ? {uri: item?.amenities?.imageUrl}
                : Images.amenities;
              return (
                <View style={styles.rowContainer}>
                  <Image style={styles.imgStyle} source={img} />
                  <Text numberOfLines={2} style={styles.text}>
                    {amenitiesName}
                  </Text>
                </View>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
      <Line />
    </>
  );
};
export default Amenities;

let size = 9;
const styles = StyleSheet.create({
  rowContainer: {
    alignItems: 'center',
    width: widthToDp(18),
    marginBottom: widthToDp(5),
  },
  text: {
    ...commonStyle.regular20,
    textAlign: 'center',
    color: colors.primaryBlue,
  },
  imgStyle: {
    resizeMode: 'cover',
    width: widthToDp(size),
    height: widthToDp(size),
    borderRadius: widthToDp(size / 2),
  },
});
