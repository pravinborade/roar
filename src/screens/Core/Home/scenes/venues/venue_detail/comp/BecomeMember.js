import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {colors, commonStyle} from '../../../../../../../styles';
import {heightToDp, widthToDp} from '../../../../../../../utils/Responsive';
import Images from '../../../../../../../assets/Images/Images';
import {venueStyle} from './style';
import {ifValid} from '../../../../../../../utils/helper';
import {useDispatch, useSelector} from 'react-redux';
import {Content} from '../../../../../../../utils/content/Content';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {setMembershipSelected} from '../action';

const BecomeMember = ({item, navigation}) => {
  const data = [2, 3, 3, 3, 4, 4, 4, 4, 4];
  const [isRefreshing, setIsRefreshing] = useState(false);
  const onRefresh = () => {
    setIsRefreshing(true);
    setIsRefreshing(false);
  };
  const dispatch = useDispatch();
  let membership = useSelector((x) => x.homeVenue.membership);

  const renderItem = (item) => {
    let membershipType = ifValid(item.membershipType)
      ? item.membershipType
      : '';
    let duration = ifValid(item.duration) ? item.duration : '';
    let name = ifValid(item.name) ? item.name : '';
    let amount = ifValid(item.amount) ? item.amount : '';
    let description = '';
    let flatRate = ifValid(item.flatRate) ? item.flatRate : '';
    let discountPercentage = ifValid(item.discountPercentage)
      ? item.discountPercentage
      : '';
    let packageFlatRate = ifValid(item.packageFlatRate)
      ? item.packageFlatRate
      : '';
    let packageDiscount = ifValid(item.packageDiscount)
      ? item.packageDiscount
      : '';

    return (
      <TouchableOpacity
        onPress={() => {
          dispatch(setMembershipSelected(item));
          navigation.navigate(ScreenNames.MEMBERSHIP_DETAIL);
        }}
        style={{
          padding: widthToDp(3),
          backgroundColor: '#DDFFDC',
          width: widthToDp(45),
          marginRight: widthToDp(1),
          flex: 1,
        }}
      >
        <View style={{flexDirection: 'row', flex: 1, alignItems: 'center'}}>
          <Image
            source={Images.box}
            style={{
              width: widthToDp(7),
              height: widthToDp(7),
              marginRight: widthToDp(1),
            }}
          />
          <Text numberOfLines={2} style={[commonStyle.narrow32, {flex: 1}]}>
            {name}{' '}
          </Text>
        </View>
        <View style={styles.rowStyle}>
          <Text style={commonStyle.narrow28}>Rs. {amount}</Text>
          <Text style={commonStyle.regular24}> ({duration})</Text>
        </View>

        {flatRate !== '' ? (
          <View style={styles.rowStyle}>
            <Text style={commonStyle.narrow28}>Slots Flat Rate: </Text>
            <Text style={commonStyle.regular24}>{flatRate} Rs</Text>
          </View>
        ) : null}

        {discountPercentage !== '' ? (
          <View style={styles.rowStyle}>
            <Text style={commonStyle.narrow28}>Slots Discount: </Text>
            <Text style={commonStyle.regular24}>{discountPercentage}%</Text>
          </View>
        ) : null}

        {packageFlatRate !== '' ? (
          <View style={styles.rowStyle}>
            <Text style={commonStyle.narrow28}>Membership Flat Rate: </Text>
            <Text style={commonStyle.regular24}>{packageFlatRate} Rs</Text>
          </View>
        ) : null}

        {packageDiscount !== '' ? (
          <View style={styles.rowStyle}>
            <Text style={commonStyle.narrow28}>Membership Discount: </Text>
            <Text style={commonStyle.regular24}>{packageDiscount}%</Text>
          </View>
        ) : null}

        {/*<View style={{flexDirection: 'row'}}>*/}
        {/*    <Text>Pre launch</Text>*/}
        {/*    <Text>Pre launch</Text>*/}
        {/*</View>*/}

        {/*<Text numberOfLines={3}>{description}</Text>*/}
      </TouchableOpacity>
    );
  };

  return ifValid(membership) && membership.length > 0 ? (
    <View>
      <View style={venueStyle.detailsContainer}>
        <Text style={commonStyle.narrow32}>{Content.BecomeAMember}</Text>
        {ifValid(membership) && membership.length > 0 ? (
          <View>
            <FlatList
              horizontal
              data={ifValid(membership) ? membership : []}
              style={{
                flexDirection: 'row',
                backgroundColor: 'white',
                paddingVertical: heightToDp(1.0),
              }}
              onRefresh={() => onRefresh()}
              refreshing={isRefreshing}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => renderItem(item)}
              keyExtractor={(item, index) => index.toString()}
            />
            <Text
              onPress={() => {
                navigation.navigate(ScreenNames.MEMBERSHIP_LIST);
              }}
              style={styles.subTitle}
            >
              {Content.knowMore}
            </Text>
          </View>
        ) : (
          <Text style={styles.container}>{Content.noResultFound}</Text>
        )}
      </View>
      <View
        style={{height: widthToDp(1.3), backgroundColor: colors.background}}
      />
    </View>
  ) : null;
};
export default BecomeMember;

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  rowStyle: {
    flexDirection: 'row',
    marginVertical: widthToDp(0.5),
  },
  subTitle: {
    ...commonStyle.regular24,
    textAlign: 'right',
    color: colors.darkGreen,
  },
});
