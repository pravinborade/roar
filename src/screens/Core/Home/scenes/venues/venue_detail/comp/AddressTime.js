import {
  Alert,
  Image,
  Linking,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {colors, commonStyle} from '../../../../../../../styles';
import {widthToDp} from '../../../../../../../utils/Responsive';
import {venueStyle} from './style';
import {ifValid} from '../../../../../../../utils/helper';
import {Content} from '../../../../../../../utils/content/Content';
import Images from '../../../../../../../assets/Images/Images';
import openMap from 'react-native-open-maps';

const AddressTime = ({item}) => {
  let address = ifValid(item?.locationArea)
    ? item?.locationArea
    : Content.noAddressFound;
  let long = ifValid(item?.longitude) ? item?.longitude : '';
  let lat = ifValid(item?.latitude) ? item?.latitude : '';
  const [isPickerVisible, setIsPickerVisible] = useState(false);

  const googleMapOpenUrl = ({latitude, longitude}) => {
    const url = Platform.select({
      ios: 'maps:' + latitude + ',' + longitude + '?q=' + address,
      android: 'geo:' + latitude + ',' + longitude + '?q=' + address,
    });
    Linking.openURL(url);
  };

  const _onPress = () => {
    if (lat !== '' && long !== '') {
      console.log(lat, long);
      if (Platform.OS === 'android') {
        googleMapOpenUrl(lat, long);
      } else {
        askOptions();
      }
    } else {
      console.log('no lat long found');
    }
  };

  const askOptions = () => {
    Alert.alert('Please choose map', 'Please choose Google or Apple map', [
      {
        text: 'Cancel',
        onPress: () => console.log('Ask me later pressed'),
      },
      {
        text: 'Google',
        onPress: () =>
          openMap({
            query: address,
            provider: 'google',
          }),
      },
      {
        text: 'Apple',
        onPress: () =>
          openMap({
            query: address,
            latitude: lat,
            longitude: long,
            provider: 'apple',
          }),
        style: 'cancel',
      },
    ]);
  };

  // const _onPress = () => {
  //     if (lat !== '' && long !== '') {
  //         successLog(lat, long);
  //         googleMapOpenUrl(lat, long);
  //     } else {
  //         console.log('no lat long found');
  //     }
  // };

  return (
    <View>
      <View
        style={[
          venueStyle.detailsContainer,
          {
            flexDirection: 'row',
            alignItems: 'center',
          },
        ]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingVertical: widthToDp(1),
          }}>
          <Image
            source={Images.locationBlue}
            style={styles.locationIconStyle}
          />
          <Text
            numberOfLines={3}
            style={[
              commonStyle.regular24,
              {
                flex: 1,
                marginHorizontal: widthToDp(0.5),
              },
            ]}>
            {address}
          </Text>
          {/*<Text style={commonStyle.regular24}>Open - 7 am to 11pm</Text>*/}
        </View>
        {Content.noAddressFound !== address ? (
          <TouchableOpacity onPress={() => _onPress()}>
            <Text style={styles.subTitle}>{Content.seeMap}</Text>
          </TouchableOpacity>
        ) : null}
      </View>
      <View
        style={{height: widthToDp(1.3), backgroundColor: colors.background}}
      />
    </View>
  );
};
export default AddressTime;
const styles = StyleSheet.create({
  container: {},
  subTitle: {
    ...commonStyle.regular24,
    alignSelf: 'center',
    color: colors.darkGreen,
  },
  locationIconStyle: {
    resizeMode: 'contain',
    marginLeft: widthToDp(0.1),
    marginRight: widthToDp(1),
    width: widthToDp(4),
    //height: widthToDp(5.5),
  },
});
