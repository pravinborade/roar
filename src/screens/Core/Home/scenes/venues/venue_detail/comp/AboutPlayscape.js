import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../../styles';
import {Line} from '../../../../../../../components';
import {venueStyle} from './style';
import {ifValid} from '../../../../../../../utils/helper';
import {Content} from '../../../../../../../utils/content/Content';
import {widthToDp} from '../../../../../../../utils/Responsive';

const AboutPlayScape = ({item}) => {
  let description = ifValid(item?.description) ? item.description : '';
  let name = ifValid(item?.name) ? item?.name : '';

  return (
    <View>
      <View style={venueStyle.detailsContainer}>
        <Text
          style={
            commonStyle.narrow32
          }>{`${Content.AboutPlayscape} ${name}`}</Text>
        <View style={venueStyle.detailsContainerInner}>
          <Text style={commonStyle.regular24}>{description}</Text>
        </View>
      </View>
      <View
        style={{height: widthToDp(1.3), backgroundColor: colors.background}}
      />
    </View>
  );
};
export default AboutPlayScape;

const styles = StyleSheet.create({
  container: {},
});
