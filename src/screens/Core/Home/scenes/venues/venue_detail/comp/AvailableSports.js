import {
  Image,
  StyleSheet,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../../styles';
import {heightToDp, widthToDp} from '../../../../../../../utils/Responsive';
import {ifValid} from '../../../../../../../utils/helper';
import {Content} from '../../../../../../../utils/content/Content';
import {useDispatch, useSelector} from 'react-redux';
import NotAvailable from '../../../../../../../components/NotAvailable';
import {setSelectedSport} from '../action';
import {capitalize} from 'lodash';

const AvailableSports = ({item}) => {
  let {selectedVenue, selectedSport} = useSelector((x) => x.homeVenue);
  const dispatch = useDispatch();

  const onResourcePress = async (obj) => {
    await dispatch(setSelectedSport(obj));
  };

  const selectedStyle = {
    backgroundColor: colors.primaryGreen,
    paddingVertical: widthToDp(0.8),
    paddingHorizontal: widthToDp(3),
  };
  const notSelected = {
    paddingVertical: widthToDp(0.8),
    paddingHorizontal: widthToDp(1),
  };

  return (
    <View>
      <View style={commonStyle.detailsContainer}>
        <Text style={commonStyle.narrow30}>{Content.availableSports}</Text>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            paddingVertical: heightToDp(2.0),
          }}>
          {ifValid(selectedVenue?.sportPlayedForIcons) &&
          selectedVenue?.sportPlayedForIcons?.length > 0 ? (
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {selectedVenue?.sportPlayedForIcons?.map((x, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={[
                      styles.imageContainer,
                      selectedSport?.name === x?.name
                        ? selectedStyle
                        : notSelected,
                    ]}
                    onPress={() => onResourcePress(x)}>
                    <View style={commonStyle.darkIconContainer}>
                      <Image
                        source={x.image}
                        style={commonStyle.darkImageStyle}
                      />
                    </View>
                    <Text style={{color: '#505C74', ...commonStyle.regular24}}>
                      {capitalize(x?.name)}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          ) : (
            <NotAvailable msg={Content.noSportsFound} />
          )}
        </View>
      </View>
      <View
        style={{height: widthToDp(1.3), backgroundColor: colors.background}}
      />
    </View>
  );
};
export default AvailableSports;

const styles = StyleSheet.create({
  container: {},
  imageStyleSport: {
    resizeMode: 'contain',
    width: widthToDp(8),
    height: widthToDp(8),
  },
  imageContainer: {
    alignItems: 'center',
    marginRight: widthToDp(3),
  },
});
