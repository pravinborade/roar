import {StyleSheet} from 'react-native';
import {heightToDp, widthToDp} from '../../../../../../../utils/Responsive';

export const venueStyle = StyleSheet.create({
  container: {},
  detailsContainer: {
    backgroundColor: 'white',
    paddingHorizontal: widthToDp(4.5),
    paddingVertical: heightToDp(2.0),
  },
  detailsContainerInner: {
    flexDirection: 'row',
    paddingVertical: heightToDp(1.0),
  },
});
