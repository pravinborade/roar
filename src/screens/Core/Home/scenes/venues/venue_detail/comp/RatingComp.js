import {StyleSheet} from 'react-native';
import React from 'react';
import {Rating} from 'react-native-ratings';
import {widthToDp} from '../../../../../../../utils';

const RatingComp = ({
  ratingCount = 0,
  max = 5,
  readonly = true,
  imageSize = 10,
}) => {
  return (
    <Rating
      readonly={readonly}
      count={max}
      showRating={false}
      startingValue={ratingCount}
      imageSize={widthToDp(imageSize)}
      // onFinishRating={()=> set}
    />
  );
};
export default RatingComp;

const styles = StyleSheet.create({
  container: {},
});
