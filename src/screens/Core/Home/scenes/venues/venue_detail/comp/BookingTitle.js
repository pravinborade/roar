import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../../styles';
import {
  Content,
  fontToDp,
  ifNotValid,
  ScreenNames,
  successLog,
} from '../../../../../../../utils';
import {heightToDp, ifValid, widthToDp} from '../../../../../../../utils';
import Images from '../../../../../../../assets/Images/Images';
import Rating from 'react-native-easy-rating';
import RatingComp from './RatingComp';

const BookingTitle = ({item = {}, navigation, rating, userDetails}) => {
  let name = ifValid(item?.name) ? item?.name : '';
  let place = ifValid(item?.area) ? item.area : '';
  rating = ifValid(rating) ? rating : 0;

  return (
    <View style={styles.container}>
      <View style={{flex: 1}}>
        <Text style={commonStyle.narrow40}>{name}</Text>
        <Text style={commonStyle.regular24}>{place}</Text>
      </View>
      <View style={styles.rateThisContainer}>
        {navigation && ifValid(userDetails?.id) && (
          <TouchableOpacity
            onPress={() => navigation?.navigate(ScreenNames.RATINGS_ADD)}>
            <Text style={styles.rateThis}>{Content.rateThis}</Text>
          </TouchableOpacity>
        )}

        {rating !== 0 ? (
          <View style={{...commonStyle.rowAlignCenter}}>
            <Text style={styles.ratingText}>{rating}</Text>
            <RatingComp ratingCount={rating} imageSize={3.5} max={5} />
          </View>
        ) : null}
      </View>
    </View>
  );
};
export default BookingTitle;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: heightToDp(1.2),
    paddingHorizontal: widthToDp(4.5),
    alignItems: 'flex-end',
  },
  rateThisContainer: {
    justifyContent: 'space-between',
  },
  rateThis: {
    color: colors.primaryBlue,
    fontSize: fontToDp(14),
    ...commonStyle.bold24,
  },
  ratingText: {
    ...commonStyle.regular24,
    alignSelf: 'center',
    marginRight: widthToDp(1),
    color: colors.darkGreen,
  },
});
