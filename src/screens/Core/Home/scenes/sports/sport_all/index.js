import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import React, {useState} from 'react';
import {
  Container,
  HeaderBack,
  InputSearch,
  Line,
} from '../../../../../../components';
import {Content} from '../../../../../../utils/content/Content';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../../../styles';
import {setActiveSport} from './action';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import {_dataPlain} from '../data';

const SportAll = ({navigation}) => {
  const [data, setData] = useState(_dataPlain);
  let sportsWithImage = useSelector(
    (state) => state.homeSport.sportsWithImagePlain,
  );
  const dispatch = useDispatch();

  const searchSport = (s) => {
    if (s.length > 0) {
      let temp = data.filter((x, i) => {
        if (x.name.includes(s)) {
          return x;
        }
      });
      setData(temp);
    } else {
      setData(_dataPlain);
    }
  };

  return (
    <Container>
      <HeaderBack
        navigation={navigation}
        style={{backgroundColor: colors.background}}
        title={Content.sports}
      />
      <Text style={[commonStyle.narrow32, styles.container]}>
        {Content.selectSports}
      </Text>
      <InputSearch
        mainContainer={{backgroundColor: colors.background}}
        containerStyle={{
          backgroundColor: colors.white,
          marginBottom: heightToDp('2'),
        }}
        placeholder={'Ex: Football'}
        onChangeText={(v) => {
          searchSport(v);
        }}
      />

      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginHorizontal: widthToDp(4.5),
          marginBottom: widthToDp(4.5),
        }}>
        <Line style={{flex: 1}} lineColor={'gray'} />
        <Text style={[commonStyle.regular24, {margin: widthToDp(0.5)}]}>
          Or
        </Text>
        <Line style={{flex: 1}} lineColor={'gray'} />
      </View>

      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text style={[commonStyle.narrow32, styles.container]}>
          {Content.selectFromHere}
        </Text>
        <Text style={[commonStyle.regular18, styles.container]}>
          {Content.selectAll}
        </Text>
      </View>
      <View style={[commonStyle.detailsContainer, styles.container]}>
        <FlatList
          style={{paddingVertical: widthToDp(3)}}
          contentContainerStyle={{alignItems: 'center'}}
          numColumns={4}
          data={sportsWithImage}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  dispatch(setActiveSport(item.name));
                  navigation.navigate(ScreenNames.HOME_VENUES);
                }}>
                <Image style={styles.imageStyle} source={item.image} />
                <Text style={commonStyle.regular24}>{item.name}</Text>
              </TouchableOpacity>
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </Container>
  );
};
export default SportAll;

const styles = StyleSheet.create({
  container: {marginHorizontal: widthToDp(4.5), marginBottom: widthToDp(1)},
  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp(8),
    height: widthToDp(8),
  },
  button: {
    alignItems: 'center',
    width: widthToDp(21),
    marginRight: 1,
    marginBottom: widthToDp(5),
  },
});
