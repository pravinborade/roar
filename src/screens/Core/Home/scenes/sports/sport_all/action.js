import {GET_SPORT_LIST} from '../../../../../../utils/ActionConstant';
import axiosService from '../../../../../../commons/axiosService';
import {
  calculateDistance,
  calculateRating,
  ifNotValid,
  ifValid,
} from '../../../../../../utils/helper';
import {getLatLong} from '../../../comp/action';
import {_dataPlain} from '../data';
import {setLoader} from '../../../../../../redux/actions/loader';
import {errorLog, successLog} from '../../../../../../utils/fireLog';
import Images from '../../../../../../assets/Images/Images';

var _ = require('lodash');

export const getVenuesList = (
  location,
  popularitySelected = false,
  fromFilter = false,
) => {
  return (dispatch) => {
    dispatch(setLoader(true));
    dispatch({type: GET_SPORT_LIST, payload: []});
    axiosService
      .get('api/sites/getAllSites')
      .then(async (response) => {
        successLog('api/sites/getAllSites', response.status);
        try {
          if (response.status === 200) {
            response = response.data;
            if (ifNotValid(response)) {
              errorLog('invalid response ');
              return;
            }
            let siteData = response.data;
            console.log('sites', JSON.stringify(siteData));
            siteData = removeDuplicateSports(siteData);
            siteData = removeIleseumSite(siteData);
            let latitude = '';
            let longitude = '';
            if (ifValid(location.lat)) {
              latitude = location.lat;
              longitude = location.lng;
            } else {
              let latLong = await getLatLong();
              if (ifValid(latLong) && ifValid(latLong.lat)) {
                latitude = latLong.lat;
                longitude = latLong.lng;
              }
            }
            if (latitude !== '') {
              siteData.map((item) => {
                item.distance = -1;
                let latBackend = item?.latitude;
                let lngBackend = item?.longitude;
                if (ifValid(latBackend) && ifValid(lngBackend)) {
                  item.distance = parseInt(
                    calculateDistance(
                      latBackend,
                      lngBackend,
                      latitude,
                      longitude,
                    ),
                  );
                }
              });

              siteData = siteData.sort(function (a, b) {
                return a.distance - b.distance;
              });

              let withMinus1 = siteData.filter((x) => x.distance === -1);
              let withGreater0 = siteData.filter((x) => x.distance >= 0);
              siteData = [...withGreater0, ...withMinus1];
            }

            siteData = siteData.map((item) => {
              let ratings = ifValid(item?.ratings) ? item?.ratings : [];
              let rating = calculateRating(ratings);
              return {...item, rating};
            });

            if (popularitySelected) {
              siteData = siteData.sort((x, y) => x.rating - y.rating).reverse();
            }

            filterDashData(siteData, null, dispatch, fromFilter);
            dispatch(setLoader(false));
          } else {
            //helper.errorToast('Something went wrong.');
          }
        } catch (e) {
          errorLog('error getSportList data ' + e);
        }
      })
      .catch((o) => {
        errorLog('getSportList error ' + JSON.stringify(o));
      });
  };
};

export const getSportListForHome = () => {
  return (dispatch) => {
    dispatch(setLoader(true));
    axiosService
      .get(`api/resources/category`)
      .then(async (response) => {
        successLog('api/resources/category', response.status);
        if (response.status === 200) {
          response = response.data;
          if (ifNotValid(response)) {
            errorLog('invalid response ');
            return;
          }
          let datarescat = response.data;
          filterDashData(datarescat, null, dispatch);
          dispatch(setLoader(false));
        }
      })
      .catch((o) => {
        errorLog('getSportListHomeApi error ' + JSON.stringify(o));
      });
  };
};

export const filterDashData = (data, isSearchText, dispatch, fromFilter) => {
  try {
    let allData = data;
    if (ifNotValid(data)) {
      errorLog('invalid data in filter');
      return;
    }
    //   data = sortArrayAsc(data);

    if (ifValid(data) && data.length > 0) {
      try {
        data = data.map((x, index) => {
          return {
            ...x,
            sportPlayedForIcons: addSportImage(x?.sport, _dataPlain),
          };
        });
      } catch (e) {
        errorLog('error sportImageGroup');
      }
    }

    if (fromFilter) {
      dispatch(
        setVenuesCopyAndOriginalForFilter({
          filterData: data,
          filterOriginal: data,
          orginalData: allData,
          sportVenues: data,
          sportVenuesCopy: data,
        }),
      );
    } else {
      dispatch(
        setVenuesCopyAndOriginal({
          filterData: data,
          filterOriginal: data,
          orginalData: allData,
        }),
      );
    }
  } catch (e) {
    errorLog('error filter data ' + e);
  }
};

export const addSportImage = (categories = [], iconList = []) => {
  let sportWithIcon = [];
  if (ifValid(categories)) {
    sportWithIcon = categories.map((k) => {
      return {...k, image: Images.defaultSport};
    });
    sportWithIcon.map((k, index) => {
      iconList.map((y) => {
        if ((k?.name).toLowerCase() === y.name.toLowerCase()) {
          sportWithIcon[index].image = y.image;
        }
      });
    });
  }
  return sportWithIcon;
};

export const sportImageGroupCategory = (categories = [], iconList = []) => {
  let sportWithIcon = [];
  if (ifValid(categories)) {
    sportWithIcon = categories.map((k) => {
      return {...k, image: Images.defaultSport};
    });
    sportWithIcon.map((k, index) => {
      iconList.map((y) => {
        if (k.category.toLowerCase() === y.name.toLowerCase()) {
          sportWithIcon[index].image = y.image;
        }
      });
    });
  }
  return sportWithIcon;
};

export const sportImageGroup = (sports = [], iconList = []) => {
  let sportWithIcon = [];
  if (ifValid(sports)) {
    sportWithIcon = sports.map((k) => {
      return {name: capitalize(k), id: k, image: Images.defaultSport};
    });
    sportWithIcon.map((k, index) => {
      iconList.map((y) => {
        if (k.name.toLowerCase() === y.name.toLowerCase()) {
          sportWithIcon[index].image = y.image;
        }
      });
    });
  }
  return sportWithIcon;
};

function capitalize(s) {
  return s && s[0].toUpperCase() + s.slice(1);
}

const sportImage = (sports = [], iconList = []) => {
  let sportWithIcon = [];
  if (ifValid(sports)) {
    for (let k in sports) {
      if (k !== 'ALL' && k !== '_ALL') {
        sportWithIcon.push({
          name: capitalize(k),
          id: k,
          image: Images.defaultSport,
        });
      }
    }
    sportWithIcon.map((k, index) => {
      iconList.map((y) => {
        if (k.name.toLowerCase() === y.name.toLowerCase()) {
          sportWithIcon[index].image = y.image;
        }
      });
    });
  }
  return sportWithIcon;
};

export const removeDuplicateSports = (data) => {
  let filteredData = [];
  data.map(function (item) {
    let siteIndex = filteredData.findIndex(function (x) {
      return x.id === item.id;
    });
    if (siteIndex === -1) {
      filteredData.push(item);
    }
  });
  return filteredData;
};

export const removeDuplicate = (data) => {
  let filteredData = [];
  data.map(function (item) {
    let siteIndex = filteredData.findIndex(function (x) {
      return x?.site?.id === item?.site?.id;
    });

    if (siteIndex && siteIndex === -1) {
      let obj = {...item, resources: []};
      filteredData.push(obj);
    }
  });
  return filteredData;
};

export const removeIleseumSite = (data) => {
  let filteredData = [];
  data.map(function (item) {
    item.id == 16 || item.id == 18 ? '' : filteredData.push(item);
  });
  return filteredData;
};

export const multipleResource = (data) => {
  try {
    /// make unique site array
    let uniqueSite = removeDuplicate(data);
    /// compare unique with all duplicate..
    data.forEach((x) => {
      uniqueSite.forEach((obj) => {
        if (x?.site?.id === obj?.site?.id) {
          obj.resources.push(x.resource);
        }
      });
    });
    /// make unique resources
    return uniqueSite.map((x) => {
      return {
        ...x,
        resources: _.uniqBy(x.resources, (x) => {
          return x?.category?.toLowerCase();
        }),
      };
    });
  } catch (e) {
    errorLog(e);
  }
};

export function setSportsWithImageDarkOnly(filterData) {
  return {type: 'SET_SPORTS_IMAGE', payload: filterData};
}

export function setVenuesCopyAndOriginal(payload) {
  return (dispatch) => {
    dispatch({type: 'SET_VENUES_VENUES_COPY_ORIGINAL', payload: payload});
  };
}

export function setVenuesCopyAndOriginalForFilter(payload) {
  return (dispatch) => {
    dispatch({
      type: 'SET_VENUES_VENUES_COPY_ORIGINAL_FOR_FILTER',
      payload: payload,
    });
  };
}

export function setFilterData(filterData) {
  return {
    type: 'SET_FILTER_DATA',
    filterData: filterData,
  };
}

export function setFilterDataAndCopy(filterData) {
  return {
    type: 'SET_FILTER_DATA_AND_COPY',
    payload: filterData,
  };
}

export function setActiveSport(tab) {
  return {
    type: 'SET_ACTIVE_SPORT',
    payload: tab,
  };
}
