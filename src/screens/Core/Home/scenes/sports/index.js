import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  Content,
  heightToDp,
  ifValid,
  ScreenNames,
  widthToDp,
} from '../../../../../utils';
import {colors, commonStyle} from '../../../../../styles';
import {useDispatch, useSelector} from 'react-redux';
import {setActiveSport} from './sport_all/action';
import {clearSlotsAndSlotSportDateTime} from '../bookings/action';
import {setSearchValue} from '../../comp/action';
import {getVenuesFilteredApi} from '../venues/venue_all/filter_screen/action';

const margin = widthToDp('4.5%');

const Sports = ({props}) => {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const dispatch = useDispatch();
  let homeVenue = useSelector((x) => x.homeVenue);
  let venuesOriginal =
    ifValid(homeVenue.filterOriginal) && homeVenue.filterOriginal.length > 0
      ? homeVenue.filterOriginal
      : [];
  let venuesFiltered =
    ifValid(homeVenue.filterData) && homeVenue.filterData.length > 0
      ? homeVenue.filterData
      : [];
  const latLong = useSelector((state) => state.login.latLong);
  let {sportsFilterList, sportsFilterListCopy} = useSelector(
    (state) => state.homeVenue,
  );

  const onRefresh = () => {
    setIsRefreshing(true);
    setIsRefreshing(false);
  };

  useEffect(() => {}, []);

  return (
    <View style={{margin}}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{Content.sports}</Text>
      </View>
      <View>
        {ifValid(sportsFilterList) && sportsFilterList.length > 0 ? (
          <FlatList
            horizontal
            data={sportsFilterList}
            onRefresh={() => onRefresh()}
            refreshing={isRefreshing}
            showsVerticalScrollIndicator={false}
            renderItem={({item}, position) => {
              return (
                <TouchableOpacity
                  style={styles.renderContainer}
                  onPress={async () => {
                    await dispatch(clearSlotsAndSlotSportDateTime());
                    await dispatch(setSearchValue());
                    dispatch(setActiveSport(item.name));
                    dispatch(getVenuesFilteredApi(item?.name, latLong));
                    props.navigation.navigate(ScreenNames.HOME_VENUES);
                  }}>
                  <View style={commonStyle.darkIconContainer}>
                    <Image
                      source={item.image}
                      style={commonStyle.darkImageStyle}
                    />
                  </View>
                  <Text style={styles.sportNameStyle}>{item.name}</Text>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          <Text style={styles.container}>{Content.noSportsFound}</Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  sportNameStyle: {
    textAlign: 'center',
    ...commonStyle.regular24,
  },
  renderContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: widthToDp(2),
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: heightToDp(1),
  },
  title: {...commonStyle.bold40, alignSelf: 'center'},
  subTitle: {
    ...commonStyle.regular24,
    alignSelf: 'center',
    color: colors.primaryGreen,
  },
});
export default Sports;
