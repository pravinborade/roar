import Images from '../../../../../assets/Images/Images';

export const _dataPlain = [
  { name: 'basketball', image: Images.basketball },
  { name: 'football', image: Images.footBall },
  { name: 'cricket', image: Images.bat },
  { name: 'tennis', image: Images.tenis },
  { name: 'fencing', image: Images.fencing },
  { name: 'Diving', image: Images.diving },
  { name: 'Hockey', image: Images.hockky },
  { name: 'Boxing', image: Images.boxing },
  { name: 'badminton', image: Images.badminton },
  { name: 'Archery', image: Images.archary },
  { name: 'Vollyball', image: Images.vollyball },
  { name: 'Yoga', image: Images.yoga },
  { name: 'Zumba', image: Images.zumba },
  { name: 'Rugby', image: Images.rugby },
  { name: 'baseball', image: Images.baseball },
  { name: 'pool', image: Images.baseball },
  { name: 'dodgeball', image: Images.dogeball },
  { name: 'default', image: Images.defaultSport },
  { name: 'volleyball', image: Images.volleyball },
  { name: 'khokho', image: Images.khokho },
  { name: 'gymnastics', image: Images.gymnastics },
  { name: 'squash', image: Images.squash },
  { name: 'swimming', image: Images.swimming },
  { name: 'table Tennis', image: Images.tenis },
];

export const _dataDark = [
  { name: 'basketball', image: Images.basketBallDark },
  { name: 'football', image: Images.footballDark },
  { name: 'cricket', image: Images.batDark },
  { name: 'tennis', image: Images.tenisDark },
  { name: 'fencing', image: Images.fencing }, //
  { name: 'Diving', image: Images.diving }, //
  { name: 'Hockey', image: Images.hockky }, //
  { name: 'Boxing', image: Images.boxing }, //
  { name: 'badminton', image: Images.badmintonDark },
  { name: 'Archery', image: Images.archary }, //
  { name: 'Vollyball', image: Images.vollyball }, //
  { name: 'baseball', image: Images.baseBallDark },
  { name: 'pool', image: Images.basketBallDark },
];
