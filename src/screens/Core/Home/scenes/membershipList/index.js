import React from 'react';
import {FlatList} from 'react-native';
import Images from '../../../../../assets/Images/Images';
import {HeaderBack} from '../../../../../components';
import Container from '../../../../../components/Container';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import MembershipPoster from './comp/membershipPoster';

const MembershipList = ({navigation}) => {
  const DATA = [
    {
      id: 1,
      backgroundColor: '#FEE2E2',
      logoImage: Images.orangeCircle,
      title: 'Pre Launch',
      subtitle: 'Complete Package for the club including all sports/fitness.',
      price: 'Rs. 7698.82',
      months: 12,
      posterImage: Images.orangeImg,
      posterImageHeight: 90,
      posterImageWidth: 70,
    },
    {
      id: 2,
      backgroundColor: '#DDFFDC',
      logoImage: Images.greenCircle,
      title: 'Pre Launch - Family of 4',
      subtitle:
        'Husband, wife and 2 children below 18 years. Complete Package for the club including all sports/fitness.',
      price: 'Rs. 7698.82',
      months: 12,
      posterImage: Images.greenImg,
      posterImageHeight: 90,
      posterImageWidth: 105,
    },
    {
      id: 3,
      backgroundColor: '#E5ECF9',
      logoImage: Images.blueCircle,
      title: 'Pre Launch- Couple',
      subtitle: 'Complete Package for the club including all sports/fitness.',
      price: 'Rs. 7698.82',
      months: 12,
      posterImage: Images.blueImg,
      posterImageHeight: 100,
      posterImageWidth: 80,
    },
  ];

  const renderItem = ({item}) => (
    <MembershipPoster
      logoImage={item.logoImage}
      backgroundColor={item.backgroundColor}
      title={item.title}
      subtitle={item.subtitle}
      price={item.price}
      months={item.months}
      posterImage={item.posterImage}
      posterImageHeight={item.posterImageHeight}
      posterImageWidth={item.posterImageWidth}
    />
  );

  return (
    <Container>
      <HeaderBack title={ScreenNames.MYMEMBERSHIP} />
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </Container>
  );
};

export default MembershipList;
