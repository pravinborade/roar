import {StyleSheet} from 'react-native';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../styles/typography';
import {fontToDp} from '../../../../../../utils/Responsive';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: '30%',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  mainSubContainer: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 10,
  },
  view_one: {
    marginLeft: 20,
    marginTop: 20,
  },
  view_two: {
    flex: 1,
    marginTop: 20,
    marginLeft: 10,
  },
  title: {
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(17),
  },
  subtitle: {
    color: '#505C74',
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(10),
    marginTop: 2,
    marginRight: 20,
    opacity: 1,
  },
  imageStyle_one: {
    width: 50,
    height: 50,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 10,
  },
  bottomSubContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottomText_1: {
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bottomText_2: {
    color: '#505C74',
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(10),
  },

  buyButton: {
    backgroundColor: '#AFFF00',
    width: 60,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },
  buyButtonText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
  },
});
