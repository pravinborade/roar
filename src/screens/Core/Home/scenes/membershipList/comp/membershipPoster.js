import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {styles} from './membershipPoster.style';

const membershipPoster = ({
  logoImage,
  title,
  subtitle,
  price,
  months,
  buyonPress,
  posterImage,
  posterImageHeight,
  posterImageWidth,
  backgroundColor,
}) => {
  return (
    <View style={{...styles.mainContainer, backgroundColor: backgroundColor}}>
      <View style={styles.mainSubContainer}>
        <View style={styles.view_one}>
          <Image style={styles.imageStyle_one} source={logoImage} />
        </View>
        <View style={styles.view_two}>
          <View>
            <Text style={styles.title}>{title}</Text>
          </View>
          <View>
            <Text style={styles.subtitle}>{subtitle}</Text>
          </View>
        </View>
        <View style={{marginRight: 20}}>
          <Image
            style={{
              width: posterImageWidth,
              height: posterImageHeight,
              resizeMode: 'contain',
            }}
            source={posterImage}
          />
        </View>
      </View>
      <View style={styles.bottomContainer}>
        <View style={styles.bottomSubContainer}>
          <Text style={styles.bottomText_1}>{price}</Text>
          <Text style={styles.bottomText_2}> ({months} months)</Text>
        </View>
        <TouchableOpacity style={styles.buyButton}>
          <Text style={styles.buyButtonText}>Buy</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default membershipPoster;
