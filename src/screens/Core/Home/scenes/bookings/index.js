import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
  DropDownCommon,
} from '../../../../../components';
import {
  Content,
  errorLog,
  heightToDp,
  If,
  ifValid,
  ScreenNames,
  widthToDp,
} from '../../../../../utils';
import BookingTitle from '../venues/venue_detail/comp/BookingTitle';
import {Carousal} from '../venues/venue_detail/comp';
import SlotDate from './comp/SlotDate';
import SlotTime from './comp/SlotTime';
import Sports from './comp/Sports';
import {useDispatch, useSelector} from 'react-redux';
import {colors, commonStyle} from '../../../../../styles';
import axiosService from '../../../../../commons/axiosService';
import moment from 'moment';
import Toast from 'react-native-toast-message';
import {
  checkSlotsOnServer,
  checkValidation,
  makeEntryToFirebaseForLockingSlot,
  setCurrentCourtAndAvailbleSlot,
  setFinalPrice,
} from './action';
import {
  clearMembershipServiceInventory,
  getServicesInventories,
} from '../services/action';
import {calculateRating} from '../../../../../utils/helper';

const venuePalava = 'Olympic Sports Complex Palava';

const HomeBookings = (props) => {
  let homeVenue = useSelector((x) => x.homeVenue.selectedVenue);
  let userToken = useSelector((state) => state.login.userToken);
  const dispatch = useDispatch();
  let userDetails = useSelector((state) => state.login.data);
  let {
    selectedSport,
    slotSelectedByOther,
    slot,
    slotsSelected,
    court,
    selectedCourt,
    courts,
    tempUserId,
    selectedDate,
  } = useSelector((x) => x.homeBookings);
  let loading = useSelector((x) => x.loader.loading);
  const {services, inventories, membership} = useSelector(
    (state) => state.homeServices,
  );

  let ratings = ifValid(homeVenue?.ratings) ? homeVenue?.ratings : [];
  let rating = calculateRating(ratings);

  const [disabled, setIsDisabled] = useState(true);
  const [isPickerVisible, setIsPickerVisible] = useState(false);
  const [playerCount, setPlayerCount] = useState(null);
  const [totalcount, setTotal] = useState('');
  /**
   * getting services and inventories list.
   */
  useEffect(() => {
    dispatch(getServicesInventories(homeVenue?.id));
    let subscriber = checkSlotsOnServer(
      homeVenue.id,
      dispatch,
      slotSelectedByOther,
    );
    return () => subscriber();
  }, []);

  useEffect(() => {
    checkValid();
    check();
  }, [slotsSelected, selectedSport, court]);

  const check = () => {
    let total = 0;
    for (var i = 0; i < slotsSelected.length; i++) {
      total = total + slotsSelected[i].offerRate;
      setTotal(total.toString());
    }
  };

  const checkValid = () => {
    checkValidation(court, slotsSelected, selectedSport)
      .then((x) => setIsDisabled(x))
      .catch((x) => setIsDisabled(x));
  };

  const getavailableslot = () => {
    if (
      slotsSelected[0].site.isSlotTimeEnable == true &&
      slotsSelected.length == 1
    ) {
      let payload = {
        startDate: moment(selectedDate).format('YYYY-MM-DD'),
        startTime: slotsSelected[0].startTime,
        site: {
          id: slotsSelected[0].site.id,
        },
      };
     
      axiosService
        .post(
          `api/offers/slot/available?access_token=${userDetails.token}`,
          payload,
        )
        .then((response) => {
          if (response.status === 200) {
            //// checking if services and inventories and memberships avaible else navigating to summary
            if (
              membership?.length === 0 &&
              services?.length === 0 &&
              inventories?.length === 0
            ) {           
              dispatch(
                clearMembershipServiceInventory([], [], [], slotsSelected),
              );
              props.navigation.navigate(ScreenNames.HOME_SUMMARY);
            } else {
              props.navigation.navigate(ScreenNames.HOME_SERVICES);
            }
            dispatch(setFinalPrice(slot.offerRate));
          } else {
            Toast.show({
              type: 'error',
              text1: `sorry you can only book within ${slotsSelected[0].site.slotAvailable} hrs`,
            });
          }
        })
        .catch((o) => errorLog('makeInitialTransaction error ' + o));
    } else {
      dispatch(setFinalPrice(slot.offerRate));
      //// checking if services and inventories and memberships avaible else navigating to summary
      if (
        membership?.length === 0 &&
        services?.length === 0 &&
        inventories?.length === 0
      ) {      
        dispatch(clearMembershipServiceInventory([], [], [], slotsSelected));
        props.navigation.navigate(ScreenNames.HOME_SUMMARY);
      } else {     
        props.navigation.navigate(ScreenNames.HOME_SERVICES);
      }
    }
  };

  const splitpaymentcond = () => {
    if (playerCount > 1) {  
      let data = {
        playerC: playerCount,
        totalamt: totalcount,
      };
      dispatch({type: 'SET_MY_SELECTED_PLAYER_TOTALAMOUNT', payload: data});
      props.navigation.navigate(ScreenNames.SPLITPAYMENT);
    } else {    
      getavailableslot();
    }
  };

  return (
    <Container>
      <HeaderBack navigation={props.navigation} title={Content.bookings} />
      <BookingTitle
        item={homeVenue}
        navigation={props.navigation}
        rating={rating}
        userDetails={userDetails}
      />
      <Carousal item={homeVenue} />
      <ScrollView>
        <DropDownCommon
          isVisible={isPickerVisible}
          onPickerTouch={() => {
            if (courts?.length < 1) {
              return;
            }
            setIsPickerVisible(!isPickerVisible);
          }}
          onPickerSelect={(court) => {
            setIsPickerVisible(false);
            dispatch(setCurrentCourtAndAvailbleSlot(court));
          }}
          onPickerClose={() => setIsPickerVisible(false)}
          title={Content.selectCourt}
          data={courts}
          court={court}
          value={court?.resource?.name}
          onValueChange={(court, index) => {
            dispatch(setCurrentCourtAndAvailbleSlot(court));
          }}
        />

        <View style={styles.heightView} />

        <Sports userDetails={userDetails} />

        <If show={userToken}>
          {homeVenue?.name === venuePalava && (
            <View style={commonStyle.detailsContainer}>
              <Text style={commonStyle.narrow32}>{Content.playercount}</Text>
              <TextInput
                onChangeText={(val) => setPlayerCount(val)}
                keyboardType="numeric"
                keyboardAppearance="light"
                value={playerCount}
                placeholder={Content.enterNumOfPlayer}
                placeholderTextColor={'#BFC7D8'}
                style={styles.numOfPlayersStyle}
              />
            </View>
          )}
        </If>

        <SlotDate userDetails={userDetails} />
        <SlotTime tempUserId={tempUserId} />
        {loading ? (
          <ActivityIndicator
            style={styles.venueDetailProgress}
            color={colors.primaryBlue}
            size={'small'}
          />
        ) : null}
      </ScrollView>
      <ButtonCommon
        disabled={disabled}
        style={{backgroundColor: disabled ? '#b5b5b5' : colors.primaryGreen}}
        onPress={() => {
          splitpaymentcond();
        }}
        text={Content.next}
      />
    </Container>
  );
};
export default HomeBookings;

const styles = StyleSheet.create({
  detailsContainerInner: {
    flexDirection: 'row',
    paddingVertical: heightToDp(1.0),
  },
  numOfPlayersStyle: {
    borderWidth: 0.5,
    borderRadius: 3,
    height: heightToDp(5),
    top: heightToDp(2),
  },
  heightView: {
    height: widthToDp(1.3),
    marginTop: widthToDp(4),
    backgroundColor: colors.background,
  },
  venueDetailProgress: {
    alignSelf: 'center',
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1,
  },
});
