import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import { colors, commonStyle } from '../../../../../../styles';
import {
  Content,
  fontToDp,
  heightToDp,
  ifNotValid,
  ifValid,
  showToastMsg,
  ValidationMsg,
  widthToDp,
} from '../../../../../../utils';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { setCourts, setCurrentCourtAndSelectedSlots } from '../action';
import NotAvailable from '../../../../../../components/NotAvailable';

moment.suppressDeprecationWarnings = true;

const SlotTime = ({ tempUserId }) => {
  const dispatch = useDispatch();
  const loading = useSelector((x) => x.loader.loading);
  let {
    selectedSport,
    slotSelectedByOther,
    slotsSelected,
    selectedDate,
    courts,
    court,
    slot,
    slots,
  } = useSelector((x) => x.homeBookings);
  /**
   * select multiple slots and update court with selected slots.
   * @param slot
   */
  const onSlotSelection = (slot) => {

    if (ifNotValid(slot.offerRate)) {
      showToastMsg(Content.oops, ValidationMsg.slotPriceNotValid);
      return;
    }
    // // dev
    // var SIDE_11 = 395//469;
    // var SIDE_9_A = 396//470//;
    // var SIDE_9_B = 397//471;

    //qa
    var SIDE_11 = 469;
    var SIDE_9_A = 470;
    var SIDE_9_B = 471;  

    var _courtList;

    let _court = {
      ...court,
      slots: court?.slots.map((o) => {
        if (o.id === slot.id) {
          return { ...o, isSelected: !o.isSelected, isCalulable: true };
        } else {
          return { ...o };
        }
      }),
    };


    if (_court.id == SIDE_11) {
      _courtList = courts.map((c) => {
        if (c.id != SIDE_11 && c.id == SIDE_9_A || c.id == SIDE_9_B) {
          return {
            ...c,
            slots: c.slots.map((o) => {
              if (slot.day == o.day && slot.startTime == o.startTime && slot.endTime == o.endTime) {
                return { ...o, isSelected: !o.isSelected, isCalulable: false }
              } else {
                return { ...o }
              }
            })
          }
        } else {
          return c
        }
      })
    }

    if (_court.id == SIDE_9_A || _court.id == SIDE_9_B) {
      _courtList = courts.map((c) => {
        if (c.id != _court.id && c.id == SIDE_11) {
          return {
            ...c,
            slots: c.slots.map((o) => {

              if (slot.day == o.day && slot.startTime == o.startTime && slot.endTime == o.endTime && o.isBooked == "NO") {
                return { ...o, isSelected: !o.isSelected, isCalulable: false }
              } else {
                return { ...o }
              }
            })
          }
        } else {
          return c
        }
      })
    }

    let slotsSelected = _court?.slots.filter((x) => x.isSelected === true);

    if (_courtList && _courtList.length > 0) {
      let slotsSelectedTemp1 = [];
      _courtList.map((c) => {
        let slotsSelectedTemp = [];
        slotsSelectedTemp = c.slots.filter((x) => x.isSelected === true)
        slotsSelectedTemp1 = slotsSelectedTemp1.concat(slotsSelectedTemp)
      })
      slotsSelected = slotsSelected.concat(slotsSelectedTemp1)
      dispatch(setCourts(_courtList))
    }

    dispatch(setCurrentCourtAndSelectedSlots(_court, slotsSelected));
  };

  const _renderItem = (slot, i) => {
    let isSlotDisable = slot.isBooked === 'YES' || slot.isTimeOut === true;
    let isIncluded = false;
    if (slotSelectedByOther) {
      Object.entries(slotSelectedByOther).forEach(([key, value]) => {
        if (key == slot.id && value == true) {
          isIncluded = true;
        }
      });
    }
    let slotPriceAndTime = `${moment(slot?.startTime, 'hh:mm a').format(
      'hh:mm A',
    )}`;
    let slotPrice = `Price: ${ifValid(slot.offerRate) ? slot.offerRate : '-'
      } Rs`;
    return isSlotDisable ? (
      <TouchableOpacity
        disabled={true}
        style={[styles.itemContainer]}
        onPress={() => onSlotSelection(slot)}>
        <View style={[styles.itemDisabled]}>
          <Text style={styles.slotTextStyle}>{slotPriceAndTime}</Text>
          <Text style={styles.slotTextStylePrice}>{slotPrice}</Text>
        </View>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => onSlotSelection(slot)}>
        <View
          style={[
            slot.isSelected ? styles.itemSelected : styles.itemNotSelected,
          ]}>
          <Text style={styles.slotTextStyle}> {slotPriceAndTime}</Text>
          <Text style={styles.slotTextStylePrice}>{slotPrice}</Text>
          {isIncluded && (
            <Text style={styles.someElseStyle}>
              {ValidationMsg.someoneElseAlsoBooking}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <View style={commonStyle.detailsContainer}>
        <Text style={commonStyle.narrow32}>{Content.slotTime}</Text>
        {loading ? (
          <View style={commonStyle.justifyAlignCenter}>
            <ActivityIndicator
              color={colors.primaryBlue}
              style={{ alignSelf: 'center' }}
              size={'small'}
            />
          </View>
        ) : (
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            scrollEnabled={false}
            style={{ marginTop: heightToDp(1) }}
            data={court?.slots ? court.slots : []}
            ListEmptyComponent={() => {
              return selectedSport === '' ? (
                <NotAvailable msg={Content.noSlotsFoundNotSelectedSport} />
              ) : (
                <NotAvailable msg={Content.noSlotsFound} />
              );
            }}
            numColumns={3}
            renderItem={({ item, index }) => {
              return _renderItem(item, index);
            }}
          />
        )}
      </View>
      <View style={styles.lineStyle} />
    </>
  );
};
export default SlotTime;

const styles = StyleSheet.create({
  container: {},
  lineStyle: { height: widthToDp(1.3), backgroundColor: colors.background },
  itemContainer: {
    width: widthToDp(29),
    marginRight: widthToDp(2),
    marginBottom: widthToDp(2),
    marginTop: widthToDp(0),
  },
  someElseStyle: {
    fontSize: fontToDp(9),
    paddingHorizontal: 2,
    textAlign: 'center',
  },
  slotTextStyle: {
    color: '#4e5c76',
    ...commonStyle.regular24,
    textAlign: 'center',
  },
  slotTextStylePrice: {
    color: '#4e5c76',
    ...commonStyle.regular13,
    textAlign: 'center',
  },
  itemDisabled: {
    borderRadius: widthToDp(1),
    padding: widthToDp(0.1),
    backgroundColor: '#b5b5b5',
    ...commonStyle.justifyAlignCenter,
  },
  itemSelected: {
    borderRadius: widthToDp(1),
    padding: widthToDp(0.1),
    backgroundColor: colors.primaryGreen,
    ...commonStyle.justifyAlignCenter,
  },
  itemNotSelected: {
    borderRadius: widthToDp(1),
    padding: widthToDp(0.1),
    backgroundColor: colors.lightBlue,
    ...commonStyle.justifyAlignCenter,
  },
});
