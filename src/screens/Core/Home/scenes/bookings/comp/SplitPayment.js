import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  View,
  TextInput,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
} from '../../../../../../components';
import {Content} from '../../../../../../utils/content/Content';
import Images from '../../../../../../assets/Images/Images';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import {ifNotValid, ifValid} from '../../../../../../utils/helper';
import {useDispatch, useSelector} from 'react-redux';
import {colors, commonStyle} from '../../../../../../styles';
import axiosService from '../../../../../../commons/axiosService';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import {setCurrentCourtAndAvailbleSlot, setFinalPrice} from '../action';
import Toast from 'react-native-toast-message';
import helper from '../../../../../../commons/helperFunctions';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../styles/typography';
import {fontToDp} from '../../../../../../utils/Responsive';
import moment from 'moment';
var validate = require('validate.js');
// playerC
// import DropDownCommon from '../../../../../components/DropDownCommon';
// import {successLog} from '../../../../../utils/fireLog';
// import { heightToDp } from '../../../../../utils/Responsive';

const SplitPayment = (props) => {
  let homeVenue = useSelector((x) => x.homeVenue);
  const dispatch = useDispatch();
  homeVenue =
    ifValid(homeVenue) && ifValid(homeVenue.selectedVenue)
      ? homeVenue.selectedVenue
      : {};
  let userDetails = useSelector((state) => state.login.data);
  let {
    selectedSport,
    slot,
    slotsSelected,
    court,
    selectedCourt,
    courts,
    selectedDate,
  } = useSelector((x) => x.homeBookings);
  let loading = useSelector((x) => x.loader.loading);
  let splitPayData = useSelector((state) => state.myBookings.splitpaymentdata);
  let siteId =
    ifValid(slotsSelected) && ifValid(slotsSelected[0])
      ? slotsSelected[0].site.id
      : undefined;
  const [loadingforadd, setLoadingForAdd] = useState(false);
  const [loadingerror, setLoadingerror] = useState(false);
  const [inserteddata, setInsertedData] = useState([]);
  const [inserted, setInserted] = useState([]);
  const [playerCount, setPlayerCount] = useState([]);
  const [locationValue, setLocationValue] = useState('');
  const [allsearch, setAllSearch] = useState([]);
  const [inputRef, setInputRef] = useState(null);
  const [totalcount, setTotal] = useState('');
  const [loadingadd, setLoadingAdd] = useState(false);

  console.log('slotsSelected', splitPayData.playerC);

  useEffect(() => {
    setInsertedData([userDetails]);
  }, []);

  const getavailableuser = async (v) => {
    if (locationValue != '') {
      let response = await axiosService.get(
        `api/users/user/search/phone/autocomplete?siteId=${siteId}&term=${v}`,
      );
      console.log('@#@DATA---->', response);
      try {
        if (response.status === 200) {
          if (v != '') {
            let totalnumbers = response?.data?.data;
            setAllSearch(totalnumbers);
          } else {
            setAllSearch([]);
          }
        } else {
          if (v.length == 10) {
            setLoadingerror(true);
            setAllSearch([]);
          }
        }
      } catch (error) {
        setLoadingForAdd(false);
        console.log(error);
      }
    }
  };

  const slotbook = async () => {
    dispatch({type: 'SET_LOADING', payload: true});
    let endTime = slotsSelected[slotsSelected.length - 1].endTime;
    let payload = slotsSelected.map((item) => {
      return {
        endDate: moment(selectedDate).format('YYYY-MM-DD'),
        endTime: endTime,
        noOfPlayers: parseInt(splitPayData.playerC),
        offerRate: item.offerRate,
        payment_status: 'UNPAID',
        resource: {
          id: item.resource.id,
        },
        site: {...item.site},
        slotId: item.id,
        sportPlayedFor: selectedSport,
        startDate: moment(selectedDate).format('YYYY-MM-DD'),
        startTime: item.startTime,
        status: 'UNAVAILABLE',
        userId: userDetails.id,
      };
    });
    console.log('PAYLOAD FOR OFFER------->', payload);
    let response = await axiosService.post(`api/offers`, payload);
    console.log('DATA OFFER API---->', response);
    try {
      let datavalid = response?.data?.data;
      if (response.status === 200) {
        dispatch({type: 'SET_LOADING', payload: false});
        for (var i = 0; i <= datavalid.length; i++) {
          let responses = response?.data?.data[i];
          createLink(responses);
        }
      } else {
      }
    } catch (error) {
      console.log(error);
    } finally {
      dispatch({type: 'SET_LOADING', payload: false});
    }
  };

  const createLink = async (responses) => {
    let payload = inserteddata?.map((item) => {
      return {
        amount: item.amount == undefined ? 0 : item.amount,
        bookingId: responses.id,
        currency: 'INR',
        email: item.email,
        mobileNo: item.phoneNo,
        refUserId: userDetails.id,
        siteId: responses.site.id,
        slotId: responses.slotId,
        userId: userDetails.id,
        userName: item.firstName,
      };
    });
    console.log('PAYLOAD LINK------->', payload);
    let response = await axiosService.post(`api/payments/createLink`, payload);
    console.log('DATA OFFER API LINK---->', response);
    try {
      if (response.status === 200) {
        props.navigation.navigate(ScreenNames.HOME_ACKNOWLEDGEMENT);
      } else {
        alert('Something went wrong');
        dispatch({type: 'SET_LOADING', payload: false});
      }
    } catch (error) {
      console.log(error);
    }
  };

  const hasPropOrStateChange = {propKeyToWatch: props};

  const deleteData = (id) => {
    console.log('DELETE DATA');
    setInsertedData(inserteddata.filter((item) => item.phoneNo !== id));
  };

  const _update = (text, index) => {
    let total = 0;
    let newArray = [...inserteddata];
    newArray[index] = {
      ...newArray[index],
      amount: text == '' ? 0 : parseInt(text),
    };
    for (var i = 0; i < newArray.length; i++) {
      if (newArray[i].amount != undefined && newArray[i].amount != NaN) {
        total = total + newArray[i].amount;
        setTotal(total.toString());
      }
    }
    setInsertedData(newArray);
  };

  const checkcond = () => {
    if (inserteddata?.length == splitPayData?.playerC) {
      let payload = inserteddata.map((item) => {
        return {
          membershipcount: item.membership,
        };
      });
      var count = payload.filter(function (s) {
        return s.membershipcount;
      }).length;
      let total = (splitPayData.totalamt / inserteddata.length) * count;
      if (splitPayData.totalamt == totalcount) {
        slotbook();
      } else {
        Toast.show({
          type: 'error',
          text1: 'Sorry please Check your amount',
        });
      }
    } else {
      Toast.show({
        type: 'error',
        text1: 'Sorry please add member for payment',
      });
    }
  };

  const addPerson = (item) => {
    try {
      console.log('@@@@@DATA---->', inserteddata.length, splitPayData.playerC);
      if (inserteddata.length < splitPayData.playerC) {
        let data = inserteddata.find((el) => el.phoneNo === item.phoneNo);
        if (data == undefined) {
          let list = [...inserteddata];
          let personToList = allsearch.find(
            (el) => el.phoneNo === item.phoneNo,
          );
          list.push(personToList);
          setInsertedData(list);
        } else {
          Toast.show({
            type: 'error',
            text1: 'sorry already added',
          });
        }
      } else {
        Toast.show({
          type: 'error',
          text1: 'sorry you can not add more members',
        });
      }
    } catch (error) {
      console.log('@@@@error', error);
    }
  };

  return (
    <Container>
      <HeaderBack navigation={props.navigation} title={Content.splitPayment} />
      <View style={{backgroundColor: colors.background, flex: 1}}>
        <Text
          style={{
            fontFamily: PT_SANS_NARROW_BOLD,
            color: '#202B46',
            fontSize: fontToDp(14),
            textAlign: 'center',
            top: 5,
          }}>
          Slot Amount : <Text> Rs.{splitPayData.totalamt}</Text>
        </Text>
        <View style={{top: '2%'}}>
          <View style={[styles.containerStyle]}>
            <TextInput
              placeholderTextColor="#505C74"
              placeholder={'Search Contact No...'}
              maxLength={10}
              style={[styles.inputStyle]}
              value={locationValue}
              keyboardType="numeric"
              onChangeText={(v) => {
                setLocationValue(v);
                getavailableuser(v);
              }}
            />
          </View>

          {loadingerror == true && (
            <ButtonCommon
              style={{width: '50%', alignSelf: 'center'}}
              onPress={() => {
                props.navigation.navigate(ScreenNames.ADDMEMBER);
              }}
              text={'Add Member'}
            />
          )}
        </View>

        <View style={{marginTop: '4%'}}>
          <FlatList
            contentContainerStyle={{justifyContent: 'space-between'}}
            scrollEnabled={true}
            style={{padding: 10, height: heightToDp('22')}}
            data={allsearch}
            extraData={hasPropOrStateChange}
            horizontal={false}
            refreshing={true}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={item.phoneNo}
                onPress={() => {
                  addPerson(item), setAllSearch([]), setLocationValue('');
                }}>
                <View>
                  <View
                    style={{
                      padding: 10,
                      backgroundColor: colors.white,
                      marginBottom: 5,
                    }}>
                    <View>
                      <Text
                        style={{
                          fontFamily: PT_SANS_NARROW_BOLD,
                          color: '#202B46',
                          fontSize: fontToDp(14),
                        }}>
                        {item.firstName}
                        <Text> {item.lastName}</Text>
                        <Text> {item.phoneNo}</Text>
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>

        <ScrollView>
          <FlatList
            contentContainerStyle={{justifyContent: 'space-between'}}
            scrollEnabled={true}
            style={{padding: 10}}
            data={inserteddata}
            extraData={hasPropOrStateChange}
            horizontal={false}
            refreshing={true}
            renderItem={({item, index}) => (
              <View key={item.phoneNo}>
                <View
                  style={{
                    padding: 10,
                    backgroundColor: colors.white,
                    marginBottom: 10,
                  }}>
                  <View
                    style={{
                      justifyContent: 'space-between',
                      marginHorizontal: 15,
                      flexDirection: 'row',
                    }}>
                    <View>
                      <Image
                        style={{height: 90, width: 100, borderRadius: 5}}
                        source={
                          item.image == null ||
                          item.image == '' ||
                          item.image == undefined
                            ? Images.User_avatar
                            : {uri: item.image}
                        }
                      />
                    </View>
                    <View style={{width: '50%'}}>
                      <Text
                        style={{
                          fontFamily: PT_SANS_NARROW_BOLD,
                          color: '#202B46',
                          fontSize: fontToDp(14),
                        }}>
                        {item.firstName}
                        <Text> {item.lastName}</Text>
                      </Text>
                      <Text
                        style={{
                          fontFamily: PT_SANS_REGULAR,
                          color: '#505C74',
                          fontSize: fontToDp(12),
                        }}>
                        {item.email}
                      </Text>
                      <Text
                        style={{
                          fontFamily: PT_SANS_REGULAR,
                          color: '#202B46',
                          fontSize: fontToDp(12),
                        }}>
                        {item.phoneNo}
                      </Text>

                      <TextInput
                        style={styles.input}
                        onChangeText={(text) => _update(text, index)}
                        value={playerCount[index]}
                        placeholder="enter amount"
                        keyboardType="numeric"
                      />
                    </View>
                    <View>
                      <TouchableOpacity
                        onPress={() => {
                          deleteData(item.phoneNo);
                        }}
                        style={styles.crossStyle}>
                        <Image
                          source={Images.cross}
                          style={styles.crossImage}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )}
          />
        </ScrollView>
        <KeyboardAvoidingView behavior="padding">
          <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
            <Text
              style={{
                fontFamily: PT_SANS_NARROW_BOLD,
                color: '#202B46',
                fontSize: fontToDp(18),
                textAlignVertical: 'center',
              }}>
              Total :{' '}
            </Text>
            <TextInput
              style={styles.inputtotal}
              onChangeText={(val) => setTotal(val)}
              value={totalcount}
              placeholder="Total amount"
              keyboardType="numeric"
            />
          </View>
          <ButtonCommon
            loading={loading}
            onPress={() => {
              checkcond();
              // console.log("@@@@@DATA---->",inserteddata.length,splitPayData.playerC)
            }}
            text={'next'}
          />
        </KeyboardAvoidingView>
      </View>
    </Container>
  );
};
export default SplitPayment;

const styles = StyleSheet.create({
  containerStyle: {
    marginHorizontal: widthToDp('4%'),
    flexDirection: 'row',
    backgroundColor: colors.lightBlue,
    alignItems: 'center',
  },
  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp('5%'),
    height: widthToDp('5%'),
    marginLeft: widthToDp('2%'),
    marginRight: widthToDp('1%'),
  },
  input: {
    height: 35,
    width: 80,
    top: 4,
    borderRadius: 7,
    borderWidth: 0.5,
    fontSize: 12,
  },
  inputtotal: {
    height: heightToDp(6),
    width: widthToDp(50),
    borderRadius: 7,
    borderWidth: 0.5,
    marginRight: '5%',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  inputStyle: {
    flex: 1,
    ...commonStyle.regular24,
    height: widthToDp(10),
    padding: widthToDp('2'),
  },
  mainContainer: {backgroundColor: colors.white},
  crossStyle: {
    position: 'absolute',
  },
  crossImage: {
    width: widthToDp(3),
    height: widthToDp(3),
    resizeMode: 'contain',
  },
  errorText: {
    ...commonStyle.regular16,
    marginHorizontal: widthToDp(4.5),
    color: 'red',
  },
  boxStyleAmount: {},
});
