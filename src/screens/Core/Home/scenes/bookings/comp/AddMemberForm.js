import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  View,
  TextInput,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  ButtonCommon,
  Container,
  HeaderBack,
} from '../../../../../../components';
import {Content} from '../../../../../../utils/content/Content';
import Images from '../../../../../../assets/Images/Images';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import {ifNotValid, ifValid} from '../../../../../../utils/helper';
import {useDispatch, useSelector} from 'react-redux';
import {colors, commonStyle} from '../../../../../../styles';
import axiosService from '../../../../../../commons/axiosService';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import DropDownSample from '../../../../../../components/DropDownSample';
import {setCurrentCourtAndAvailbleSlot, setFinalPrice} from '../action';
import Toast from 'react-native-toast-message';
import {
  PT_SANS_BOLD,
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../styles/typography';
import {fontToDp} from '../../../../../../utils/Responsive';
var validate = require('validate.js');
// import {successLog} from '../../../../../utils/fireLog';
// import { heightToDp } from '../../../../../utils/Responsive';

const AddMemberForm = (props) => {
  let homeVenue = useSelector((x) => x.homeVenue);
  const dispatch = useDispatch();
  homeVenue =
    ifValid(homeVenue) && ifValid(homeVenue.selectedVenue)
      ? homeVenue.selectedVenue
      : {};
  let userDetails = useSelector((state) => state.login.data);
  let {
    selectedSport,
    slot,
    slotsSelected,
    court,
    selectedCourt,
    courts,
    selectedDate,
  } = useSelector((x) => x.homeBookings);
  let siteId =
    ifValid(slotsSelected) && ifValid(slotsSelected[0])
      ? slotsSelected[0].site.id
      : undefined;
  console.log('DATA--->', slotsSelected);
  let data = [
    {
      id: 1,
      type: 'Team',
    },
    {
      id: 2,
      type: 'Student',
    },
    {
      id: 3,
      type: 'Corporate',
    },
    {
      id: 4,
      type: 'Non User',
    },
  ];
  let genderr = [
    {
      id: 1,
      type: 'Male',
    },
    {
      id: 1,
      type: 'Female',
    },
  ];
  const [loading, setloading] = useState(false);
  const [isPickerVisible, setIsPickerVisible] = useState(false);
  const [isPickerVisibleforgender, setIsPickerVisibleForGender] =
    useState(false);
  const [isVisible, setIsVisible] = useState(true);
  const [disabled, setIsDisabled] = useState(true);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [contact, setContact] = useState('');
  const [address, setAddress] = useState('');
  const [occupation, setOccupation] = useState('');
  const [dob, setDob] = useState('');
  const [gender, setGender] = useState('');
  const [selectedtype, setSelectedType] = useState('');

  const [fnamerror, setfnameError] = useState(false);
  const [lnamerror, setlnameError] = useState(false);
  const [emailerror, setemailError] = useState(false);
  const [phoneNoerror, setphoneNoError] = useState(false);
  const [addresserror, setaddressError] = useState(false);
  const [occupationerror, setoccupationError] = useState(false);
  const [doberror, setdobError] = useState(false);
  const [gendererror, setgenderError] = useState(false);
  const [selectedtypeerror, setselectedtypeError] = useState(false);

  const addcustomer = async () => {
    if (validation()) {
      console.log('ENTER VALIDATION');
      setloading(true);
      let payload = {
        address: address,
        customerType: selectedtype,
        dob: dob,
        email: email,
        firstName: name,
        gender: gender == 'Male' ? 'true' : 'false',
        nonUser: userDetails.nonUser,
        occupation: occupation,
        organization: {
          id: slotsSelected[0].site.organization.id,
        },
        phoneNo: contact,
        siteId: slotsSelected[0].site.id,
        type: userDetails.type,
        userRole: userDetails.role.roleName,
        username: userDetails.username,
      };
      console.log('DATA PAYLOAD---->', payload);
      let response = await axiosService.post(
        `login/signup?customer=true`,
        payload,
      );
      console.log(response);
      try {
        if (response.status === 200) {
          console.log('ALL TOTAL NUMBER----->');
          Toast.show({
            type: 'success',
            text1: 'record added successfully',
          });
          setloading(false);
          props.navigation.navigate(ScreenNames.SPLITPAYMENT);
        } else {
          setloading(false);
        }
      } catch (error) {
        Toast.show({
          type: 'error',
          text1: error,
        });
        console.log(error);
        setloading(false);
      }
    }
  };

  const emeilid = () => {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  };
  // ^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$
  const validateMobile = () => {
    var re = /^(\+\d{1,3}[- ]?)?\d{10}$/;
    return re.test(contact);
  };

  const validateDob = () => {
    var re = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/;
    return re.test(dob);
  };

  const validation = () => {
    if (validate.isEmpty(name)) {
      setfnameError(true);
    } else if (validate.isEmpty(selectedtype)) {
      setselectedtypeError(true);
    } else if (!emeilid(email) || validate.isEmpty(email)) {
      setemailError(true);
    } else if (!validateMobile(contact) || validate.isEmpty(contact)) {
      setphoneNoError(true);
    } else if (validate.isEmpty(address)) {
      setaddressError(true);
    } else if (validate.isEmpty(occupation)) {
      setoccupationError(true);
    } else if (!validateDob(dob) || validate.isEmpty(dob)) {
      setdobError(true);
    } else if (validate.isEmpty(gender)) {
      setgenderError(true);
    } else {
      return true;
    }
    return false;
  };

  return (
    <Container>
      <View style={{backgroundColor: colors.background, flex: 1}}>
        <HeaderBack navigation={props.navigation} title={Content.addMember} />

        <ScrollView>
          <View style={styles.subView}>
            <Text style={styles.viewText}>{Content.FirstName}</Text>
            <TextInput
              onChangeText={(text) => setName(text)}
              value={name}
              style={styles.textInput}
            />
            {fnamerror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Please enter first name
              </Text>
            )}
          </View>
          <View style={styles.subView}>
            <DropDownSample
              onPickerTouch={() => setIsPickerVisible(!isPickerVisible)}
              onPickerSelect={(data) => {
                setIsPickerVisible(false);
                setSelectedType(data);
              }}
              onPickerClose={() => setIsPickerVisible(false)}
              isVisible={isPickerVisible}
              data={data}
              value={selectedtype}
              title={Content.titleadd}
            />
            {selectedtypeerror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Please select cutomer type
              </Text>
            )}
          </View>

          <View style={styles.subView}>
            <Text style={styles.viewText}>{Content.eMailId}</Text>
            <TextInput
              onChangeText={(text) => setEmail(text)}
              value={email}
              style={styles.textInput}
            />
            {emailerror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *email cannot be empty and should be valid
              </Text>
            )}
          </View>

          <View style={styles.subView}>
            <Text style={styles.viewText}>{Content.phNo}</Text>
            <TextInput
              onChangeText={(text) => setContact(text)}
              value={contact}
              maxLength={10}
              style={styles.textInput}
            />
            {phoneNoerror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Phone No cannot be empty and should be 10 Digit
              </Text>
            )}
          </View>

          <View style={styles.subView}>
            <Text style={styles.viewText}>{Content.address}</Text>
            <TextInput
              onChangeText={(text) => setAddress(text)}
              maxLength={10}
              value={address}
              style={styles.textInput}
            />
            {addresserror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Address cannot be empty
              </Text>
            )}
          </View>

          <View style={styles.subView}>
            <Text style={styles.viewText}>{Content.occupastion}</Text>
            <TextInput
              onChangeText={(text) => setOccupation(text)}
              value={occupation}
              style={styles.textInput}
            />
            {occupationerror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Occupation cannot be empty
              </Text>
            )}
          </View>
          <View style={styles.subView}>
            <Text style={styles.viewText}>{Content.Dob}</Text>
            <TextInput
              onChangeText={(text) => setDob(text)}
              value={dob}
              placeholder={'for ex : YYYY-MM-DD'}
              style={styles.textInput}
            />
            {doberror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Dob cannot be empty
              </Text>
            )}
          </View>
          <View style={styles.subView}>
            <DropDownSample
              onPickerTouch={() =>
                setIsPickerVisibleForGender(!isPickerVisibleforgender)
              }
              onPickerSelect={(genderr) => {
                setIsPickerVisibleForGender(false);
                setGender(genderr);
              }}
              onPickerClose={() => setIsPickerVisibleForGender(false)}
              isVisible={isPickerVisibleforgender}
              data={genderr}
              value={gender}
              title={Content.gender}
            />
            {gendererror && (
              <Text style={{color: '#FF0033', marginHorizontal: '0%'}}>
                *Please select gender
              </Text>
            )}
          </View>
        </ScrollView>
        <KeyboardAvoidingView behavior="padding">
          <ButtonCommon
            loading={loading}
            onPress={() => {
              addcustomer();
            }}
            text={'next'}
          />
        </KeyboardAvoidingView>
      </View>
    </Container>
  );
};
export default AddMemberForm;

const styles = StyleSheet.create({
  subView: {
    padding: 12,
    borderBottomWidth: 3,
    borderBottomColor: colors.background,
  },
  mainContainer: {
    borderTopColor: colors.background,
    borderTopWidth: 4,
    backgroundColor: colors.white,
    height: heightToDp('20%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewText: {
    color: '#505C74',
    fontFamily: PT_SANS_BOLD,
    fontSize: fontToDp(12),
    marginBottom: 5,
  },
  textInput: {
    padding: 10,
    backgroundColor: colors.white,
    height: 40,
  },
  headerText: {
    padding: 10,
    backgroundColor: colors.white,
    height: 40,
    color: '#202B46',
    fontFamily: PT_SANS_REGULAR,
    fontSize: fontToDp(14),
  },
  imageView: {
    resizeMode: 'cover',
    height: fontToDp(80),
    width: fontToDp(80),
    borderRadius: fontToDp(80 / 2),
    overflow: 'hidden',
  },
});
