import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../styles';
import {Line} from '../../../../../../components';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {
  getCourtsAndSlotsOnDateChangeApi,
  setSelectedSportIcon,
} from '../action';
import {ifValid} from '../../../../../../utils/helper';
import NotAvailable from '../../../../../../components/NotAvailable';
import {Content} from '../../../../../../utils/content/Content';

const Sports = ({userDetails = {}}) => {
  const dispatch = useDispatch();
  let sportPlayedFinal = useSelector(
    (state) => state.homeBookings.sportPlayedPlain,
  );

  const selectedVenue = useSelector((state) => state.homeVenue.selectedVenue);
  let {selectedSport, selectedDate, courts, court, selectedCourt} = useSelector(
    (x) => x.homeBookings,
  );

  let selectedSportVenue = useSelector((x) => x.homeVenue.selectedSport);

  const onSelectedItemPress = async (item) => {
    // dispatch(
    //   getCourtsAndSlotsOnDateChangeApi(
    //     selectedDate,
    //     selectedVenue,
    //     selectedSportVenue?.name,
    //     selectedCourt,
    //     selectedSport,
    //   ),
    // );
    let sportSportIconName = '';
    let v = court.sportPlayedFor.map((o) => {
      if (o.id === item.id) {
        if (!o.isSelected) {
          sportSportIconName = item.name;
        }
        return {...o, isSelected: !o.isSelected};
      } else {
        return {...o, isSelected: false};
      }
    });
    if (sportSportIconName === '') {
      console.log('sportIcon is empty');
      return;
    }

    await dispatch(setSelectedSportIcon(sportSportIconName));
  };

  return (
    <View>
      <View style={commonStyle.detailsContainer}>
        <Text style={commonStyle.narrow30}>{Content.availableSports}</Text>

        <View style={styles.sportsRow}>
          {court && court.sportPlayedFor && court.sportPlayedFor.length > 0 ? (
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {court.sportPlayedFor.map((x, i) => {
                return (
                  <TouchableOpacity
                    style={[
                      {
                        backgroundColor:
                          selectedSport === (ifValid(x.name) ? x.name : '')
                            ? colors.primaryGreen
                            : colors.white,
                      },
                      styles.imageContainer,
                    ]}
                    key={i.toString()}
                    onPress={() => onSelectedItemPress(x)}
                  >
                    <Image source={x.image} style={styles.imageStyleSport} />
                    <Text style={commonStyle.regular24}>{x.name}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          ) : (
            <NotAvailable msg={'No available sports found'} />
          )}
        </View>
      </View>
      <View style={styles.lineStyle} />
    </View>
  );
};
export default Sports;

const styles = StyleSheet.create({
  container: {},
  imageStyleSport: {
    resizeMode: 'contain',
    width: widthToDp(6),
    height: widthToDp(6),
  },
  sportsRow: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: heightToDp(2.0),
  },
  imageContainer: {
    padding: widthToDp(1.4),
    alignItems: 'center',
    marginRight: widthToDp(3),
  },
  lineStyle: {
    height: widthToDp(1.5),
    backgroundColor: colors.background,
  },
});
