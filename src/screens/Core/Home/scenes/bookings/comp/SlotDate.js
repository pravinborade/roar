import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {colors, commonStyle} from '../../../../../../styles';
import {Content} from '../../../../../../utils/content/Content';
import CalendarStrip from 'react-native-calendar-strip';
import {widthToDp} from '../../../../../../utils/Responsive';
import {useDispatch, useSelector} from 'react-redux';
import {getCourtsAndSlotsOnDateChangeApi} from '../action';
import moment from 'moment';

const SlotDate = ({userDetails = {}}) => {
  const dispatch = useDispatch();
  const selectedVenue = useSelector((state) => state.homeVenue.selectedVenue);
  const {selectedCategory, selectedDate, selectedCourt, selectedSport} =
    useSelector((state) => state.homeBookings);
  let selectedSportVenue = useSelector((x) => x.homeVenue.selectedSport);

  useEffect(() => {}, []);
  const onDateSelected = (date) => {
    try {
      dispatch(
        getCourtsAndSlotsOnDateChangeApi(
          moment(date),
          selectedVenue,
          selectedSportVenue?.name,
          selectedCourt,
          selectedSport,
        ),
      );
    } catch (e) {
      console.warn(e);
    }
  };

  return (
    <View>
      <View style={commonStyle.detailsContainer}>
        <Text style={commonStyle.narrow32}>{Content.slotDate}</Text>

        <CalendarStrip
          scrollable
          selectedDate={selectedDate === '' ? new Date() : selectedDate}
          calendarAnimation={{type: 'sequence', duration: 30}}
          daySelectionAnimation={{
            type: 'background',
            duration: 300,
            highlightColor: '#9265DC',
          }}
          style={{paddingTop: widthToDp(1), paddingBottom: widthToDp(3)}}
          dateNumberStyle={commonStyle.regular13}
          dateNameStyle={commonStyle.regular13}
          iconContainer={{flex: 0.1}}
          highlightDateNameStyle={commonStyle.regular13}
          highlightDateNumberStyle={commonStyle.regular13}
          highlightDateContainerStyle={{
            backgroundColor: colors.primaryGreen,
            borderRadius: 0,
            width: widthToDp(12),
            height: widthToDp(10),
          }}
          onDateSelected={(d) => onDateSelected(d)}
          useIsoWeekday={false}
        />
      </View>
      <View style={styles.lineStyle} />
    </View>
  );
};
export default SlotDate;

const styles = StyleSheet.create({
  container: {},
  lineStyle: {height: widthToDp(1.3), backgroundColor: colors.background},
});
