import axiosService from '../../../../../commons/axiosService';
import moment from 'moment';
import {ifGreaterArray, ifNotValid, ifValid} from '../../../../../utils/helper';
import {errorLog, successLog} from '../../../../../utils';
import {_dataPlain} from '../sports/data';
import {sportImageGroup} from '../sports/sport_all/action';
import {capitalize} from 'lodash';
import firestore from '@react-native-firebase/firestore';
import {store} from '../../../../../redux/store';

export const getCourtsAndSlotsApi = (
  date,
  selectedVenue,
  userDetails = {},
  selectedCategory,
) => {
  return (dispatch) => {
    let siteId = ifValid(selectedVenue?.id) ? selectedVenue?.id : undefined;
    let organizationId = selectedVenue.organization.id;
    let userId = ifValid(userDetails.id) ? userDetails.id : undefined;
    if (ifNotValid(siteId)) {
      errorLog('site id is invalid');
      return;
    }
    if (!ifValid(selectedCategory) || selectedCategory === '') {
      errorLog('selected sport is invalid');
      return;
    }
    if (ifNotValid(organizationId)) {
      errorLog('organizationId  is invalid');
      return;
    }
    if (ifNotValid(date) || date === '') {
      errorLog('date  is invalid');
      return;
    }
    dispatch(getSlots(date, siteId, selectedCategory, organizationId));
  };
};

export const getSlots = (date, siteId, category, organizationId) => {
  if (ifNotValid(date) || date === '') {
    errorLog('selected date is not valid.!');
    return;
  }
  return (dispatch) => {
    dispatch(clearCourtSelectedSportSlotsSlot());
    dispatch({type: 'SET_LOADING', payload: true});
    //let url = `customer/dashboard/${siteId}/${category}?orgId=${organizationId}&day=${moment(date).format('dddd').toUpperCase()}&startDate=${moment(date).format('YYYY-MM-DD')}&reqType=${'CWIDGET'}`;
    //// new api provided by Ankush
    let url = `customer/dashboard/${siteId}/${capitalize(
      category,
    )}?orgId=${organizationId}&day=${moment(date)
      .format('dddd')
      .toUpperCase()}&startDate=${moment(date).format(
      'YYYY-MM-DD',
    )}&reqType=${'CWIDGET'}`;
    axiosService
      .get(url)
      .then((res) => {
        successLog('slot api', res.status);
        if (res.status === 200) {
          res = res.data;
          if (ifNotValid(res)) {
            errorLog('invalid response ');
            return;
          }
          let courtId = res.data[0].resource.id;
          let courts = res.data;
          let selectedDate = date;
          if (courtId) {
            try {
              let selectedMonth = moment(selectedDate).format('M');
              let selectedYear = moment(selectedDate).format('YYYY');
              let index = courts.findIndex((x) => x.resource.id === courtId);
              if (index !== -1) {
                let _courts = courts.map((x, i) => {
                  let array = x.resource.sportPlayedFor.split(',');
                  return {
                    ...x,
                    sportPlayedFor: sportImageGroup(array, _dataPlain),
                    slots: x.slots?.map((y) => {
                      return {
                        ...y,
                        duration: calculateTime(y),
                        isSelected: false,
                      };
                    }),
                  };
                });

                let selectedSport =
                  ifGreaterArray(_courts) &&
                  ifGreaterArray(_courts[0].sportPlayedFor) &&
                  ifValid(_courts[0].sportPlayedFor[0].name)
                    ? _courts[0].sportPlayedFor[0].name
                    : '';
                dispatch({
                  type: 'SET_CORT_MONTH_YEAR_SLOTS',
                  payload: {
                    selectedCourt: {
                      courtId: courtId,
                      name:
                        _courts &&
                        _courts.length &&
                        ifValid(_courts[0].resource.name)
                          ? _courts[0].resource.name
                          : '',
                    },
                    courts: _courts,
                    court: _courts && _courts.length ? _courts[0] : {},
                    selectedSport: selectedSport,
                    selectedDate,
                    selectedMonth,
                    selectedYear,
                    slot: {},
                  },
                });
              }
            } catch (e) {
              errorLog(e);
            }
          }
        }
        dispatch({type: 'SET_LOADING', payload: false});
      })
      .catch((o) => {
        dispatch({type: 'SET_LOADING', payload: false});
        errorLog('getSportList error ' + o);
      });
  };
};
/**
 * on date change update slots of that date by clearing previous and
 * taking previous selected sport and court.
 * @param date
 * @param selectedVenue
 * @param category
 * @param courtSelected
 * @param sportSelected
 */
export const getCourtsAndSlotsOnDateChangeApi = (
  date,
  selectedVenue = {},
  category,
  courtSelected,
  sportSelected,
) => {
  try {
    return (dispatch) => {
      let siteId = ifValid(selectedVenue?.id) ? selectedVenue.id : undefined;
      let organizationId = selectedVenue?.organization?.id;
      if (ifNotValid(siteId)) {
        errorLog('site id is invalid');
        return;
      }
      if (!ifValid(category) || category === '') {
        errorLog('selected sport is invalid');
        return;
      }
      if (ifNotValid(organizationId)) {
        errorLog('organizationId  is invalid');
        return;
      }
      if (ifNotValid(date) || date === '') {
        errorLog('date  is invalid');
        return;
      }

      dispatch({type: 'SET_LOADING', payload: true});
      dispatch(clearSlotsAndSlot());

      let url = `customer/dashboard/${siteId}/${category}?orgId=${organizationId}&day=${moment(
        date,
      )
        .format('dddd')
        .toUpperCase()}&startDate=${moment(date).format(
        'YYYY-MM-DD',
      )}&reqType=${'CWIDGET'}`;

      axiosService
        .get(url)
        .then((res) => {
          successLog('get slot api', res.status);
          if (res.status === 200) {
            res = res.data;
            if (ifNotValid(res)) {
              errorLog('invalid response ');
              return;
            }
            let courtId = store.getState().homeBookings.selectedCourt.courtId;
            let courts = res.data;
            let selectedDate = date;
            if (courtId) {
              try {
                let selectedMonth = moment(selectedDate).format('M');
                let selectedYear = moment(selectedDate).format('YYYY');
                let courtposition = 0;
                let _courts = courts.map((x, i) => {
                  let array = x.resource.sportPlayedFor.split(',');
                  if (x.resource.id == courtId) {
                    courtposition = i;
                  }
                  return {
                    ...x,
                    sportPlayedFor: sportImageGroup(array, _dataPlain),
                    slots: x.slots?.map((y) => {
                      return {
                        ...y,
                        duration: calculateTime(y),
                        isSelected: false,
                      };
                    }),
                  };
                });
                dispatch({
                  type: 'SET_CORT_MONTH_YEAR_SLOTS',
                  payload: {
                    selectedCourt: courtSelected,
                    courts: _courts,
                    court:
                      _courts && _courts.length ? _courts[courtposition] : {},
                    selectedSport: sportSelected,
                    selectedDate,
                    selectedMonth,
                    selectedYear,
                    slot: {},
                  },
                });
              } catch (e) {
                errorLog(e);
              }
            }
          }
          dispatch({type: 'SET_LOADING', payload: false});
        })
        .catch((o) => {
          dispatch({type: 'SET_LOADING', payload: false});
          errorLog('getSportList error ' + o);
        });
    };
  } catch (e) {
    errorLog(e);
  }
};

export const setCourts = (courts) => {
  return {type: 'SET_COURTS', payload: courts};
};

const calculateTime = (slot) => {
  var endTime = slot.endTime === '00:00:00' ? '23:59:59' : slot.endTime;
  var start_date = moment(`01/01/2019 ${slot.startTime}`);
  var end_date = moment(`01/01/2019 ${endTime}`);
  var duration = moment.duration(moment(end_date).diff(moment(start_date)));
  var minutes = duration.asMinutes();
  return minutes;
};

/**
 *
 * @param court to get site id to store on firebase on this id.
 * @param slotsSelected
 * @param value firebase value
 * @param slotSelectedByOther
 * @returns {Promise<void>}
 */
export const makeEntryToFirebaseForLockingSlot = async (
  court,
  slotsSelected,
  value = true,
  slotSelectedByOther,
) => {
  let siteId = court?.site?.id?.toString();
  if (ifNotValid(siteId) || !slotsSelected?.length > 0) {
    errorLog('site or site id is invalid');
    return;
  }

  let obj = {};
  if (slotSelectedByOther && JSON.stringify(slotSelectedByOther) !== '{}') {
    obj = {...slotSelectedByOther};
  }
  slotsSelected.map((x) => {
    obj = {...obj, [x.id]: value};
    return x;
  });
  firestore()
    .collection('sites')
    .doc(siteId)
    .set(obj)
    .then(() => {})
    .catch((c) => errorLog('catch' + c));
};

/**
 *
 * @returns {Promise<void>}
 * @param siteId
 * @param dispatch
 * @param slotSelectedByOther
 */
export const checkSlotsOnServer = (
  siteId,
  dispatch,
  slotSelectedByOther = {},
) => {
  let id = siteId?.toString();
  return firestore()
    .collection('sites')
    .doc(id)
    .onSnapshot((documentSnapshot) => {
      let data = documentSnapshot.data();
      if (ifNotValid(documentSnapshot.data())) {
        data = {};
      }
      if (
        data &&
        JSON.stringify(data) !== JSON.stringify(slotSelectedByOther)
      ) {
        dispatch({type: 'SET_SLOTS_SELECTED_BY_OTHER', payload: data});
      }
    });
};

/**
 *
 * @returns {Promise<void>}
 * @param slots
 * @param slotSelectedByOther
 * @param dispatch
 */
export const compareSlotsAndUpdateSomeoneElseSlot = (
  slots,
  slotSelectedByOther,
  dispatch,
) => {
  try {
    if (slots && slotSelectedByOther && dispatch) {
      let keys = Object.keys(slotSelectedByOther);
      if (keys && keys.length > 0) {
        for (let i = 0; i < keys.length; i++) {
          for (let j = 0; j < slots.length; j++) {
            if (keys[i] === slots[j]?.id) {
            }
          }
        }
      }
    }
  } catch (e) {
    errorLog('compareSlotsAndUpdateSomeone e ', e);
  }
};

export const checkValidation = (court, slotsSelected, selectedSport) =>
  new Promise((resolve, reject) => {
    let disable = false;

    if (ifNotValid(slotsSelected)) {
      disable = true;
    }
    if (ifValid(slotsSelected) && slotsSelected.length === 0) {
      disable = true;
    }

    if (
      (ifValid(court) && ifNotValid(court?.slots)) ||
      court?.slots.length === 0
    ) {
      disable = true;
    }
    if (selectedSport === '') {
      disable = true;
    }
    if (disable) {
      reject(disable);
    } else {
      resolve(disable);
    }
  });

export const setSlots = (slots) => {
  return {type: 'SET_SLOTS', payload: slots};
};

export const setSlotMultiple = (slotsSelected) => {
  return {type: 'SET_SLOT_MULTIPLE', payload: slotsSelected};
};

export const clearSlotsAndSlot = () => {
  return (dispatch) => {
    dispatch({type: 'CLEAR_SLOTS_SLOT', payload: ''});
  };
};
export const clearCourtSelectedSportSlotsSlot = () => {
  return (dispatch) => {
    dispatch({type: 'CLEAR_COURT_SELECTED_SLOTS_SLOT', payload: ''});
  };
};
export const clearSlotAndAvailableSport = () => {
  return (dispatch) => {
    dispatch({
      type: 'CLEAR_SLOT_AVAILABLE_SPORT',
      payload: {},
    });
  };
};
export const clearSlotsAndSlotSportDateTime = () => {
  return (dispatch) => {
    dispatch({type: 'CLEAR_SLOTS_SLOT_SPORT_DATE_TIME', payload: ''});
  };
};

export const setSelectedSportIcon = (icon) => {
  return async (dispatch) => {
    await dispatch({type: 'SET_SELECTED_SPORT_BOOKING', payload: icon});
  };
};

export function setCurrentCourtAndAvailbleSlot(court) {
  return (dispatch) => {
    dispatch({
      type: 'SET_SELECTED_COURT',
      payload: {
        court: court,
        selectedCourt: {
          courtId: court.resource.id,
          name: court.resource.name,
        },
        selectedSport:
          court?.sportPlayedFor && court.sportPlayedFor.length > 0
            ? court.sportPlayedFor[0].name
            : '',
      },
    });
  };
}

export function setCurrentCourt(court) {
  return (dispatch) => {
    dispatch({type: 'SET_COURT', payload: court});
  };
}

export function setCurrentCourtAndSelectedSlots(court, slotsSelected) {
  return (dispatch) => {
    dispatch({type: 'SET_COURT_AND_S_SLOTS', payload: {court, slotsSelected}});
  };
}

export const setFinalPrice = (price) => {
  return async (dispatch) => {
    await dispatch({type: 'SET_FINAL_PRICE', payload: price});
  };
};
