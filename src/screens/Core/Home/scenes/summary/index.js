import {
  BackHandler,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import {
  ButtonCommon,
  Container,
  DropDownCommon,
  HeaderBack,
  Line,
} from '../../../../../components';
import { Content } from '../../../../../utils/content/Content';
import { commonStyle } from '../../../../../styles';
import { fontToDp, widthToDp } from '../../../../../utils/Responsive';
import { RenderItemSelected } from './comp/RenderItemSelected';
import { venueStyle } from '../venues/venue_detail/comp/style';
import { getLineView, ifNotValid, ifValid } from '../../../../../utils/helper';
import { useDispatch, useSelector } from 'react-redux';
import RazorpayCheckout from 'react-native-razorpay';
import helper from '../../../../../commons/helperFunctions';
import { errorLog, successLog } from '../../../../../utils/fireLog';
import { bookSlotMembershipInventoryService, getOptions } from './comp/action';
import axiosService from '../../../../../commons/axiosService';
import {
  inventoryOnClose,
  inventoryOnSelect,
  inventoryOnTouch,
  inventoryRemoveFromCart,
  membershipRemoveFromCart,
  serviceOnClose,
  serviceOnSelect,
  serviceOnTouch,
  serviceRemoveFromCart,
  updateServicesTaken,
  updateTotalPrice,
} from '../services/action';
import { RenderServiceTaken } from './comp/RenderServiceTaken';
import MembershipCardTaken from './comp/RenderMembershipTaken';
import ListHeader from '../../../../../components/ListHeader';
import { ScreenNames } from '../../../../../utils/ScreenNames';
import { setFromPayment } from '../../../../../redux/actions/login';
import moment from 'moment';
import { colors } from '../../../../../styles/colors';
import { makeEntryToFirebaseForLockingSlot } from '../bookings/action';
import SlotsMembershipDropdown from './comp/SlotsMembershipDropdown';

const HomeSummary = (props) => {
  const dispatch = useDispatch();
  let homeVenue = useSelector((x) => x.homeVenue.selectedVenue);
  homeVenue = ifValid(homeVenue) ? homeVenue : {};
  let userData = useSelector((x) => x.login.data);
  let loading = useSelector((x) => x.loader.loading);
  let homeBookings = useSelector((x) => x.homeBookings);
  userData = ifValid(userData) ? userData : {};
  let {
    selectedCourt,
    slotsSelected,
    selectedDate,
    selectedTime,
    selectedSport,
    initialTransaction,
    finalPrice,
    slotSelectedByOther,
    court,
  } = homeBookings;
  court

  let description = '';
  const {
    services,
    servicesTaken,
    inventories,
    inventoriesTaken,
    membership,
    membershipTaken,
  } = useSelector((state) => state.homeServices);
  let userDetails = useSelector((state) => state.login.data);
  let slot = slotsSelected && slotsSelected.length > 0 ? slotsSelected[0] : {};
  let gst =
    ifValid(slot?.site?.organization) &&
      ifValid(slot?.site?.organization?.bookingTotalGst)
      ? slot?.site?.organization?.bookingTotalGst
      : undefined;
  let selectedSportInVenues = useSelector((x) => x.homeVenue.selectedSport);


  const [disountt, setDiscountt] = useState(0);
  const [disounttforbook, setDiscounttForBook] = useState(0);
  const [percentHundred, setPercentHundred] = useState(0);
  const [checkpercentorflatrate, setCheckPercentOrFlatrate] = useState('');
  const [discountrate, setDiscountRate] = useState(0);
  const [slotmembership, setSlotMembership] = useState([]);
  const [isPickerVisible, setIsPickerVisible] = useState(false);
  const [selectedmembership, setSelectedMembership] = useState(0);

  useEffect(() => {
    _fetchMembershipbysiteid();
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
    return () => {
      backButtonHandler();
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, []);

  async function backButtonHandler() {
    await makeEntryToFirebaseForLockingSlot(
      court,
      slotsSelected,
      false,
      slotSelectedByOther,
    );
  }

  //TODO: step 1 : validate price name.

  const _fetchMembershipbysiteid = () => {
    try {
      if (ifNotValid(userDetails?.id)) {
        errorLog('user is not valid..!');
        return;
      }

      axiosService
        .get(
          `api/memberships/site/${homeVenue?.id}/user/${userDetails.id}?sportId=${selectedSportInVenues?.id}`,
        )
        .then((response) => {
          console.log('###_fetchMembership by site id------------->', response);
          if (response && response.status === 200) {
            if (response?.data?.data) {
              var temp = [];
              response.data.data.map((membership) => {
                if (membership.membershipActive == 'Active') {
                  temp.push(membership);
                }
              });
              response.data.data = temp;
              if (response?.data?.data[0].plan.discountPercentage >= 0) {
                console.log('FLATRATE DISCOUNT');
                setPercentHundred(
                  response.data.data[0].plan.discountPercentage,
                );
                setCheckPercentOrFlatrate('Percentage');
                setDiscountRate(response.data.data[0].plan.discountPercentage);
                discountCalculastion(
                  response.data.data[0].plan.discountPercentage,
                );
                setSelectedMembership(response.data.data[0]);
                setSlotMembership(response.data.data);
              } else if (response.data.data[0].plan.flatRate >= 0) {
                console.log('PERCENTAGE DISCOUNT');
                let flatrate = response.data.data[0].plan.flatRate;
                setCheckPercentOrFlatrate('Rupee');
                setDiscountRate(flatrate);
                //setDiscountt(flatrate * slotsSelected.length);
                setDiscountt(flatrate);
                setDiscounttForBook(flatrate);
                setSelectedMembership(response.data.data[0]);
                setSlotMembership(response.data.data);
              }
            }
          }
        });
    } catch (e) {
      errorLog(e);
    }
  };

  const selectMembership = (data) => {
    if (data.plan.discountPercentage >= 0) {
      console.log('FLATRATE DISCOUNT');
      setPercentHundred(data.plan.discountPercentage);
      setCheckPercentOrFlatrate('Percentage');
      setDiscountRate(data.plan.discountPercentage);
      discountCalculastion(data.plan.discountPercentage);
      setSelectedMembership(data);
    } else if (data.plan.flatRate >= 0) {
      console.log('PERCENTAGE DISCOUNT');
      let flatrate = data.plan.flatRate;
      setCheckPercentOrFlatrate('Rupee');
      setDiscountRate(flatrate);
      setDiscountt(flatrate);
      setDiscounttForBook(flatrate);
      setSelectedMembership(data);
    }
  };

  const discountCalculastion = (data) => {
    let finalTotal = 0;
    slotsSelected &&
      slotsSelected.map((item) => {
        finalTotal += item.offerRate;
      });
    //let finalTotal = slot.offerRate * slotsSelected.length;
    //let datageta = (slot.offerRate * data) / 100;
    let datageta = (finalTotal * data) / 100;
    let percentfinal = (finalTotal * data) / 100;
    setDiscountt(percentfinal);
    setDiscounttForBook(datageta);
  };

  const _onPaymentButtonPress = () => {
    console.log('USERDATA', userData);
    successLog('validating');
    description = `Name: ${userData.firstName
      }  selected date is ${selectedDate}  userId ${userData.id
      } selected time is ${selectedTime} selected sport is ${selectedSport} slot selected ${JSON.stringify(
        slot,
      )}  `;


    if (disountt > 0) {
      dispatch({ type: 'SET_MEMBERSHIP_DISCOUNT_AMOUNT', payload: disountt });
    }
    let finalTotal = helper.convertRstoPs(finalPrice);
    let discount = helper.convertRstoPs(disountt);
    let priceTotal = finalTotal - discount;
    successLog(priceTotal);

    if (percentHundred == 100 && priceTotal === 0) {
      console.log('INSIDE PERCENTAGE');
      if (!ifValid(userData) || !ifValid(userData.firstName)) {
        dispatch(setFromPayment('venue_book'));
        //props.navigation.navigate(ScreenNames.INITIAL)
        props.navigation.navigate(ScreenNames.LOGIN);
        return;
      }
      _withoutPaymentslotBook();
    } else {
      if (ifNotValid(priceTotal) || priceTotal === 0) {
        console.warn('invalid price' + priceTotal);
        return;
      }

      if (!ifValid(userData) || !ifValid(userData.firstName)) {
        dispatch(setFromPayment('venue_book'));
        //props.navigation.navigate(ScreenNames.INITIAL)
        props.navigation.navigate(ScreenNames.LOGIN);
        return;
      }
      console.log('INSIDE FLATRATE');
      prePaymentApiCall(priceTotal);
    }
  };

  const _withoutPaymentslotBook = () => {
    dispatch({ type: 'SET_LOADING', payload: true });
    let payload = slotsSelected.map((item) => {
      return {
        discountFormat: checkpercentorflatrate,
        discountRate: disounttforbook,
        endDate: moment(selectedDate).format('YYYY-MM-DD'),
        endTime: item.endTime,
        extraAmount: 0,
        note: 'sportly',
        offerRate: item.offerRate,
        payment_status: 'PAID',
        resource: {
          id: item.resource.id,
        },
        site: { ...item.site },
        slotId: item.id,
        sportPlayedFor: selectedSport,
        startDate: moment(selectedDate).format('YYYY-MM-DD'),
        startTime: item.startTime,
        status: 'UNAVAILABLE',
        userId: userDetails.id,
      };
    });
    console.log('PAYLOAD============>', payload);

    axiosService
      .post(`api/offers`, payload)
      .then((response) => {
        console.log('WITHOUT PAYMENT SLOT BOOK------------->', response);
        if (response.status === 200) {
          dispatch({ type: 'SET_LOADING', payload: false });
          props.navigation.navigate(ScreenNames.HOME_ACKNOWLEDGEMENT);
        } else {
          dispatch({ type: 'SET_LOADING', payload: false });
          alert('Something went wrong..');
        }
      })
      .catch((o) => {
        console.log('makeInitialTransaction error ' + o);
      });
  };

  //TODO: step: 2 prepayment api
  const prePaymentApiCall = (priceTotal) => {
    successLog('prePaymentApiCall');
    dispatch({ type: 'SET_LOADING', payload: true });
    let payload = {
      amount: priceTotal,
      userId: userData.id,
      type: 'slot_booking',
    };

    axiosService
      .post(`api/payments?order=true`, payload)
      .then((response) => {
        successLog('prePaymentApiCall', response.status);
        if (response.status === 200) {
          response = response.data;
          if (ifNotValid(response)) {
            successLog('invalid response');
            return;
          }
          if (ifValid(response.data[0])) {
            makePayment(response.data[0], priceTotal);
          } else {
            alert('Something went wrong while pre payment check');
          }
        } else {
          dispatch({ type: 'SET_LOADING', payload: false });
          alert('Something went wrong..');
        }
      })
      .catch((o) => {
        console.log('makeInitialTransaction error ' + JSON.stringify(o));
      });
  };

  //TODO: step: 3 make payment
  const makePayment = (prePaymentRes, priceTotal) => {
    successLog('makePayment');
    let options = getOptions(
      userData,
      priceTotal,
      prePaymentRes.orderId,
      `Booking slot`,
    );
    var exAmount = extraAmountCalculation()
    RazorpayCheckout.open(options)
      .then((res) => {
        successLog('onPaymentSuccess');
        //TODO: step: 4  book slot, membership, inventory and service.
        console.log('Service Taken makePayment', servicesTaken);
        dispatch(
          bookSlotMembershipInventoryService(
            selectedDate,
            slotsSelected,
            userData,
            prePaymentRes,
            res,
            inventoriesTaken,
            servicesTaken,
            membershipTaken,
            props,
            selectedSport,
            checkpercentorflatrate,
            disounttforbook,
            exAmount
          ),
        );
      })
      .catch((error) => {
        dispatch({ type: 'SET_LOADING', payload: false });
        console.log('ERRORR------------>', error.error.description);
        alert(error.error.description);
      });
  };

  const _removeFromCart = async (item) => {
    let _servicesTaken = serviceRemoveFromCart(
      item,
      services,
      servicesTaken,
      dispatch,
    );
    await dispatch(
      updateTotalPrice(
        _servicesTaken,
        membershipTaken,
        inventoriesTaken,
        slotsSelected,
      ),
    );
  };

  const _removeFromCartMembership = async (item) => {
    let _membershipTaken = membershipRemoveFromCart(
      item,
      membership,
      membershipTaken,
      dispatch,
    );
    await dispatch(
      updateTotalPrice(
        servicesTaken,
        _membershipTaken,
        inventoriesTaken,
        slotsSelected,
      ),
    );
  };

  const _removeFromCartInventory = async (item) => {
    let _inventoriesTaken = inventoryRemoveFromCart(
      item,
      inventories,
      inventoriesTaken,
      dispatch,
    );
    await dispatch(
      updateTotalPrice(
        servicesTaken,
        membershipTaken,
        _inventoriesTaken,
        slotsSelected,
      ),
    );
  };

  // services
  const _onPickerTouchService = (item) =>
    serviceOnTouch(item, services, servicesTaken, dispatch);
  const _onPickerSelectService = (value, item) =>
    serviceOnSelect(value, item, services, servicesTaken, dispatch);
  const _onPickerCloseService = () =>
    serviceOnClose(services, servicesTaken, dispatch);
  // Inventory..!
  const _onPickerTouchInventory = (item) =>
    inventoryOnTouch(item, inventories, inventoriesTaken, dispatch);
  const _onPickerSelectInventory = (value, item) =>
    inventoryOnSelect(value, item, inventories, inventoriesTaken, dispatch);
  const _onPickerCloseInventory = () =>
    inventoryOnClose(inventories, inventoriesTaken, dispatch);

  const _clearCart = () => {
    let _services = services.map((x) => {
      return { ...x, addedToCart: false, quantitySelected: '' };
    });
    dispatch(updateServicesTaken([], _services));
  };

  const extraAmountCalculation = () => {
    let sumServices = servicesTaken
      .map((x) => {
        let quantitySelected =
          x.quantitySelected !== '' ? parseInt(x.quantitySelected) : 1;
        return x.rate * quantitySelected;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
    successLog('sumServices', sumServices);


    let sumMembership = membershipTaken
      .map((x) => {
        return x.priceToPay;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
    successLog('sumMembership', sumMembership);

    let sumInventories = inventoriesTaken
      .map((x) => {
        let quantitySelected =
          x.quantitySelected !== '' ? parseInt(x.quantitySelected) : 1;
        return x.mrpAmount * quantitySelected;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);

    successLog('sumInventories', sumInventories);
    return sumServices + sumMembership + sumInventories
  };

  return (
    <Container>
      <HeaderBack title={Content.summary} navigation={props.navigation} />
      <ScrollView style={{ flex: 1, backgroundColor: colors.background }}>
        <View
          style={[
            commonStyle.detailsContainer,
            {
              marginVertical: widthToDp(1.5),
              paddingVertical: widthToDp(1.2),
            },
          ]}
        >
          <Text style={commonStyle.regular26}>
            {slotsSelected.filter((x) => x.isCalulable ==true).length}{' '}
            {slotsSelected.length === 1
              ? Content.slotSelected
              : Content.slotsSelected}
          </Text>
        </View>

        {slotsSelected &&
          slotsSelected.map((x) => {
            return (
              x.isCalulable && <RenderItemSelected
                userDetails={userData}
                item={x}
                selectedSport={selectedSport}
                court={court}
              />
            );
          })}

        <View style={{ backgroundColor: colors.background }}>
          {membershipTaken && membershipTaken.length > 0 && (
            <View>
              {getLineView()}
              <ListHeader title={'Memberships'} />
              {membershipTaken.map((item, i) => {
                return (
                  <MembershipCardTaken
                    removeFromCartMembership={(item) =>
                      _removeFromCartMembership(item)
                    }
                    name={item.name}
                    duration={item.duration}
                    item={item}
                  />
                );
              })}
            </View>
          )}

          {servicesTaken && servicesTaken.length > 0 && (
            <View>
              {getLineView()}
              <ListHeader title={'Additional services'} />
              {servicesTaken.map((item, i) => {
                return (
                  <RenderServiceTaken
                    isVisible={item.isVisible}
                    onPickerTouch={() => _onPickerTouchService(item)}
                    onPickerSelect={(value) =>
                      _onPickerSelectService(value, item)
                    }
                    onPickerClose={() => _onPickerCloseService(item)}
                    name={item.name}
                    rate={item.rate}
                    removeFromCart={(item) => _removeFromCart(item)}
                    item={item}
                    type={'st'}
                  />
                );
              })}
            </View>
          )}

          {inventoriesTaken && inventoriesTaken.length > 0 && (
            <View>
              {getLineView()}
              <ListHeader title={'Other items'} />
              {inventoriesTaken.map((item, i) => {
                return (
                  <RenderServiceTaken
                    isVisible={item.isVisible}
                    onPickerTouch={() => _onPickerTouchInventory(item)}
                    onPickerSelect={(value) =>
                      _onPickerSelectInventory(value, item)
                    }
                    onPickerClose={() => _onPickerCloseInventory(item)}
                    name={item.productName}
                    rate={item.mrpAmount}
                    removeFromCart={(item) => _removeFromCartInventory(item)}
                    item={item}
                    type={'it'}
                  />
                );
              })}
            </View>
          )}
        </View>

        {getLineView()}
        {slotmembership.length > 0 && (
          <SlotsMembershipDropdown
            isVisible={isPickerVisible}
            onPickerTouch={() => {
              if (slotmembership?.length < 1) {
                return;
              }
              setIsPickerVisible(!isPickerVisible);
            }}
            onPickerSelect={(slotmembership) => {
              setIsPickerVisible(false);
              selectMembership(slotmembership);
              //setSelectedMembership(slotmembership)
            }}
            onPickerClose={() => setIsPickerVisible(false)}
            title={'Select Discount Membership'}
            data={slotmembership}
            //court={selectedplan}
            value={selectedmembership?.plan?.name}
          // onValueChange={(court, index) => {
          //   dispatch(setCurrentCourtAndAvailbleSlot(court));
          // }}
          />
        )}

        {getLineView()}

        <View style={venueStyle.detailsContainer}>
          <Text style={commonStyle.narrow32}>{Content.paymentSummary}</Text>
          <View style={styles.rowContainer}>
            <Text style={commonStyle.regular22}>{Content.subTotal}</Text>
            <Text style={commonStyle.regular22}>
              {Content.rupies} {finalPrice?.toString()}
            </Text>
          </View>
          <View style={styles.rowContainer}>
            <Text style={commonStyle.regular22}>{Content.gst}</Text>
            <Text style={commonStyle.regular22}>{`Inclu. of ${gst}% GST`}</Text>
          </View>
          <View style={styles.rowContainer}>
            <Text style={commonStyle.regular22}>{Content.convenienceFee}</Text>
            <Text style={commonStyle.regular22}>
              {Content.rupies} {0}
            </Text>
          </View>

          {disountt != 0 && (
            <View style={styles.rowContainer}>
              <Text style={commonStyle.regular22}>{Content.discount}</Text>
              <Text style={commonStyle.regular22}>
                {Content.rupies} {disountt}
              </Text>
            </View>
          )}

          <Line />

          <View style={[styles.rowContainer, { paddingVertical: widthToDp(1) }]}>
            <Text style={[commonStyle.narrow28, styles.font14]}>
              {Content.totalAmount}
            </Text>
            <Text style={[commonStyle.narrow28, styles.font15]}>
              {Content.rupies} {finalPrice?.toString() - disountt}
            </Text>
          </View>
        </View>

        <View style={[venueStyle.detailsContainer, { marginTop: widthToDp(1) }]}>
          <Text style={commonStyle.narrow32}>{Content.bookingPolicy}</Text>
          <View style={venueStyle.detailsContainerInner}>
            <TouchableOpacity
              onPress={() =>
                props.navigation.navigate(ScreenNames.BOOKING_POLICY)
              }
            >
              <Text style={{ color: '#0000EE' }}>
                Click here to read booking policy
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Line />
      </ScrollView>

      <ButtonCommon
        loading={loading}
        onPress={() => {
          _onPaymentButtonPress();
        }}
        text={
          percentHundred == 100 && finalPrice?.toString() - disountt == 0
            ? Content.proceedToBook
            : Content.proceedToPay
        }
      />
    </Container>
  );
};
export default HomeSummary;

const styles = StyleSheet.create({
  paymentTextStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: widthToDp(1),
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: widthToDp(1),
  },
  font14: { fontSize: fontToDp(14) },
  font15: { fontSize: fontToDp(15) },
});

//
// let display_data = {
//     'site_name': slot.site.name,
//     'site_id': slot.site.id,
//     'org_id': userData.role.organization.id,
//     'resource_name': slot.resource.category,
//     'price': offerRate,
//     'duration': `${moment(selectedDate).format('ddd, DD MMM YYYY')}`,
//     'action': 'slot_booking',
//     'selectedDate': `${moment(selectedDate).format('DD MMM YYYY')}`,
//     'userName': userData.firstName,
//     'userPhoneNo': userData.phoneNo,
//     'emailId': userData.email,
//     'userId': userData.id,
// };
// if (ifNotValid(userData.id)) {
//     console.warn('user id not valid ');
//     return;
// }
// if (ifNotValid(slot.site) || ifNotValid(slot.site.id)) {
//     console.warn('slot.site.id is not valid ');
//     return;
// }
// if (ifNotValid(slot.site.organization) || ifNotValid(slot.site.organization.id)) {
//     console.warn('slot.site.organization.id is not valid ');
//     return;
// }
// dispatch(checkPreviousTransactions(userData.id, slot.site.id, slot.site.organization.id));
