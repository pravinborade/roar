import React from 'react';
import {
  FlatList,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {commonStyle} from '../../../../../../styles';
import {height, widthToDp} from '../../../../../../utils/Responsive';
import Line from '../../../../../../components/Line';
import Images from '../../../../../../assets/Images/Images';

const SlotsMembershipDropdown = ({
  title = '',
  isVisible = false,
  data,
  onPickerClose,
  onDonePress,
  value = '',
  onPickerTouch,
  onPickerSelect,
  containerStyle,
  disabled = false,
}) => {
  const pickerView = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {}}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => onPickerClose()}
          style={styles.centeredView}
        >
          <View style={styles.modalView}>
            <View>
              <FlatList
                scrollEnabled
                style={{backgroundColor: 'white', maxHeight: height / 3}}
                showsVerticalScrollIndicator={true}
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={(props) => <Line />}
                data={data}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => onPickerSelect(item)}
                    style={styles.flatListItem}
                  >
                    <Text
                      style={[
                        commonStyle.regular26,
                        {
                          flex: 8,
                        },
                      ]}
                    >
                      {item?.plan?.name}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  };

  return (
    <View
      style={[
        commonStyle.detailsContainer,
        {
          width: '100%',
          paddingTop: widthToDp(2),
          paddingBottom: widthToDp(0),
        },
      ]}
    >
      <Text style={commonStyle.narrow32}>{title}</Text>
      <TouchableOpacity
        activeOpacity={1}
        onPress={onPickerTouch}
        style={[
          {
            flexDirection: 'row',
            paddingHorizontal: widthToDp(2),
          },
          styles.dropDownContainer,
        ]}
      >
        <Text style={[commonStyle.regular24, {flex: 1}]}>{value}</Text>
        <Image
          source={Images.arrowBottom}
          style={{width: widthToDp(2), height: widthToDp(2)}}
        />
      </TouchableOpacity>
      {pickerView()}
    </View>
  );
};
export default SlotsMembershipDropdown;

const styles = StyleSheet.create({
  dropDownContainer: {
    alignItems: 'center',
    height: widthToDp(9),
    borderRadius: 2,
    justifyContent: 'center',
    margin: widthToDp(0.5),
    borderWidth: 0.5,
    borderColor: 'black',
  },
  dropDown: {
    width: '100%',
  },

  //    model
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 2,
    paddingLeft: widthToDp(4),
    paddingVertical: widthToDp(2),
    width: widthToDp(80),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  flatListItem: {
    paddingVertical: widthToDp(2),
  },
});
