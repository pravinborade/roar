import {Image, StyleSheet, Text, View} from 'react-native';
import Images from '../../../../../../assets/Images/Images';
import React from 'react';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../../../styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import DropDownSmall from '../../services/comp/DropdownSmall';
import {Content} from '../../../../../../utils/content/Content';

export const RenderItemServices = ({navigation, key}) => {
  return (
    <View style={styles.renderContainer} key={key}>
      <Image source={Images.add1} style={styles.imageBox} />
      <View style={styles.cardContent}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={commonStyle.regular28}>{Content.rebootSport} </Text>
          <Text style={commonStyle.narrow30}>Rs 300</Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            width: '85%',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={commonStyle.rowAlignCenter}>
            <DropDownSmall title={Content.size} />
            <DropDownSmall
              containerStyle={{marginLeft: widthToDp(1.2)}}
              title={Content.qty}
            />
          </View>
        </View>

        <TouchableOpacity
          style={{
            paddingVertical: widthToDp(0.5),
            paddingHorizontal: widthToDp(3),
            borderRadius: widthToDp(1),
            borderWidth: 0.6,
            borderColor: 'black',
            alignSelf: 'flex-start',
          }}>
          <Text style={commonStyle.narrow22}>{Content.remv}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  renderContainer: {
    marginVertical: widthToDp(0.5),
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  imageBox: {
    margin: widthToDp(4),
    width: widthToDp(20),
    resizeMode: 'stretch',
    height: heightToDp(12),
  },
  cardContent: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'space-around',
    paddingVertical: widthToDp('3'),
  },
  smallImage: {
    width: widthToDp(4),
    height: widthToDp(4),
    resizeMode: 'contain',
  },
});
