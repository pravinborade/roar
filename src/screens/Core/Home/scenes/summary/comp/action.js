import axiosService from '../../../../../../commons/axiosService';
import { ifNotEmpty, ifNotValid, ifValid } from '../../../../../../utils/helper';
import { ScreenNames } from '../../../../../../utils/ScreenNames';
import moment from 'moment';
import { REZER_PAY_KEY } from '../../../../../../config';
import { errorLog, successLog } from '../../../../../../utils/fireLog';
import { colors } from '../../../../../../styles';
import { _dataPlain } from '../../sports/data';
import Images from '../../../../../../assets/Images/Images';
import { membershipRenewurlMultiple } from '../../../../../../root/navigation/deeplinking/action';
import { store } from '../../../../../../redux/store';
export const checkPreviousTransactions = (userId, siteId, orgId) => {
  if (ifNotValid(userId) || ifNotValid(siteId) || ifNotValid(orgId)) {
    console.warn('id not valid ' + userId, siteId, orgId);
    return;
  }
  return (dispatch) => {
    axiosService
      .get(`api/payments/user/${userId}/${siteId}/${orgId}`)
      .then((response) => {
        if (response.status === 200) {
          let res = ifValid(response.data[0]) ? response.data[0] : [];
          dispatch({ type: 'INITIAL_TRANSACTION', payload: res });
        } else {
          console.log('not valid res ' + JSON.stringify(response));
        }
      })
      .catch((o) => {
        console.log('makeInitialTransaction error ' + JSON.stringify(o));
      });
  };
};

export const getOptions = (
  userData = {},
  price: number,
  orderID,
  description = '',
) => {
  successLog(userData);
  let firstName = ifNotEmpty(userData.firstName) ? userData.firstName : '';
  let lastName = ifNotEmpty(userData.lastName) ? userData.lastName : '';
  let email = ifNotEmpty(userData.email) ? userData.email : '';
  let phoneNo = ifNotEmpty(userData.phoneNo) ? userData.phoneNo : '';
  if (lastName !== '') {
    firstName = firstName + ' ' + lastName;
  }
  return {
    description: `Roar app ,  ${description}`,
    image:
      'https://firebasestorage.googleapis.com/v0/b/roar-1.appspot.com/o/logo.png?alt=media&token=eeb72533-9678-47b4-8bf2-145ac635e1b4',
    currency: 'INR',
    key: REZER_PAY_KEY, // Your api key
    amount: price,
    name: firstName,
    order_id: orderID,
    prefill: {
      email: email,
      contact: phoneNo,
      name: 'Razorpay Software',
    },
    theme: { color: colors.primaryGreen },
    notes: ['ROAR SPORTS']
  };
};

/**
 *
 * @param selectedDate
 * @param slotsSelected: multiple slots.
 * @param offerRate: actual price to be paid
 * @param userData
 * @param prePaymentRes
 * @param paymentResponse
 * @param inventoriesTaken
 * @param servicesTaken
 * @param membershipTaken
 * @param props
 * @param sportPlayedFor
 * @returns {function(...[*]=)}
 */

export const bookSlotMembershipInventoryService = (
  selectedDate,
  slotsSelected,
  userData,
  prePaymentRes,
  paymentResponse,
  inventoriesTaken,
  servicesTaken,
  membershipTaken,
  props,
  sportPlayedFor,
  checkpercentorflatrate,
  disounttforbook,
  exAmount
) => {
  return (dispatch) => {
    
  
    let slot = {};
    if (ifValid(slotsSelected) && slotsSelected.length > 0) {
      slot = slotsSelected[0];
    } else {
      errorLog('invalid slotsSelected while booking.');
      return;
    }

    let requestBodySlot = getSlotPayload(
      selectedDate,
      slotsSelected,
      userData,
      prePaymentRes,
      paymentResponse,
      sportPlayedFor,
      checkpercentorflatrate,
      disounttforbook,
      exAmount
    );

    let requestBodyMembership = getMembershipPayload(
      membershipTaken,
      prePaymentRes,
      userData,
      paymentResponse,
    );
    let requestBodyServices = getServicePayload(
      slot,
      userData,
      prePaymentRes,
      paymentResponse,
      servicesTaken,
    );
    let requestBodyInventory = getBookInventoryPayload(
      inventoriesTaken,
      slot,
      userData,
      prePaymentRes,
      paymentResponse,
    );
    console.log('&*', JSON.stringify(requestBodySlot));
    axiosService.post('api/offers', requestBodySlot).then((response) => {
      successLog('api/offers slot api', JSON.stringify(response));    
      console.log('payment successful slot..!', response);
      if (response.status === 200) {
        var ids = [];
        for (let i = 0; i < response.data.data.length; i++) {
          ids.push({ id: response.data.data[i].id });
        }
        store.dispatch({ type: 'SET_SLOTS_IDS', payload: ids });
        console.log('payment successful slot..!');
        ///TODO book membership
        let resapioffer = response.data.data[0];
        bookMembershipApi(
          requestBodyMembership,
          requestBodyServices,
          requestBodyInventory,
          props,
          dispatch,
          userData,
          resapioffer,
        );
      } else {
        alert('something went wrong while booking appointment');
      }
    });
  };
};

const bookMembershipApi = (
  requestBodyMembership,
  requestBodyServices,
  requestBodyInventory,
  props,
  dispatch,
  userData,
  resapioffer,
) => {
  successLog('bookMembershipApi-------', JSON.stringify(requestBodyMembership));
  if (ifValid(requestBodyMembership)) {
    axiosService
      .post('api/memberships/multiple', requestBodyMembership)
      .then(async (response) => {
        console.log('2) memberships api', response);
        if (response.status === 200) {
          await membershipRenewurlMultiple(response.data.data);
          var ids = [];
          for (let i = 0; i < response.data.data.length; i++) {
            ids.push({ id: response.data.data[i].id });
          }
          store.dispatch({ type: 'SET_MEMERSHIP_IDS', payload: ids });
          console.log('membership successful..! calling inventory api');
          bookInventoryApi(
            requestBodyServices,
            requestBodyInventory,
            props,
            dispatch,
            userData,
            resapioffer,
          );
        } else {
          alert('something went wrong while book membership appointment');
        }
      });
  } else {
    errorLog('bookMembershipApi no payload, so calling inventory api');
    bookInventoryApi(
      requestBodyServices,
      requestBodyInventory,
      props,
      dispatch,
      userData,
      resapioffer,
    );
  }
};

const bookInventoryApi = (
  requestBodyServices,
  requestBodyInventory,
  props,
  dispatch,
  userData,
  resapioffer,
) => {
  if (ifValid(requestBodyInventory)) {
    let BodyInventory = { ...requestBodyInventory, offerNo: resapioffer.offerNo };
    axiosService
      .post('api/inventories/inventory?paymentStaus=PAID', BodyInventory)
      .then((response) => {
        successLog('3) inventories api', response.status);
        if (response.status === 200) {
          console.log(
            'bookInventory successful..! calling service api..!',
            response,
          );
          var ids = [];
          for (
            let i = 0;
            i < response.data.data[0].purchasedInventoryDetail.length;
            i++
          ) {
            ids.push({
              id: response.data.data[0].purchasedInventoryDetail[i].id,
              inventory_qty: response.data.data[0].purchasedInventoryDetail[i].quantity
            });
          }
          store.dispatch({ type: 'SET_INVENTORIES_IDS', payload: ids });
          bookServiceApi(
            requestBodyServices,
            props,
            dispatch,
            userData,
            resapioffer,
          );
        } else {
          alert('something went wrong while book inventory appointment');
        }
      });
  } else {
    console.log('bookInventory no payload calling service api..!');
    bookServiceApi(requestBodyServices, props, dispatch, userData, resapioffer);
  }
};

const bookServiceApi = (
  requestBodyServices,
  props,
  dispatch,
  userData,
  resapioffer,
) => {
  if (ifValid(requestBodyServices)) {
    for (let i = 0; i < requestBodyServices.serviceRequest.length; i++) {
      requestBodyServices.serviceRequest[i].amount =
        requestBodyServices.serviceRequest[i].amount *
        Number(requestBodyServices.serviceRequest[i].quantity);
    }
    axiosService
      .post(
        `api/services/save/serviceRequestList?userId=${userData.id
        }&comment=${'no comment'}`,
        requestBodyServices,
      )
      .then((response) => {
        successLog('4) bookServiceApi', response.status);
        if (response.status === 200) {
          console.log('bookService api successful');
          var ids = [];
          var d = response.data.data[0];
          for (let i = 0; i < d.length; i++) {
            ids.push({ id: d[i].id });
          }
          store.dispatch({ type: 'SET_SERVICES_IDS', payload: ids });
          sendInvoiceIds();
          props.navigation.navigate(ScreenNames.HOME_ACKNOWLEDGEMENT, {
            response: resapioffer,
          });
        } else {
          alert('something went wrong while book service appointment');
        }
        dispatch({ type: 'SET_LOADING', payload: false });
      });
  } else {
    sendInvoiceIds();
    dispatch({ type: 'SET_LOADING', payload: false });
    console.log('bookService api no payload moving to ACKNOWLEDGEMENT ');
    props.navigation.navigate(ScreenNames.HOME_ACKNOWLEDGEMENT, {
      response: resapioffer,
    });
  }
};

export const getBookInventoryPayload = (
  inventoriesTaken,
  slot,
  userData,
  prePaymentRes,
  paymentResponse,
) => {
  try {
    if (ifNotValid(inventoriesTaken)) {
      return undefined;
    }
    if (inventoriesTaken.length === 0) {
      return undefined;
    }
    let _inventoriesTaken = inventoriesTaken.map((x) => {
      return {
        id: x.id,
        inventory: { id: x.id },
        quantity:
          ifValid(x.quantitySelected) && x.quantitySelected !== ''
            ? x.quantitySelected
            : 1,
        amount: x.rate,
      };
    });
    //make total.
    let sum = inventoriesTaken
      .map((x) => {
        let quantitySelected =
          x.quantitySelected !== '' ? x.quantitySelected : 1;
        return x.mrpAmount * quantitySelected;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);

    return {
      id: '',
      orgId: slot.site.organization.id,
      site: { id: slot.site.id },
      user: { id: userData.id },
      purchasedInventoryDetail: _inventoriesTaken,
      paymentDetail: [
        {
          ...prePaymentRes,
          amount: prePaymentRes.amount,
          paymentSource: 'MOBILE',
          extraAmount: 0,
          siteId: slot.site.id,
          orgId: slot.site.organization.id,
          paymentId: paymentResponse.razorpay_payment_id,
        },
      ],
    };
  } catch (e) {
    errorLog('something went wrong while in getBookInventoryPayload', e);
  }
};

export const getSlotPayload = (
  selectedDate,
  slotsSelected,
  userData,
  prePaymentRes,
  paymentResponse,
  sportPlayedFor,
  checkpercentorflatrate,
  disounttforbook,
  exAmount
) => {
  console.log('DISCOUNT FORMAT---->', checkpercentorflatrate, disounttforbook);
  try {
    return slotsSelected.map((slot) => {
      return slot.isCalulable ? {
        lastUpdatedDate: 1614675901442,
        startDate: moment(selectedDate).format('YYYY-MM-DD'),
        endDate: moment(selectedDate).format('YYYY-MM-DD'),
        day: slot.day,
        startTime: slot.startTime,
        endTime: slot.endTime,
        //slotId: slot.id,
        status: 'UNAVAILABLE',
        multiDay: false,
        site: { ...slot.site },
        resource: { ...slot.resource },
        discountRate: disounttforbook,
        discountFormat: 'Rupee',
        thirdParty: false,
        isBooked: 'NO',
        sportPlayedFor: ifValid(sportPlayedFor) ? sportPlayedFor : '',
        offerRate: slot.offerRate,
        userId: userData.id,
        orgId: slot.site.organization.id,
        razorPay: true,
        payment_status: 'PAID',
        paymentDetail: [{
          ...prePaymentRes,
          paymentSource: 'MOBILE',
          extraAmount: exAmount,
          siteId: slot.site.id,
          orgId: slot.site.organization.id,
          paymentId: paymentResponse.razorpay_payment_id,
        },
        ],
      } : {
        lastUpdatedDate: 1614675901442,
        startDate: moment(selectedDate).format('YYYY-MM-DD'),
        endDate: moment(selectedDate).format('YYYY-MM-DD'),
        day: slot.day,
        startTime: slot.startTime,
        endTime: slot.endTime,
        sideFlag: true,
        //slotId: slot.id,
        status: 'INACTIVE',
        multiDay: false,
        site: { ...slot.site },
        resource: { ...slot.resource },
        discountRate: disounttforbook,
        discountFormat: 'Rupee',
        thirdParty: false,
        isBooked: 'NO',
        sportPlayedFor: ifValid(sportPlayedFor) ? sportPlayedFor : '',
        offerRate: slot.offerRate,
        userId: userData.id,
        orgId: slot.site.organization.id,
        razorPay: false,
        payment_status: 'UNPAID',
      }
    });
  } catch (e) {
    errorLog('something went wrong while in getSlotPayload', e);
  }
};

export const getServicePayload = (
  slot,
  userData,
  prePaymentRes,
  paymentResponse,
  servicesTaken,
) => {
  try {
    if (ifNotValid(servicesTaken)) {
      return undefined;
    }
    if (servicesTaken.length === 0) {
      return undefined;
    }
    return {
      paymentDetails: {
        ...prePaymentRes,
        ...paymentResponse,
        extraAmount: 0,
        paymentSource: 'MOBILE',
        siteId: slot.site.id,
        orgId: slot.site.organization.id,
        paymentId: paymentResponse.razorpay_payment_id,
      },
      serviceRequest: servicesTaken.map((x) => {
        return {
          quantity: x.quantitySelected !== '' ? x.quantitySelected : 1,
          services: {
            id: x.id,
          },
          user: {
            id: userData.id,
          },
          site: {
            id: slot.site.id,
          },
          amount: x.rate,
          paymentMode: 'UPI',
          paymentStatus: 'PAID',
        };
      }),
    };
  } catch (e) {
    errorLog('something went wrong while in getSlotPayload', e);
  }
};

export const getMembershipPayload = (
  membershipTaken,
  prePaymentRes,
  userData,
  paymentResponse,
) => {
  try {
    successLog(membershipTaken);
    if (ifNotValid(membershipTaken)) {
      return undefined;
    }
    if (membershipTaken.length === 0) {
      return undefined;
    }
    return membershipTaken.map((x) => {
      return {
        orderId: prePaymentRes.orderId,
        user: { id: userData.id },
        plan: { id: x.id },
        startDate: moment(new Date()),
        endDate: moment(new Date()).add(x.periodUnit, x.period),
        payment_status: 'PAID',
        dofbooking: `(${moment(new Date()).format('DD MMM YYYY')} - ${moment(
          new Date(),
        )
          .add(x.periodUnit, x.period)
          .format('DD MMM YYYY')})`,
        siteId: x.site.id,
        razorPay: true,
        paymentMode: 'UPI',
        paymentDetail: [
          {
            ...prePaymentRes,
            extraAmount: 0,
            paymentSource: 'MOBILE',
            siteId: x.site.id,
            orgId: x.site.organization.id,
            userId: userData.id,
            planId: x.id,
            razorPay: true,
            type: 'membership_booking',
            amount: parseInt(x.amount),
            orderId: paymentResponse.razorpay_order_id,
            paymentId: paymentResponse.razorpay_payment_id,
          },
        ],
        paidAmount: x.amount,
      };
    });
  } catch (e) {
    errorLog('something went wrong while in getSlotPayload', e);
  }
};

export const getSportImage = (sportName) => {
  try {
    let result = _dataPlain.find((x) => {
      return sportName?.toLowerCase() === x?.name?.toLowerCase();
    });
    return ifValid(result) ? result.image : Images.defaultSport;
  } catch (e) {
    errorLog('in getSportImage something wrong');
    return Images.defaultSport;
  }
};

const sendInvoiceIds = () => {
  var dt = store.getState().homeBookings.summaryBooking;
  if (dt.memberships || dt.inventories || dt.serviceRequests) {
    axiosService
      .post('api/slots/invoice', dt)
      .then((response) => {
        store.dispatch({ type: 'RESET_SUMMARY' });
        console.log(response);
      })
      .catch((err) => {
        store.dispatch({ type: 'RESET_SUMMARY' });
      });
  }
};
