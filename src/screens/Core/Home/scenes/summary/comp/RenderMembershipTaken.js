import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../../../../../../styles';
import {heightToDp, width, widthToDp} from '../../../../../../utils/Responsive';
import {Line} from '../../../../../../components';
import {generateKey} from '../../../../../../utils/helper';

const MembershipCardTaken = ({
  item,
  name = '-',
  duration = '',
  addToCartMembership,
  removeFromCartMembership,
}) => {
  return (
    <View key={generateKey(name, 'tak')}>
      <View style={[commonStyle.detailsContainer, styles.container]}>
        <View style={{width: '80%'}}>
          <View style={styles.rowCenter}>
            <Text style={[styles.mainHeader, {width: '70%'}]}>{name} </Text>
            <Text style={[commonStyle.regular24, {width: '30%'}]}>
              ( {duration} )
            </Text>
          </View>
          <TouchableOpacity
            style={styles.buttonRemove}
            onPress={() => removeFromCartMembership(item)}
          >
            <Text style={commonStyle.narrow28}>{'Remove'}</Text>
          </TouchableOpacity>
        </View>
        <Text
          style={[commonStyle.narrow30, styles.marginRight, {width: '20%'}]}
        >
          Rs {item.priceToPay}
        </Text>
      </View>
      <Line />
    </View>
  );
};
export default MembershipCardTaken;

const styles = StyleSheet.create({
  mainHeader: {
    ...commonStyle.regular28Little,
    fontWeight: 'bold',
  },
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  marginRight: {},
  container: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  imageStyle: {
    width: width,
    height: heightToDp(25),
  },

  buttonRemove: {
    borderRadius: 3,
    borderColor: 'black',
    borderWidth: 1,
    alignSelf: 'flex-start',
    marginRight: widthToDp(1),
    marginVertical: widthToDp(2),
    backgroundColor: colors.white,
    paddingVertical: widthToDp(0.5),
    paddingHorizontal: widthToDp(2.9),
  },
});
