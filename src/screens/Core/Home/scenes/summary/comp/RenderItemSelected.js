import {Image, StyleSheet, Text, View} from 'react-native';
import Images from '../../../../../../assets/Images/Images';
import React from 'react';
import {
  fontToDp,
  heightToDp,
  widthToDp,
} from '../../../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../../../styles';
import {ifValid} from '../../../../../../utils/helper';
import {useSelector} from 'react-redux';
import moment from 'moment';
import validate from 'validate.js';
import {getSportImage} from './action';

export const RenderItemSelected = ({
  item = {},
  userDetails = {},
  selectedSport = '',
  navigation,
  court
}) => {
  let name =
    ifValid(item.site) && ifValid(item.site.name) ? item.site.name : '';
  let homeBookings = useSelector((x) => x.homeBookings);
  let slot = ifValid(item) ? item : {};
  let startTime = ifValid(slot.startTime)
    ? moment(slot.startTime, 'hh:mm a').format('hh:mm A')
    : '';
  let endTime = ifValid(slot.endTime)
    ? moment(slot.endTime, 'hh:mm a').format('hh:mm A')
    : '';
  let day = ifValid(slot.startTime)
    ? moment(slot.startTime, 'hh:mm a').format('ddd')
    : '';
  let date = ifValid(homeBookings.selectedDate)
    ? homeBookings.selectedDate
    : '';
  let offerRate = ifValid(slot.offerRate) ? slot.offerRate : '';
  let resourceId =
    ifValid(item.resource) && ifValid(item.resource.id)
      ? item.resource.id
      : undefined;

  let checkUrl = validate({website: item.image}, {website: {url: true}});
  let image =
    ifValid(item.image) && item.image !== '' && checkUrl?.website === undefined
      ? {uri: item.image}
      : Images.sampleBooking;

  return (
    <View style={styles.renderContainer} key={item.id}>
      <Image source={image} style={styles.imageBox} />
      <View style={styles.cardContent}>
        <Text numberOfLines={1} style={styles.mainHeader}>
          {name}
        </Text>

        <Text style={styles.offerRate}>
          <Image source={Images.dollar} style={styles.smallImage} /> {offerRate}
        </Text>
        <Text style={styles.subTitle}>
          <Image
            source={
              ifValid(selectedSport) && selectedSport !== ''
                ? getSportImage(selectedSport)
                : Images.footBall
            }
            style={styles.smallImage}
          />{' '}
          {selectedSport}{"("+court?.resource?.name+")"}
        </Text>
        <Text style={styles.subTitle}>
          <Image source={Images.calendar} style={styles.smallImage} /> {day},{' '}
          {moment(date).format('Do MMM')}
        </Text>

        <Text style={styles.subTitle}>
          <Image source={Images.clock} style={styles.smallImage} /> {startTime}
          <Text> to </Text> {endTime}
        </Text>
        {/*<Text style={commonStyle.regular24}><Image source={Images.footBall} style={styles.smallImage}/> 4.0</Text>*/}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainHeader: {
    ...commonStyle.regular28Little,
    fontWeight: 'bold',
  },
  offerRate: {
    ...commonStyle.regular24,
    fontWeight: 'bold',
    fontSize: fontToDp(14),
  },
  subTitle: {
    ...commonStyle.regular24,
    color: '#505C74',
  },

  renderContainer: {
    marginBottom: widthToDp(1),
    flexDirection: 'row',
    backgroundColor: colors.white,
  },
  imageBox: {
    margin: widthToDp(4),
    width: widthToDp('35%'),
    resizeMode: 'stretch',
    height: heightToDp(18),
  },
  cardContent: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'space-around',
    paddingVertical: widthToDp('3'),
  },
  smallImage: {
    width: widthToDp(4),
    height: widthToDp(4),
    marginRight: widthToDp(1),
    resizeMode: 'contain',
  },
});
