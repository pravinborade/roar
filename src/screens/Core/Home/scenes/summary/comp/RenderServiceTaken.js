import React from 'react';
import {width, widthToDp} from '../../../../../../utils/Responsive';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, commonStyle} from '../../../../../../styles';
import Images from '../../../../../../assets/Images/Images';
import DropDownSmall from '../../services/comp/DropdownSmall';
import {Content} from '../../../../../../utils/content/Content';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {generateKey, ifValid} from '../../../../../../utils/helper';
import validate from 'validate.js';

export const RenderServiceTaken = ({
  item,
  onPickerTouch,
  onPickerSelect,
  onPickerClose,
  isVisible,
  removeFromCart,
  type = '3',
  name = '',
  rate = 0,
}) => {
  let checkUrl = validate({website: item.image}, {website: {url: true}});
  let image =
    ifValid(item.image) && item.image !== '' && checkUrl?.website === undefined
      ? {uri: item.image}
      : Images.product;

  return (
    <View key={generateKey(item.id, type)} style={styles.container}>
      <View style={styles.imageContainer}>
        <Image style={styles.imageStyle} source={image} />
      </View>
      <View style={styles.containerText}>
        <View style={styles.titleRateContainer}>
          <Text style={[styles.mainHeader, {width: '80%'}]}>{name}</Text>
          <Text style={[commonStyle.narrow30, {width: '20%'}]}>
            Rs{' '}
            {rate *
              (item.quantitySelected !== ''
                ? parseInt(item.quantitySelected)
                : 1)}
          </Text>
        </View>
        <DropDownSmall
          disabled={true}
          onValueChange={(selected, index) => {}}
          value={item.quantitySelected}
          title={Content.qty}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => removeFromCart(item)}
        >
          <Text style={commonStyle.narrow28}>{'Remove'}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  containerText: {flex: 1, marginVertical: widthToDp(2)},
  imageContainer: {alignItems: 'center', marginTop: widthToDp(2)},
  titleRateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    //width:'80%'
  },
  mainHeader: {
    ...commonStyle.regular28Little,
    fontWeight: 'bold',
  },
  container: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    marginTop: widthToDp(1.5),
    paddingHorizontal: widthToDp(4.5),
  },
  button: {
    borderRadius: 3,
    borderColor: 'black',
    borderWidth: 1,
    alignSelf: 'flex-start',
    marginRight: widthToDp(2),
    marginVertical: widthToDp(2),
    backgroundColor: colors.white,
    paddingVertical: widthToDp(0.5),
    paddingHorizontal: widthToDp(2.9),
  },
  paymentTextStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: widthToDp(1),
  },
  imageStyle: {
    width: widthToDp(20),
    height: widthToDp(20),
    // resizeMode:'contain',
    marginRight: widthToDp(2),
  },
});
