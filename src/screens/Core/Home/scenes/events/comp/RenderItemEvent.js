import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {heightToDp, widthToDp} from '../../../../../../utils/Responsive';
import {commonStyle} from '../../../../../../styles';
import {ifValid} from '../../../../../../utils/helper';
import {useDispatch} from 'react-redux';
import Images from '../../../../../../assets/Images/Images';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import {setSearchValue} from '../../../comp/action';
import {setSelectedEvent} from './action';
import validate from 'validate.js';

const margin = widthToDp('3%');

export const RenderItemEvent = ({item, navigation, userDetails = {}}) => {
  const dispatch = useDispatch();
  let title = ifValid(item.title) ? item.title : '';
  let resourceId =
    ifValid(item.resource) && ifValid(item.resource.id)
      ? item.resource.id
      : undefined;
  let checkUrl = validate({website: item.image}, {website: {url: true}});
  let image =
    ifValid(item.image) && item.image !== '' && checkUrl?.website === undefined
      ? {uri: item.image}
      : Images.sampleEvents;

  const _onPress = (item: {}) => {
    dispatch(setSearchValue());
    dispatch(setSelectedEvent(item));
    navigation.navigate(ScreenNames.HOME_EVENT_DETAIL);
  };

  return (
    <TouchableOpacity onPress={() => _onPress(item)} style={styles.container}>
      <Image source={image} style={styles.imageStyl} />
      <Text style={commonStyle.regular28} numberOfLines={2}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  imageStyl: {
    resizeMode: 'stretch',
    width: widthToDp('33%'),
    height: heightToDp('16%'),
  },
  container: {width: widthToDp('35%'), marginRight: widthToDp('2%')},
  subtitle: {...commonStyle.regular20, color: '#505C74'},
});
