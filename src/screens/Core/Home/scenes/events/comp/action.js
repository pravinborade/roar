export const setSelectedEvent = (item) => {
  return {type: 'SET_SELECTED_EVENT', payload: item};
};
