import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {heightToDp, widthToDp} from '../../../../../utils/Responsive';
import {ifValid} from '../../../../../utils/helper';
import {colors, commonStyle} from '../../../../../styles';
import {useSelector} from 'react-redux';
import {RenderItemEvent} from './comp/RenderItemEvent';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import {Content} from '../../../../../utils/content/Content';

const margin = widthToDp('4.5%');

const Events = ({props}) => {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const events = useSelector((state) => state.homeEvents.events);
  const onRefresh = () => {
    setIsRefreshing(true);
    setIsRefreshing(false);
  };
  return (
    <View style={{margin}}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{Content.event}</Text>
        {events && events.length > 3 ? (
          <Text
            onPress={() => {
              props.navigation.navigate(ScreenNames.HOME_EVENT_LIST);
            }}
            style={styles.subTitle}>
            SEE ALL
          </Text>
        ) : null}
      </View>
      <View style={{}}>
        {ifValid(events) && events.length > 0 ? (
          <FlatList
            horizontal
            data={events.slice(0, 3)}
            onRefresh={() => onRefresh()}
            refreshing={isRefreshing}
            showsHorizontalScrollIndicator={false}
            renderItem={({item, position}) => {
              return (
                <RenderItemEvent
                  item={item}
                  navigation={props.navigation}
                  index={position}
                />
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          <Text style={styles.container}>{Content.noEvntFound}</Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: heightToDp(1),
  },
  title: {...commonStyle.bold40, alignSelf: 'center'},
  subTitle: {
    ...commonStyle.regular24,
    alignSelf: 'center',
    color: colors.darkGreen,
  },
});
export default Events;
