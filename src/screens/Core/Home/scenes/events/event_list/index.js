import {
  BackHandler,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  Content,
  heightToDp,
  ScreenNames,
  widthToDp,
} from '../../../../../../utils';
import Images from '../../../../../../assets/Images/Images';
import {colors, commonStyle} from '../../../../../../styles';
import {Container, HeaderBack, InputSearch} from '../../../../../../components';
import {RenderItem} from './comp/RenderItem';
import {ifValid} from '../../../../../../utils/helper';
import {useDispatch, useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';
import {getEventList, setEvents} from '../../../comp/action';

const EventsList = ({navigation}) => {
  const dispatch = useDispatch();
  const [isRefreshing, setIsRefreshing] = useState(false);
  let {location} = useSelector((x) => x.login);
  let userDetails = useSelector((x) => x.login.data);
  let searchValue = useSelector((x) => x.inputs.searchValue);
  const {events, eventsTemp} = useSelector((state) => state.homeEvents);
  const latLong = useSelector((state) => state.login.latLong);

  const onRefresh = () => {
    setIsRefreshing(true);
    dispatch(getEventList(latLong));
    setIsRefreshing(false);
  };

  useEffect(() => {
    _onSearchVenues();
  }, [searchValue]);

  const _onSearchVenues = () => {
    try {
      if (searchValue !== '') {
        let e = eventsTemp.filter((x) => {
          let title = ifValid(x.title) ? x.title.toLowerCase() : '';
          if (title.includes(searchValue.toLowerCase())) {
            return x;
          }
        });
        dispatch(setEvents(e));
      } else {
        dispatch(setEvents(eventsTemp));
      }
    } catch (e) {
      dispatch(setEvents(eventsTemp));
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
      return () => {
        BackHandler.removeEventListener(
          'hardwareBackPress',
          handleBackButtonClick,
        );
      };
    }, []),
  );

  function handleBackButtonClick(): boolean {
    dispatch({type: 'SET_SEARCH_VALUE', payload: ''});
    navigation.goBack();
    return true;
  }

  return (
    <Container>
      <HeaderBack
        onPress={() => {
          dispatch({type: 'SET_SEARCH_VALUE', payload: ''});
        }}
        title={ScreenNames.EVENTS}
        navigation={navigation}
      />
      <InputSearch
        containerStyle={{marginVertical: heightToDp('1')}}
        placeholder={Content.eventPlaceHolder}
        onChangeText={(v) => {}}
      />
      <View style={styles.locFilterContainer}>
        <View style={styles.locationContainer}>
          <Image
            source={Images.homeLocation}
            style={commonStyle.locationImageStyle}
          />
          <Text style={{...commonStyle.regular24, flex: 1}}>{location}</Text>
        </View>
        {/*<FilterButton onPress={() => {*/}
        {/*    navigation.navigate(ScreenNames.VENUES_FILTER);*/}
        {/*}}/>*/}
      </View>
      <View
        style={[styles.listContainer, {backgroundColor: colors.background}]}>
        <Text style={styles.listHeaderTitle}>
          {events.length} {Content.resultFound}
        </Text>
        {ifValid(events) && events.length > 0 ? (
          <FlatList
            data={events}
            onRefresh={() => onRefresh()}
            refreshing={isRefreshing}
            showsVerticalScrollIndicator={false}
            renderItem={({item}, position) => {
              return (
                <RenderItem
                  userDetails={userDetails}
                  item={item}
                  position={position}
                  navigation={navigation}
                />
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          <Text style={[styles.listHeaderTitle, {alignSelf: 'center'}]}>
            {Content.noResultFound}
          </Text>
        )}
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  locFilterContainer: {
    justifyContent: 'space-between',
    padding: widthToDp(4.5),
    backgroundColor: 'white',
    ...commonStyle.rowAlignCenter,
  },
  locationContainer: {...commonStyle.rowAlignCenter},
  listContainer: {flex: 1},
  listHeaderTitle: {
    marginVertical: heightToDp(1),
    marginHorizontal: widthToDp(4.5),
  },
});
export default EventsList;
