import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {widthToDp} from '../../../../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../../../../styles';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {ifValid} from '../../../../../../../utils/helper';
import {useDispatch} from 'react-redux';
import {clearSlotsAndSlotSportDateTime} from '../../../bookings/action';
import moment from 'moment';
import Images from '../../../../../../../assets/Images/Images';
import {Content} from '../../../../../../../utils/content/Content';
import {setSelectedEvent} from '../../comp/action';
import validate from 'validate.js';

export const RenderItem = ({
  navigation,
  item = {},
  position,
  userDetails = {},
}) => {
  const dispatch = useDispatch();

  let title = ifValid(item.title) ? item.title : '';
  let locationArea =
    ifValid(item.location) && ifValid(item.location) ? item.location : '';
  let amount = ifValid(item.amount) ? item.amount : '';
  let startDate = ifValid(item.startDate) ? item.startDate : '';
  let distance =
    ifValid(item.distance) && item.distance !== -1
      ? ` | ${item.distance} Km`
      : '';
  let checkUrl = validate({website: item.image}, {website: {url: true}});
  let image =
    ifValid(item.image) && item.image !== '' && checkUrl?.website === undefined
      ? {uri: item.image}
      : Images.sampleBooking;

  const _onPress = (item) => {
    dispatch(clearSlotsAndSlotSportDateTime());
    dispatch(setSelectedEvent(item));
    navigation.navigate(ScreenNames.HOME_EVENT_DETAIL);
  };

  return (
    <TouchableOpacity
      style={styles.renderContainer}
      onPress={() => _onPress(item)}>
      <Image source={image} style={styles.imageBox} />
      <View style={styles.cardContent}>
        {/*| {distance} Km*/}
        <Text numberOfLines={1} style={commonStyle.narrow36}>
          {title}
        </Text>
        <Text numberOfLines={1} style={commonStyle.regular24}>
          {locationArea} {distance}{' '}
        </Text>

        <View style={styles.amountTitleContainer}>
          <View>
            <Text style={commonStyle.regular24}>
              {moment(startDate).format('DD MMM YYYY')}
            </Text>
            <Text style={styles.amountStyle}>
              {' '}
              {Content.rupies} {amount}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => _onPress(item)}>
            <Text style={styles.buttonTextStyle}>{Content.book}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const paddingSpace = {
  paddingHorizontal: widthToDp('1'),
  justifyContent: 'space-between',
  paddingVertical: widthToDp(1),
};

const styles = StyleSheet.create({
  amountTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  renderContainer: {
    paddingRight: 3,
    flexDirection: 'row',
    backgroundColor: colors.white,
    marginHorizontal: widthToDp(4.5),
    marginVertical: widthToDp(1.5),
  },
  amountStyle: {
    ...commonStyle.narrow22,
    fontWeight: '600',
    color: '#202B46',
  },
  imageBox: {
    width: widthToDp('32%'),
    resizeMode: 'stretch',
    height: '100%',
  },
  cardContent: {
    marginHorizontal: widthToDp(2),
    flex: 1,
    paddingVertical: widthToDp('3'),
  },
  textContainer: {
    width: widthToDp('38%'),
    ...paddingSpace,
  },
  buttonContainer: {
    width: widthToDp('20%'),
    ...paddingSpace,
  },
  buttonStyle: {
    paddingVertical: widthToDp(1),
    paddingHorizontal: widthToDp(5),
    backgroundColor: colors.primaryGreen,
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
  buttonTextStyle: {
    ...commonStyle.narrow22,
  },

  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp(4),
    height: widthToDp(4),
  },
});
