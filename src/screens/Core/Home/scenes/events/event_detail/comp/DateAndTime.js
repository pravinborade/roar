import {Image, StyleSheet, Text, View} from 'react-native';
import {widthToDp} from '../../../../../../../utils/Responsive';
import {colors, commonStyle} from '../../../../../../../styles';
import Images from '../../../../../../../assets/Images/Images';
import React from 'react';

export const DateAndTimeView = ({
  style,
  item = {},
  date = '',
  title = '',
  time = '',
}) => {
  return (
    <View style={[{marginVertical: widthToDp(3)}, style]}>
      <Text style={commonStyle.regular24}>{title}</Text>
      <View style={[styles.container]}>
        <Image source={Images.calendar} style={styles.dateIcon} />
        <Text style={[commonStyle.regular16, {color: colors.primaryBlue}]}>
          {' '}
          {date}
        </Text>
      </View>
      <Text style={[commonStyle.bold16]}>{time}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  dateIcon: {
    width: widthToDp(3),
    height: widthToDp(3),
    marginRight: widthToDp(1),
  },
  container: {
    flexDirection: 'row',
    paddingHorizontal: widthToDp(1.3),
    paddingVertical: widthToDp(1.3),
    backgroundColor: colors.background,
    alignSelf: 'flex-start',
    alignItems: 'center',
  },
});
