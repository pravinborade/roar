import {Image, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {
  heightToDp,
  width,
  widthToDp,
} from '../../../../../../../utils/Responsive';
import Carousel from 'react-native-snap-carousel';
import {colors, commonStyle} from '../../../../../../../styles';
import {useSelector} from 'react-redux';
import {ifValid} from '../../../../../../../utils/helper';

const Carousal = ({item = {}, image}) => {
  const [activeSlide, setActiveSlide] = useState(0);
  let userDetails = useSelector((x) => x.login.data);

  let resourceId =
    ifValid(item?.resource) && ifValid(item?.resource?.id)
      ? item.resource.id
      : undefined;

  const _renderItem = ({item, index}) => {
    return (
      <View style={styles.slide}>
        {ifValid(image) && (
          <Image source={image} style={styles.carousalStyle} />
        )}
      </View>
    );
  };

  function pagination() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          borderRadius: widthToDp(1),
          paddingHorizontal: widthToDp(2),
          position: 'absolute',
          bottom: 5,
          alignSelf: 'center',
        }}>
        <Text style={[commonStyle.regular20, {color: colors.primaryGrey}]}>
          {(activeSlide + 1).toString()}/1
        </Text>
      </View>
    );
  }

  return (
    <View style={{height: widthToDp(45)}}>
      <Carousel
        layout={'default'}
        data={[1]}
        onSnapToItem={(index) => setActiveSlide(index)}
        renderItem={_renderItem}
        sliderWidth={width}
        itemWidth={width}
      />
      {/*{pagination()}*/}
      {/*<Line/>*/}
    </View>
  );
};
export default Carousal;

const styles = StyleSheet.create({
  container: {},
  carousalStyle: {
    resizeMode: 'cover',
    width: width,
    height: heightToDp('45%'),
  },
});
