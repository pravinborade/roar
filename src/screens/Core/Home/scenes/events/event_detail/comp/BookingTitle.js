import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {commonStyle} from '../../../../../../../styles';
import {heightToDp, widthToDp} from '../../../../../../../utils';
import {ifValid} from '../../../../../../../utils/helper';
import Images from '../../../../../../../assets/Images/Images';

const BookingTitle = ({item = {}}) => {
  let name = ifValid(item.site.name) ? item.site.name : '';
  //let name = ifValid(item.title) ? item.title : '';
  let location = ifValid(item.location) ? item.location : '';
  let city = ifValid(item.city) ? item.city : '';
  let address = `${location}${city !== '' ? ', ' + city : ''}`;

  return (
    <View style={styles.container}>
      <View style={styles.flex1}>
        <Text style={commonStyle.narrow36}>{name}</Text>
        <View style={styles.row}>
          <Image
            source={Images.locationBlue}
            style={styles.locationIconStyle}
          />
          <Text numberOfLines={2} style={commonStyle.regular24}>
            {address}
          </Text>
        </View>
      </View>
      <View style={{...commonStyle.rowAlignCenter}}>
        {/*<Text style={{...commonStyle.regular20, marginRight: widthToDp(1)}}>4.0</Text>*/}
        {/*<Rating*/}
        {/*    isDisabled={true}*/}
        {/*    defaultRating={3}*/}
        {/*    ratingCount={5}*/}
        {/*    startingValue={3}*/}
        {/*    showRating={false}*/}
        {/*    imageSize={widthToDp(3)}*/}
        {/*/>*/}
      </View>
    </View>
  );
};
export default BookingTitle;
const styles = StyleSheet.create({
  flex1: {flex: 1},
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: heightToDp(1.2),
    paddingHorizontal: widthToDp(4.5),
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  locationIconStyle: {
    resizeMode: 'contain',
    marginRight: widthToDp(1),
    width: widthToDp(4),
  },
});
