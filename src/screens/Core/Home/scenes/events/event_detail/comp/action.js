import axiosService from '../../../../../../../commons/axiosService';
import {ifNotValid} from '../../../../../../../utils/helper';
import {successLog} from '../../../../../../../utils/fireLog';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {setLoading} from '../../../../../../../redux/actions/loader';
import {getEventList} from '../../../../comp/action';

export const bookEventApi = (requestBody, navigation, latLng) => {
  if (ifNotValid(requestBody)) {
    console.warn(' requestBody valid ');
    return;
  }
  return (dispatch) => {
    axiosService
      .post(`api/events`, requestBody)
      .then((response) => {
        if (response.status === 200) {
          successLog(response.data);
          navigation.navigate(ScreenNames.HOME_EVENT_ACKNOWLEDGEMENT);
          console.log('Event has been booked successfully.');
          dispatch(getEventList(latLng));
          // Event has been booked successfully.
        } else {
          console.log('not valid res ' + JSON.stringify(response));
        }
        dispatch(setLoading(false));
      })
      .catch((o) => {
        console.log('bookEventApi error ' + JSON.stringify(o));
        dispatch(setLoading(false));
      });
  };
};
