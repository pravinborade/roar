import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  ButtonCommon,
  Container,
  DropDownSmall,
  HeaderBack,
} from '../../../../../../components';
import {commonStyle} from '../../../../../../styles';
import {widthToDp} from '../../../../../../utils/Responsive';
import {DateAndTimeView} from './comp/DateAndTime';
import {
  getLineView,
  ifNotValid,
  ifValid,
  showToastMsg,
} from '../../../../../../utils/helper';
import moment from 'moment';
import {Content} from '../../../../../../utils/content/Content';
import {errorLog, successLog} from '../../../../../../utils/fireLog';
import {getOptions} from '../../summary/comp/action';
import RazorpayCheckout from 'react-native-razorpay';
import axiosService from '../../../../../../commons/axiosService';
import {bookEventApi} from './comp/action';
import BookingTitle from './comp/BookingTitle';
import Carousal from './comp/Carousal';
import helperFunctions from '../../../../../../commons/helperFunctions';
import {setLoading} from '../../../../../../redux/actions/loader';
import Images from '../../../../../../assets/Images/Images';
import {setFromPayment} from '../../../../../../redux/actions/login';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import validate from 'validate.js';

const EventDetails = ({navigation}) => {
  const dispatch = useDispatch();

  const [isVisible, setVisible] = useState(false);
  const [quantity, setQuantity] = useState(1);

  let homeVenue = useSelector((x) => x.homeVenue.selectedVenue);
  let userData = useSelector((x) => x.login.data);
  let selectedEvent = useSelector((x) => x.homeEvents.selectedEvent);
  let loading = useSelector((x) => x.loader.loading);
  const latLong = useSelector((state) => state.login.latLong);

  let name = ifValid(selectedEvent.title) ? selectedEvent.title : '';
  let location = ifValid(selectedEvent.location) ? selectedEvent.location : '';
  const [amount, setAmount] = useState(
    ifValid(selectedEvent.amount) ? selectedEvent.amount : '',
  );
  //let amount = ifValid(selectedEvent.amount) ? selectedEvent.amount : '';
  let psAmount = ifValid(selectedEvent.amount) ? selectedEvent.amount : '';
  let admitPerTicket = ifValid(selectedEvent.admitPerTicket)
    ? selectedEvent.admitPerTicket
    : '';

  let startDate = ifValid(selectedEvent.startDate)
    ? moment(selectedEvent.startDate).format('DD MMM, YYYY')
    : '';
  let startTime = ifValid(selectedEvent.startTime)
    ? moment(selectedEvent.startTime, 'hh:mm a').format('hh:mm a')
    : '';
  let endDate = ifValid(selectedEvent.endDate)
    ? moment(selectedEvent.endDate).format('DD MMM, YYYY')
    : '';
  let endTime = ifValid(selectedEvent.endTime)
    ? moment(selectedEvent.endTime, 'hh:mm a').format('hh:mm a')
    : '';
  let description = ifValid(selectedEvent.description)
    ? selectedEvent.description
    : '';
  let checkUrl = validate(
    {website: selectedEvent?.image},
    {website: {url: true}},
  );
  let image =
    ifValid(selectedEvent?.image) &&
    selectedEvent?.image !== '' &&
    checkUrl?.website === undefined
      ? {uri: selectedEvent?.image}
      : Images.sampleEvents;

  const _onPressBook = () => {
    if (selectedEvent.isBooked) {
      showToastMsg('You already booked this event..!');
      return;
    }
    psAmount = helperFunctions.convertRstoPs(amount);
    if (!ifValid(psAmount) || psAmount === '' || psAmount == 0) {
      showToastMsg('amount is not valid');
      errorLog('amount not valid');
      return;
    }

    if (moment(endDate).isBefore(moment(), 'day')) {
      showToastMsg('Cant book past events');
      return;
    }

    if (!ifValid(userData.id)) {
      dispatch(setLoading(false));
      dispatch(setFromPayment('event_book'));
      //navigation.navigate(ScreenNames.INITIAL)
      navigation.navigate(ScreenNames.LOGIN);
      //errorLog('id userData not valid');
      return;
    }

    /// checking is event already booked by same user...!
    console.log(
      'EVENT API----------------------->',
      `api/events/userEvents?userId=${userData.id}&eventId=${selectedEvent.id}`,
    );
    axiosService
      .get(
        `api/events/userEvents?userId=${userData.id}&eventId=${selectedEvent.id}`,
      )
      .then((response) => {
        console.log('EVENT API RESPONSE---------------->', response);
        successLog(response.status);
        if (response.status === 200) {
          showToastMsg(name, 'You already booked this event..!');
          dispatch(setLoading(false));
        }
        if (response.status === 204) {
          dispatch(setLoading(true));
          _initialTransaction();
        } else if (
          response.response &&
          response.response.status &&
          response.response.status === 400
        ) {
          showToastMsg('This event is already booked..!');
        }
      })
      .catch((o) => {
        console.log('makeInitialTransaction error ' + JSON.stringify(o));
      });
  };

  //TODO : step 1: initialTransaction
  const _initialTransaction = () => {
    successLog('prePaymentApiCall');
    let payload = {
      amount: psAmount,
      userId: userData.id,
      type: 'event_booking',
      planId: selectedEvent.id,
      eventId: selectedEvent.id,
    };
    axiosService
      .post(`api/payments?order=true`, payload)
      .then((response) => {
        successLog('prePaymentApiCall', response.status);
        if (response.status === 200) {
          response = response.data;
          if (ifNotValid(response)) {
            errorLog('invalid response ');
            return;
          }
          if (ifValid(response.data[0])) {
            _makePayment(response.data[0]);
          } else {
            alert('Something went wrong while pre payment check');
          }
        } else {
          dispatch(setLoading(false));
          alert('Something went wrong..');
        }
      })
      .catch((o) => {
        console.log('makeInitialTransaction error ' + JSON.stringify(o));
      });
  };

  //TODO : step 2: makePayment
  const _makePayment = (prePaymentRes) => {
    let options = getOptions(
      userData,
      psAmount,
      prePaymentRes.orderId,
      `Booking name : ${name} , userId : ${userData.id} , eventId: ${selectedEvent.id} event`,
    );
    successLog('_makePayment');
    RazorpayCheckout.open(options)
      .then((res) => {
        successLog('onPaymentSuccess');
        successLog(res);
        _bookEvent(prePaymentRes, res);
      })
      .catch((error) => {
        dispatch({type: 'SET_LOADING', payload: false});
        alert(`${error.error.description}`);
      });
  };

  //TODO: step: 3 book event..!
  const _bookEvent = (prePaymentRes, paymentResponse) => {
    selectedEvent.quantity = quantity;
    let data = {
      orderId: prePaymentRes.orderId,
      user: {id: userData.id},
      event: selectedEvent,
      quantity: quantity,
      payment_status: 'PAID',
      siteId: selectedEvent.site.id,
      paymentId: prePaymentRes.id,
      razorPay: true,
      paymentMode: 'UPI',
      paymentDetail: [
        {
          id: prePaymentRes.id,
          siteId: selectedEvent.site.id,
          userId: userData.id,
          eventId: selectedEvent.id,
          orderId: paymentResponse.razorpay_order_id,
          orgId: selectedEvent.site.organization.id,
          paymentId: paymentResponse.razorpay_payment_id,
          razorPay: true,
          paymentSource: 'MOBILE',
          type: 'event_booking',
          amount: parseInt(psAmount),
          ...paymentResponse,
        },
      ],
      paidAmount: psAmount,
    };
    dispatch(bookEventApi(data, navigation, latLong));
  };

  const onPickerSelect = (qty) => {
    var QuntityNumber = Number(qty);
    setAmount(selectedEvent.amount * QuntityNumber);
    setVisible(false);
    setQuantity(QuntityNumber);
  };

  const renderQuntity = () => {
    let qty = selectedEvent.maxCapacity / selectedEvent.admitPerTicket;
    return qty;
  };

  return (
    <Container>
      <HeaderBack navigation={navigation} title={'Details'} />
      <BookingTitle item={selectedEvent} />
      <Carousal image={image} item={homeVenue} />
      <ScrollView>
        <View style={[commonStyle.detailsContainer]}>
          <Text style={commonStyle.narrow32}>{name}</Text>
          <Text style={commonStyle.regular24} numberOfLines={3}>
            {description}
          </Text>
        </View>
        {getLineView()}

        <View style={[commonStyle.detailsContainer]}>
          <View style={{flexDirection: 'row'}}>
            <Text style={[commonStyle.regular24]}>{'Admit Per Ticket :'}</Text>
            <Text style={[commonStyle.regular24]}> {admitPerTicket}</Text>
          </View>
          <DropDownSmall
            quantity={renderQuntity()}
            key={1}
            onPickerTouch={() => setVisible(true)}
            onPickerSelect={(qty) => onPickerSelect(qty)}
            onPickerClose={() => setVisible(false)}
            isVisible={isVisible}
            value={quantity}
            title={'Ticket Qty'}
          />
        </View>

        {getLineView()}
        <View
          style={[commonStyle.detailsContainer, {marginVertical: widthToDp(1)}]}
        >
          <Text style={commonStyle.narrow32}>
            {Content.rupies} {amount}
          </Text>
        </View>
        {getLineView()}

        <View style={[commonStyle.detailsContainer, styles.dateContainer]}>
          <Text style={commonStyle.narrow32}>{Content.datTime}</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <DateAndTimeView
              title={'Start'}
              date={startDate}
              time={startTime}
            />
            <Text style={commonStyle.regular24}>{Content.to}</Text>
            <DateAndTimeView
              title={'End'}
              date={endDate}
              time={endTime}
              style={styles.dateTimeStyle}
            />
          </View>
        </View>
      </ScrollView>
      <ButtonCommon
        loading={loading}
        onPress={() => _onPressBook()}
        text={Content.book}
      />
    </Container>
  );
};
export default EventDetails;

const styles = StyleSheet.create({
  container: {},
  dateContainer: {
    paddingVertical: widthToDp(2),
  },
  dateTimeStyle: {
    justifyContent: 'center',
    alignSelf: 'flex-end',
    alignItems: 'flex-start',
  },
});
