import React, {useEffect, useState} from 'react';
import {
  Alert,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Linking,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import Images from '../../../../../assets/Images/Images';
import {ButtonCommon, Container, HeaderBack} from '../../../../../components';
import {colors, commonStyle} from '../../../../../styles';
import {
  PT_SANS_BOLD,
  PT_SANS_NARROW_BOLD,
} from '../../../../../styles/typography';
import {
  Content,
  errorLog,
  fontToDp,
  heightToDp,
  ifValid,
  ScreenNames,
  widthToDp,
} from '../../../../../utils';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getEventList, setLatLong, setLatLongOnly} from '../../comp/action';
import {setLocation} from '../../../../../redux/actions/login';
import {getVenuesList} from '../sports/sport_all/action';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoder';
import {GoogleCloudKey} from '../../../../../config';

const Location = ({navigation}) => {
  const location = useSelector((state) => state.login.location);
  const [locationValue, setlocationValue] = useState(location);
  const isFirstLaunch = useSelector((state) => state.onBoarding.isFirstLaunch);
  const dispatch = useDispatch();

  useEffect(() => {
    setlocationValue(location);
  }, [location]);

  const onPress = (data, details) => {
    dispatch(setLocation(data.description));
    let latLong =
      ifValid(details.geometry) &&
      ifValid(details.geometry.location) &&
      ifValid(details.geometry.location)
        ? details.geometry.location
        : undefined;
    if (latLong) {
      AsyncStorage.setItem('latLong', JSON.stringify(latLong));
      dispatch(setLatLong(latLong, data.description));
      dispatch(getVenuesList(latLong));
      dispatch(getEventList(latLong));
    }
  };

  // Location Function IOS
  const hasLocationPermissionIOS = async () => {
    const openSetting = () => {
      Linking.openSettings().catch(() => {
        Alert.alert('Unable to open settings');
      });
    };

    const status = await Geolocation.requestAuthorization('whenInUse');

    if (status === 'granted') {
      return true;
    }

    if (status === 'denied') {
      Alert.alert('Location permission denied');
    }

    if (status === 'disabled') {
      Alert.alert(
        `Turn on Location Services to allow "Roar Sports" to determine your location.`,
        '',
        [
          {text: 'Go to Settings', onPress: openSetting},
          {
            text: "Don't Use Location",
            onPress: () => {},
          },
        ],
      );
    }

    return false;
  };

  //   Location Permission IOS AND Android

  const hasLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      const hasPermission = await hasLocationPermissionIOS();
      return hasPermission;
    }

    if (Platform.OS === 'android') {
      const hasPermission = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );

      if (hasPermission) {
        return true;
      }

      const status = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );

      if (status === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
      }

      if (status === PermissionsAndroid.RESULTS.DENIED) {
        ToastAndroid.show(
          'Location permission denied by user.',
          ToastAndroid.LONG,
        );
      } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        ToastAndroid.show(
          'Location permission revoked by user.',
          ToastAndroid.LONG,
        );
      }
      return false;
    } else {
      return false;
    }
  };

  // Get Location Function
  const getLocation = async () => {
    const hasPermissionLocation = await hasLocationPermission();

    if (hasPermissionLocation == true) {
      Geolocation.getCurrentPosition(
        (position) => {
          geocoder(position);
        },
        (error) => {
          Alert.alert(`Code ${error.code}`, error.message);
          errorLog(error);
        },
        {
          accuracy: {
            android: 'high',
            ios: 'best',
          },
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 10000,
          distanceFilter: 0,
          forceRequestLocation: true,
          showLocationDialog: true,
        },
      );
    }
  };

  const geocoder = (position) => {
    Geocoder.fallbackToGoogle(GoogleCloudKey);
    Geocoder.geocodePosition({
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    })
      .then(async (res) => {
        setlocationValue(res[0].formattedAddress);
        dispatch(setLocation(res[0].formattedAddress));
        dispatch(
          setLatLongOnly({
            lat: position?.coords?.latitude,
            lng: position?.coords?.longitude,
          }),
        );
        await AsyncStorage.setItem(
          'latLong',
          JSON.stringify({
            lat: position?.coords?.latitude,
            lng: position?.coords?.longitude,
          }),
        );
      })
      .catch((err) => errorLog(err));
  };

  return (
    <Container>
      <ImageBackground
        imageStyle={styles.imgBackground}
        source={Images.location}
        style={{backgroundColor: colors.background, flex: 1}}>
        <HeaderBack
          title={ScreenNames.MY_LOCATION}
          onPress={() =>
            isFirstLaunch
              ? navigation.reset({
                  index: 0,
                  routes: [{name: ScreenNames.HOME}],
                })
              : navigation.goBack()
          }
        />
        <View style={{padding: 15, flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.locTxt}>{Content.location}</Text>
            <TouchableOpacity
              style={styles.touchableLoc}
              onPress={() => getLocation()}>
              <Image
                style={styles.currentLocImg}
                source={Images.cuurentlocation}
              />
              <Text style={{color: '#87C500'}}> {Content.picMyLocation}</Text>
            </TouchableOpacity>
          </View>

          <GooglePlacesAutocomplete
            fetchDetails={true}
            placeholder={Content.enterYourLocation}
            filterReverseGeocodingByTypes={['administrative_area_level_1']}
            autoFillOnNotFound={true}
            styles={{
              textInput: styles.placeHolderGoogle,
            }}
            renderRow={(rowData) => {
              const title = rowData.structured_formatting.main_text;
              const address = rowData.structured_formatting.secondary_text;
              return (
                <View style={commonStyle.rowAlignCenter}>
                  <View style={{marginRight: 5}}>
                    <Image
                      resizeMode={'contain'}
                      style={styles.locationImg}
                      source={Images.locationBlue}
                    />
                  </View>

                  <Text style={styles.locationTxt}>
                    {title + ', ' + address}
                  </Text>
                </View>
              );
            }}
            onPress={(data, details = null) => onPress(data, details)}
            renderRightButton={() => (
              <TouchableOpacity
                style={styles.clearButton}
                onPress={() => {
                  setlocationValue('');
                  dispatch(setLocation(''));
                }}>
                {locationValue.length == 0 ? null : (
                  <View style={styles.containerCross}>
                    <Image style={styles.crossImage} source={Images.cross} />
                  </View>
                )}
              </TouchableOpacity>
            )}
            textInputProps={{
              clearButtonMode: 'never',
              value: locationValue,
              onChangeText: (text) => {
                setlocationValue(text);
              },
            }}
            query={{
              key: GoogleCloudKey,
              language: 'en',
              types: '(regions)',
            }}
            enablePoweredByContainer={false}
            currentLocation={false}
          />
        </View>
        <KeyboardAvoidingView behavior="padding">
          <View style={{marginVertical: '5%'}}>
            <ButtonCommon
              disabled={!location.length}
              style={{
                ...styles.ContinueButton,
                backgroundColor: location.length
                  ? colors.primaryGreen
                  : '#BFC7D8',
              }}
              onPress={() => {
                navigation.reset({
                  index: 0,
                  routes: [{name: ScreenNames.HOME}],
                });
              }}
              textStyle={[
                styles.btnText,
                {color: location.length ? 'black' : colors.white},
              ]}
              text={Content.continue}
            />
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    </Container>
  );
};

export default Location;

const styles = StyleSheet.create({
  ContinueButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#BFC7D8',
    height: heightToDp('6%'),
    marginHorizontal: '5%',
    opacity: 1,
    borderRadius: 4,
  },
  currentLocImg: {
    height: 20,
    width: 20,
  },
  touchableLoc: {
    flexDirection: 'row',
    position: 'absolute',
    right: 0,
  },
  locTxt: {
    color: '#505C74',
    fontFamily: PT_SANS_BOLD,
    marginBottom: 10,
  },
  imgBackground: {
    height: 180,
    width: 180,
    resizeMode: 'contain',
    marginTop: heightToDp(50),
    marginLeft: widthToDp(25),
  },
  placeHolderGoogle: {
    borderRadius: 0,
    fontFamily: PT_SANS_BOLD,
    fontSize: fontToDp(15),
  },
  btnText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: fontToDp(14),
  },
  locationTxt: {
    fontSize: fontToDp(14),
    fontFamily: PT_SANS_BOLD,
  },
  locationImg: {
    height: 14,
    width: 14,
    tintColor: 'black',
  },
  clearButton: {
    position: 'absolute',
    right: 0,
    alignSelf: 'center',
    marginRight: '2%',
  },
  containerCross: {
    height: widthToDp(9),
    width: widthToDp(10),
    backgroundColor: colors.white,
    alignItems: 'center',
    alignSelf: 'flex-end',
    justifyContent: 'center',
  },
  crossImage: {
    tintColor: 'black',
    height: widthToDp(4),
    width: widthToDp(4),
  },
});
