import {BackHandler, Image, StyleSheet, Text} from 'react-native';
import React, {useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {Content} from '../../../../../utils/content/Content';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Images from '../../../../../assets/Images/Images';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import {ackStyle} from '../../comp/ackStyle';
import {StackActions, CommonActions} from '@react-navigation/native';

const Acknowledgement = (props) => {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  function handleBackButtonClick() {
    // resetStack();
    return true;
  }

  const resetStack = () =>
    props.navigation.reset({index: 0, routes: [{name: ScreenNames.HOME}]});

  return (
    <LinearGradient
      colors={['#ECFFC3', '#AFFF00']}
      style={ackStyle.linearGradient}>
      <Image style={ackStyle.logo} source={Images.basketball} />
      <Text style={ackStyle.yayStyle}>
        {props?.route?.params?.response?.isBooked === 'YES'
          ? Content.oops
          : Content.yay}
      </Text>
      <Text style={ackStyle.slotBookedStyl}>
        {props?.route?.params?.response?.isBooked === 'YES'
          ? Content.yourSlotNotBooked
          : Content.yourSlotBooked}
      </Text>

      {props?.route?.params?.response?.isBooked === 'YES' && (
        <Text style={ackStyle.slotSubBookedStyl}>
          {Content.amountWillBeRefunded}
        </Text>
      )}

      <TouchableOpacity
        style={ackStyle.button}
        onPress={() => {
          if (props?.route?.params?.response?.isBooked === 'YES') {
            // your slot is booked by someone else.!
            props.navigation.pop(4);
          } else {
            resetStack();
            props.navigation.dispatch(
              StackActions.replace(ScreenNames.HOME, {
                screen: ScreenNames.BOOKINGS,
              }),
            );
          }
        }}>
        <Text style={ackStyle.buttonText}>
          {props?.route?.params?.response?.isBooked !== 'YES'
            ? Content.myBooking
            : Content.bookAnotherSlot}
        </Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};
export default Acknowledgement;

const styles = StyleSheet.create({
  container: {},
});
