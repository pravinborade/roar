export const setSelectedDate = (
  date,
  siteId,
  category,
  organizationId,
  userId,
) => {
  return (dispatch) => {
    // dispatch({type: 'SET_SELECTED_DATE', payload: date});
    dispatch(getSlots(date, siteId, category, organizationId, userId));
  };
};
