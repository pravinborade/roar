import {Image, StyleSheet, Text} from 'react-native';
import React from 'react';
import {heightToDp, widthToDp} from '../../../../../utils/Responsive';
import {commonStyle} from '../../../../../styles';
import {ifValid} from '../../../../../utils/helper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {setSelectedVenue} from '../venues/venue_all/action';
import {getAminities} from '../venues/venue_detail/action';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import {useDispatch} from 'react-redux';
import Images from '../../../../../assets/Images/Images';
import {setSearchValue} from '../../comp/action';
import {clearSlotsAndSlotSportDateTime} from '../bookings/action';
import validate from 'validate.js';

const margin = widthToDp('3%');

const HomeImageBox = ({
  item = {},
  index,
  onPress,
  navigation,
  userDetails = {},
}) => {
  const dispatch = useDispatch();
  let title = ifValid(item?.name) ? item.name : '';
  let locationArea = ifValid(item?.area) ? item.area : '';
  let distance =
    ifValid(item?.distance) && item.distance !== -1
      ? ` | ${item?.distance} Km`
      : '';
  let id = ifValid(item.id) ? item.id : undefined;
  let subTitle = locationArea + ' ' + distance;
  let checkUrl = validate({website: item?.image}, {website: {url: true}});
  let image =
    ifValid(item?.image) &&
    item?.image !== '' &&
    checkUrl?.website === undefined
      ? {uri: item?.image}
      : Images.sampleBooking;

  const _onPress = () => {
    dispatch(clearSlotsAndSlotSportDateTime());
    dispatch(setSearchValue());
    dispatch(setSelectedVenue(item));
    dispatch(
      getAminities(id, ifValid(userDetails) ? userDetails.id : undefined),
    );
    navigation.navigate(ScreenNames.VENUE_DETAILS);
  };

  return (
    <TouchableOpacity onPress={() => _onPress()} style={styles.container}>
      <Image source={image} style={styles.imageStyl} />
      <Text style={styles.title} numberOfLines={2}>
        {title}
      </Text>
      <Text style={styles.subtitle} numberOfLines={1}>
        {subTitle}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  imageStyl: {
    resizeMode: 'stretch',
    width: widthToDp('33%'),
    height: heightToDp('16%'),
  },
  container: {
    width: widthToDp('33%'),
    marginRight: widthToDp('2%'),
  },
  subtitle: {...commonStyle.regular20, color: '#505C74'},
  title: {...commonStyle.regular28},
});
export default HomeImageBox;
