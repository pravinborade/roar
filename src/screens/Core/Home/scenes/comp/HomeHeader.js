import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Images from '../../../../../assets/Images/Images';
import {colors, commonStyle} from '../../../../../styles';
import React, {useEffect, useState} from 'react';
import {
  Content,
  heightToDp,
  If,
  ScreenNames,
  widthToDp,
} from '../../../../../utils';
import {useDispatch, useSelector} from 'react-redux';
import {receiveNotification} from '../../../../Notification/action';

const margin = widthToDp('3%');

const HomeHeader = ({navigation}) => {
  const userToken = useSelector((state) => state.login.userToken);
  const location = useSelector((state) => state.login.location);
  const badgestate = useSelector((state) => state.login.recivenotification);
  const dispatch = useDispatch();

  return (
    <View style={styles.headerStyle}>
      <View style={styles.locationStyle}>
        <Image
          source={Images.homeLocation}
          style={commonStyle.locationImageStyle}
        />
        <Text
          ellipsizeMode="tail"
          numberOfLines={1}
          style={[
            commonStyle.regular24,
            {marginRight: 4},
            location.length > 10 && {width: '45%'},
          ]}>
          {location}
        </Text>
        <TouchableOpacity
          style={{justifyContent: 'center'}}
          onPress={() => navigation.navigate(ScreenNames.MY_LOCATION)}>
          <Text style={[commonStyle.regular16, {}]}>
            {Content.changeButton}
          </Text>
        </TouchableOpacity>
      </View>
      <If show={userToken}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate(ScreenNames.NOTIFICATION);
            dispatch(receiveNotification(false));
          }}>
          {badgestate === false ? (
            <Image source={Images.homeBell} style={styles.bellStyle} />
          ) : (
            <>
              <Image source={Images.homeBell} style={styles.bellStyle} />
              <View style={styles.notificationRedDot} />
            </>
          )}
        </TouchableOpacity>
      </If>
    </View>
  );
};

const styles = StyleSheet.create({
  headerStyle: {
    flexDirection: 'row',
    marginHorizontal: margin,
    backgroundColor: colors.white,
    padding: heightToDp('1%'),
  },
  notificationRedDot: {
    width: 8,
    height: 8,
    borderRadius: 9,
    backgroundColor: 'red',
    position: 'absolute',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    top: '10%',
    right: '5%',
  },
  bellStyle: {
    resizeMode: 'contain',
    width: widthToDp('5%'),
    height: widthToDp('5.9%'),
  },
  locationStyle: {flexDirection: 'row', flex: 1, alignItems: 'center'},
});
export default HomeHeader;
