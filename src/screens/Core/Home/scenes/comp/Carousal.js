import {Image, StyleSheet, View} from 'react-native';
import React, {useState} from 'react';
import {heightToDp, width, widthToDp} from '../../../../../utils/Responsive';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {ifValid} from '../../../../../utils/helper';

const margin = widthToDp('3%');

const Carousal = ({imagesData}) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const _renderItem = ({item, index}) => {
    return (
      <View style={styles.slide}>
        {ifValid(item) && (
          <Image source={{uri: item}} style={styles.carousalStyle} />
        )}
      </View>
    );
  };

  return imagesData && imagesData?.length > 0 ? (
    <View
      style={{
        height: heightToDp('28%'),
      }}>
      <Carousel
        // loop
        // autoplay={true}
        // autoplayDelay={1500}
        // autoplayInterval={3000}
        layout={'default'}
        data={imagesData}
        onSnapToItem={(index) => setActiveSlide(index)}
        renderItem={_renderItem}
        sliderWidth={width}
        itemWidth={width}
      />
      <Pagination
        dotsLength={imagesData?.length}
        containerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: widthToDp(0.5),
        }}
        activeDotIndex={activeSlide}
        dotStyle={{
          width: widthToDp(2),
          height: widthToDp(2),
          borderRadius: widthToDp(1),
          marginHorizontal: widthToDp(0),
          backgroundColor: '#AFFF00',
        }}
        inactiveDotStyle={{
          backgroundColor: '#BFC7D8',
          // Define styles for inactive dots here
        }}
        inactiveDotOpacity={0.9}
        inactiveDotScale={0.9}
      />
    </View>
  ) : null;
};

const styles = StyleSheet.create({
  carousalStyle: {
    resizeMode: 'contain',
    width: '100%',
    height: heightToDp('30%'),
  },
  slide: {
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: widthToDp(4.5),
  },
});
export default Carousal;
