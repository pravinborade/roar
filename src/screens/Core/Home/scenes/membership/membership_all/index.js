import React, {useState} from 'react';
import {FlatList, RefreshControl, StyleSheet} from 'react-native';
import Images from '../../../../../../assets/Images/Images';
import {HeaderBack} from '../../../../../../components';
import Container from '../../../../../../components/Container';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import MembershipPoster from './comp/membershipPoster';
import {useDispatch, useSelector} from 'react-redux';
import {ifValid} from '../../../../../../utils/helper';
import {
  getMembership,
  setMembershipSelected,
} from '../../venues/venue_detail/action';

const MembershipAll = ({navigation}) => {
  let membership = useSelector((x) => x.homeVenue.membership);
  const userDetails = useSelector((state) => state.login.data);
  const selectedVenue = useSelector((state) => state.homeVenue.selectedVenue);
  const [refreshing, setrefreshing] = useState(false);

  const dispatch = useDispatch();

  console.log('MEMBRSHIP ALL------------>', membership);
  const _onPressBuy = (item) => {
    console.log('item&&', item);
    dispatch(setMembershipSelected(item));
    navigation.navigate(ScreenNames.MEMBERSHIP_DETAIL);
  };

  const onRefresh = async () => {
    setrefreshing(true);
    await dispatch(getMembership(selectedVenue?.site?.id, userDetails?.id));
    setrefreshing(false);
  };

  const renderItem = ({item = {}}) => {
    // console.log("@@-", selectedVenue?.site?.id);
    let id =
      ifValid(item.site) && ifValid(item.site.id) ? item.site.id : undefined;
    let duration = ifValid(item.duration) ? item.duration : '';
    let amount = ifValid(item.amount) ? item.amount : '';
    let name = ifValid(item.name) ? item.name : '';
    let membershipType = ifValid(item.membershipType)
      ? item.membershipType
      : '';
    let flatRate = ifValid(item.flatRate) ? item.flatRate : '';
    let discountPercentage = ifValid(item.discountPercentage)
      ? item.discountPercentage
      : '';
    let packageFlatRate = ifValid(item.packageFlatRate)
      ? item.packageFlatRate
      : '';
    let packageDiscount = ifValid(item.packageDiscount)
      ? item.packageDiscount
      : '';
    let priceToPay = ifValid(item.priceToPay) ? item.priceToPay : '';
    return (
      <MembershipPoster
        logoImage={Images.blueCircle}
        backgroundColor={'#E5ECF9'}
        onPress={() => {
          _onPressBuy(item);
        }}
        title={name}
        navigation={navigation}
        subtitle={membershipType}
        flatRate={flatRate}
        discountPercentage={discountPercentage}
        packageFlatRate={packageFlatRate}
        packageDiscount={packageDiscount}
        price={amount}
        months={duration}
        priceToPay={priceToPay}
        posterImage={Images.blueImg}
      />
    );
  };

  return (
    <Container>
      <HeaderBack navigation={navigation} title={'Membership'} />
      <FlatList
        data={membership}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
    </Container>
  );
};

export default MembershipAll;

const styles = StyleSheet.create({});
