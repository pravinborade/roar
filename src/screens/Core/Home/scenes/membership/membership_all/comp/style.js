import {StyleSheet} from 'react-native';
import {widthToDp} from '../../../../../../../utils/Responsive';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../../styles/typography';

export const styles = StyleSheet.create({
  rowStyle: {
    marginVertical: widthToDp(1),
    flexDirection: 'row',
    marginHorizontal: widthToDp(4.5),
  },

  imageStyle: {
    width: widthToDp(20),
    height: widthToDp(25),
    resizeMode: 'contain',
  },
  mainContainer: {
    flex: 1,
    height: '30%',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  mainSubContainer: {
    flexDirection: 'row',
    marginTop: 5,
    marginRight: widthToDp(4.5),
  },
  view_one: {
    marginLeft: 20,
    marginTop: 20,
  },
  view_two: {
    flex: 1,
    marginTop: 20,
    marginLeft: 10,
  },
  title: {
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: 18,
  },
  subtitle: {
    color: '#505C74',
    fontFamily: PT_SANS_REGULAR,
    fontSize: 10,
    marginTop: 2,
    marginRight: 20,
    opacity: 1,
  },
  imageStyle_one: {
    width: 50,
    height: 50,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
    marginVertical: widthToDp(3),
    marginBottom: 10,
  },
  bottomSubContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buyButton: {
    backgroundColor: '#AFFF00',
    width: 60,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },
  buyButtonText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
  },
});
