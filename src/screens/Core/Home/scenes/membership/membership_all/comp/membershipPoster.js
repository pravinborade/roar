import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../../styles/typography';
import {widthToDp} from '../../../../../../../utils/Responsive';
import Line from '../../../../../../../components/Line';
import {colors, commonStyle} from '../../../../../../../styles';
import {setMembershipSelected} from '../../../venues/venue_detail/action';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {useDispatch} from 'react-redux';
import {Content} from '../../../../../../../utils/content/Content';
import {styles} from './style';
const membershipPoster = ({
  logoImage,
  discountPercentage,
  flatRate,
  onPress,
  title,
  subtitle,
  price,
  months,
  buyonPress,
  posterImage,
  navigation,
  backgroundColor,
  packageFlatRate,
  packageDiscount,
  priceToPay,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{...styles.mainContainer, backgroundColor: backgroundColor}}
    >
      <View style={styles.mainSubContainer}>
        <View style={styles.view_one}>
          <Image style={styles.imageStyle_one} source={logoImage} />
        </View>
        <View style={styles.view_two}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}> {subtitle} </Text>
        </View>
        <Image style={styles.imageStyle} source={posterImage} />
      </View>

      <View style={styles.rowStyle}>
        <Text style={[commonStyle.narrow28]}>{Content.rupies}</Text>
        <Text style={commonStyle.regular24}>{price} </Text>
        <Text style={{...commonStyle.regular16, color: colors.primaryBlue}}>
          {' '}
          ({months})
        </Text>
      </View>

      {flatRate !== '' ? (
        <View style={styles.rowStyle}>
          <Text style={commonStyle.narrow28}>Slots Flat Rate: </Text>
          <Text style={commonStyle.regular24}>{flatRate} Rs</Text>
        </View>
      ) : null}

      {discountPercentage !== '' ? (
        <View style={styles.rowStyle}>
          <Text style={commonStyle.narrow28}>Slots Discount: </Text>
          <Text style={commonStyle.regular24}>{discountPercentage}%</Text>
        </View>
      ) : null}

      {packageFlatRate !== '' ? (
        <View style={styles.rowStyle}>
          <Text style={commonStyle.narrow28}>Membership Flat Rate: </Text>
          <Text style={commonStyle.regular24}>{packageFlatRate} Rs</Text>
        </View>
      ) : null}

      {packageDiscount !== '' ? (
        <View style={styles.rowStyle}>
          <Text style={commonStyle.narrow28}>Membership Discount: </Text>
          <Text style={commonStyle.regular24}>{packageDiscount}%</Text>
        </View>
      ) : null}

      <Line lineColor={'gray'} />

      <View style={styles.bottomContainer}>
        {/* <View style={styles.bottomSubContainer}>
          <Text style={commonStyle.narrow22}>
            {Content.rupies} {price}{' '}
          </Text>
          <Text style={{...commonStyle.regular16, color: colors.primaryBlue}}>
            {' '}
            ({months})
          </Text>
        </View> */}
        <View style={styles.bottomSubContainer}>
          <Text style={[commonStyle.narrow22]}>
            {'Price To Pay: '} {priceToPay}{' '}
          </Text>
        </View>

        <TouchableOpacity onPress={onPress} style={styles.buyButton}>
          <Text style={styles.buyButtonText}>Buy</Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export default membershipPoster;
