import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  ButtonCommon,
  Container,
  HeaderBack,
  Line,
} from '../../../../../../components';
import {
  fontToDp,
  heightToDp,
  width,
  widthToDp,
} from '../../../../../../utils/Responsive';
import Images from '../../../../../../assets/Images/Images';
import {commonStyle} from '../../../../../../styles';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../styles/typography';
import {
  ifNotValid,
  ifValid,
  showToastMsg,
} from '../../../../../../utils/helper';
import {errorLog, successLog} from '../../../../../../utils/fireLog';
import {setLoading} from '../../../../../../redux/actions/loader';
import helperFunctions from '../../../../../../commons/helperFunctions';
import axiosService from '../../../../../../commons/axiosService';
import {getOptions} from '../../summary/comp/action';
import RazorpayCheckout from 'react-native-razorpay';
import {bookMembership} from './comp/action';
import {Content} from '../../../../../../utils/content/Content';
import moment from 'moment';
import {setFromPayment} from '../../../../../../redux/actions/login';
import {ScreenNames} from '../../../../../../utils/ScreenNames';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {membershipRenewurl} from '../../../../../../root/navigation/deeplinking/action';

const MembershipDetails = (props) => {
  const dispatch = useDispatch();
  let userData = useSelector((x) => x.login.data);
  let loading = useSelector((x) => x.loader.loading);
  let selected = useSelector((x) => x.homeVenue.membershipSelected);
  let duration = ifValid(selected.duration) ? selected.duration : '';
  let amount = ifValid(selected.amount) ? selected.amount : '';
  let name = ifValid(selected.name) ? selected.name : '';
  let membershipType = ifValid(selected.membershipType)
    ? selected.membershipType
    : '';
  let siteName =
    ifValid(selected.site) && ifValid(selected.site.name)
      ? selected.site.name
      : '';
  let area =
    ifValid(selected.site) && ifValid(selected.site.area)
      ? selected.site.area
      : '';
  let city =
    ifValid(selected.site) && ifValid(selected.site.city)
      ? selected.site.city
      : '';
  let startDate = ifValid(selected.startDate) ? selected.startDate : '-';
  let endDate = ifValid(selected.endDate) ? selected.endDate : '-';
  let flatRate = ifValid(selected.flatRate) ? selected.flatRate : '';
  let discountPercentage = ifValid(selected.discountPercentage)
    ? selected.discountPercentage
    : '';
  let packageFlatRate = ifValid(selected.packageFlatRate)
    ? selected.packageFlatRate
    : '';
  let packageDiscount = ifValid(selected.packageDiscount)
    ? selected.packageDiscount
    : '';

  let priceToPay = ifValid(selected.priceToPay) ? selected.priceToPay : '';

  //TODO : step 1: initialTransaction
  const _onPressBook = () => {
    if (!ifValid(userData.id)) {
      dispatch(setFromPayment('membership_book'));
      //props.navigation.navigate(ScreenNames.INITIAL)
      props.navigation.navigate(ScreenNames.LOGIN);
      // errorLog('id userData not valid');
      return;
    }

    if (selected.isMember) {
      showToastMsg('Membership', 'This membership is already purchased');
      return;
    }
    successLog('makePayment');
    dispatch(setLoading(true));
    priceToPay = helperFunctions.convertRstoPs(priceToPay);

    if (!ifValid(priceToPay) || priceToPay === '') {
      errorLog('amount not valid');
      alert('amount not valid');
      dispatch(setLoading(false));
      return;
    }

    successLog('prePaymentApiCall');
    let payload = {
      amount: priceToPay,
      userId: userData.id,
      type: 'membership_booking',
      planId: null,
      eventId: null,
    };
    axiosService
      .post(`api/payments?order=true`, payload)
      .then((response) => {
        successLog('prepayment', response.status);
        if (response.status === 200) {
          response = response.data;
          if (ifNotValid(response)) {
            errorLog('invalid response ');
            return;
          }
          successLog('prePaymentApiCall', response.status);
          if (ifValid(response.data[0])) {
            _makePayment(response.data[0]);
          } else {
            alert('Something went wrong while pre payment check');
          }
        } else {
          dispatch(setLoading(false));
          alert('Something went wrong..');
        }
      })
      .catch((o) => {
        dispatch(setLoading(false));
        console.log('makeInitialTransaction error ' + JSON.stringify(o));
      });
  };

  //TODO : step 2: makePayment
  const _makePayment = (prePaymentRes) => {
    let prePayment = prePaymentRes;
    if (ifNotValid(prePaymentRes.orderId)) {
      errorLog('order id is invalid');
      return;
    }
    successLog(prePaymentRes);
    let options = getOptions(
      userData,
      priceToPay,
      prePaymentRes.orderId,
      `Booking ${name} membership`,
    );
    successLog('_makePayment');
    RazorpayCheckout.open(options)
      .then((res) => {
        _bookMembership(prePayment, res);
      })
      .catch((error) => {
        dispatch(setLoading(false));
        alert(`${error.error.description}`);
      });
  };

  const getStartAndEndDate = () => {};

  //TODO: step: 3 book event..!
  const _bookMembership = async (prePaymentRes, paymentResponse) => {
    // moment(new Date())
    let dd = `(${moment(new Date()).format('DD MMM YYYY')} - ${moment(
      new Date(),
    )
      .add(selected.periodUnit, selected.period)
      .format('DD MMM YYYY')})`;

    try {
      let data = {
        orderId: prePaymentRes.orderId,
        user: {id: userData?.id},
        plan: {id: selected?.id},
        id: selected?.membershipId,
        startDate: moment(new Date()),
        endDate: moment(new Date()).add(selected.periodUnit, selected?.period),
        payment_status: 'PAID',
        dofbooking: `(${moment(new Date()).format('DD MMM YYYY')} - ${moment(
          new Date(),
        )
          .add(selected.periodUnit, selected.period)
          .format('DD MMM YYYY')})`,
        siteId: selected?.site?.id,
        razorPay: true,
        paymentMode: 'UPI',
        paymentDetail: [
          {
            ...prePaymentRes,
            paymentSource: 'MOBILE',
            siteId: selected?.site?.id,
            orgId: selected?.site?.organization?.id,
            userId: userData?.id,
            planId: selected?.id,
            razorPay: true,
            type: 'membership_booking',
            amount: parseInt(amount),
            orderId: paymentResponse.razorpay_order_id,
            paymentId: paymentResponse.razorpay_payment_id,
          },
        ],
        paidAmount: priceToPay,
      };
      dispatch(bookMembership(data, props.navigation));
    } catch (e) {
      dispatch(setLoading(false));
      successLog('errr ', e);
    }
  };

  // useEffect(() => {
  //     console.log("duration: ", duration)
  // }, [])

  return (
    <Container>
      <HeaderBack title={'Membership'} navigation={props.navigation} />
      <View style={{width, justifyContent: 'space-around'}}>
        <View style={styles.container}>
          <View
            style={{
              width: '100%',
              paddingVertical: widthToDp(2),
              backgroundColor: '#FFD8D8',
              alignItems: 'center',
            }}
          >
            <Image style={styles.image} source={Images.orangeCircle} />
            <Text style={commonStyle.narrow32}>{name}</Text>
          </View>

          <View style={styles.middleContainer}>
            <View style={styles.middleSubContainer}>
              <Text style={styles.middleText_1}>
                ({' '}
                {duration
                  ? duration
                  : selected?.period + ' ' + selected?.periodUnit}{' '}
                )
              </Text>
              <Text style={styles.middleText_2}>
                {Content.rupies} {amount}
              </Text>
            </View>
            <Text style={styles.middleText_3}>{membershipType}</Text>

            {flatRate !== '' ? (
              <View style={styles.rowStyle}>
                <Text style={commonStyle.narrow28}>Slots Flat Rate: </Text>
                <Text style={commonStyle.regular24}>{flatRate} Rs</Text>
              </View>
            ) : null}

            {discountPercentage !== '' ? (
              <View style={styles.rowStyle}>
                <Text style={commonStyle.narrow28}>Slots Discount: </Text>
                <Text style={commonStyle.regular24}>{discountPercentage}%</Text>
              </View>
            ) : null}
            {packageFlatRate !== '' ? (
              <View style={styles.rowStyle}>
                <Text style={commonStyle.narrow28}>Membership Flat Rate: </Text>
                <Text style={commonStyle.regular24}>{packageFlatRate} Rs</Text>
              </View>
            ) : null}
            {packageDiscount !== '' ? (
              <View style={styles.rowStyle}>
                <Text style={commonStyle.narrow28}>Membership Discount: </Text>
                <Text style={commonStyle.regular24}>{packageDiscount}%</Text>
              </View>
            ) : null}
            {priceToPay !== '' ? (
              <View style={styles.rowStyle}>
                <Text style={[commonStyle.narrow28]}>Price To Pay: </Text>
                <Text style={commonStyle.regular24}>{priceToPay}</Text>
              </View>
            ) : null}
          </View>

          <Line lineColor={'gray'} />

          <View style={styles.bottomContainer}>
            <View>
              <Text style={styles.bottomText_1}>{siteName}</Text>
            </View>
            <View>
              <Text style={styles.bottomText_2}>
                {area}, {city}
              </Text>
            </View>

            {/*<View style={{flexDirection: 'row'}}>*/}
            {/*    <View>*/}
            {/*        <Text style={styles.startDate_Text_1}>Start date</Text>*/}
            {/*        <View style={styles.startDate_View}>*/}
            {/*            <Image style={styles.startDate_Text_2_Image} source={Images.calenderButton}/>*/}
            {/*            <Text style={styles.startDate_Text_2}>{startDate}</Text>*/}
            {/*        </View>*/}
            {/*    </View>*/}

            {/*    <View style={{marginLeft: 10}}>*/}
            {/*        <Text style={styles.startDate_Text_1}>End date</Text>*/}
            {/*        <View style={styles.startDate_View}>*/}
            {/*            <Image style={styles.startDate_Text_2_Image} source={Images.calenderButton}/>*/}
            {/*            <Text style={styles.startDate_Text_2}>{endDate}</Text>*/}
            {/*        </View>*/}
            {/*    </View>*/}
            {/*</View>*/}
          </View>
        </View>
        <ButtonCommon
          loading={loading}
          onPress={() => _onPressBook()}
          text={'buy'}
        />
      </View>
    </Container>
  );
};
export default MembershipDetails;

const styles = StyleSheet.create({
  rowStyle: {
    marginVertical: widthToDp(1),
    flexDirection: 'row',
  },

  container: {
    backgroundColor: '#FEE2E2',
    margin: widthToDp(4.5),
  },
  image: {
    height: heightToDp(10),
    width: widthToDp(15),
    resizeMode: 'contain',
  },
  middleContainer: {
    borderBottomColor: '#FFD8D8',
    borderBottomWidth: 2,
    backgroundColor: '#FEE2E2',
    width: '100%',
    padding: 15,
    justifyContent: 'space-evenly',
  },
  middleSubContainer: {
    flexDirection: 'row',
    width: '100%',
    marginTop: widthToDp(2),
    justifyContent: 'space-between',
  },
  middleText_1: {
    color: '#505C74',
    fontSize: fontToDp(14),
  },
  middleText_2: {
    fontSize: fontToDp(14),
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  middleText_3: {
    color: '#202B46',
    marginTop: widthToDp(2),
    fontSize: fontToDp(13),
    fontFamily: PT_SANS_REGULAR,
  },
  bottomContainer: {
    backgroundColor: '#FEE2E2',
    padding: widthToDp(4.5),
    justifyContent: 'space-evenly',
  },
  bottomText_1: {
    color: '#202B46',
    fontSize: fontToDp(14),
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bottomText_2: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontToDp(10),
  },
  startDate_Text_1: {
    color: '#202B46',
    fontSize: fontToDp(12),
    fontFamily: PT_SANS_REGULAR,
    marginBottom: 5,
  },
  startDate_View: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: widthToDp(22),
    height: 'auto',
    padding: 3,
  },
  startDate_Text_2_Image: {
    resizeMode: 'contain',
    height: 10,
    width: 10,
    alignSelf: 'center',
    margin: 2,
  },
  startDate_Text_2: {
    fontFamily: PT_SANS_REGULAR,
    color: '#202B46',
    fontSize: fontToDp(10),
  },
});
