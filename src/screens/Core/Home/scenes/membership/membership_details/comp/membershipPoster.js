import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../../../styles/typography';
import {widthToDp} from '../../../../../../../utils/Responsive';
import Line from '../../../../../../../components/Line';
import {colors, commonStyle} from '../../../../../../../styles';
import {Content} from '../../../../../../../utils/content/Content';

const membershipPoster = ({
  logoImage,
  title,
  subtitle,
  price,
  months,
  buyonPress,
  posterImage,
  backgroundColor,
}) => {
  return (
    <View style={{...styles.mainContainer, backgroundColor: backgroundColor}}>
      <View style={styles.mainSubContainer}>
        <View style={styles.view_one}>
          <Image style={styles.imageStyle_one} source={logoImage} />
        </View>

        <View style={styles.view_two}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}> {subtitle} </Text>
        </View>
        <Image style={styles.imageStyle} source={posterImage} />
      </View>

      <Line lineColor={'gray'} />

      <View style={styles.bottomContainer}>
        <View style={styles.bottomSubContainer}>
          <Text style={commonStyle.narrow22}>
            {Content.rupies} {price}{' '}
          </Text>
          <Text style={{...commonStyle.regular16, color: colors.primaryBlue}}>
            {' '}
            ({months})
          </Text>
        </View>
        <TouchableOpacity style={styles.buyButton}>
          <Text style={styles.buyButtonText}>Buy</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default membershipPoster;

const styles = StyleSheet.create({
  imageStyle: {
    width: widthToDp(20),
    height: widthToDp(25),
    resizeMode: 'contain',
  },
  mainContainer: {
    flex: 1,
    height: '30%',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  mainSubContainer: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 10,
    marginRight: widthToDp(4.5),
  },
  view_one: {
    marginLeft: 20,
    marginTop: 20,
  },
  view_two: {
    flex: 1,
    marginTop: 20,
    marginLeft: 10,
  },
  title: {
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
    fontSize: 18,
  },
  subtitle: {
    color: '#505C74',
    fontFamily: PT_SANS_REGULAR,
    fontSize: 10,
    marginTop: 2,
    marginRight: 20,
    opacity: 1,
  },
  imageStyle_one: {
    width: 50,
    height: 50,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
    marginVertical: widthToDp(3),
    marginBottom: 10,
  },
  bottomSubContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buyButton: {
    backgroundColor: '#AFFF00',
    width: 60,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },
  buyButtonText: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
  },
});
