import {ifNotValid} from '../../../../../../../utils/helper';
import axiosService from '../../../../../../../commons/axiosService';
import {successLog} from '../../../../../../../utils/fireLog';
import {ScreenNames} from '../../../../../../../utils/ScreenNames';
import {setLoading} from '../../../../../../../redux/actions/loader';
import {membershipRenewurl} from '../../../../../../../root/navigation/deeplinking/action';

export const bookMembership = (requestBody, navigation) => {
  if (ifNotValid(requestBody)) {
    console.warn(' requestBody valid ');
    return;
  }

  return (dispatch) => {
    axiosService
      .post(`api/memberships`, requestBody)
      .then(async (response) => {
        if (response.status === 200) {
          await membershipRenewurl(response.data.data[0]);
          successLog(response.data, response.status);
          navigation.navigate(ScreenNames.MEMBERSHIP_ACKNOWLEDGEMENT);
          console.log('Your membership payment has been done successfully');
        } else {
          console.log('not valid res ' + JSON.stringify(response));
        }
        dispatch(setLoading(false));
      })
      .catch((o) => {
        console.log('bookEventApi error ' + JSON.stringify(o));
        dispatch(setLoading(false));
      });
  };
};
