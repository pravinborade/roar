import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {commonStyle} from '../../../../../../styles';
import {widthToDp} from '../../../../../../utils/Responsive';

const MembershipDiscount = ({title = '', value = '', type}) => {
  return (
    <View style={styles.rowStyle}>
      <Text style={commonStyle.narrow28}>{title}: </Text>
      <Text style={commonStyle.regular24}>
        `${value}
        {type ? '%' : ' Rs'}`
      </Text>
    </View>
  );
};
export default MembershipDiscount;

const styles = StyleSheet.create({
  rowStyle: {
    flexDirection: 'row',
    marginVertical: widthToDp(0.5),
  },
});
