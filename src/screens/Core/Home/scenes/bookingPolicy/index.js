import React, {useState} from 'react';
import {View, ActivityIndicator, Dimensions} from 'react-native';
import {WebView} from 'react-native-webview';
import {Container, HeaderBack} from '../../../../../components';
import {ScreenNames} from '../../../../../utils/ScreenNames';
import {If} from '../../../../../utils/If';

const BookingPolicy = ({navigation}) => {
  const [loading, setLoading] = useState(false);
  const width = Dimensions.get('window').width;
  const height = Dimensions.get('window').height;
  return (
    <Container>
      <HeaderBack title={ScreenNames.BOOKING_POLICY} navigation={navigation} />
      <WebView
        source={{uri: 'https://www.roarsports.in/privacy-policy/'}}
        style={{height: height, width: width}}
        onLoadStart={() => {
          setLoading(true);
        }}
        onLoadEnd={() => {
          setLoading(false);
        }}
      />
      <If show={loading}>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <ActivityIndicator color="#009688" size="large" />
        </View>
      </If>
    </Container>
  );
};

export default BookingPolicy;
