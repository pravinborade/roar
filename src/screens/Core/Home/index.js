import React, {memo, useEffect, useState} from 'react';
import {RefreshControl, ScrollView, StyleSheet, Text, View} from 'react-native';
import {colors} from '../../../styles';
import HomeHeader from './scenes/comp/HomeHeader';
import {Content} from '../../../utils/content/Content';
import Carousal from './scenes/comp/Carousal';
import Venues from './scenes/venues/';
import Events from './scenes/events/';
import {heightToDp, width} from '../../../utils/Responsive';
import {Container, InputSearch} from '../../../components';
import Sports from './scenes/sports';
import {useDispatch, useSelector} from 'react-redux';
import {getVenuesList, setFilterData} from './scenes/sports/sport_all/action';
import {ifNotValid, ifValid, ifValidNLenZ} from '../../../utils/helper';
import {getEventList, updateTokenToDB} from './comp/action';
import ProfileSkelton from '../../../components/SkeletonViewHome';
import {
  getSportListFilterApi,
  setSelectedFilterSport,
} from './scenes/venues/venue_all/filter_screen/action';
import axiosService from '../../../commons/axiosService';
import {successLog} from '../../../utils/fireLog';

const Home = (props) => {
  const dispatch = useDispatch();
  let searchValue = useSelector((state) => state.inputs.searchValue);
  const latLong = useSelector((state) => state.login.latLong);
  const homeVenue = useSelector((state) => state.homeVenue);
  let venuesOriginal =
    ifValid(homeVenue.filterOriginal) && homeVenue.filterOriginal.length > 0
      ? homeVenue.filterOriginal
      : [];
  let venuesFiltered =
    ifValid(homeVenue.filterData) && homeVenue.filterData.length > 0
      ? homeVenue.filterData
      : [];
  let {events, eventsTemp} = useSelector((state) => state.homeEvents);
  const isLoader = useSelector((state) => state.loader.loader);
  const [refreshing, setRefreshing] = useState(false);
  let {sportsFilterList, sportsFilterListCopy} = useSelector(
    (state) => state.homeVenue,
  );
  const [imagesData, setImagesData] = useState([]);
  let userDetails = useSelector((x) => x.login.data);

  useEffect(() => {
    dispatch(getVenuesList(latLong, false));
    dispatch(getEventList(latLong));
    dispatch(getSportListFilterApi());
    getCarousalImages();
    getTokenAndUpdate();
  }, []);

  const getTokenAndUpdate = () => {
    setTimeout(() => updateTokenToDB(userDetails, dispatch), 2000);
  };

  const getCarousalImages = () => {
    let result = axiosService
      .get('api/amazon/splashImages')
      .then(async (response) => {
        successLog('api/amazon/splashImages', response?.status);
        if (response.status === 200) {
          setImagesData(
            ifNotValid(response?.data?.data) ? [] : response?.data?.data,
          );
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    _onSearchVenues();
    _onSearchSports();
    _onSearchEvents();
  }, [searchValue]);

  const _onSearchVenues = () => {
    try {
      if (searchValue !== '') {
        dispatch(setFilterData([]));
        let venues = venuesOriginal?.filter((x) => {
          let title = x?.name?.toLowerCase();
          let area = x?.area?.toLowerCase();
          let sport = x?.sport
            ?.map((x) => x.name)
            ?.toString()
            ?.toLowerCase();
          title = title + ' ' + area + ' ' + sport ?? '';
          if (title.includes(searchValue.toLowerCase())) {
            return x;
          }
        });
        dispatch(setFilterData(venues));
      } else {
        console.log('in else');
        dispatch(setFilterData(venuesOriginal));
      }
    } catch (e) {
      console.log(e);
    }
  };

  const _onSearchEvents = () => {
    try {
      if (searchValue !== '') {
        let _events = eventsTemp?.filter((x) => {
          let title = ifValid(x?.title) ? x.title.toLowerCase() : '';
          if (title.includes(searchValue.toLowerCase())) {
            return x;
          }
        });
        dispatch({type: 'SET_EVENTS', payload: _events});
      } else {
        dispatch({type: 'SET_EVENTS', payload: eventsTemp});
      }
    } catch (e) {
      console.log(e);
    }
  };

  const _onSearchSports = () => {
    try {
      if (searchValue !== '') {
        let result = sportsFilterListCopy?.filter((x) => {
          let sportName = x.name.toLowerCase();
          if (sportName.includes(searchValue.toLowerCase())) {
            return x;
          }
        });
        dispatch(setSelectedFilterSport(result));
      } else {
        dispatch(setSelectedFilterSport(sportsFilterListCopy));
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Container>
      <View
        style={{backgroundColor: colors.white, paddingVertical: heightToDp(2)}}>
        <HomeHeader navigation={props.navigation} />
        <InputSearch
          containerStyle={{marginVertical: heightToDp('1')}}
          placeholder={Content.inputPlaceholder}
          onChangeText={(v) => {}}
        />
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => {
              setRefreshing(true);
              dispatch(getVenuesList(latLong, false));
              dispatch(getEventList(latLong));
              dispatch(getSportListFilterApi());
              setRefreshing(false);
            }}
          />
        }
        style={{flex: 1, backgroundColor: colors.background}}>
        {searchValue !== '' &&
        ifValidNLenZ(venuesFiltered) &&
        ifValidNLenZ(events) &&
        ifValid(searchValue) &&
        ifValidNLenZ(sportsFilterList) ? (
          <View style={{flex: 1}}>
            <View style={styles.alignCenter}>
              <Text>{Content.noResultFound}</Text>
            </View>
          </View>
        ) : (
          <View style={{flex: 1}}>
            {isLoader === true ? (
              <ProfileSkelton />
            ) : (
              <View>
                {searchValue === '' && <Carousal imagesData={imagesData} />}
                {ifValidNLenZ(venuesFiltered) ? null : <Venues props={props} />}
                {ifValidNLenZ(events) ? null : <Events props={props} />}
                {ifValidNLenZ(sportsFilterList) ? null : (
                  <Sports props={props} />
                )}
              </View>
            )}
          </View>
        )}
      </ScrollView>
    </Container>
  );
};

export default memo(Home);

const styles = StyleSheet.create({
  alignCenter: {
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
    height: heightToDp(10),
  },
});
