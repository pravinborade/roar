import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {commonStyle} from '../../../../styles';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../styles/typography';
import Images from '../../../../assets/Images/Images';
import {fontToDp, heightToDp, widthToDp} from '../../../../utils/Responsive';
import moment from 'moment';
import {ifValid} from '../../../../utils/helper';
import {ButtonCommon} from '../../../../components';
import {Content} from '../../../../utils/content/Content';
import {useDispatch, useSelector} from 'react-redux';
import {setMembershipSelected} from '../../Home/scenes/venues/venue_detail/action';
import {ScreenNames} from '../../../../utils/ScreenNames';
import {capitalize} from 'lodash';
import axiosService from '../../../../commons/axiosService';

const MymembershipPoster = ({navigation, data, token}) => {
  const dispatch = useDispatch();
  let flatRate = ifValid(data?.plan?.flatRate) ? data.plan.flatRate : '';
  let sportName = ifValid(data?.plan?.sport?.name) ? data.plan.sport.name : '';
  let discountPercentage = ifValid(data.plan.discountPercentage)
    ? data.plan.discountPercentage
    : '';
  let packageFlatRate = ifValid(data.plan.packageFlatRate)
    ? data.plan.packageFlatRate
    : '';
  let packageDiscount = ifValid(data.plan.packageDiscount)
    ? data.plan.packageDiscount
    : '';
  const userDetails = useSelector((state) => state.login.data);
  /**
   * checking if membership is already booked
   * @param item
   * @private
   */
  const _onPressBuy = (item) => {
    item.membershipId = data.id;
    axiosService
      .get(`api/memberships/user/${userDetails?.id}/plan/${item?.id}`)
      .then((res) => {
        console.log('@@post@@', JSON.stringify(res));
        if (res?.status === 200) {
          dispatch(setMembershipSelected(item));
          navigation.navigate(ScreenNames.MEMBERSHIP_DETAIL);
        }
      })
      .catch((o) => console.log('membership error ' + o));
  };

  return (
    <ScrollView style={{flex: 1}}>
      <View style={styles.mainContainer}>
        <View style={styles.mainSubContainer}>
          <View>
            <Image style={styles.mainImage} source={Images.orangeCircle} />
          </View>
          <View>
            <Text style={styles.mainText}>{data.plan.name}</Text>
          </View>
        </View>

        <View style={styles.middleContainer}>
          <View style={styles.middleSubContainer}>
            <View>
              <Text style={styles.middleText_1}>
                ({data.plan.period} {data.plan.periodUnit})
                {data.isExpired === 'NO' ? null : 'Expired'}
              </Text>
            </View>
            <View>
              <Text style={styles.middleText_2}>Rs. {data.plan.amount}</Text>
            </View>
          </View>
          <View>
            <Text style={styles.middleText_3}>{data?.plan?.description}</Text>
          </View>
          {sportName !== '' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={commonStyle.narrow28}>Sport: </Text>
              <Text style={commonStyle.regular24}>{capitalize(sportName)}</Text>
            </View>
          ) : null}

          {flatRate !== '' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={commonStyle.narrow28}>Slots Flat Rate: </Text>
              <Text style={commonStyle.regular24}>{flatRate} Rs</Text>
            </View>
          ) : null}

          {discountPercentage !== '' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={commonStyle.narrow28}>Slots Discount: </Text>
              <Text style={commonStyle.regular24}>{discountPercentage}%</Text>
            </View>
          ) : null}

          {packageFlatRate !== '' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={commonStyle.narrow28}>Membership Flat Rate: </Text>
              <Text style={commonStyle.regular24}>{packageFlatRate} Rs</Text>
            </View>
          ) : null}

          {packageDiscount !== '' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={commonStyle.narrow28}>Membership Discount: </Text>
              <Text style={commonStyle.regular24}>{packageDiscount}%</Text>
            </View>
          ) : null}

          {data.plan.priceToPay !== '' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={[commonStyle.narrow28]}>
                {data.isExpired === 'YES' ? 'Price To Pay' : 'Paid Amount'}:{' '}
              </Text>
              <Text style={commonStyle.regular24}>{data.plan.priceToPay}</Text>
            </View>
          ) : null}
        </View>

        <View style={styles.bottomContainer}>
          <View>
            <Text style={styles.bottomText_1}>{data.plan.site.name}</Text>
          </View>
          <View>
            <Text style={styles.bottomText_2}>
              {data.plan.site.area}, {data.plan.site.city}
            </Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.startDate_Text_1}>{Content.startDate}</Text>
              <View style={styles.startDate_View}>
                <Image
                  style={styles.startDate_Text_2_Image}
                  source={Images.calenderButton}
                />
                <Text style={styles.startDate_Text_2}>
                  {moment(data.startDate).format('DD MMM, YYYY')}
                </Text>
              </View>
            </View>

            <View style={{marginLeft: 10, flex: 1}}>
              <Text style={styles.startDate_Text_1}>{Content.endDate}</Text>
              <View style={styles.startDate_View}>
                <Image
                  style={styles.startDate_Text_2_Image}
                  source={Images.calenderButton}
                />
                <Text style={styles.startDate_Text_2}>
                  {moment(data.startDate)
                    .add(parseInt(data?.plan?.period), data?.plan?.periodUnit)
                    .format('DD MMM, YYYY')}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>

      {data.isExpired == 'YES' && (
        <ButtonCommon
          onPress={() => _onPressBuy(data.plan)}
          text={'RENEW'}
          style={{width: '90%', alignSelf: 'center'}}
          textStyle={{fontSize: fontToDp(16)}}
        />
      )}
    </ScrollView>
  );
};

export default MymembershipPoster;

const styles = StyleSheet.create({
  mainContainer: {
    marginTop: heightToDp(2),
    backgroundColor: '#FEE2E2',
    height: heightToDp(60),
    width: '90%',
    borderRadius: 2,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 1,
    alignSelf: 'center',
  },
  mainSubContainer: {
    backgroundColor: '#FFD8D8',
    height: '33.3%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainImage: {
    height: heightToDp(10),
    width: widthToDp(15),
    resizeMode: 'contain',
  },
  mainText: {
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
    marginTop: 5,
    fontSize: fontToDp(18),
  },
  middleContainer: {
    borderBottomColor: '#FFD8D8',
    borderBottomWidth: 2,
    width: '100%',
    height: '33.3%',
    padding: 15,
    justifyContent: 'space-evenly',
  },
  middleSubContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  middleText_1: {
    color: '#505C74',
    fontSize: fontToDp(14),
  },
  middleText_2: {
    fontSize: fontToDp(14),
    color: '#202B46',
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  middleText_3: {
    color: '#202B46',
    fontSize: fontToDp(13),
    fontFamily: PT_SANS_REGULAR,
  },
  bottomContainer: {
    padding: 15,
    justifyContent: 'space-evenly',
    height: 120,
  },
  bottomText_1: {
    color: '#202B46',
    fontSize: fontToDp(14),
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  bottomText_2: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontToDp(10),
  },
  startDate_Text_1: {
    color: '#202B46',
    fontSize: fontToDp(12),
    fontFamily: PT_SANS_REGULAR,
    marginBottom: 5,
  },
  startDate_View: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: 100,
    height: 'auto',
    padding: 3,
  },
  startDate_Text_2_Image: {
    resizeMode: 'contain',
    height: 10,
    width: 10,
    alignSelf: 'center',
    margin: 2,
  },
  startDate_Text_2: {
    fontFamily: PT_SANS_REGULAR,
    color: '#202B46',
    fontSize: fontToDp(10),
  },
});
