import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  RefreshControl,
  BackHandler,
  Alert,
} from 'react-native';
import {HeaderBack} from '../../../components';
import Container from '../../../components/Container';
import {ScreenNames} from '../../../utils/ScreenNames';
import MymembershipPoster from './comp/MymembershipPoster';
import AsyncStorage from '@react-native-async-storage/async-storage';
import httpService from '../../../commons/httpServices';
import {backHandler} from '../../../commons/settingsBackHandler';
import {setLoader} from '../../../redux/actions/loader';
import ProfileSkelton from '../../../components/SkeletonViewMembership';
import {store} from '../../../redux/store';
import helperFunctions from '../../../commons/helperFunctions';

// const MyMembership = () => {

//     const [userDetails, setuserDetails] = useState(null);
//     const [memberships, setmemberships] = useState([]);
//     const [refreshing, setrefreshing] = useState(false);

//     return (
//         <View>
//             <Text></Text>
//         </View>
//     )
// }

// export default MyMembership

class MyMembership extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: null,
      memberships: [],
      refreshing: false,
    };
  }

  async componentDidMount() {
    await AsyncStorage.getItem('userDetails').then((details) => {
      if (details) {
        details = JSON.parse(details);
        this.setState(
          {
            userDetails: details,
          },
          () => {
            this.props.setLoader(true);
            this._fetchMembership();
          },
        );
      }
    });
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.backActionHandlerAlert,
    );
  }

  _fetchMembership() {
    this.setState({refreshing: true});
    httpService
      .get(
        `api/memberships/${this.state.userDetails.id}?access_token=${this.props.token}`,
        this.props,
      )
      .then((response) => {
        console.log('###_fetchMembership', response);
        if (response && response.status === 200) {
          let memList = response.data[0];
          const tempMemList = memList.map((membership, index) => {
            membership.plan.priceToPay = helperFunctions.calculatePriceToPay(
              membership.plan,
            );
            return membership;
          });
          this.setState({memberships: tempMemList});
          this.setState({refreshing: false});
          this.props.setLoader(false);
        }
      });
  }

  onRefresh = () => this._fetchMembership();

  renderMyMembership = () => {
    if (this.state.memberships && this.state.memberships.length > 0) {
      return this.state.memberships.map((item, index) => {
        return (
          <MymembershipPoster
            navigation={this.props.navigation}
            data={item}
            token={this.props.token}
            key={index}
          />
        );
      });
    } else {
      return null;
    }
  };

  backActionHandlerAlert = () => {
    this.props.setting
      ? backHandler(this.props.dispatch, this.props.navigation)
      : Alert.alert('Hold on!', 'Are you sure you want to go back?', [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          {text: 'YES', onPress: () => BackHandler.exitApp()},
        ]);

    return true;
  };

  // componentWillUnmount() {
  //   this.backHandler.remove();
  // }

  render() {
    return (
      <Container>
        {console.log('Hello --', this.props.setting, this.props.token)}
        <HeaderBack
          mainHeader={this.props.setting ? false : true}
          title={ScreenNames.MYMEMBERSHIP}
          onPress={() => backHandler(store.dispatch, this.props.navigation)}
        />

        {!this.props.isLoader && this.state.memberships.length <= 0 && (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
            }}
          >
            <Text>No Membership</Text>
          </View>
        )}
        <ScrollView
          style={{flex: 1, alignSelf: 'center'}}
          refreshControl={
            <RefreshControl
              refreshing={this.props.isLoader ? false : this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        >
          {this.props.isLoader ? (
            <ProfileSkelton />
          ) : (
            <View style={{alignSelf: 'center'}}>
              {this.renderMyMembership()}
            </View>
          )}
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.login.userToken,
    setting: state.screenState.settingsState,
    isLoader: state.loader.loader,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLoader: (loader) => {
      dispatch(setLoader(loader));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyMembership);
