import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Images from '../../../../assets/Images/Images';
import {colors, commonStyle} from '../../../../styles';
import moment from 'moment';
import {ifValid} from '../../../../utils/helper';
import {Content} from '../../../../utils/content/Content';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../styles/typography';
import validate from 'validate.js';
import {widthToDp} from '../../../../utils/Responsive';

class myEventsCard extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    var data = this.props.data && this.props.data;
    // var system_date = moment(this.props.data.systemDateTime);
    // var booking_end_date = moment(this.props.data.endDate + ' ' + this.props.data.endTime)
    // var booking_start_date = moment(this.props.data.startDate + ' ' + this.props.data.startTime)
    // var isPastDate = (system_date >= booking_end_date) ? 'past' : ((system_date <= booking_start_date) ? 'coming' : 'ongoing');
    // var message = (isPastDate == 'past') ? 'Completed' : (isPastDate == 'coming') ? 'Upcoming' : 'Ongoing';
    let checkUrl = validate({website: data.image}, {website: {url: true}});
    let image =
      ifValid(data?.image) &&
      data.image !== '' &&
      checkUrl?.website === undefined
        ? {uri: data.image}
        : Images.sampleEvents;
    let date = ifValid(data?.startDate)
      ? `${moment(data?.startDate).format('MMM DD')} | ${moment(
          data?.startDate,
        ).format('ddd')} ${moment(data?.startTime, 'hh:mm a').format(
          'hh:mm a',
        )}`
      : '';
    return (
      <View style={styles.container}>
        <View>
          <Image style={styles.image} source={image} />
        </View>
        <View style={styles.details}>
          <Text style={styles.title}>{data?.title}</Text>
          <Text style={styles.city}>
            {data?.site?.area}, {data.site.city}
          </Text>
          <Text style={styles.id}>
            {Content.bookingNo} {data?.id}
          </Text>
          {data.quantity && (
            <View>
              <Text style={styles.id}>{'Tickets Qty:' + data.quantity}</Text>
              <Text style={[styles.id, {paddingBottom: 3}]}>
                {'Amount Paid : ' + data.amount * data.quantity}
              </Text>
            </View>
          )}

          <View style={styles.dateContainer}>
            <Text style={styles.dateStyle}>
              <Image source={Images.calendar} style={commonStyle.dateIcon} />{' '}
              {date?.toUpperCase()}
            </Text>
          </View>
        </View>
        <View style={styles.amountContainer}>
          <Text style={styles.amount}>
            {Content.rupies} {data?.amount}
          </Text>
        </View>
      </View>
    );
  }
}

export default myEventsCard;

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 5,
    borderBottomColor: colors.background,
    flexDirection: 'row',
    padding: 15,
    backgroundColor: colors.white,
    marginBottom: 10,
  },
  image: {height: 90, width: 100, borderRadius: 5},
  details: {justifyContent: 'space-between', marginHorizontal: 15},
  title: {fontFamily: PT_SANS_NARROW_BOLD, color: '#202B46', fontSize: 14},
  city: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: 12,
  },
  id: {
    fontFamily: PT_SANS_REGULAR,
    color: '#202B46',
    fontSize: 12,
  },
  dateContainer: {
    height: 20,
    paddingHorizontal: widthToDp(1.2),
    backgroundColor: '#E5ECF9',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateStyle: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: 10,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  amount: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    textAlign: 'center',
  },
  amountContainer: {alignSelf: 'flex-end', flex: 1},
});
