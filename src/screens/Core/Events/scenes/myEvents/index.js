import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {Component} from 'react';
import {FlatList, RefreshControl, Text, View} from 'react-native';
import {connect} from 'react-redux';
import MyEventsCard from '../../comp/myEventsCard';
import ProfileSkelton from '../../../../../components/SkeletonView';
import axiosService from '../../../../../commons/axiosService';
import {ifNotValid} from '../../../../../utils/helper';

class MyEvents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orginalEvents: [],
      events: [],
      pageNo: 0,
      pageSize: 20,
      loadingMoreData: false,
      userDetails: null,
      noData: false,
      loader: false,
      refreshing: false,
    };
  }

  async componentDidMount() {
    await AsyncStorage.getItem('userDetails').then((details) => {
      if (details) {
        details = JSON.parse(details);
        this.setState({userDetails: details}, () => this._fetchEvents(details));
      }
    });
  }

  _fetchEvents() {
    this.setState({loader: true});
    this.setState({refreshing: true});
    //let url = `api/events/user/${this.state.userDetails.id}?pageNo=${this.state.pageNo}&pageSize=${this.state.pageSize}&access_token=${this.props.token}`;
    let url = `api/events/status/${this.props.enum}?access_token=${this.props.token}&userId=${this.state.userDetails.id}`;
    console.log('$$$url', url);
    axiosService.get(url, this.props).then((response) => {
      console.log('$$$$responseData EVENT ', response.status);
      if (response && response.status === 200 && response.data) {
        response = response.data;
        if (ifNotValid(response)) {
          console.log('invalid response');
          return;
        }
        this.setState({loader: false});
        if (response.data.length) {
          this.setState({events: response.data});
        }
        this.setState({refreshing: false});
        console.log(this.state.events);
        //this.setState({ noData: false, orginalEvents: [...this.state.orginalEvents, ...response.data[0]] }, () => this._pagingation());
      } else {
        this.setState({loader: false});
        //this.setState({ noData: true })
      }
    });
  }

  onRefresh = () => this._fetchEvents();

  //used customize pagination to reduce the load from the app
  // _pagingation() {
  //     let remainRecords = this.state.orginalEvents.length // calculating remaining count of the records
  //     if (remainRecords > 0) { //if there are remaing records of this.state.orginalEvents then using intenal pagination
  //         let spliceNoOfRecord = remainRecords >= 5 ? 5 : remainRecords; //showing only 5 records at a time
  //         let events = this.state.orginalEvents.splice(0, spliceNoOfRecord); // coping records from orginalEvents
  //         this.setState({
  //             events: [...this.state.events, ...events],
  //             loadingMoreData: false
  //         })
  //     } else { //fetching the next page records from the API
  //         this.setState({ page: this.state.page + 1 }, () => this._fetchEvents())
  //     }
  // }

  //When reach to screen bottom on scroll
  // _onEndReached = () => {
  //     if (!this.state.loadingMoreData && this.state.noData) {
  //         this.setState({ loadingMoreData: true }, () => this._pagingation())
  //     }
  // };

  //Rendering booking using FlatList
  _renderEvents() {
    return (
      <FlatList
        data={this.state.events}
        renderItem={this._renderEventCards}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
      />
    );
  }

  _renderEventCards = ({item, index}) => {
    item.event.quantity = item.quantity;
    return <MyEventsCard data={item.event} token={this.props.token} />;
  };

  render() {
    return (
      <View style={{flex: 1}}>
        {this.state.loader === true ? (
          <ProfileSkelton />
        ) : this.state.events.length <= 0 ? (
          <View
            style={{
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text> No Events </Text>
          </View>
        ) : this.state.events.length > 0 ? (
          this._renderEvents()
        ) : this.state.noData ? null : null}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.login.userToken,
  };
};

export default connect(mapStateToProps)(MyEvents);
