import React, {useState} from 'react';
import {Alert, BackHandler, StyleSheet, Text, View} from 'react-native';
import Container from '../../../components/Container';
import HeaderBack from '../../../components/HeaderBack';
import {colors} from '../../../styles';
import {Content, fontToDp, ScreenNames, widthToDp} from '../../../utils';
import MyEvents from './scenes/myEvents';
import {useDispatch, useSelector} from 'react-redux';
import {backHandler} from '../../../commons/settingsBackHandler';
import {useBackHandler} from '@react-native-community/hooks';

const Events = ({navigation}) => {
  const [index, setIndex] = useState('1');
  const onChangeIndex = (text) => {
    setIndex(text);
  };

  const dispatch = useDispatch();
  const settingsState = useSelector((state) => state.screenState.settingsState);

  const backActionHandlerAlert = () => {
    Alert.alert(Content.alert, Content.areYouSure, [
      {
        text: Content.cancelBtn,
        onPress: () => null,
        style: 'cancel',
      },
      {text: Content.yesBtn, onPress: () => BackHandler?.exitApp()},
    ]);
    return true;
  };

  const backActionHandler = () => {
    settingsState
      ? backHandler(dispatch, navigation)
      : backActionHandlerAlert();
    return true;
  };
  useBackHandler(backActionHandler);

  return (
    <Container>
      <View>
        <HeaderBack
          mainHeader={settingsState ? false : true}
          title={ScreenNames.MY_EVENTS}
          onPress={() => backHandler(dispatch, navigation)}
        />
        <View
          style={{
            backgroundColor: 'white',
            borderBottomWidth: 5,
            borderBottomColor: colors.background,
          }}>
          <View style={styles.mainContainer}>
            <Text
              onPress={() => {
                onChangeIndex('1');
              }}
              style={
                index === '1' ? styles.selectedIndex : styles.unselectedIndex
              }>
              {Content.upCome}
            </Text>

            <Text
              onPress={() => {
                onChangeIndex('2');
              }}
              style={
                index == '2' ? styles.selectedIndex : styles.unselectedIndex
              }>
              {Content.past}
            </Text>
          </View>
        </View>
      </View>

      {index === '1' && <MyEvents enum={Content.upcoming} />}
      {index === '2' && <MyEvents enum={Content.past} />}
    </Container>
  );
};

export default Events;

const styles = StyleSheet.create({
  selectedIndex: {
    color: colors.darkGreen,
    fontSize: fontToDp(11),
    fontWeight: 'bold',
    marginRight: widthToDp(5),
    borderBottomWidth: 2,
    borderBottomColor: colors.darkGreen,
    lineHeight: 25,
  },
  unselectedIndex: {
    marginRight: widthToDp(5),
    color: 'black',
    fontWeight: 'bold',
    fontSize: fontToDp(11),
  },
  mainContainer: {
    //height: Platform.OS === 'android' ? '6%' : '5%',
    paddingHorizontal: 20,
    paddingTop: 10,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
});
