import React, {useState} from 'react';
import {Alert, BackHandler, StyleSheet, Text, View} from 'react-native';
import {HeaderBack} from '../../../components';
import Container from '../../../components/Container';
import {colors} from '../../../styles';
import {Content, fontToDp, ScreenNames, widthToDp} from '../../../utils';
import MyBookings from './scenes/myBookings';
import {useDispatch, useSelector} from 'react-redux';
import {backHandler} from '../../../commons/settingsBackHandler';
import {useBackHandler} from '@react-native-community/hooks';

const ScrollableTabView = require('react-native-scrollable-tab-view');

const Bookings = ({navigation, props}) => {
  const [index, setIndex] = useState('1');
  const onChangeIndex = (text) => {
    setIndex(text);
  };

  const dispatch = useDispatch();
  const settingsState = useSelector((state) => state.screenState.settingsState);

  const backActionHandlerAlert = () => {
    Alert.alert(Content.alert, Content.areYouSure, [
      {
        text: Content.cancel,
        onPress: () => null,
        style: 'cancel',
      },
      {text: Content.yesBtn, onPress: () => BackHandler.exitApp()},
    ]);
    return true;
  };

  const backActionHandler = () => {
    settingsState
      ? backHandler(dispatch, navigation)
      : backActionHandlerAlert();
    return true;
  };
  useBackHandler(backActionHandler);

  return (
    <Container>
      <HeaderBack
        mainHeader={settingsState ? false : true}
        title={ScreenNames.MY_BOOKINGS}
        onPress={() => backHandler(dispatch, navigation)}
      />

      <View style={styles.containerStyle}>
        <View style={styles.mainContainer}>
          <Text
            onPress={() => onChangeIndex('1')}
            style={
              index === '1' ? styles.selectedIndex : styles.unselectedIndex
            }>
            {Content.upCome}
          </Text>
          <Text
            onPress={() => onChangeIndex('2')}
            style={
              index === '2' ? styles.selectedIndex : styles.unselectedIndex
            }>
            {Content.past}
          </Text>

          <Text
            onPress={() => onChangeIndex('3')}
            style={
              index === '3' ? styles.selectedIndex : styles.unselectedIndex
            }>
            {Content.cancel}
          </Text>
        </View>
      </View>

      {index === '1' && (
        <MyBookings
          title={'UNAVAILABLE'}
          navigation={navigation}
          props={props}
        />
      )}

      {index === '2' && (
        <MyBookings title={'COMPLETED'} navigation={navigation} props={props} />
      )}

      {index === '3' && (
        <MyBookings title={'CANCELLED'} navigation={navigation} props={props} />
      )}
    </Container>
  );
};

export default Bookings;

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: 'white',
    borderBottomWidth: 5,
    borderBottomColor: colors.background,
  },
  selectedIndex: {
    color: colors.darkGreen,
    fontSize: fontToDp(11),
    fontWeight: 'bold',
    marginRight: widthToDp(5),
    borderBottomWidth: 2,
    borderBottomColor: colors.darkGreen,
    lineHeight: 25,
  },
  unselectedIndex: {
    marginRight: widthToDp(5),
    color: 'black',
    fontWeight: 'bold',
    fontSize: fontToDp(11),
  },
  mainContainer: {
    paddingHorizontal: 20,
    paddingTop: 10,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
});
