import {StyleSheet} from 'react-native';
import {
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../styles/typography';
import {fontToDp, widthToDp} from '../../../../utils/Responsive';
import {colors} from '../../../../styles';

export const bookingStyle = StyleSheet.create({
  carContainer: {
    flexDirection: 'row',
    padding: 15,
    backgroundColor: colors.white,
    marginBottom: 10,
    borderBottomWidth: 5,
    borderBottomColor: colors.background,
  },
  cardTextContainer: {
    justifyContent: 'space-between',
    marginHorizontal: 15,
  },
  titleStyle: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    fontSize: fontToDp(14),
  },
  subTitleStyle: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontToDp(12),
  },
  dateContainer: {
    height: 20,
    paddingHorizontal: widthToDp(1.2),
    backgroundColor: '#E5ECF9',
    justifyContent: 'center',
    alignItems: 'center',
  },
  date: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontToDp(10),
  },
  bookingImg: {
    height: 90,
    width: 100,
    borderRadius: 5,
  },
  finalRateStyle: {
    fontFamily: PT_SANS_NARROW_BOLD,
    color: '#202B46',
    textAlign: 'center',
  },
  row: {flexDirection: 'row'},
  marginLeft: {marginLeft: '20%'},
  alignSelfEng: {alignSelf: 'flex-end', flex: 1},
});
