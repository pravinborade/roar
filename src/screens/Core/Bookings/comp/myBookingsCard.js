import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Images from '../../../../assets/Images/Images';
import moment from 'moment';
import {ifValid} from '../../../../utils/helper';
import {Content} from '../../../../utils/content/Content';
import {ScreenNames} from '../../../../utils/ScreenNames';
import {multipleMyBookings} from '../../../../redux/actions/myBookings';
import validate from 'validate.js';
import {bookingStyle} from './style';
import {commonStyle} from '../../../../styles';

const myBookingsCard = ({data, navigation, dispatch}) => {
  let checkUrl = validate({website: data?.site?.image}, {website: {url: true}});
  let image =
    ifValid(data?.site?.image) &&
    data?.site?.image !== '' &&
    checkUrl?.website === undefined
      ? {uri: data?.site?.image}
      : Images.sampleBooking;
  let date = ifValid(data?.startDate)
    ? `${moment(data?.startDate).format('MMM DD')} | ${moment(
        data?.startDate,
      ).format('ddd')} ${moment(data.startTime, 'hh:mm a').format('hh:mm a')}`
    : '';

  const isValidURL = (string = '') => {
    var res = string?.match(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g,
    );
    return res !== null;
  };

  console.log('booking data---');
  console.log(data);

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          if (ifValid(dispatch)) {
            dispatch({type: 'SET_MY_SELECTED_BOOKING', payload: data});
            dispatch(multipleMyBookings(data.offerNo, data.startDate));
            navigation.navigate(ScreenNames.DETAILSMYBOOKINGS);
          }
        }}>
        <View style={bookingStyle.carContainer} key={data?.offerNo}>
          <View>
            <Image style={bookingStyle.bookingImg} source={image} />
          </View>
          <View style={bookingStyle.cardTextContainer}>
            <Text style={bookingStyle.titleStyle}>{data?.sportPlayedFor}</Text>
            <Text style={bookingStyle.subTitleStyle}>{data?.site?.name}{"("+data?.resource?.name+")"}</Text>
            <Text style={bookingStyle.subTitleStyle}>
              {Content.bookingNo} {data?.offerNo?.toString()}
            </Text>

            <View style={bookingStyle.row}>
              <View style={bookingStyle.dateContainer}>
                <Text style={bookingStyle.date}>
                  <Image
                    source={Images.calendar}
                    style={commonStyle.dateIcon}
                  />{' '}
                  {date?.toUpperCase()}
                </Text>
              </View>
              <View style={bookingStyle.marginLeft}>
                <Text style={bookingStyle.finalRateStyle}>
                  {Content.rupies}
                  {data?.finalRate?.toString()}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>

      {data?.purchasedInventories !== undefined && (
        <FlatList
          scrollEnabled={true}
          data={data.purchasedInventories}
          horizontal={false}
          refreshing={true}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => {
                if (ifValid(dispatch)) {
                  let dataa = {
                    ...item,
                    offerNo: data?.offerNo,
                    dateinvent: date,
                  };
                  dispatch({type: 'SET_MY_SELECTED_BOOKING', payload: dataa});
                  navigation.navigate(ScreenNames.DETAILSMYBOOKINGS);
                }
              }}>
              <View style={bookingStyle.carContainer} key={item.id}>
                <View>
                  <Image
                    style={bookingStyle.bookingImg}
                    source={
                      isValidURL(item?.inventory?.image) == true
                        ? {uri: item.inventory.image}
                        : Images.product
                    }
                  />
                </View>
                <View style={bookingStyle.cardTextContainer}>
                  <Text style={bookingStyle.titleStyle}>
                    {item.inventory.productName}
                  </Text>
                  <Text style={bookingStyle.subTitleStyle}>
                    {data?.site?.name}
                  </Text>
                  <Text style={bookingStyle.subTitleStyle}>
                    {Content.bookingNo} {data?.offerNo?.toString()}
                  </Text>

                  <View style={bookingStyle.row}>
                    <View style={bookingStyle.dateContainer}>
                      <Text style={bookingStyle.date}>
                        <Image
                          source={Images.calendar}
                          style={commonStyle.dateIcon}
                        />{' '}
                        {date.toUpperCase()}
                      </Text>
                    </View>
                    <View style={bookingStyle.marginLeft}>
                      <Text style={bookingStyle.finalRateStyle}>
                        {Content.rupies}
                        {item.totalAmount?.toString()}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      )}
    </View>
  );
};

export default myBookingsCard;

const styles = StyleSheet.create({
  container: {},
  categoryNameStyle: {},
});
