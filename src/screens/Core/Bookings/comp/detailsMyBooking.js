import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';
import { Container, HeaderBack, Line } from '../../../../components';
import Images from '../../../../assets/Images/Images';
import { colors, commonStyle } from '../../../../styles';
import moment from 'moment';
import { ifValid } from '../../../../utils/helper';
import { helper } from '../../../../commons/helpers';
import { fontToDp, heightToDp, widthToDp } from '../../../../utils/Responsive';
import { Content } from '../../../../utils/content/Content';
import { PT_SANS_REGULAR } from '../../../../styles/typography';
import { venueStyle } from '../../Home/scenes/venues/venue_detail/comp/style';
import axiosService from '../../../../commons/axiosService';
import { multipleMyBookings } from '../../../../redux/actions/myBookings';
import { bookingStyle } from './style';
import validate from 'validate.js';
import { useSelector } from 'react-redux';

const detailsMyBookings = ({ bookingdata, navigation }) => {
  // const [bookingData, setBookingData] = useState(route.params.bookingdata);
  const bookingData = useSelector(
    (state) => state.myBookings.mySelectedBooking,
  );
  const _multipleMyBookings = useSelector(
    (state) => state.myBookings.multipleMyBookings,
  );
  const _multipleMyBookingsTotal = useSelector(
    (state) => state.myBookings.multipleMyBookingTotal,
  );
  const _multipleMyBookingsCount = useSelector(
    (state) => state.myBookings.multipleMyBookingCount,
  );

  const [totalFinal, setTotalFinal] = useState('');
  const [totalinvent, setTotalInvent] = useState(0);
  const [splitPaymentData, setSplitPaymentdata] = useState([]);
  let gst =
    ifValid(bookingData?.site?.organization) &&
      ifValid(bookingData?.site?.organization?.bookingTotalGst)
      ? bookingData?.site?.organization?.bookingTotalGst
      : undefined;

  let checkUrl = validate(
    { website: bookingData?.site?.image },
    { website: { url: true } },
  );
  let image =
    ifValid(bookingData?.site?.image) &&
      bookingData?.site?.image !== '' &&
      checkUrl?.website === undefined
      ? { uri: bookingData?.site?.image }
      : Images.sampleBooking;

  // let date = ifValid(bookingData?.startDate) ? `${moment(bookingData?.startDate).format('MMM DD')} | ${moment(bookingData?.startDate).format('ddd')} ${helper.formatTime(bookingData?.startTime)}` : '';

  let checkUrlInvent = validate(
    { website: bookingData?.inventory?.image },
    { website: { url: true } },
  );
  let imageInvent =
    ifValid(bookingData?.inventory?.image) &&
      bookingData?.inventory?.image !== '' &&
      checkUrlInvent?.website === undefined
      ? { uri: bookingData?.inventory?.image }
      : Images.product;

  console.log('@@##MULTIPLE BOKKING', _multipleMyBookings);
  useEffect(() => {
    inventorycal();
    addCustomer();
  }, []);

  const addCustomer = async () => {
    let response = await axiosService.get(
      `api/payments/booking/${bookingData.id}`,
    );
    console.log('@@$$SPLIT PAYMENT', response);
    try {
      if (response.status === 200) {
        let splitPay = response?.data?.data;
        setSplitPaymentdata(splitPay);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const inventorycal = () => {
    if (bookingData.purchasedInventories != undefined) {
      let invent = bookingData.purchasedInventories;
      let total = 0;
      for (var i = 0; i < invent.length; i++) {
        if (invent[i].totalAmount != undefined) {
          total = total + invent[i].totalAmount;
          setTotalInvent(total);
        }
      }
    }
  };

  return (
    <Container containerStyle={{ backgroundColor: colors.background }}>
      <HeaderBack title={Content.details_booking} navigation={navigation} />
      <ScrollView>
        {bookingData?.inventory === undefined ? (
          <View>
            <FlatList
              scrollEnabled={true}
              data={_multipleMyBookings}
              horizontal={false}
              refreshing={true}
              renderItem={({ item, index }) => (
                <View style={bookingStyle.carContainer} key={item?.offerNo}>
                  <Image style={bookingStyle.bookingImg} source={image} />
                  <View style={[bookingStyle.cardTextContainer,{maxWidth:'45%'}]}>
                    <Text style={bookingStyle.titleStyle}>
                      {item?.sportPlayedFor}
                    </Text>
                    <Text style={bookingStyle.subTitleStyle}>
                      {item?.site?.name}{"(" + item?.resource?.name + ")"}
                    </Text>
                    <Text style={bookingStyle.subTitleStyle}>
                      {Content.bookingNo} {item?.offerNo?.toString()}
                    </Text>
                    <View style={[bookingStyle.dateContainer,{alignSelf:'baseline'}]}>
                      <Text style={bookingStyle.date}>
                        <Image
                          source={Images.calendar}
                          style={commonStyle.dateIcon}
                        />{' '}
                        {ifValid(item?.startDate)
                          ? `${moment(item?.startDate).format(
                            'MMM DD',
                          )} | ${moment(item?.startDate).format(
                            'ddd',
                          )} ${moment(item.startTime, 'hh:mm a').format(
                            'hh:mm a',
                          )}`.toUpperCase()
                          : ''.toUpperCase()}
                      </Text>
                    </View>
                  </View>
                  <View style={bookingStyle.alignSelfEng}>
                    <Text style={bookingStyle.finalRateStyle}>
                      {Content.rupies}
                      {item?.finalRate?.toString()}
                    </Text>
                  </View>
                </View>
              )}
            />

            <View style={venueStyle.detailsContainer}>
              <Text style={commonStyle.narrow32}>{Content.paymentSummary}</Text>
              <View style={styles.amountContainer}>
                <Text style={commonStyle.regular24}>{Content.gst}</Text>
                <Text
                  style={commonStyle.regular24}
                >{`Inclu. of ${gst}% GST`}</Text>
              </View>
              <View style={styles.amountContainer}>
                <Text style={commonStyle.regular24}>
                  {Content.convenienceFee}
                </Text>
                <Text style={commonStyle.regular24}>
                  {Content.rupies} {0}
                </Text>
              </View>

              {bookingData?.discountRate === undefined ? null : (
                <View style={styles.amountContainer}>
                  <Text style={commonStyle.regular24}>{Content.discount}</Text>
                  <Text style={commonStyle.regular24}>
                    Rs. {bookingData?.discountRate}
                  </Text>
                </View>
              )}

              {bookingData?.noOfPlayers !== undefined && (
                <View style={styles.amountContainer}>
                  <Text style={commonStyle.regular24}>
                    {Content.noofplayer}
                  </Text>
                  <Text style={commonStyle.regular24}>
                    {bookingData?.noOfPlayers?.toString()}
                  </Text>
                </View>
              )}

              <Line />

              <View style={styles.totalContainer}>
                <Text style={commonStyle.narrow28}>{Content.totalAmount}</Text>
                <Text style={commonStyle.narrow28}>
                  {Content.rupies}
                  {/* {_multipleMyBookingsTotal -
                    (bookingData?.discountRate == undefined
                      ? 0
                      : bookingData?.discountRate) +totalinvent} */}
                  {_multipleMyBookingsTotal -
                    (bookingData?.discountRate == undefined
                      ? 0
                      : bookingData?.discountRate)}
                </Text>
              </View>
            </View>

            {splitPaymentData && splitPaymentData.length > 0 && (
              <FlatList
                contentContainerStyle={styles.flatContainer}
                scrollEnabled={true}
                style={styles.flatBoxStyle}
                data={splitPaymentData}
                refreshing={true}
                renderItem={({ item, index }) => (
                  <View
                    style={{
                      padding: 10,
                      backgroundColor: colors.white,
                      marginBottom: 10,
                    }}
                  >
                    <Text style={styles.titleStyle}>
                      Name :{' '}
                      <Text style={styles.valueStyle}>{item?.userName}</Text>
                    </Text>
                    <Text style={styles.titleStyle}>
                      Email :{' '}
                      <Text style={styles.valueStyle}>{item?.email}</Text>
                    </Text>
                    <Text style={styles.titleStyle}>
                      Contact No :{' '}
                      <Text style={styles.valueStyle}>{item?.mobileNo}</Text>
                    </Text>
                    <Text style={styles.titleStyle}>
                      Amount :{' '}
                      <Text style={styles.valueStyle}>{item?.amount}</Text>
                    </Text>
                    <Text style={styles.titleStyle}>
                      Payment Status :{' '}
                      <Text style={styles.valueStyle}>{item?.status}</Text>
                    </Text>
                  </View>
                )}
              />
            )}
          </View>
        ) : (
          <View>
            <View style={bookingStyle.carContainer} key={bookingData?.offerNo}>
              <Image style={bookingStyle.bookingImg} source={imageInvent} />
              <View style={bookingStyle.cardTextContainer}>
                <Text style={bookingStyle.titleStyle}>
                  {bookingData?.inventory?.productName}
                </Text>
                <Text style={bookingStyle.subTitleStyle}>
                  {bookingData?.inventory?.site?.name}
                </Text>
                <Text style={bookingStyle.subTitleStyle}>
                  {Content.bookingNo} {bookingData?.offerNo}
                </Text>
                <View style={bookingStyle.dateContainer}>
                  <Text style={bookingStyle.date}>
                    <Image
                      source={Images.calendar}
                      style={commonStyle.dateIcon}
                    />{' '}
                    {bookingData?.dateinvent.toUpperCase()}
                  </Text>
                </View>
              </View>
              <View style={bookingStyle.alignSelfEng}>
                <Text style={bookingStyle.finalRateStyle}>
                  {Content.rupies}
                  {bookingData?.inventory?.mrpAmount}
                </Text>
              </View>
            </View>

            <View style={venueStyle.detailsContainer}>
              <Text style={commonStyle.narrow32}>{Content.paymentSummary}</Text>
              <View style={styles.amountContainer}>
                <Text style={commonStyle.regular24}>{Content.qty}</Text>
                <Text style={commonStyle.regular24}>
                  {bookingData?.quantity}
                </Text>
              </View>
              <Line />

              <View style={styles.totalContainer}>
                <Text style={commonStyle.narrow28}>{Content.totalAmount}</Text>
                <Text style={commonStyle.narrow28}>
                  {Content.rupies}
                  {bookingData?.totalAmount}
                </Text>
              </View>
            </View>
          </View>
        )}
      </ScrollView>
    </Container>
  );
};

export default detailsMyBookings;
const styles = StyleSheet.create({
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleStyle: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontSize: fontToDp(13),
    fontWeight: 'bold',
  },
  valueStyle: {
    fontFamily: PT_SANS_REGULAR,
    color: '#505C74',
    fontWeight: 'normal',
    fontSize: fontToDp(13),
  },
  flatContainer: { justifyContent: 'space-between', paddingBottom: 20 },
  flatBoxStyle: { padding: 10 },

  totalContainer: {
    flexDirection: 'row',
    paddingVertical: widthToDp(1),
    justifyContent: 'space-between',
  },

  containerStyle: {
    marginHorizontal: widthToDp('4%'),
    flexDirection: 'row',
    backgroundColor: colors.lightBlue,
    alignItems: 'center',
  },
  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp('5%'),
    height: widthToDp('5%'),
    marginLeft: widthToDp('2%'),
    marginRight: widthToDp('1%'),
  },
  input: {
    height: 35,
    width: 80,
    top: 4,
    borderRadius: 7,
    borderWidth: 0.5,
    fontSize: 12,
  },
  inputtotal: {
    height: heightToDp(6),
    width: widthToDp(50),
    borderRadius: 7,
    borderWidth: 0.5,
    marginRight: '5%',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  inputStyle: {
    flex: 1,
    ...commonStyle.regular24,
    height: widthToDp(10),
    padding: widthToDp('2'),
  },
  mainContainer: { backgroundColor: colors.white },
  crossStyle: {
    position: 'absolute',
  },
  crossImage: {
    width: widthToDp(3),
    height: widthToDp(3),
    resizeMode: 'contain',
  },
  errorText: {
    ...commonStyle.regular16,
    marginHorizontal: widthToDp(4.5),
    color: 'red',
  },
  boxStyleAmount: {},
  dateIcon: {
    width: widthToDp(3),
    height: widthToDp(3),
    marginRight: widthToDp(1),
    resizeMode: 'contain',
    padding: widthToDp(1),
  },
});
