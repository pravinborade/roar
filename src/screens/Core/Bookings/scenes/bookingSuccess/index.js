import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Container from '../../../../../components/Container';
import LinearGradient from 'react-native-linear-gradient';
import Images from '../../../../../assets/Images/Images';
import {heightToDp, widthToDp} from '../../../../../utils/Responsive';
import {colors} from '../../../../../styles';
import {Content} from '../../../../../utils/content/Content';
import {
  PT_SANS_BOLD,
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../../../../../styles/typography';

const BookingSuccess = ({navigation}) => {
  return (
    <Container>
      <LinearGradient
        colors={[colors.lightGreen, colors.primaryGreen]}
        style={{flex: 1}}>
        <View style={styles.TopContainer}>
          <Image style={styles.logo} source={Images.basketball} />

          <View style={styles.middleContainer}>
            <Text style={styles.heading}> {Content.bSuccessHeading} </Text>
            <Text style={styles.subHeading}>
              {' '}
              {Content.bSuccessSubHeading}{' '}
            </Text>
          </View>

          <View style={styles.bottomContainer}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={{...styles.containerStyle, ...styles.myBookingButton}}>
                <Text style={styles.myBookingText}>{Content.myBooking}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </LinearGradient>
    </Container>
  );
};

export default BookingSuccess;

const styles = StyleSheet.create({
  myBookingText: {
    color: colors.white,
    fontFamily: PT_SANS_NARROW_BOLD,
  },
  myBookingButton: {
    backgroundColor: colors.primaryBlue,
  },
  successStyle: {},
  containerStyle: {
    borderColor: colors.primaryBlue,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    height: heightToDp('6%'),
    width: widthToDp('45%'),
  },
  logo: {
    height: heightToDp('25%'),
    width: 'auto',
    overflow: 'hidden',
    resizeMode: 'contain',
    marginTop: '10%',
  },

  heading: {
    fontFamily: PT_SANS_BOLD,
    fontSize: widthToDp('7%'),
    lineHeight: 25,
    color: '#202B46',
    textAlign: 'center',
  },

  subHeading: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: widthToDp('4%'),
    color: '#505C74',
    textAlign: 'center',
  },
  terms: {
    fontFamily: PT_SANS_REGULAR,
    fontSize: widthToDp('2.8%'),
    color: '#505C74',
    textAlign: 'center',
    margin: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  TopContainer: {
    height: '100%',
    justifyContent: 'space-between',
  },
  middleContainer: {
    height: '18%',
    justifyContent: 'space-between',
  },
  bottomContainer: {
    height: heightToDp('20%'),
    justifyContent: 'space-evenly',
    marginBottom: '5%',
  },
});
