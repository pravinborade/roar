import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {colors} from '../../../../../styles';
import MyBookingsCard from '../../comp/myBookingsCard';
import ProfileSkelton from '../../../../../components/SkeletonView';
import {
  clearMyBookings,
  getmyBookings,
} from '../../../../../redux/actions/myBookings';
import {ifNotValid} from '../../../../../utils/helper';

const MyBookings = ({index, navigation, title, props}) => {
  const dispatch = useDispatch();
  const [isRefreshing, setIsRefreshing] = useState(false);
  const _myBookings = useSelector((state) => state.myBookings.myBookings);
  const hasMore = useSelector((state) => state.myBookings.myBookingsHasMore);
  const pageNo = useSelector((state) => state.myBookings.pageNo);
  const loadingData = useSelector(
    (state) => state.myBookings.loadingBookingsData,
  );
  const loaderSkeleton = useSelector(
    (state) => state.myBookings.loaderSkeleton,
  );
  const userData = useSelector((state) => state.login.data);
  const [myBookings, setmyBookings] = useState([]);

  useEffect(() => {
    if (!ifNotValid(_myBookings)) {  
      setmyBookings(_myBookings);
    }
  }, [_myBookings]);

  useEffect(() => {
    dispatch(clearMyBookings());
    dispatch(getmyBookings(userData.id, title, 0));
  }, []);

  const BookingsPageIncrement = () => {
    let profileId = userData?.id;
    dispatch(getmyBookings(profileId, title, pageNo + 1));
  };

  const handleLoadMore = () => {
    hasMore && BookingsPageIncrement();
  };

  const _onRefresh = () => {
    let profileId = userData?.id;
    dispatch(getmyBookings(profileId, title, 0));
    setIsRefreshing(false);
  };

  //Rendering booking cards with booking name
  const _renderBookingCards = ({item, index}) => {
    return (
      <MyBookingsCard data={item} navigation={navigation} dispatch={dispatch} />
    );
  };

  const renderFooter = () => {
    return (
      <View style={styles.footerStyle}>
        {loadingData && (
          <ActivityIndicator color={colors.darkGreen} size="small" />
        )}
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      {loaderSkeleton ? (
        <ProfileSkelton />
      ) : myBookings && myBookings.length > 0 ? (
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={myBookings}
          renderItem={_renderBookingCards}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          ListFooterComponent={renderFooter}
          refreshControl={
            <RefreshControl
              tintColor={colors.darkGreen}
              refreshing={isRefreshing}
              onRefresh={() => _onRefresh()}
            />
          }
        />
      ) : (
        <View style={styles.noBookings}>
          <Text>No Bookings</Text>
        </View>
      )}
    </View>
  );
};

export default MyBookings;

const styles = StyleSheet.create({
  noBookings: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerStyle: {
    flex: 1,
    height: 25,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
