let intialState = {
  isFirstLaunch: false,
};
const isFirstLaunch = (state = intialState, action) => {
  switch (action.type) {
    case 'SET_IS_FIRST_LAUNCH':
      return {
        ...state,
        isFirstLaunch: action.payload,
      };
    default:
      return state;
  }
};

export default isFirstLaunch;
