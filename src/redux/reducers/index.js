import {combineReducers} from 'redux';

import signUp from './signUp';
import loader from './loader';
import login from './login';
import homeVenue from './homeVenue';
import homeBookings from './homeBookings';
import inputs from './inputs';
import homeEvents from './homeEvents';
import homeServices from './homeServices';
import myBookings from './myBookings';
import onBoarding from './onBoarding';
import myServices from './myServices';
import screenState from './screenState';
import barcode from './barcode';

const appReducer = combineReducers({
  loader,
  signUp,
  login,
  homeVenue,
  homeBookings,
  inputs,
  homeEvents,
  homeServices,
  myBookings,
  onBoarding,
  myServices,
  screenState,
  barcode,
});

const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};

export default rootReducer;
