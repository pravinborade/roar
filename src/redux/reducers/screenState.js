let intialState = {
  settingsState: false,
  loginMainActive: false,
};
const screenState = (state = intialState, action) => {
  switch (action.type) {
    case 'SET_SETTINGS_STATE':
      return {
        ...state,
        settingsState: action.payload,
      };
    case 'SET_LOGIN_MAIN_ACTIVE':
      return {
        ...state,
        loginMainActive: action.payload,
      };
    default:
      return state;
  }
};

export default screenState;
