const initialState = {
  myServices: [],
  myServicesHasMore: true,
  loadingServicesData: false,
  pageNo: 0,
};

const myServices = (state = initialState, action) => {
  switch (action.type) {
    case 'MY_SERVICES':
      const {myServices} = state;
      let oldServicesData = action.isInitialCall ? [] : [...myServices];
      action.payload.length &&
        action.payload.map((item) => {
          console.log('***map', item);
          let index = oldServicesData.findIndex((x) => x.id === item.id);
          if (index === -1) {
            oldServicesData.push(item);
          }
        });
      console.log('***oldServicesData', oldServicesData);
      return {
        ...state,
        myServices: [...oldServicesData],
      };

    case 'SERVICES_PAGE_NO_INCREMENT':
      return {
        ...state,
        pageNo: action.payload,
      };

    case 'SERVICES_HAS_MORE':
      return {
        ...state,
        myServicesHasMore: action.payload,
      };
    case 'LOADING_SERVICES_DATA':
      return {
        ...state,
        loadingServicesData: action.payload,
      };

    case 'CLEAR_SERVICES_DATA':
      return initialState;

    default:
      return state;
  }
};

export default myServices;
