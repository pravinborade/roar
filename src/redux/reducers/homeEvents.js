const initialState = {
  events: [],
  eventsTemp: [],
  selectedEvent: {},
};

const homeEvents = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_EVENTS':
      return {...state, events: action.payload};

    case 'SET_EVENTS_WITH_TEMP':
      return {
        ...state,
        events: action.payload,
        eventsTemp: action.payload,
      };

    case 'SET_SELECTED_EVENT':
      return {...state, selectedEvent: action.payload};

    default:
      return state;
  }
};

export default homeEvents;
