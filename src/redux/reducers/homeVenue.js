const initialState = {
  selectedVenue: {},
  amenities: [],
  membership: [],
  membershipSelected: {},
  sportsFilterList: [],
  sportsFilterListCopy: [],
  sportsSelected: {},
  popularitySelected: false,
  distanceSelected: false,
  //homeSport reducer replaced.
  orginalData: [],
  filterData: [],
  filterDataCopy: [],
  filterOriginal: [],
  sportsWithImageDark: [],
  sportsWithImageDarkTemp: [],
  sportsWithImagePlain: [],
  activeSport: '',
  sportVenues: [],
  sportVenuesCopy: [],
  selectedSport: {},
};

const homeVenue = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SELECTED_VENUE':
      return {
        ...state,
        selectedVenue: action.payload,
        selectedSport:
          action.payload?.sport && action.payload?.sport?.length > 0
            ? action.payload?.sport[0]
            : {},
      };

    case 'SET_AMINITIES':
      return {...state, amenities: action.payload};

    case 'SET_MEMBERSHIP':
      return {...state, membership: action.payload};

    case 'SET_AMENITIES_MEMBERSHIP':
      return {
        ...state,
        membership: action.payload.membership,
        amenities: action.payload.amenities,
      };

    case 'SET_MEMBERSHIP_SELECTED':
      return {...state, membershipSelected: action.payload};

    case 'SET_SELECTED_SPORT_FILTER':
      return {...state, sportsFilterList: action.payload};

    case 'SET_SELECTED_SPORT_FILTER_AND_COPY':
      return {
        ...state,
        sportsFilterList: action.payload,
        sportsFilterListCopy: action.payload,
      };

    case 'SET_SELECTED_SPORT_FILTER_VALUE':
      return {...state, sportsSelected: action.payload};

    case 'SET_SELECTED_SPORT_FILTER_AND_VALUE':
      return {
        ...state,
        sportsFilterList: action.payload.sportsFilterList,
        sportsSelected: action.payload.sportsSelected,
      };

    case 'SET_POPULARITY_SELECTED': {
      let value = action.payload;
      return {
        ...state,
        popularitySelected: value,
        distanceSelected: value ? !value : value,
      };
    }

    case 'SET_DISTANCE_SELECTED': {
      let value = action.payload;
      return {
        ...state,
        distanceSelected: value,
        popularitySelected: value ? !value : value,
      };
    }

    case 'RESET_FILTER':
      return {
        ...state,
        distanceSelected: false,
        popularitySelected: false,
        sportsSelected: {},
        sportsFilterList: action.payload,
      };

    //home sport.
    case 'SET_DATA':
      return {...state, orginalData: action.orginalData};

    case 'SET_FILTER_DATA':
      return {...state, filterData: action.filterData};

    case 'SET_FILTER_DATA_AND_COPY':
      return {
        ...state,
        filterData: action.payload,
        filterDataCopy: action.payload,
      };

    case 'SET_SPORTS_IMAGE':
      return {...state, sportsWithImageDark: action.payload};

    case 'SET_SPORTS_IMAGE_TEMP':
      return {...state, sportsWithImageDarkTemp: action.payload};

    case 'SET_SPORTS_IMAGE_Plain':
      return {...state, sportsWithImagePlain: action.payload};

    case 'SET_ACTIVE_SPORT':
      return {
        ...state,
        activeSport: action.payload,
        sportsSelected: action.payload === 'ALL' ? {} : state.sportsSelected,
      };

    case 'SET_VENUES_VENUES_COPY_ORIGINAL':
      return {
        ...state,
        filterData: action.payload.filterData,
        filterOriginal: action.payload.filterOriginal,
        orginalData: action.payload.orginalData,
      };

    case 'SET_VENUES_VENUES_COPY_ORIGINAL_FOR_FILTER':
      return {
        ...state,
        filterData: action.payload.filterData,
        filterOriginal: action.payload.filterOriginal,
        orginalData: action.payload.orginalData,
        sportVenues: action.payload.sportVenues,
        sportVenuesCopy: action.payload.sportVenuesCopy,
      };

    case 'SET_FILTERED_BY_SPORT_VENUES':
      return {
        ...state,
        sportVenues: action.payload.sportVenues,
        sportVenuesCopy: action.payload.sportVenuesCopy,
      };

    case 'SET_SPORT_VENUES':
      return {
        ...state,
        sportVenues: action.payload,
      };

    case 'SET_SELECTED_SPORT':
      return {...state, selectedSport: action.payload};

    default:
      return state;
  }
};
export default homeVenue;
