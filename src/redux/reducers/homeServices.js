const initialState = {
  service: {},
  services: [],
  servicesTaken: [],
  inventory: {},
  inventories: [],
  inventoriesTaken: [],
  membership: [],
  membershipTaken: [],
  membershipSelected: {},
  loading: false,
  isVisible: false,
};

const homeServices = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SERVICES':
      return {...state, services: action.payload, servicesTaken: []};

    case 'SET_SERVICE':
      return {...state, service: action.payload};

    case 'CLEAR_SERVICES':
      return {...state, service: {}, services: []};

    case 'UPDATE_SERVICES_TAKEN':
      return {
        ...state,
        servicesTaken: action.payload.servicesTaken,
        services: action.payload.services,
      };

    case 'SET_LOADING_SERVICES':
      return {...state, loading: action.payload};

    case 'SET_IS_PICKER_VISIBLE':
      return {...state, isVisible: action.payload};

    case 'SET_SERVICES_INVENTORIES':
      return {
        ...state,
        services: action.payload.services,
        servicesTaken: [],
        inventories: action.payload.inventories,
        inventoriesTaken: [],
      };

    case 'SET_INVENTORIES':
      return {...state, inventories: action.payload, inventoriesTaken: []};

    case 'CLEAR_INVENTORIES':
      return {...state, inventory: {}, inventoriesTaken: []};

    case 'UPDATE_INVENTORIES_TAKEN':
      return {
        ...state,
        inventoriesTaken: action.payload.inventoriesTaken,
        inventories: action.payload.inventories,
      };

    case 'SET_MEMBERSHIP_LIST':
      return {...state, membership: action.payload, membershipTaken: []};

    case 'UPDATE_MEMBERSHIP_TAKEN':
      return {
        ...state,
        membershipTaken: action.payload.membershipTaken,
        membership: action.payload.membership,
      };

    case 'CLEAR_CART':
      return {
        ...state,
        inventories: action.payload.inventories,
        inventoriesTaken: [],
        membership: action.payload.membership,
        membershipTaken: [],
        services: action.payload.services,
        servicesTaken: [],
      };

    default:
      return state;
  }
};
export default homeServices;
