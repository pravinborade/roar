const initialState = {
  myBookings: [],
  mySelectedBooking: {},
  myBookingsHasMore: true,
  loadingBookingsData: false,
  splitpaymentdata: {},
  pageNo: 0,
  loaderSkeleton: false,
  multipleMyBookings: [],
  multipleMyBookingTotal: 0,
  multipleMyBookingCount: 0,
};

const myBookings = (state = initialState, action) => {
  switch (action.type) {
    case 'MY_BOOKINGS':
      const {myBookings} = state;
      let oldBookingsData = action.isInitialCall ? [] : [...myBookings];
      action.payload.length &&
        action.payload.map((item) => {
          console.log('***map', item);
          let index = oldBookingsData.findIndex((x) => x.id === item.id);
          if (index === -1) {
            oldBookingsData.push(item);
          }
        });
      console.log('***oldBookingsData', oldBookingsData);
      return {
        ...state,
        myBookings: [...oldBookingsData],
      };

    case 'BOOKINGS_PAGE_NO_INCREMENT':
      return {
        ...state,
        pageNo: action.payload,
      };

    case 'BOOKINGS_HAS_MORE':
      return {
        ...state,
        myBookingsHasMore: action.payload,
      };
    case 'LOADING_BOOKINGS_DATA':
      return {
        ...state,
        loadingBookingsData: action.payload,
      };
    case 'SKELETON_LOADER':
      return {
        ...state,
        loaderSkeleton: action.payload,
      };

    case 'SET_MY_SELECTED_BOOKING':
      return {...state, mySelectedBooking: action.payload};

    case 'MY_MULTIPLE_BOOKING':
      return {...state, multipleMyBookings: action.payload};

    case 'MY_MULTIPLE_BOOKING_TOTAL':
      return {...state, multipleMyBookingTotal: action.payload};

    case 'MY_MULTIPLE_BOOKING_COUNT':
      return {...state, multipleMyBookingCount: action.payload};

    case 'SET_MY_SELECTED_PLAYER_TOTALAMOUNT':
      return {...state, splitpaymentdata: action.payload};

    case 'CLEAR_BOOKINGS_DATA':
      return initialState;

    default:
      return state;
  }
};

export default myBookings;
