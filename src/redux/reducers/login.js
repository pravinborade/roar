const initialState = {
  userToken: null,
  data: {},
  loginError: {},
  latLong: {},
  location: '',
  fcmtoken: null,
  fromPayment: 'default',
  recivenotification: false,
};

const login = (state = initialState, action) => {
  if (typeof state === 'undefined') {
    return initialState;
  }
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        data: action.payload,
        userToken: action.payload.token,
      };
    case 'LOCATION':
      return {
        ...state,
        location: action.payload,
      };
    case 'LAT_LONG':
      return {
        ...state,
        latLong: action.payload,
      };
    case 'LAT_LONG_ADDRESS':
      return {
        ...state,
        latLong: action.payload.location,
        location: action.payload.address,
      };
    case 'RETRIEVE_TOKEN':
      return {
        ...state,
        userToken: action.payload,
      };
    case 'SET_PROFILE':
      console.log('action-----', action.payload);
      return {
        ...state,
        data: action.payload,
      };
    case 'LOGIN_ERROR':
      return {
        ...state,
        loginError: action.payload,
      };
    case 'FROM_PAYMENT':
      return {
        ...state,
        fromPayment: action.payload,
      };
    case 'FCM_TOKEN':
      return {
        ...state,
        fcmtoken: action.payload,
      };
    case 'RECEIVED_NOTIFICTIONS':
      return {
        ...state,
        recivenotification: action.payload,
      };
    case 'CLEAR_LOGIN_DATA':
      return {
        userToken: null,
        data: {},
        loginError: {},
        latLong: {},
        location: '',
        fcmtoken: null,
        fromPayment: 'default',
        recivenotification: false,
      };
    default:
      return state;
  }
};

export default login;
