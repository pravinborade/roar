const initialState = {
  data: {},
};

const signup = (state = initialState, action) => {
  switch (action.type) {
    case 'SIGNUP_SUCCESS':
      return {
        ...state,
        data: action.signupData,
      };

    default:
      return state;
  }
};

export default signup;
