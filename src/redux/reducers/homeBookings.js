const initialState = {
  slot: {},
  slots: [],
  slotsSelected: [],
  courts: [],
  court: {},
  selectedMonth: '',
  selectedYear: '',
  selectedCourt: {},
  selectedDate: '',
  selectedTime: '',
  selectedSport: '',
  availableSportsIconPlain: [],
  initialTransaction: [],
  finalPrice: 0,
  sportPlayedPlain: [],
  slotSelectedByOther: {},
  tempUserId: undefined,
  summaryBooking: {},
};

const homeBookings = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_SLOTS':
      return { ...state, slots: action.payload };

    case 'SET_SELECTED_SLOTS':
      return { ...state, slotsSelected: action.payload };

    case 'SET_COURT_AND_S_SLOTS':
      return {
        ...state,
        slotsSelected: action.payload.slotsSelected,
        court: action.payload.court,
      };

    case 'SET_SELECTED_SPORT_BOOKING':
      return { ...state, selectedSport: action.payload };

    case 'SET_FINAL_PRICE':
      return { ...state, finalPrice: action.payload };

    case 'SET_DATE_M_Y':
      return {
        ...state,
        selectedDate: action.payload.selectedDate,
        selectedYear: action.payload.selectedYear,
        selectedMonth: action.payload.selectedMonth,
      };

    case 'SET_CORT_MONTH_YEAR_SLOTS':
      return {
        ...state,
        courts: action.payload.courts,
        court: action.payload.court,
        selectedCourt: action.payload.selectedCourt,
        selectedDate: action.payload.selectedDate,
        slots: action.payload.slots,
        selectedMonth: action.payload.selectedMonth,
        selectedYear: action.payload.selectedYear,
        selectedSport: action.payload.selectedSport,
        slotsSelected: [],
        slot: {},
      };

    case 'SET_SELECTED_CORT_SPORT_MONTH_YEAR_SLOTS':
      return {
        ...state,
        court: action.payload.court,
        selectedCourt: action.payload.selectedCourt,
        selectedDate: action.payload.selectedDate,
        slots: action.payload.slots,
        selectedMonth: action.payload.selectedMonth,
        selectedYear: action.payload.selectedYear,
        selectedSport: action.payload.selectedSport,
        slot: {},
      };

    case 'SET_COURTS':
      return {
        ...state,
        courts: action.payload,
      };

    case 'SET_SELECTED_COURT':
      return {
        ...state,
        court: action.payload.court,
        selectedCourt: action.payload.selectedCourt,
        selectedSport: action.payload.selectedSport,
        slot: {},
      };

    case 'SET_COURT':
      return { ...state, court: action.payload };

    case 'SET_ICONS_DARK_PLAIN':
      return {
        ...state,
        sportPlayedPlain: action.payload.sportPlayedPlain,
      };

    case 'INITIAL_TRANSACTION':
      return {
        ...state,
        initialTransaction: action.payload,
      };

    case 'CLEAR_SLOTS_SLOT':
      return {
        ...state,
        slot: {},
        slots: [],
      };

    case 'CLEAR_SLOTS_SLOT_SPORT_DATE_TIME':
      return {
        ...state,
        slot: {},
        slots: [],
        selectedSport: '',
        selectedDate: '',
        selectedTime: '',
      };

    case 'CLEAR_COURT_SELECTED_SLOTS_SLOT':
      return {
        ...state,
        courts: [],
        court: {},
        slot: {},
        selectedSport: '',
        selectedDate: '',
        selectedTime: '',
      };

    case 'CLEAR_SLOT_AVAILABLE_SPORT':
      return {
        ...state,
        slot: {},
        selectedSport: '',
      };

    case 'SET_SLOTS_SELECTED_BY_OTHER':
      return {
        ...state,
        slotSelectedByOther: action.payload,
      };

    case 'SET_TEMP_USER_ID':
      return { ...state, tempUserId: action.payload };

    case 'SET_SLOTS_IDS':
      return {
        ...state,
        summaryBooking: { ...state.summaryBooking, booking: action.payload },
      };

    case 'SET_MEMERSHIP_IDS':
      return {
        ...state,
        summaryBooking: { ...state.summaryBooking, memberships: action.payload },
      };

    case 'SET_INVENTORIES_IDS':
      return {
        ...state,
        summaryBooking: { ...state.summaryBooking, inventories: action.payload },
      };

    case 'SET_TOTAL_AMOUNT':
      return {
        ...state,
        summaryBooking: {
          ...state.summaryBooking,
          totalAllAmount: action.payload,
        },
      };

      case 'SET_MEMBERSHIP_DISCOUNT_AMOUNT':
      return {
        ...state,
        summaryBooking: {
          ...state.summaryBooking,
          membershipDiscountAmount: action.payload,
        },
      };
      case 'RESET_SUMMARY':
      return {
        ...state,
        summaryBooking:{},
      };

    default:
      return state;
  }
};
export default homeBookings;
