const initialState = {
  loading: false,
  barcodeUrl: '',
};

const barcode = (state = initialState, action) => {
  switch (action.type) {
    case 'BARCODE_LOADING':
      return {...state, loading: action.payload};
    case 'BARCODE':
      return {...state, barcodeUrl: action.payload, loading: false};
    default:
      return state;
  }
};
export default barcode;
