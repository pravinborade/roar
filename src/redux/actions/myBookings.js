import axiosService from '../../commons/axiosService';
import {successLog} from '../../utils/fireLog';

export const getmyBookings = (profileId, title, pageNo) => {
  console.log('***actionnnnn', profileId, title, pageNo);
  return async (dispatch, getState) => {
    let link = `api/bookings/userId/${profileId}/status/${title}?pageNo=${pageNo}&pageSize=10`;
    successLog('link -' + link);
    if (pageNo === 0) {
      dispatch({type: 'BOOKINGS_HAS_MORE', payload: true});
    }
    dispatch({type: 'BOOKINGS_PAGE_NO_INCREMENT', payload: pageNo});
    dispatch({type: 'LOADING_BOOKINGS_DATA', payload: true});

    if (pageNo === 0) {
      dispatch({type: 'SKELETON_LOADER', payload: true});
    }
    return await axiosService
      .get(link, dispatch)
      .then((res) => {
        dispatch({type: 'SKELETON_LOADER', payload: false});
        console.log('***response', res.data.data[0].content);
        if (res.data === '') {
          console.log('***enteringgg');
          dispatch({type: 'MY_BOOKINGS', payload: []});
          dispatch({type: 'BOOKINGS_HAS_MORE', payload: false});
          dispatch({type: 'LOADING_BOOKINGS_DATA', payload: false});
        } else {
          console.log('***responseeeee', res);
          dispatch({
            type: 'MY_BOOKINGS',
            payload: res.data.data[0].content,
            isInitialCall: pageNo === 0,
          });
          dispatch({type: 'LOADING_BOOKINGS_DATA', payload: false});
        }
      })
      .catch((error) => {
        dispatch({type: 'SKELETON_LOADER', payload: false});
        dispatch({type: 'LOADING_BOOKINGS_DATA', payload: false});
        // helper.errorToast(error.response.data.message);
      });
  };
};

export const clearMyBookings = () => {
  return {type: 'CLEAR_BOOKINGS_DATA'};
};

export const multipleMyBookings = (offerNo, startDate) => {
  return async (dispatch, getState) => {
    let link = `api/bookings/slotsbyofferNo/${offerNo}?date=${startDate}`;
    successLog('link -' + link);
    return await axiosService
      .get(link, dispatch)
      .then((res) => {
        console.log('***response Multiple Booking', res);
        if (res.data === '') {
          console.log('***enteringgg');
          dispatch({type: 'MY_MULTIPLE_BOOKING', payload: []});
        } else {
          console.log('***responseeeee', res);
          let newArray = res.data.data;
          let total = 0;
          let finalTotal = 0;
          dispatch({type: 'MY_MULTIPLE_BOOKING', payload: res.data.data});

          if (newArray && newArray.length > 0) {
            for (var i = 0; i < newArray.length; i++) {
              total = total + newArray[i].finalRate;
            }
            finalTotal = total;
          }
          dispatch({type: 'MY_MULTIPLE_BOOKING_TOTAL', payload: finalTotal});
          dispatch({
            type: 'MY_MULTIPLE_BOOKING_COUNT',
            payload: newArray.length,
          });
        }
      })
      .catch((error) => {
        // helper.errorToast(error.response.data.message);
      });
  };
  // return { type: 'MULTIPLE_BOOKING_DATA' }
};
