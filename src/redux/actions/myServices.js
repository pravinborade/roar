import axiosService from '../../commons/axiosService';

export const getmyServices = (profileId, pageNo) => {
  console.log('***actionnn', profileId, pageNo);
  return async (dispatch, getState) => {
    let link = `api/services/user?userId=${profileId}&pageNo=${pageNo}&pageSize=10`;
    //let link = `api/bookings/userId/${profileId}/status/${title}?pageNo=${pageNo}&pageSize=10`

    if (pageNo === 0) {
      dispatch({type: 'SERVICES_HAS_MORE', payload: true});
    }
    dispatch({type: 'SERVICES_PAGE_NO_INCREMENT', payload: pageNo});
    dispatch({type: 'LOADING_SERVICES_DATA', payload: true});

    return await axiosService
      .get(link, dispatch)
      .then((res) => {
        console.log('***response', res);
        //console.log("***response", res.data.data[0].content)
        if (res.data === '') {
          console.log('***enteringgg');
          dispatch({type: 'MY_SERVICES', payload: []});
          dispatch({type: 'SERVICES_HAS_MORE', payload: false});
          dispatch({type: 'LOADING_SERVICES_DATA', payload: false});
        } else {
          console.log('***responseeeee', res);
          dispatch({
            type: 'MY_SERVICES',
            payload: res.data.data[0].content,
            isInitialCall: pageNo === 0,
          });
          dispatch({type: 'LOADING_SERVICES_DATA', payload: false});
        }
      })
      .catch((error) => {
        dispatch({type: 'LOADING_SERVICES_DATA', payload: false});
        // helper.errorToast(error.response.data.message);
      });
  };
};
