export function set(token) {
  console.log('Action token called', token);
  return {
    type: 'SET_TOKEN',
    payload: token,
  };
}
