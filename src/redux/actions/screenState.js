export const fromSettingsScreen = (value) => {
  return {type: 'SET_SETTINGS_STATE', payload: value};
};

export const loginMainActive = (value) => {
  return {type: 'SET_LOGIN_MAIN_ACTIVE', payload: value};
};
