import AsyncStorage from '@react-native-async-storage/async-storage';
import axiosService from '../../commons/axiosService';
import {errorLog, showToastMsg} from '../../utils';

export const setLoginData = (loginData) => {
  return async (dispatch) => {
    await setToAsyncStorage(loginData);
    await dispatch({
      type: 'SET_PROFILE',
      payload: loginData,
    });
    await dispatch({
      type: 'RETRIEVE_TOKEN',
      payload: loginData.token,
    });
  };
};

export const setFcmToken = (body) => {
  return async (dispatch) => {
    axiosService
      .put(`api/users/fcmToken`, body)
      .then((response) => {
        if (response.status === 200)
          dispatch({type: 'FCM_TOKEN', payload: response.data});
      })
      .catch((error) => {
        errorLog('@#ERROR NOTIFICATION ' + error);
      });
  };
};

export const deleteFcmToken = (fcmToken, token) => {
  console.log('@#DELETE BODY AND TOKEN', fcmToken);
  return async (dispatch) => {
    axiosService
      .delete(`api/users/fcmToken?access_token=${token}&fcmToken=${fcmToken}`)
      .then((response) => {
        // dispatch({ type: 'FCM_TOKEN', payload: response.data })
        // console.log("@#PUT FCMTOKEN DELETE FROM USER REDUCER---------------->",response);
      })
      .catch((error) => {
        console.log('@#ERROR NOTIFICATION', error);
      });
  };
};

export const setLocation = (location) => {
  AsyncStorage.setItem('location', location);
  return (dispatch) => {
    dispatch({
      type: 'LOCATION',
      payload: location,
    });
  };
};

const setToAsyncStorage = async (loginData) => {
  try {
    await AsyncStorage.setItem('userToken', loginData.token);
    await AsyncStorage.setItem('userDetails', JSON.stringify(loginData));
  } catch (e) {
    console.log(e);
  }
};

export const setFromPayment = (value) => {
  return (dispatch) => {
    dispatch({
      type: 'FROM_PAYMENT',
      payload: value,
    });
  };
};

export const clearLoginData = () => {
  return {type: 'CLEAR_LOGIN_DATA'};
};
