export const setisFirstLaunch = (value) => {
  return {type: 'SET_IS_FIRST_LAUNCH', payload: value};
};
