import React, {useEffect} from 'react';
import {
  Alert,
  BackHandler,
  Image,
  Linking,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Navigator from './navigation/Navigator';
import {Provider} from 'react-redux';
import Toast from 'react-native-toast-message';
import VersionCheck from 'react-native-version-check';
import {getAppstoreAppMetadata} from 'react-native-appstore-version-checker';
import messaging from '@react-native-firebase/messaging';
import {QueryClient, QueryClientProvider} from 'react-query';
import Images from '../assets/Images/Images';
import {PT_SANS_BOLD, PT_SANS_REGULAR} from '../styles/typography';
import {Content, errorLog} from '../utils';
import withCodePush from '../../codepush';
import {receiveNotification} from '../screens/Notification/action';
import {store} from '../redux/store';
import Orientation from 'react-native-orientation-locker';

// Create a client
const queryClient = new QueryClient();

const App = () => {
  //  GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;
  const isAndroid = Platform.OS === 'android';
  const _checkVersion = async () => {
    try {
      let PackageString = isAndroid ? 'com.chirpn.roar' : '1519176256';
      let latestVersion = await getAppstoreAppMetadata(PackageString); //put any apps packageId here
      let currentVersion = await VersionCheck.getCurrentVersion();
      let StoreUrl = 'www.google.com';
      if (isAndroid) StoreUrl = await VersionCheck.getPlayStoreUrl();
      else StoreUrl = 'itms-apps://itunes.apple.com/us/app/id1519176256?mt=8';

      if (parseFloat(latestVersion) > parseFloat(currentVersion)) {
        Alert.alert(
          Content.plsUpdate,
          Content.updateMsg,
          [
            {
              text: Content.update,
              onPress: () => {
                BackHandler.exitApp();
                Linking.openURL(StoreUrl);
              },
            },
          ],
          {cancelable: false},
        );
      }
    } catch (error) {
      errorLog(error);
    }
  };

  useEffect(() => {
    try {
      receiveMessagesss();
      _checkVersion();
      requestUserPermission();
      if (!isAndroid) Orientation.lockToPortrait();
    } catch (e) {
      errorLog(e);
    }
  }, []);

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
  }

  const receiveMessagesss = () => {
    if (!isAndroid) {
      messaging().onMessage(async (remoteMessage) => {
        store.dispatch(receiveNotification(true));
        Toast.show({
          type: 'success',
          text1: remoteMessage.notification.title,
          text2: remoteMessage.notification.body,
        });
      });
    }
  };

  const toastConfig = {
    error: ({text1, text2 = '', props, ...rest}) => (
      <View style={styles.containerToast}>
        <View style={styles.crossContainer}>
          <Image style={styles.crossImage} source={Images.crossImage} />
        </View>
        <View style={styles.textHeader}>
          <Text style={styles.text}>{text1}</Text>
          {text2 && text2 !== '' ? (
            <Text style={styles.subText}>{text2}</Text>
          ) : null}
        </View>
      </View>
    ),
    info: () => {},
    any_custom_type: () => {},
  };

  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <Navigator />
        <Toast config={toastConfig} ref={(ref) => Toast.setRef(ref)} />
      </Provider>
    </QueryClientProvider>
  );
};

export default withCodePush(App);
const styles = StyleSheet.create({
  containerToast: {
    flex: 1,
    borderLeftColor: '#f44336',
    flexDirection: 'row',
    borderLeftWidth: 5,
    borderRadius: 5,
    width: '90%',
    backgroundColor: 'white',
  },
  crossContainer: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  crossImage: {
    height: 15,
    width: 15,
  },
  textHeader: {
    flex: 1,
    marginLeft: 20,
    marginVertical: 10,
    justifyContent: 'center',
  },
  text: {
    fontFamily: PT_SANS_BOLD,
    fontSize: 14,
  },
  subText: {
    fontFamily: PT_SANS_REGULAR,
    marginTop: 5,
    fontSize: 12,
  },
});
