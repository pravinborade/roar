import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {App, AuthStack, OnBoardingStack, TabStack} from './Routes';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ActivityIndicator, Alert, Linking, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  isReadyRef,
  navigateDeeplinking,
  navigationRef,
  routeNameRef,
} from './RootNavigation';
import {GoogleSignin} from '@react-native-community/google-signin';
import {
  getAndSetLatLong,
  getAndSetToken,
  getLatLong,
} from '../../screens/Core/Home/comp/action';
import analytics from '@react-native-firebase/analytics';
import {setisFirstLaunch} from '../../redux/actions/onBoarding';
import {ScreenNames} from '../../utils/ScreenNames';
import {loginMainActive} from '../../redux/actions/screenState';

import {setMembershipSelected} from '../../screens/Core/Home/scenes/venues/venue_detail/action';
import {geturl} from './deeplinking/action';
import branch, {BranchEvent} from 'react-native-branch';

const Navigator = () => {
  const loginToken = useSelector((state) => state.login.userToken);
  const userDetails = useSelector((state) => state.login.data);
  const isFirstLaunch = useSelector((state) => state.onBoarding.isFirstLaunch);
  const dispatch = useDispatch();
  const [isAuth, setisAuth] = useState(null);

  useEffect(() => {
    dispatch(geturl());
    GoogleSignin.configure({
      webClientId:
        '149846864740-g6r3mb4fbr16g2hcbc0adg4dhd5h6s7q.apps.googleusercontent.com',
    });
    //dispatch(getAndSetToken());
    dispatch(getAndSetLatLong());

    AsyncStorage.getItem('userDetails').then((item) => {
      if (item) {
        dispatch({type: 'SET_PROFILE', payload: JSON.parse(item)});
        setisAuth(true);
      } else {
        getUserToken(undefined);
        setisAuth(false);
      }
    });

    AsyncStorage.getItem('alreadyLaunched').then((value) => {
      if (value == null) {
        AsyncStorage.setItem('alreadyLaunched', 'true');
        dispatch(setisFirstLaunch(true));
      } else {
        dispatch(setisFirstLaunch(false));
      }
    });

    AsyncStorage.getItem('userToken').then((item) => {
      if (item) {
        dispatch({type: 'RETRIEVE_TOKEN', payload: item});
        setisAuth(true);
      } else {
        dispatch({type: 'RETRIEVE_TOKEN', payload: null});
        setisAuth(false);
      }
    });
    return () => {
      isReadyRef.current = false;
    };
  }, [loginToken]);

  if (isFirstLaunch === null) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else if (isAuth == null) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <NavigationContainer
        ref={navigationRef}
        onReady={() =>
          (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
        }
        onStateChange={async () => {
          const previousRouteName = routeNameRef.current;
          const currentRouteName = navigationRef.current.getCurrentRoute().name;

          if (previousRouteName !== currentRouteName) {
            if (
              currentRouteName === ScreenNames.BOOKINGS ||
              currentRouteName === ScreenNames.MYMEMBERSHIP ||
              currentRouteName === ScreenNames.EVENTS
            ) {
              dispatch(loginMainActive(true));
            }
            if (
              currentRouteName === ScreenNames.HOME ||
              currentRouteName === ScreenNames.SETTINGS
            ) {
              dispatch(loginMainActive(false));
            }

            await analytics().logScreenView({
              screen_name: currentRouteName,
              screen_class: currentRouteName,
            });
          }

          // Save the current route name for later comparison
          routeNameRef.current = currentRouteName;
        }}
      >
        <App />
      </NavigationContainer>
    );
  }
};
export default Navigator;
