import {Alert, Linking} from 'react-native';
import axiosService from '../../../commons/axiosService';
import {setMembershipSelected} from '../../../screens/Core/Home/scenes/venues/venue_detail/action';
import {
  errorLog,
  ifNotValid,
  ifValid,
  ScreenNames,
  successLog,
} from '../../../utils';
import {navigateDeeplinking} from '../RootNavigation';
import branch, {BranchEvent} from 'react-native-branch';
import {setFromPayment} from '../../../redux/actions/login';
import {store} from '../../../redux/store';
import helperFunctions from '../../../commons/helperFunctions';
import {loginMainActive} from '../../../redux/actions/screenState';

export const geturl = () => {
  return async (dispatch) => {
    branch.subscribe(async ({error, params, uri}) => {
      if (params.id) {
        axiosService
          .get(`api/memberships/membership/${params.id}`)
          .then((res) => {
            if (res.status === 200) {
              let data = res.data.data[0];
              if (ifNotValid(data)) {
                errorLog('invalid response ');
                return;
              }
              data.plan.membershipId = data.id;
              data.plan.priceToPay = helperFunctions.calculatePriceToPay(
                data.plan,
              );
              dispatch(setMembershipSelected(data.plan));
              dispatch(loginMainActive(true));
              if (!store.getState().login.data.id) {
                navigateDeeplinking(ScreenNames.MEMBERSHIP_DETAIL);
                dispatch(setFromPayment('membership_book'));
                navigateDeeplinking(ScreenNames.LOGIN);
              } else if (params.userId == store.getState().login.data.id) {
                navigateDeeplinking(ScreenNames.MEMBERSHIP_DETAIL);
              } else {
                alert('User Authentication Fail!');
              }
            } else {
              console.log('not valid response ' + JSON.stringify(res));
            }
          })
          .catch((o) => {
            console.log('getMembership error ' + o);
          });
      }
    });
  };
};

export const membershipRenewurl = async (data) => {
  await generateUri(data);
};

export const membershipRenewurlMultiple = async (data) => {
  for (let i = 0; i < data.length; i++) {
    await generateUri(data[i]);
  }
};

const generateUri = async (data) => {
  let membershipData = await branch.createBranchUniversalObject('membership', {
    contentMetadata: {
      customMetadata: {
        userId: data.user.id.toString(),
        id: data.id.toString(),
      },
    },
  });
  let {url} = await membershipData.generateShortUrl();
  axiosService
    .put(
      `api/memberships/renewUrl?access_token=${
        store.getState().login.userToken
      }`,
      {id: data.id, renewUrl: url},
    )
    .then((res) => {
      if (res.status === 200) {
      } else {
        console.log('not valid response ' + JSON.stringify(res));
      }
    })
    .catch((o) => {
      console.log('getMembership error ' + o);
    });
};
