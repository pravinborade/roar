import * as React from 'react';
import {createStackNavigator, HeaderBackButton} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//Auth
import Initial from '../../screens/Auth/Initial';
import Login from '../../screens/Auth/LogIn';
import Signup from '../../screens/Auth/SignUp';
import ForgotPassword from '../../screens/Auth/ForgotPassword';
import Reset from '../../screens/Auth/Reset';
import Onboarding from '../../screens/OnBoarding';
//Home
import Home from '../../screens/Core/Home';
import VenueDetails from '../../screens/Core/Home/scenes/venues/venue_detail';
import VenuesAll from '../../screens/Core/Home/scenes/venues/venue_all';
import VenueFilterScreen from '../../screens/Core/Home/scenes/venues/venue_all/filter_screen';
import HomeBookings from '../../screens/Core/Home/scenes/bookings';
import HomeServices from '../../screens/Core/Home/scenes/services';
import HomeSummary from '../../screens/Core/Home/scenes/summary';
import SportAll from '../../screens/Core/Home/scenes/sports/sport_all';
import Notification from '../../screens/Notification';
//Bookings
import Bookings from '../../screens/Core/Bookings';
import BookingSuccess from '../../screens/Core/Bookings/scenes/bookingSuccess';
import MyBookings from '../../screens/Core/Bookings/scenes/myBookings';
import DetailsMyBookings from '../../screens/Core/Bookings/comp/detailsMyBooking';
//Membership
import MyMembership from '../../screens/Core/MyMembership';
//Events
import Events from '../../screens/Core/Events';
//Settings
import Settings from '../../screens/Settings';
import AboutRoar from '../../screens/Settings/scenes/AboutRoar';
import TermsOfUse from '../../screens/Settings/scenes/TermsOfUse';
import myServices from '../../screens/Settings/scenes/MyServices';
import Profile from '../../screens/Settings/scenes/Profile';
import ProfileEdit from '../../screens/Settings/scenes/ProfileEdit';

import {ScreenNames} from '../../utils/ScreenNames';
import {Image} from 'react-native';
import Images from '../../assets/Images/Images';
import {screenOption, tabBarOption} from './constants/options';
import Location from '../../screens/Core/Home/scenes/Location';
import Acknowledgement from '../../screens/Core/Home/scenes/acknoledgement';
import Reports from '../../screens/Settings/scenes/reports';
import EventList from '../../screens/Core/Home/scenes/events/event_list';
import EventDetails from '../../screens/Core/Home/scenes/events/event_detail';
import EventAcknowledgement from '../../screens/Core/Home/scenes/events/event_acknoledgement';
import ServiceAcknowledgement from '../../screens/Core/Home/scenes/services/comp/service_acknoledgementt/index';
import MembershipAll from '../../screens/Core/Home/scenes/membership/membership_all';
import MembershipDetails from '../../screens/Core/Home/scenes/membership/membership_details';
import MembershipAcknowledgement from '../../screens/Core/Home/scenes/membership/membership_acknoledgement';
import ServiceData from '../../screens/Core/Home/scenes/services/mymember';
import ServiceDetails from '../../screens/Settings/scenes/ServiceDetails';
import HomeServicesAll from '../../screens/Core/Home/scenes/services/services_all';
import HomeInventoriesAll from '../../screens/Core/Home/scenes/services/inventories_all';
import SplitPayment from '../../../src/screens/Core/Home/scenes/bookings/comp/SplitPayment';
import AddMemberForm from '../../../src/screens/Core/Home/scenes/bookings/comp/AddMemberForm';
import BookingPolicy from '../../screens/Core/Home/scenes/bookingPolicy';
import {useDispatch, useSelector} from 'react-redux';
import {backHandler} from '../../commons/settingsBackHandler';
import BarcodeScreen from '../../screens/Settings/scenes/Profile/comp/BarcodeScreen';
import RatingsAdd from '../../screens/Core/Home/scenes/venues/venue_detail/rating-add';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const TabStack = () => {
  const userToken = useSelector((state) => state.login.userToken);
  const settingsState = useSelector((state) => state.screenState.settingsState);

  console.log('----seting', settingsState);
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === ScreenNames.HOME) {
            iconName = Images.home;
          } else if (route.name === ScreenNames.BOOKINGS) {
            iconName = Images.bookings;
          } else if (route.name === ScreenNames.MYMEMBERSHIP) {
            iconName = Images.membership;
          } else if (route.name === ScreenNames.EVENTS) {
            iconName = Images.events;
          } else if (route.name === ScreenNames.SETTINGS) {
            iconName = Images.settings;
          }

          return (
            <Image
              source={iconName}
              style={{
                height: size,
                width: size,
                resizeMode: 'contain',
                tintColor: `${color}`,
              }}
            />
          );
        },
      })}
      backBehavior="none"
      tabBarOptions={tabBarOption}>
      <Tab.Screen name={ScreenNames.HOME} component={Home} />
      <Tab.Screen
        options={{
          tabBarVisible: settingsState ? false : true,
        }}
        name={ScreenNames.BOOKINGS}
        component={userToken ? Bookings : Login}
      />

      <Tab.Screen
        options={{
          tabBarVisible: settingsState ? false : true,
        }}
        name={ScreenNames.MYMEMBERSHIP}
        component={userToken ? MyMembership : Login}
      />
      <Tab.Screen
        options={{
          tabBarVisible: settingsState ? false : true,
        }}
        name={ScreenNames.EVENTS}
        component={userToken ? Events : Login}
      />
      <Tab.Screen name={ScreenNames.SETTINGS} component={Settings} />
    </Tab.Navigator>
  );
};

export const App = () => {
  const dispatch = useDispatch();
  const isFirstLaunch = useSelector((state) => state.onBoarding.isFirstLaunch);
  return (
    <Stack.Navigator
      headerMode="none"
      initialRouteName={
        isFirstLaunch ? ScreenNames.ONBOARDING : ScreenNames.HOME
      }
      screenOptions={screenOption}>
      <Stack.Screen name={ScreenNames.HOME} component={TabStack} />
      <Stack.Screen name={ScreenNames.ONBOARDING} component={Onboarding} />
      <Stack.Screen name={ScreenNames.MY_LOCATION} component={Location} />
      <Stack.Screen name={ScreenNames.VENUE_DETAILS} component={VenueDetails} />
      <Stack.Screen name={ScreenNames.RATINGS_ADD} component={RatingsAdd} />
      <Stack.Screen name={ScreenNames.HOME_VENUES} component={VenuesAll} />
      <Stack.Screen
        name={ScreenNames.VENUES_FILTER}
        component={VenueFilterScreen}
      />
      <Stack.Screen name={ScreenNames.HOME_BOOKINGS} component={HomeBookings} />
      <Stack.Screen name={ScreenNames.HOME_SERVICES} component={HomeServices} />
      <Stack.Screen
        name={ScreenNames.HOME_SERVICES_ALL}
        component={HomeServicesAll}
      />
      <Stack.Screen
        name={ScreenNames.HOME_INVENTORIES_ALL}
        component={HomeInventoriesAll}
      />
      <Stack.Screen name={ScreenNames.HOME_SUMMARY} component={HomeSummary} />
      <Stack.Screen
        name={ScreenNames.HOME_ACKNOWLEDGEMENT}
        component={Acknowledgement}
      />
      <Stack.Screen name={ScreenNames.NOTIFICATION} component={Notification} />
      <Stack.Screen
        name={ScreenNames.BOOKING_POLICY}
        component={BookingPolicy}
      />

      {/*sport */}
      <Stack.Screen name={ScreenNames.HOME_SPORT_ALL} component={SportAll} />
      <Stack.Screen name={ScreenNames.MY_MEMBER} component={ServiceData} />

      {/*EVENT*/}
      <Stack.Screen name={ScreenNames.HOME_EVENT_LIST} component={EventList} />
      <Stack.Screen
        name={ScreenNames.HOME_EVENT_DETAIL}
        component={EventDetails}
      />
      <Stack.Screen
        name={ScreenNames.HOME_EVENT_ACKNOWLEDGEMENT}
        component={EventAcknowledgement}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_ACKNOWLEDGEMENT}
        component={ServiceAcknowledgement}
      />

      <Stack.Screen
        name={ScreenNames.MEMBERSHIP_LIST}
        component={MembershipAll}
      />
      <Stack.Screen
        name={ScreenNames.MEMBERSHIP_DETAIL}
        component={MembershipDetails}
      />
      <Stack.Screen
        name={ScreenNames.MEMBERSHIP_ACKNOWLEDGEMENT}
        component={MembershipAcknowledgement}
      />

      <Stack.Screen name={ScreenNames.SPLITPAYMENT} component={SplitPayment} />
      <Stack.Screen name={ScreenNames.ADDMEMBER} component={AddMemberForm} />

      <Stack.Screen name={ScreenNames.BOOKINGS} component={Bookings} />
      <Stack.Screen
        name={ScreenNames.BOOKING_SUCCESS}
        component={BookingSuccess}
      />
      <Stack.Screen name={ScreenNames.MY_BOOKINGS} component={MyBookings} />

      <Stack.Screen
        name={ScreenNames.DETAILSMYBOOKINGS}
        component={DetailsMyBookings}
      />

      <Stack.Screen name={ScreenNames.MYMEMBERSHIP} component={MyMembership} />

      <Stack.Screen name={ScreenNames.EVENTS} component={Events} />

      <Stack.Screen name={ScreenNames.SETTINGS} component={Settings} />
      <Stack.Screen name={ScreenNames.MY_PROFILE} component={Profile} />
      <Stack.Screen name={ScreenNames.ABOUT_ROAR} component={AboutRoar} />
      <Stack.Screen name={ScreenNames.TERMS_OF_USE} component={TermsOfUse} />
      <Stack.Screen name={ScreenNames.MY_SERVICES} component={myServices} />
      <Stack.Screen name={ScreenNames.PROFILE_EDIT} component={ProfileEdit} />
      <Stack.Screen name={ScreenNames.RESET} component={Reset} />
      <Stack.Screen name={ScreenNames.REPORTS} component={Reports} />
      <Stack.Screen
        name={ScreenNames.Service_Details}
        component={ServiceDetails}
      />
      <Stack.Screen
        name={ScreenNames.BARCODE_SCREEN}
        component={BarcodeScreen}
      />

      {/* <Stack.Screen name={ScreenNames.INITIAL} component={Initial} /> */}
      <Stack.Screen name={ScreenNames.LOGIN} component={Login} />
      <Stack.Screen name={ScreenNames.SIGNUP} component={Signup} />
      <Stack.Screen
        name={ScreenNames.FORGOT_PASSWORD}
        component={ForgotPassword}
      />
    </Stack.Navigator>
  );
};
