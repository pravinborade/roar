import React, {PureComponent} from 'react';
import {Text, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {heightToDp, widthToDp} from '../utils/Responsive';

const SKELETON_SPEED = 2500;
const SKELETON_BG = '#dddddd';
const SKELETON_HIGHLIGHT = '#e7e7e7';

const SkeletonViewMembership = (props) => {
  return (
    <SkeletonPlaceholder
      speed={SKELETON_SPEED}
      backgroundColor={SKELETON_BG}
      highlightColor={SKELETON_HIGHLIGHT}
    >
      <View style={{padding: 15}}>
        <View>
          <View
            style={{
              height: heightToDp('19%'),
              width: widthToDp('90%'),
              borderRadius: 5,
              alignSelf: 'center',
            }}
          />
        </View>
        <View>
          <View
            style={{
              height: heightToDp('19%'),
              width: widthToDp('90%'),
              borderRadius: 5,
              alignSelf: 'center',
              top: heightToDp('3%'),
            }}
          />
        </View>
        <View>
          <View
            style={{
              height: heightToDp('19%'),
              width: widthToDp('90%'),
              borderRadius: 5,
              alignSelf: 'center',
              top: heightToDp('6%'),
            }}
          />
        </View>
      </View>
    </SkeletonPlaceholder>
  );
};
export default SkeletonViewMembership;
