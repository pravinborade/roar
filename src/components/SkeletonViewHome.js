import React, {PureComponent} from 'react';
import {ScrollView, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {heightToDp, widthToDp} from '../utils/Responsive';

const SKELETON_SPEED = 2500;
const SKELETON_BG = '#dddddd';
const SKELETON_HIGHLIGHT = '#e7e7e7';

const SkeletonViewHome = (props) => {
  return (
    <SkeletonPlaceholder
      speed={SKELETON_SPEED}
      backgroundColor={SKELETON_BG}
      highlightColor={SKELETON_HIGHLIGHT}>
      <ScrollView>
        <View style={{padding: 15, marginBottom: 50}}>
          <View>
            <View
              style={{
                height: heightToDp('19%'),
                width: widthToDp('90%'),
                borderRadius: 5,
                alignSelf: 'center',
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              top: heightToDp('3%'),
              width: widthToDp('90%'),
              justifyContent: 'space-between',
              alignSelf: 'center',
            }}>
            <View
              style={{
                width: widthToDp('29%'),
                height: heightToDp('18%'),
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: widthToDp('29%'),
                height: heightToDp('18%'),
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: widthToDp('29%'),
                height: heightToDp('18%'),
                borderRadius: 5,
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              top: heightToDp('6%'),
              width: widthToDp('90%'),
              justifyContent: 'space-between',
              alignSelf: 'center',
            }}>
            <View
              style={{
                width: widthToDp('29%'),
                height: heightToDp('18%'),
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: widthToDp('29%'),
                height: heightToDp('18%'),
                borderRadius: 5,
              }}
            />
            <View
              style={{
                width: widthToDp('29%'),
                height: heightToDp('18%'),
                borderRadius: 5,
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              top: heightToDp('8%'),
              width: widthToDp('90%'),
              justifyContent: 'space-between',
              alignSelf: 'center',
            }}>
            <View
              style={{
                width: widthToDp('14%'),
                height: heightToDp('7%'),
                borderRadius: 25,
              }}
            />
            <View
              style={{
                width: widthToDp('14%'),
                height: heightToDp('7%'),
                borderRadius: 25,
              }}
            />
            <View
              style={{
                width: widthToDp('14%'),
                height: heightToDp('7%'),
                borderRadius: 25,
              }}
            />
            <View
              style={{
                width: widthToDp('14%'),
                height: heightToDp('7%'),
                borderRadius: 25,
              }}
            />
            <View
              style={{
                width: widthToDp('14%'),
                height: heightToDp('7%'),
                borderRadius: 25,
              }}
            />
          </View>
        </View>
      </ScrollView>
    </SkeletonPlaceholder>
  );
};
export default SkeletonViewHome;
