import {Text, View} from 'react-native';
import React from 'react';
import {commonStyle} from '../styles';

const NotAvailable = ({msg = '', style}) => {
  return (
    <View style={[commonStyle.justifyAlignCenter, style]}>
      <Text style={[commonStyle.regular24, {textAlign: 'center'}]}>{msg}</Text>
    </View>
  );
};
export default NotAvailable;
