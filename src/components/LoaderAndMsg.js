import React from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import {colors} from '../styles/colors';
import {commonStyle} from '../styles/style';
import {widthToDp} from '../utils/Responsive';

const LoaderAndMsg = ({text = ''}) => (
  <View
    pointerEvents="none"
    style={[
      commonStyle.justifyAlignCenter,
      {
        flex: 1,
      },
    ]}>
    <ActivityIndicator
      color={colors.primaryBlue}
      style={{alignSelf: 'center'}}
      size={'small'}
    />
    <Text style={commonStyle.regular24}>{text}</Text>
  </View>
);
export default LoaderAndMsg;

const styles = StyleSheet.create({
  buttonContainer: {
    padding: widthToDp('2.2'),
    backgroundColor: '#F4F7F9',
    justifyContent: 'space-around',
    ...commonStyle.rowAlignCenter,
  },
  boxStyleAmount: {},
  textStyle: {color: colors.primaryBlue, ...commonStyle.regular24},
});
