import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {colors, commonStyle} from '../styles';
import {widthToDp} from '../utils/Responsive';

const ExploreMore = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.exploreButton}>
      <Text style={[commonStyle.regular24, {color: colors.darkGreen}]}>
        Explore more
      </Text>
    </TouchableOpacity>
  );
};
export default ExploreMore;

const styles = StyleSheet.create({
  exploreButton: {
    alignSelf: 'flex-end',
    marginHorizontal: widthToDp(4.5),
  },
});
