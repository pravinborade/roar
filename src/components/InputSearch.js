import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {colors, commonStyle} from '../styles';
import Images from '../assets/Images/Images';
import {widthToDp} from '../utils/Responsive';
import {useDispatch, useSelector} from 'react-redux';
import {ifNotValid, ifValid} from '../utils/helper';

const InputSearch = ({
  inputStyle,
  onChangeText,
  containerStyle,
  mainContainer,
  error = '',
  ...props
}) => {
  const dispatch = useDispatch();
  let searchValue = useSelector((state) => state.inputs.searchValue);
  if (ifNotValid(searchValue)) {
    searchValue = '';
  }

  return (
    <View style={[styles.mainContainer, mainContainer]}>
      <View style={[styles.containerStyle, containerStyle]}>
        <Image source={Images.homeSearch} style={styles.imageStyle} />
        <TextInput
          autoCorrect={false}
          autoComplete={false}
          returnKeyType="done"
          keyboardType="default"
          placeholderTextColor="#505C74"
          style={[styles.inputStyle, inputStyle]}
          value={ifValid(searchValue) ? searchValue : ''}
          onChangeText={(v) => {
            dispatch({type: 'SET_SEARCH_VALUE', payload: v});
            onChangeText();
          }}
          {...props}
        />

        {searchValue.length > 0 ? (
          <TouchableOpacity
            onPress={() => {
              dispatch({type: 'SET_SEARCH_VALUE', payload: ''});
            }}
            style={styles.crossStyle}>
            <Image source={Images.cross} style={styles.crossImage} />
          </TouchableOpacity>
        ) : null}
      </View>
      {error && error.length > 0 ? (
        <Text style={styles.errorText}>{error}</Text>
      ) : null}
    </View>
  );
};
export default InputSearch;

const styles = StyleSheet.create({
  containerStyle: {
    marginHorizontal: widthToDp('4%'),
    flexDirection: 'row',
    backgroundColor: colors.lightBlue,
    alignItems: 'center',
  },
  imageStyle: {
    resizeMode: 'contain',
    width: widthToDp('5%'),
    height: widthToDp('5%'),
    marginLeft: widthToDp('2%'),
    marginRight: widthToDp('1%'),
  },
  inputStyle: {
    flex: 1,
    ...commonStyle.regular24,
    height: widthToDp(10),
    padding: widthToDp('2'),
  },
  mainContainer: {backgroundColor: colors.white},
  crossStyle: {
    position: 'absolute',
    height: widthToDp(10),
    width: widthToDp(10),
    alignItems: 'center',
    justifyContent: 'center',
    right: widthToDp(1),
  },
  crossImage: {
    width: widthToDp(4),
    height: widthToDp(4),
    resizeMode: 'contain',
  },
  errorText: {
    ...commonStyle.regular16,
    marginHorizontal: widthToDp(4.5),
    color: 'red',
  },
  boxStyleAmount: {},
});
