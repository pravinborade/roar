import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Images from '../assets/Images/Images';
import {heightToDp, width, widthToDp} from '../utils/Responsive';
import {colors, commonStyle} from '../styles';
import {ifValid} from '../utils/helper';
import {Content} from '../utils/content/Content';

const HeaderBack = ({
  mainHeader,
  navigation,
  image,
  style,
  onPressBarcode,
  onPress,
  title = 'Title',
  onPressSkip,
  onPressEdit,
}) => {
  const _onPress = () => {
    if (ifValid(navigation)) {
      navigation.goBack();
    }
    if (ifValid(onPress)) {
      onPress();
    }
  };

  return (
    <View style={[styles.headerContainer, style]}>
      {!mainHeader && (
        <TouchableOpacity
          onPress={() => _onPress()}
          style={{padding: widthToDp('2.6')}}>
          <Image
            style={styles.imageStyle}
            source={ifValid(image) ? image : Images.backArrow}
          />
        </TouchableOpacity>
      )}
      <Text style={styles.headerStyle}>{title.toUpperCase()}</Text>
      {onPressEdit ? (
        <View style={styles.editStyle}>
          <TouchableOpacity onPress={onPressBarcode}>
            <Image style={styles.imageStyleBarcode} source={Images.barcode} />
          </TouchableOpacity>
          <Text onPress={onPressEdit} style={styles.editStyleText}>
            {Content.edit}
          </Text>
        </View>
      ) : null}
      {onPressSkip ? (
        <Text onPress={onPressSkip} style={styles.skipStyle}>
          {Content.skip}
        </Text>
      ) : null}
    </View>
  );
};
export default HeaderBack;

const styles = StyleSheet.create({
  headerContainer: {
    paddingLeft: widthToDp(2),
    backgroundColor: colors.white,
    height: heightToDp('7.0'),
    width,
    ...commonStyle.rowAlignCenter,
  },
  headerStyle: {
    position: 'absolute',
    textAlign: 'center',
    alignSelf: 'center',
    margin: widthToDp('12%'),
    left: 0,
    right: 0,
    ...commonStyle.regular30,
    fontWeight: 'bold',
  },
  imageStyle: {
    resizeMode: 'cover',
    width: widthToDp('4%'),
    height: widthToDp('4%'),
  },
  skipStyle: {
    position: 'absolute',
    right: widthToDp(2.5),
    ...commonStyle.regular24,
    color: colors.darkGreen,
  },
  editStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    right: widthToDp(2.5),
    ...commonStyle.regular24,
    color: colors.darkGreen,
  },
  editStyleText: {
    marginLeft: widthToDp(3),
    marginRight: widthToDp(0),
  },
  imageStyleBarcode: {
    resizeMode: 'cover',
    width: widthToDp('6%'),
    height: widthToDp('6%'),
  },
});
