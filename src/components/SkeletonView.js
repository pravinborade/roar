import React, {PureComponent} from 'react';
import {Text, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SKELETON_SPEED = 2500;
const SKELETON_BG = '#dddddd';
const SKELETON_HIGHLIGHT = '#e7e7e7';

const SkeletonView = (props) => {
  return (
    <SkeletonPlaceholder
      speed={SKELETON_SPEED}
      backgroundColor={SKELETON_BG}
      highlightColor={SKELETON_HIGHLIGHT}>
      <View style={{flexDirection: 'row', padding: 15, marginBottom: 10}}>
        <View>
          <View style={{height: 90, width: 100, borderRadius: 5}} />
        </View>
        <View>
          <View style={{justifyContent: 'space-between', marginHorizontal: 15}}>
            <View
              style={{
                width: 120,
                height: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>
          <View
            style={{
              justifyContent: 'space-between',
              marginHorizontal: 15,
              top: 8,
            }}>
            <View
              style={{
                width: 120,
                height: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>
          <View
            style={{
              justifyContent: 'space-between',
              marginHorizontal: 15,
              top: 17,
            }}>
            <View
              style={{
                width: 120,
                height: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                justifyContent: 'space-between',
                marginHorizontal: 15,
                top: 26,
              }}>
              <View
                style={{
                  width: 120,
                  height: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
            <View
              style={{
                justifyContent: 'space-between',
                marginHorizontal: 15,
                top: 26,
              }}>
              <View
                style={{
                  width: 90,
                  height: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </SkeletonPlaceholder>
  );
};
export default SkeletonView;
