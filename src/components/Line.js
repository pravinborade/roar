import {View} from 'react-native';
import React from 'react';

const Line = ({lineColor = '#E9EBF7', style}) => {
  return (
    <View
      style={[
        {borderBottomColor: '#E5E5E5', borderBottomWidth: 0.5},
        {borderBottomColor: lineColor},
        style,
      ]}
    />
  );
};
export default Line;
