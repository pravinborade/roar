import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Images from '../assets/Images/Images';
import {colors} from '../styles/colors';
import {commonStyle} from '../styles/style';
import {widthToDp} from '../utils/Responsive';
import {Content} from '../utils/content/Content';

const FilterButton = ({onPress}) => (
  <TouchableOpacity onPress={onPress} style={styles.buttonContainer}>
    <Image
      source={Images.filter}
      style={{
        width: widthToDp('4.5'),
        height: widthToDp('4.5'),
        resizeMode: 'contain',
      }}
    />
    <View style={{marginLeft: widthToDp('2')}}>
      <Text style={styles.textStyle}>{Content.filterButton}</Text>
    </View>
  </TouchableOpacity>
);
export default FilterButton;

const styles = StyleSheet.create({
  buttonContainer: {
    padding: widthToDp('2.2'),
    backgroundColor: '#F4F7F9',
    justifyContent: 'space-around',
    ...commonStyle.rowAlignCenter,
  },
  boxStyleAmount: {},
  textStyle: {color: colors.primaryBlue, ...commonStyle.regular24},
});
