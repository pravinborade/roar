import React from 'react';
import {
  FlatList,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {colors, commonStyle} from '../styles';
import {height, widthToDp} from '../utils/Responsive';
import {useDispatch} from 'react-redux';
import {getNumberOfQuantityItems} from '../utils/helper';
import {fontToDp} from '../utils/Responsive';
import Line from './Line';
import {
  PT_SANS_BOLD,
  PT_SANS_NARROW_BOLD,
  PT_SANS_REGULAR,
} from '../styles/typography';
import Images from '../assets/Images/Images';

const DropDownSmall = ({
  title = '',
  isVisible = false,
  value = '',
  data,
  onPickerTouch,
  onPickerSelect,
  onPickerClose,
  containerStyle,
  disabled = false,
}) => {
  const dispatch = useDispatch();
  const pickerView = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={() => {}}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => onPickerClose()}
          style={styles.centeredView}>
          <View style={styles.modalView}>
            <FlatList
              scrollEnabled
              showsVerticalScrollIndicator={true}
              keyExtractor={(item, index) => index.toString()}
              ItemSeparatorComponent={(props) => <Line />}
              data={data}
              renderItem={({item}) => (
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => onPickerSelect(item.type)}
                  style={styles.flatListItem}>
                  <Text
                    style={[
                      commonStyle.regular26,
                      {
                        flex: 8,
                        color: item.type ? '#038BEF' : 'black',
                      },
                    ]}>
                    {item.type}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </TouchableOpacity>
      </Modal>
    );
  };

  return (
    <View
      style={[{flexDirection: 'row', alignItems: 'center'}, containerStyle]}>
      <Text style={styles.viewText}>{title}</Text>
      <TouchableOpacity
        activeOpacity={1}
        onPress={onPickerTouch}
        style={[
          {
            flexDirection: 'row',
            paddingHorizontal: widthToDp(2),
          },
          styles.dropDownContainer,
        ]}>
        <Text style={[commonStyle.regular24, {flex: 1}]}>{value}</Text>
        <Image
          source={Images.arrowBottom}
          style={{width: widthToDp(2), height: widthToDp(2)}}
        />
      </TouchableOpacity>
      {pickerView()}
    </View>
  );
};
export default DropDownSmall;

const styles = StyleSheet.create({
  dropDownContainer: {
    alignItems: 'center',
    height: widthToDp(8),
    borderRadius: 2,
    justifyContent: 'center',
    margin: widthToDp(0.5),
    width: widthToDp(58),
    borderWidth: 0.5,
    borderColor: 'black',
    marginLeft: widthToDp(5),
  },
  dropDown: {
    marginRight: widthToDp(-3),
  },
  viewText: {
    color: '#505C74',
    fontFamily: PT_SANS_BOLD,
    fontSize: fontToDp(12),
    marginBottom: 5,
    marginLeft: widthToDp(3),
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: widthToDp(4.5),
    backgroundColor: 'white',
    borderRadius: 2,
    height: height / 3,
    padding: widthToDp(4),
    width: widthToDp(80),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  flatListItem: {
    paddingVertical: widthToDp(2),
  },
});
