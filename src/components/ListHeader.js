import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {commonStyle} from '../styles';
import {Line} from './index';
import {widthToDp} from '../utils/Responsive';

const ListHeader = ({title = ''}) => {
  return (
    <View>
      <View style={[commonStyle.detailsContainer, {marginTop: 0}]}>
        <Text style={commonStyle.narrow30}>{title}</Text>
      </View>
      <Line />
    </View>
  );
};
export default ListHeader;

const styles = StyleSheet.create({
  container: {},
});
