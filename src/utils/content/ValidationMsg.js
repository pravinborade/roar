export const ValidationMsg = {
  phoneNoCannotEmpty: `*Phone No cannot be empty and should be 10 Digit`,
  addressCannotEmpty: `*Address cannot be empty`,
  emailCannotBeEmpty: `*email cannot be empty and should be valid`,
  enterFirstName: `*Please enter first name`,
  enterLastName: `*Please enter last name`,
  problemWithYourProfile: `Sorry, there was a problem with your request`,

  passwordSharedToMail: 'Password has been shared on your email id.',
  canContainAZAN09: 'can only contain a-z and 0-9',
  signUpFirstName:
    '*first name can not be empty and should be minimum 3 character',

  signUpLastName:
    '*last name can not be empty and should be minimum 3 character',

  signUpEmail: '*email can not be empty and should be valid',
  signUpPhone: '*phone no can not be empty and should be valid',
  slotPriceNotValid: 'Slot price is not valid please try with different slot.',
  someoneElseAlsoBooking: `Someone else also\n booking this slot`,
  membershipAlreadyPurchased: `This membership is already purchased`,
};
