import {VERSION_CODE} from '../../config';

export const Content = {
  //INITIAL SCREEN

  Initial_Heading: `Improve booking management ${'\n'} & player experience`,
  Initial_SubHeading:
    'Trusted platform by many sports facility providers. Extend your services online.',
  Login: 'LOGIN',
  SignUp: 'SIGN UP',
  TermsCond:
    'By tapping Continue, I agree to Continental Tires Extended Warranty’s Terms of Service, Payments Terms of Service, Privacy Policy and Nondiscrimination Policy',

  //LOGIN SCREEN
  New_User: 'NEW USER?',
  register_her: 'REGISTER HERE.',
  connect: 'or Connect with',
  forgot_pw: 'FORGOT PASSWORD',

  //SIGNUUP SCREEN
  already_account: 'ALREADY HAVE AN ACCOUNT?',
  login_here: 'LOGIN HERE',

  //BOOKING SCREEN
  bSuccessHeading: 'YAY!',
  bSuccessSubHeading: 'Your slot has been booked successfully!',
  myBooking: 'MY BOOKINGS',
  bookAnotherSlot: 'BOOK ANOTHER SLOT',

  //SETTINGS SCREEN
  About: 'About Roar',
  Terms:
    'These terms and conditions govern your use of our website.By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.If you register with our website, submit any material to our website or use any of our website services, we will ask you to expressly agree to these terms and conditions.Our website uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies in accordance with the terms of our privacy and cookies policy.Copyright © 2021 Roar sports.Subject to the express provisions of these terms and conditions:we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; andall the copyright and other intellectual property rights in our website and the material on our website are reserved.',
  MyProfile: 'My Profile',
  MyBookings: 'My Bookings',
  MyMembership: 'My Membership',
  MyServices: 'My Services',
  MyEvents: 'My Events',
  Reports: 'Reports',
  AboutRoar: `Roar is a Player Experience Platform for outdoor and body workout activities enthusiasts and facility providers including sports clubs, fitness centers, and trainers to give members an amazing energetic experience around the clock. ${'\n\n'}Roar gives players and teams exclusive digital exp and helps sports clubs, groups, fitness centers around the globe to manage their members and venues. With Roar you get high-end performance with an easy to use the platform.`,
  TermsOfUse: 'Terms of Use',
  AppVersion: `App version: ${VERSION_CODE}`,

  //HOME SCREEN
  PuneMaharashtra: 'Pune, Maharashtra',
  changeButton: 'CHANGE',
  inputPlaceholder: 'Search for venues, events and sports....',
  venuesPlaceHolder: 'Search for venues...',
  noResultFound: 'No Results found',
  noVenuesFound: 'No Venues found',

  //VENUES (IN HOME)
  filterButton: 'Filter',
  resultFound: 'results found',
  venu: 'Venues',
  venues: 'VENUES',

  //filter
  filters: 'FILTERS',
  setPreference: 'Set Preferences',
  reset: 'Set RESET',
  showResults: 'Show results',
  bySport: 'By Sports',
  setPre: 'Set Preferences',
  resett: 'RESET',
  noGames: 'No games found',
  popularity: 'By Popularity',

  //details
  details: 'DETAILS',
  availableSports: 'Available Sports',
  BecomeAMember: 'Become a Member',
  Amenities: 'Amenities',
  AboutPlayscape: 'About',
  knowMore: 'KNOW MORE',
  book: 'BOOK',
  seeMap: 'SEE MAP',

  //bookings
  bookings: 'Bookings',
  splitPayment: 'Split Payment',
  addMember: 'Add New Customer',
  sports: 'Sports',
  slotDate: 'Slot date',
  playercount: 'Player Count',
  slotTime: 'Slot Time',
  next: 'next',
  upCome: 'UPCOMING',
  past: 'PAST',
  cancel: 'CANCELLED',
  bookingNo: 'Booking No.',
  rupies: 'Rs.',

  //services
  services: 'Services',
  exploremore: 'Explore more',
  additionalServices: 'Additional Services',
  learnFromExperts: 'Learn From Experts',
  equipments: 'Equipments',
  refreshments: 'Refreshments',
  medical: 'Medical',
  add: 'ADD',
  renew: 'RENEW',
  skip: 'SKIP',
  qty: 'Qty',
  titleadd: 'Customer Type',
  gender: ' Gender',
  size: 'Size',
  serProvided: 'Services Provided',
  serviceProDescription:
    'Lorem ipsum dolor sit amet, consectetur adi piscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam,quis nostrud exercitation ulla mco laboris.',
  amount: 'Rs. 17698.82',
  timetoend: '(12 months)',
  adress: 'Marunji Road, Hinjewadi, Pune',
  hotful: 'Hotfut Playscape',
  startDate: 'Start date',
  endDate: 'End date',
  stHardCodedDate: '17 May, 2017',
  enHardCodedDate: '17 May, 2017',
  to: 'To',
  equ: 'Helmet',
  yourServices: 'Your Service has been renew successfully!',

  //summary
  summary: 'Summary',
  details_booking: 'Booking Details',
  slotSelected: 'slot selected',
  slotsSelected: 'slots selected',
  applyCoupon: 'Apply Coupon',
  paymentSummary: 'Payment Summary',
  subTotal: 'Sub Total',
  gst: 'GST',
  convenienceFee: 'Convenience Fee',
  discount: 'Discount',
  noofplayer: 'No of Players',
  totalAmount: 'Total Amount',
  bookingPolicy: 'Booking Policies',
  proceedToPay: 'Proceed To Pay',
  rebootSport: 'Reboot sport',
  proceedToBook: 'Proceed To Book',

  //acknowledgement
  acknowledgement: 'acknowledgement',
  yourSlotBooked: 'Your slot has been booked successfully!',
  yourSlotNotBooked: 'Your slots has been booked by someone else',
  amountWillBeRefunded:
    'your amount will be refunded in 2-3 business days, please continue to book another slot',
  oops: 'OOPS!',
  yay: 'YAY!',
  remv: 'Remove',

  //EVENT
  myEvents: 'MY EVENTS',
  yourEventBooked: 'Your Event has been booked successfully!',
  eventPlaceHolder: 'Search for events..',
  event: 'Events',
  noEvntFound: 'No Events found',
  datTime: 'Date and Time',
  yourMembershipBooked: 'Your Membership payment has been done successfully',
  eventts: 'EVENTS',

  //Profile
  phNo: 'Phone Number',
  email: 'Email',
  address: 'Address',
  occupastion: 'occupation',
  Dob: 'DOB',
  my_sports: 'My Sports',
  spPorTs: 'Football',
  chngProfile: 'Change Profile Picture',
  FirstName: 'First Name',
  LastName: 'Last Name',
  eMailId: 'Email ID',
  //Reset
  newPassword: 'New Password',
  confPassword: 'Confirm Password',
  sendLink: 'Send Link',
  resettt: 'RESET',

  /// no data
  noSlotsFoundNotSelectedSport:
    'No slots found\n( please select any available desired Sport & date )',
  noSlotsFound: 'No slots found\n( please try with different date )',
  noAddressFound: 'No address found',
  noSportsFound: 'No sports found',
  noAmenitiesFound: 'No amenities found',
  noNotification: 'No notification found',
  noBarcodeFound: 'No Barcode found',

  // HeaderBack
  edit: 'EDIT',
  sKip: 'Skip',
  nExt: 'Next',
  dOne: 'Done',

  // Location
  location: 'Select Your Location',
  picMyLocation: 'Pick my location',

  // Sports
  seeAll: 'SEE ALL',
  selectSports: 'Select your sports',
  selectFromHere: 'Select from here',
  selectAll: 'Select All',

  // My Membership
  completePackage:
    'Complete Package for the club including all sports/ fitness.',
  membership: 'All Memberships',
  editProfile: 'EditProfile',
  profilePicture: 'Profile picture',
  updateYourProfilePic: 'Update your profile picture',
  gallery: 'Gallery',
  okBtn: 'Ok',
  yesBtn: 'Yes',
  cancelBtn: 'Cancel',
  removePhoto: 'Remove photo',
  choosePhoto: 'ChoosePhoto',
  loadingBarcodeWait: 'Loading barcode please wait',
  changePassword: 'Change Password',
  logout: 'Logout',
  signUp: 'SIGN UP',
  choosePhotod: 'ChoosePhoto',
  flatRate: 'Flat Rate: ',
  discountService: 'Discount: ',
  enterNumOfPlayer: 'Enter number of Player',
  selectCourt: 'Select Court',
  membershipService: 'Membership',
  lookingForServices: `Looking for services`,
  AdditionalItems: `Additional items`,
  AdditionalServices: `Additional services`,
  updateMsg: `You will have to update your app to the latest version to continue using.`,
  update: `Update`,
  plsUpdate: `Please update`,
  continue: `Continue`,
  enterYourLocation: `Enter your location`,
  rateThis: `Rate this facility`,
  submit: `Submit`,
  reviewComment: `Review comment`,
  submitYourReview: `Submit your review`,
  areYouSure: `Are you sure you want to exit app?`,
  upcoming: `UPCOMING`,
  alert: `Alert!`,
  review: `Review`,
  thankForReview: `'Thank you for your Review!`,
};
