export const successLog = (message, status = '') => {
  let _status = status === undefined || status === null ? '' : status;
  let result = `${JSON.stringify(message)} ${
    _status === '' ? '' : 'success status=' + _status
  }`;
  console.log(result);
};

export const errorLog = (msg = '', error = '') => {
  let e = error === undefined || error === null ? '' : error;
  msg = msg === undefined || msg === null ? '' : msg;
  console.log(msg + '  ' + JSON.stringify(e));
};

export const methodLog = (msg) => {
  console.log(msg);
};

export const recordError = (error) => {
  console.log(error);
  successLog(error);
};
export const logger = (msg1 = '', msg2 = '', msg3 = '', msg4 = '') => {
  console.log(
    JSON.stringify(msg1) +
      ' ' +
      JSON.stringify(msg2) +
      ' ' +
      JSON.stringify(msg3) +
      ' ' +
      JSON.stringify(msg4) +
      ' ',
  );
};
