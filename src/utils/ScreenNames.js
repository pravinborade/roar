export const ScreenNames = {
  INITIAL: 'INITIAL',
  LOGIN: 'LOGIN',
  SIGNUP: 'SIGNUP',
  FORGOT_PASSWORD: 'FORGOT PASSWORD',
  RESET: 'RESET',
  BOOKINGS: 'BOOKINGS',
  EVENTS: 'EVENTS',
  HOME: 'HOME',
  DETAILSMYBOOKINGS: 'DETAILSMYBOOKINGS',
  SPLITPAYMENT: 'SPLITPAYMENT',
  ADDMEMBER: 'ADDMEMBER',
  MYMEMBERSHIP: 'MY MEMBERSHIP',
  SETTINGS: 'SETTINGS',
  ONBOARDING: 'ON BOARDING',
  BARCODE_SCREEN: 'BARCODE_SCREEN',
  TAB: 'Tab',

  //Booking
  BOOKING_SUCCESS: 'BOOKING SUCCESS',
  MY_BOOKINGS: 'MY BOOKINGS',

  //Settings
  ABOUT_ROAR: 'ABOUT',
  TERMS_OF_USE: 'TERMS OF USE',

  //Home
  VENUES: 'VENUES',
  VENUE_DETAILS: 'VENUE DETAILS',
  HOME_VENUES: 'HOME_VENUES',
  VENUES_FILTER: 'VENUES_FILTER',
  HOME_BOOKINGS: 'HOME_BOOKINGS',
  HOME_SERVICES: 'HOME_SERVICES',
  HOME_SERVICES_ALL: 'HOME_SERVICES_ALL',
  HOME_INVENTORIES_ALL: 'HOME_INVENTORIES_ALL',
  HOME_SUMMARY: 'HOME_SUMMARY',
  HOME_ACKNOWLEDGEMENT: 'ACKNOWLEDGEMENT',
  //home events
  HOME_EVENT_LIST: 'HOME_EVENT_LIST',
  HOME_EVENT_DETAIL: 'HOME_EVENT_DETAILS',
  HOME_EVENT_ACKNOWLEDGEMENT: 'HOME_EVENT_ACKNOWLEDGEMENT',
  MEMBERSHIP_LIST: 'MEMBERSHIP',
  MEMBERSHIP_DETAIL: 'MEMBERSHIP_DETAIL',
  MEMBERSHIP_ACKNOWLEDGEMENT: 'MEMBERSHIP_ACKNOWLEDGEMENT',
  SERVICE_ACKNOWLEDGEMENT: 'SERVICE_ACKNOWLEDGEMENT',

  //My services
  MY_SERVICES: 'MY SERVICES',
  Service_Details: 'Service Details',

  //My events
  MY_EVENTS: 'MY EVENTS',

  //My profile
  MY_PROFILE: 'MY PROFILE',
  PROFILE_EDIT: 'EDIT PROFILE',

  //location
  MY_LOCATION: 'LOCATION',

  ///home sport
  HOME_SPORT_ALL: 'HOME_SPORT',

  //Notification
  NOTIFICATION: 'NOTIFICATION',

  //Reports
  REPORTS: 'REPORTS',

  //Booking Policy
  BOOKING_POLICY: 'BOOKING POLICY',

  MY_MEMBER: 'SERVICEDATA',
  RATINGS_ADD: 'RATINGS_ADD',
  RATINGS_LIST: 'RATINGS_LIST',
};
