// __tests__/Intro-test.js
import React from 'react';
import renderer from 'react-test-renderer';
import ForgotPassword from '../src/screens/Auth/ForgotPassword/index';

global.fetch = jest.fn(() => new Promise((resolve) => resolve()));
jest.mock('react-native-gesture-handler', () => {});

test('Forgot Password Page Snapshot', () => {
  const tree = renderer.create(<ForgotPassword />).toJSON();
  expect(tree).toMatchSnapshot();
});
