// __tests__/Intro-test.js
import React from 'react';
import renderer from 'react-test-renderer';
import Initial from '../src/screens/Auth/Initial/index';

global.fetch = jest.fn(() => new Promise((resolve) => resolve()));
jest.mock('react-native-gesture-handler', () => {});

test('Initial Page Snapshot', () => {
  const tree = renderer.create(<Initial />).toJSON();
  expect(tree).toMatchSnapshot();
});
