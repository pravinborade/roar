/**
 * @format
 */

import React, {useEffect} from 'react';
import {AppRegistry, Platform} from 'react-native';
import App from './src/root/App';
import {name as appName} from './app.json';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {showToastMsg} from './src/utils';
import {store} from './src/redux/store';
import {receiveNotification} from './src/screens/Notification/action';

const LocalNotification = (notification) => {
  PushNotification.localNotification({
    channelId: 'roar',
    title: notification.title,
    message: notification.message,
  });
};

PushNotification.createChannel(
  {
    channelId: 'roar', // (required)
    channelName: 'Channel', // (required)
  },
  (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
);

//Must be outside of any component LifeCycle (such as `componentDidMount`).

RemoteNotification = () => {
  useEffect(() => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log('TOKEN:', token);
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);
        // process the notification

        showToastMsg(notification?.title, notification?.message, 'success');
        store.dispatch(receiveNotification(true));

        if (!notification.userInteraction) {
          LocalNotification(notification);
        }

        // (required) Called when a remote is received or opened, or local notification is opened
        if (Platform.OS === 'ios') {
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);
        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // ANDROID ONLY: FCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
      // senderID: 159670667535,
      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
  }, []);
  return <App />;
};

export default RemoteNotification;

AppRegistry.registerComponent(appName, () => RemoteNotification);
